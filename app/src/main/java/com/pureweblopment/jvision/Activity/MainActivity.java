package com.pureweblopment.jvision.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.facebook.FacebookSdk;
import com.facebook.FacebookSdkNotInitializedException;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.pureweblopment.jvision.Adapter.AdapterBanner;
import com.pureweblopment.jvision.Fragment.CheckoutFragment;
import com.pureweblopment.jvision.Fragment.ContactUsFragment;
import com.pureweblopment.jvision.Fragment.FeedbackFragment;
import com.pureweblopment.jvision.Fragment.OrderHistoryFragment;
import com.pureweblopment.jvision.Fragment.ProductListFragment;
import com.pureweblopment.jvision.Global.Global;
import com.pureweblopment.jvision.Global.Typefaces;
import com.pureweblopment.jvision.Linkedin.LISessionManager;
import com.pureweblopment.jvision.Model.CMSPages;
import com.pureweblopment.jvision.Pdk.PDKClient;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;

import com.pureweblopment.jvision.Adapter.ViewPagerAdapter;
import com.pureweblopment.jvision.Fragment.CCAvenueFragment;
import com.pureweblopment.jvision.Fragment.CMSFragment;
import com.pureweblopment.jvision.Fragment.CartFragment;
import com.pureweblopment.jvision.Fragment.MegaMenuCategoryFragment;
import com.pureweblopment.jvision.Fragment.MyAccountFragment;
import com.pureweblopment.jvision.Fragment.OrderDetailFragment;
import com.pureweblopment.jvision.Fragment.PaymentFragment;
import com.pureweblopment.jvision.Fragment.PlaceOrderSuccessFragment;
import com.pureweblopment.jvision.Fragment.ProductDetailFragment;
import com.pureweblopment.jvision.Fragment.WishlistFragment;
import com.pureweblopment.jvision.Global.SendMail;
import com.pureweblopment.jvision.Global.SharedPreference;
import com.pureweblopment.jvision.Global.StaticUtility;
import com.pureweblopment.jvision.Instagram.InstagramApp;
import com.pureweblopment.jvision.Model.AddressList;
import com.pureweblopment.jvision.Model.Category;
import com.pureweblopment.jvision.Model.ProductItem;
import com.pureweblopment.jvision.R;

import de.greenrobot.event.EventBus;
import de.hdodenhof.circleimageview.CircleImageView;
import io.fabric.sdk.android.Fabric;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener,
        GoogleApiClient.OnConnectionFailedListener,
        ProductListFragment.OnFragmentInteractionListener,
        ProductDetailFragment.OnFragmentInteractionListener,
        CartFragment.OnFragmentInteractionListener,
        WishlistFragment.OnFragmentInteractionListener,
        OrderHistoryFragment.OnFragmentInteractionListener,
        MyAccountFragment.OnFragmentInteractionListener,
        FeedbackFragment.OnFragmentInteractionListener,
        CMSFragment.OnFragmentInteractionListener, TextWatcher,
        CheckoutFragment.OnFragmentInteractionListener,
        PaymentFragment.OnFragmentInteractionListener,
        PlaceOrderSuccessFragment.OnFragmentInteractionListener,
        MegaMenuCategoryFragment.OnFragmentInteractionListener,
        ContactUsFragment.OnFragmentInteractionListener,
        CCAvenueFragment.OnFragmentInteractionListener {

    ImageView imageLogo, imageOrderHistoryFilter;
    private static Context context;
    static String picUrl = null;
    private static URL urla = null;
    private static URI urin = null;
    public static String isCheckoutBack = "";
    LinearLayout linearHome, linearCatagory, linearNotification, linearWishlist, linearProfile, llLogout, llLogIn, llReviews,
            llOrderHistory, llMyAddress, llFeedback, llHome, llContactUs;
    ImageView imageHome, imageCatagory, imageNotification, imageWishlist, imageProfile,
            imageNavigation,
            imageCartBack, imageCart;
    TextView textHome, textCatagory, textNotification, textWishlist, textProfile, txtCatName, txtMyAddress, txtReviews,
            txtOrderHistory, txtFeedback, txtHome, txtContactUs;
    TextView txtLogout, txtLogIn;
    ImageView imgLogout, imgLogin, imgOrderHistory, imgMyAddress, imgReviews, imgFeedback, imgHome, imgContactUs;

    public static CircleImageView circularImageViewUser;
    static TextView txtUserName;
    static TextView txtUserEmail;
    CardView cardviewBottomNavigation;

    FragmentManager fragmentManager;
    Bundle bundle = new Bundle();
    Toolbar toolbar;
    TextView txtCardCount;

    FrameLayout frameLayoutCart;
    String qty, wishlistcount;

    FrameLayout frameLayoutBestSeller, framLayoutmostWanted, frameLayoutNewArrivals,
            frameLayoutBenner;
    LinearLayout llSreach;

    RecyclerView recyclerviewCategories;

    private ProductItem productItem;
    private ArrayList<ProductItem> productItems = new ArrayList<>();

    //region HomeFragment
    ViewPager Slider;
    ArrayList<Category> categories;
    String[] img;
    int currentPage = 0;
    Timer timer;
    RelativeLayout relativeProgress;
    RecyclerView recyclerviewBestSelling, recyclerviewMostwanted, recyclerviewNewArrivals, recyclerviewBenner;
    TextView txtBestSeller, txtMostwanted, txtNewArrivals;
    LinearLayout llBestSeller, llMostWanted, llArrivals;
    ProgressBar progress;
    //endregion

    private GoogleApiClient mGoogleApiClient;
    private InstagramApp mApp;
    Boolean doubleBackToExitPressedOnce = false;
    private static boolean isDashBoard = true;
    LinearLayout llBottomNavigation, llToolbar, llMenu;

    private EventBus eventBus = EventBus.getDefault();
    private int i = 0;
    private AlertDialog internetAlert;

    String is_silder = "", is_wishlist = "", is_mostwanted = "", is_bestselller = "", is_arrivals = "";

    public static boolean checkback = false;
    public static boolean PlaceOrderSuccessback = false;
    public static boolean ClearSharePreference = false;

    TextView txtWishlistCount;

    JSONArray jsonArrayBestSeller, jsonArrayMostWanted, jsonArrayArrivals, jsonArrayBenner;
    RecyclerView recyclerviewCMS;

    AutoCompleteTextView editSearch;
    AppBarLayout appbarlayout;
    LinearLayout llNavigationHeader;

    ArrayList<AddressList> addressLists;
    int bestSellerPosition = 0, mostWantedPosition = 0, newArrivalPosition = 0;
    static String userID = "";

    static DrawerLayout drawer;

    static ImageView imgUser;

    //pinterest
    PDKClient pdkClient;

    String strShippingaddress = null, strShippinguserName = null, strShippingphoneno = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(this);
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        eventBus.register(context);

        //Pinterest
        pdkClient = PDKClient.configureInstance(this, StaticUtility.PinterestApp_ID);
        pdkClient.onConnect(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mApp = new InstagramApp(this, StaticUtility.CLIENT_ID,
                StaticUtility.CLIENT_SECRET, StaticUtility.CALLBACK_URL);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        Initialization();
        OnClickListener();
        TypeFace();
        AppSettings();

        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            llToolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
            toolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));

            }
        }

        changeTextCard(txtCardCount);
        chanageImages(txtWishlistCount);

        imageHome.setImageResource(R.drawable.ic_home);
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            imageHome.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
            textHome.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            imgHome.setImageResource(R.drawable.ic_circle);
            imgHome.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
            txtHome.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }


        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.APP_LOGO) != "") {
            String strLOGO = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.APP_LOGO);

            //region ImageLOGO
            try {
                URL urla = null;
            /*picUrl = picUrl.replace("[", "");
            picUrl = picUrl.replace("]", "").replace("\"", "");*/
                urla = new URL(strLOGO);
                URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                picUrl = String.valueOf(urin.toURL());
                // Capture position and set to the ImageView
                Picasso.with(context)
                        .load(picUrl)
                        .into(imageLogo, new Callback() {
                            @Override
                            public void onSuccess() {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onError() {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }
                        });
            } catch (MalformedURLException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            } catch (URISyntaxException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            }
            //endregion
        }


        final CoordinatorLayout coodinator = (CoordinatorLayout) findViewById(R.id.coodinator);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                coodinator.setTranslationX(slideOffset * drawerView.getWidth());
                drawer.bringChildToFront(drawerView);
                drawer.requestLayout();
            }
        };

        drawer.setDrawerListener(toggle);
        toggle.syncState();
        toggle.setDrawerIndicatorEnabled(false);
        imageNavigation.setVisibility(View.VISIBLE);
        imageNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Slider_Active) != null) {
            is_silder = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Slider_Active);
        }
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active) != null) {
            is_wishlist = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active);
        }
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Seller_Active) != null) {
            is_bestselller = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Seller_Active);
        }
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Arrival_Active) != null) {
            is_arrivals = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Arrival_Active);
        }
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Mostwanted_Active) != null) {
            is_mostwanted = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Mostwanted_Active);
        }

        getCMSListing();

        /*GetCartCount();*/

        getCategory();

        if (is_silder.equals("1")) {
            Silder();
        } else {
            Slider.setVisibility(View.GONE);
        }
        if (is_bestselller.equals("1")) {
            getBestSeller();
        } else {
            llBestSeller.setVisibility(View.GONE);
        }
        if (is_mostwanted.equals("1")) {
            getWantedProduct();
        } else {
            llMostWanted.setVisibility(View.GONE);
        }
        if (is_arrivals.equals("1")) {
            getNewArrivals();
        } else {
            llArrivals.setVisibility(View.GONE);
        }

        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active) != null) {
            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active).equals("1")) {
                linearWishlist.setVisibility(View.VISIBLE);
            } else {
                linearWishlist.setVisibility(View.GONE);
            }
        }

        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Cart_Active) != null) {
            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Cart_Active).equals("1")) {
                frameLayoutCart.setVisibility(View.VISIBLE);
            } else {
                frameLayoutCart.setVisibility(View.GONE);
            }
        }

        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                StaticUtility.Is_MEGAMENU_Active) != null) {
            String strIsMegaMenuActive = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                    StaticUtility.Is_MEGAMENU_Active);
            if (strIsMegaMenuActive.equalsIgnoreCase("1")) {
                linearCatagory.setVisibility(View.VISIBLE);
            } else {
                linearCatagory.setVisibility(View.GONE);
            }
        }

        getBenners();
    }

    //region OnClickListener
    private void OnClickListener() {
        linearHome.setOnClickListener(this);
        linearCatagory.setOnClickListener(this);
        linearNotification.setOnClickListener(this);
        linearWishlist.setOnClickListener(this);
        linearProfile.setOnClickListener(this);
        llLogout.setOnClickListener(this);
        llLogIn.setOnClickListener(this);
        frameLayoutCart.setOnClickListener(this);
        llOrderHistory.setOnClickListener(this);
        llMyAddress.setOnClickListener(this);
        llReviews.setOnClickListener(this);
        llFeedback.setOnClickListener(this);
        llHome.setOnClickListener(this);
        llContactUs.setOnClickListener(this);
        editSearch.addTextChangedListener(this);

    }
    //endregion

    //region Initialization
    private void Initialization() {
        imageLogo = (ImageView) findViewById(R.id.imageLogo);
        linearHome = (LinearLayout) findViewById(R.id.linearHome);
        imgUser = (ImageView) findViewById(R.id.imgUser);
        linearCatagory = (LinearLayout) findViewById(R.id.linearCatagory);
        linearNotification = (LinearLayout) findViewById(R.id.linearNotification);
        linearWishlist = (LinearLayout) findViewById(R.id.linearWishlist);
        linearProfile = (LinearLayout) findViewById(R.id.linearProfile);

        llLogout = (LinearLayout) findViewById(R.id.llLogout);
        llLogIn = (LinearLayout) findViewById(R.id.llLogIn);
        llOrderHistory = (LinearLayout) findViewById(R.id.llOrderHistory);
        llMyAddress = (LinearLayout) findViewById(R.id.llMyAddress);
        llReviews = (LinearLayout) findViewById(R.id.llReviews);
        llFeedback = (LinearLayout) findViewById(R.id.llFeedback);
        llHome = (LinearLayout) findViewById(R.id.llHome);
        llContactUs = (LinearLayout) findViewById(R.id.llContactUs);
        llSreach = (LinearLayout) findViewById(R.id.llSreach);

        txtMyAddress = (TextView) findViewById(R.id.txtMyAddress);
        txtReviews = (TextView) findViewById(R.id.txtReviews);
        txtFeedback = (TextView) findViewById(R.id.txtFeedback);
        txtHome = (TextView) findViewById(R.id.txtHome);
        txtContactUs = (TextView) findViewById(R.id.txtContactUs);
        txtOrderHistory = (TextView) findViewById(R.id.txtOrderHistory);

        txtLogout = (TextView) findViewById(R.id.txtLogout);
        txtLogIn = (TextView) findViewById(R.id.txtLogIn);
        imgLogout = (ImageView) findViewById(R.id.imgLogout);
        imgLogin = (ImageView) findViewById(R.id.imgLogin);
        imgOrderHistory = (ImageView) findViewById(R.id.imgOrderHistory);
        imgMyAddress = (ImageView) findViewById(R.id.imgMyAddress);
        imgReviews = (ImageView) findViewById(R.id.imgReviews);
        imgFeedback = (ImageView) findViewById(R.id.imgFeedback);
        imgHome = (ImageView) findViewById(R.id.imgHome);
        imgContactUs = (ImageView) findViewById(R.id.imgContactUs);

        imageHome = (ImageView) findViewById(R.id.imageHome);
        imageCatagory = (ImageView) findViewById(R.id.imageCatagory);
        imageNotification = (ImageView) findViewById(R.id.imageNotification);
        imageWishlist = (ImageView) findViewById(R.id.imageWishlist);
        imageProfile = (ImageView) findViewById(R.id.imageProfile);

        imageNavigation = (ImageView) findViewById(R.id.imageNavigation);
        imageCartBack = (ImageView) findViewById(R.id.imageCartBack);
        imageCart = (ImageView) findViewById(R.id.imageCart);
        imageOrderHistoryFilter = (ImageView) findViewById(R.id.imageOrderHistoryFilter);
        txtCatName = (TextView) findViewById(R.id.txtCatName);

        imageNavigation.setImageResource(R.drawable.ic_menu);

        textHome = (TextView) findViewById(R.id.textHome);
        textCatagory = (TextView) findViewById(R.id.textCatagory);
        textNotification = (TextView) findViewById(R.id.textNotification);
        textWishlist = (TextView) findViewById(R.id.textWishlist);
        textProfile = (TextView) findViewById(R.id.textProfile);

        llBottomNavigation = (LinearLayout) findViewById(R.id.llBottomNavigation);
        progress = (ProgressBar) findViewById(R.id.progress);
        txtCardCount = (TextView) findViewById(R.id.txtCardCount);
        frameLayoutCart = (FrameLayout) findViewById(R.id.frameLayoutCart);

        circularImageViewUser = (CircleImageView) findViewById(R.id.circularImageViewUser);
        txtUserName = (TextView) findViewById(R.id.txtUserName);
        txtUserEmail = (TextView) findViewById(R.id.txtUserEmail);
        txtWishlistCount = (TextView) findViewById(R.id.txtWishlistCount);
        recyclerviewCMS = (RecyclerView) findViewById(R.id.recyclerviewCMS);

        cardviewBottomNavigation = (CardView) findViewById(R.id.cardviewBottomNavigation);
        llNavigationHeader = (LinearLayout) findViewById(R.id.llNavigationHeader);

        //region HomeFragment
        Slider = (ViewPager) findViewById(R.id.Slider);
        relativeProgress = (RelativeLayout) findViewById(R.id.relativeProgress);

        recyclerviewBestSelling = (RecyclerView) findViewById(R.id.recyclerviewBestSelling);
        recyclerviewMostwanted = (RecyclerView) findViewById(R.id.recyclerviewMostwanted);
        recyclerviewNewArrivals = (RecyclerView) findViewById(R.id.recyclerviewNewArrivals);
        recyclerviewBenner = (RecyclerView) findViewById(R.id.recyclerviewBenner);
        recyclerviewCategories = (RecyclerView) findViewById(R.id.recyclerviewCategories);

        txtBestSeller = (TextView) findViewById(R.id.txtBestSeller);
        txtMostwanted = (TextView) findViewById(R.id.txtMostwanted);
        txtNewArrivals = (TextView) findViewById(R.id.txtNewArrivals);

        llBestSeller = (LinearLayout) findViewById(R.id.llBestSeller);
        llMostWanted = (LinearLayout) findViewById(R.id.llMostWanted);
        llArrivals = (LinearLayout) findViewById(R.id.llArrivals);
        llToolbar = (LinearLayout) findViewById(R.id.llToolbar);

        /*frameLayoutBestSeller = (FrameLayout) findViewById(R.id.frameLayoutBestSeller);
        framLayoutmostWanted = (FrameLayout) findViewById(R.id.framLayoutmostWanted);
        frameLayoutNewArrivals = (FrameLayout) findViewById(R.id.frameLayoutNewArrivals);*/
        frameLayoutBenner = (FrameLayout) findViewById(R.id.frameLayoutBenner);

        editSearch = (AutoCompleteTextView) findViewById(R.id.editSearch);
        editSearch.setThreshold(0);
        //endregion

    }
    //endregion

    //region AppSettings
    private void AppSettings() {
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != null) {
            txtBestSeller.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtMostwanted.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtNewArrivals.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            editSearch.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        }

        txtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        txtUserName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtUserEmail.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtWishlistCount.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        txtMyAddress.setTextColor(Color.parseColor("#919191"));
        txtOrderHistory.setTextColor(Color.parseColor("#919191"));
        txtReviews.setTextColor(Color.parseColor("#919191"));
        txtFeedback.setTextColor(Color.parseColor("#919191"));
        txtContactUs.setTextColor(Color.parseColor("#919191"));
//        txtWishlistCount.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TEXTCOLOR)));

        llSreach.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));


        txtLogout.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        txtLogIn.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        llLogout.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        llLogIn.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        imgLogout.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        imgLogin.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

    }

    //endregion

    //region TypeFace
    private void TypeFace() {
        txtBestSeller.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtMostwanted.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtNewArrivals.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtUserEmail.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtUserName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtOrderHistory.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtMyAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtLogout.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtLogIn.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtReviews.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtFeedback.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtWishlistCount.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editSearch.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtContactUs.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtHome.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
    }
    //endregion

    //region movetofragment
    public void movetofragment(Bundle bundle, Fragment fragment) {
        try {
            fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//            fragmentManager.popBackStack();
            fragment.setArguments(bundle);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.replace(R.id.framelayout, fragment);
            fragmentTransaction.commit();
        } catch (IllegalStateException i) {
            i.printStackTrace();
          /*  //Creating SendMail object
            SendMail sm = new SendMail(this, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When call methos move to fragmen.\n" + i.toString());
            //Executing sendmail to send email
            sm.execute();*/
        }
    }
    //endregion

    //region manageBackPress
    public static void manageBackPress(boolean value) {
        if (value) {
            isDashBoard = true;
        } else {
            isDashBoard = false;
        }
    }
    //endregion

    //region chanageTextCard
    public void changeTextCard(TextView textView) {
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            textView.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
// textView.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.BTNTEXTCOLOR)));

        textView.setBackgroundResource(R.drawable.bg_card);
        GradientDrawable gd = (GradientDrawable) textView.getBackground().getCurrent();
        gd.setShape(GradientDrawable.OVAL);
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor) != "") {
            gd.setColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        }
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
    }
    //endregion

    //region chanageImages
    public void chanageImages(TextView textView) {
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor) != "") {
            textView.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        }
//        button.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TEXTCOLOR)));
        textView.setBackgroundResource(R.drawable.bg_cricler);
        GradientDrawable gd = (GradientDrawable) textView.getBackground().getCurrent();
        gd.setShape(GradientDrawable.OVAL);
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
//        gd.setCornerRadius(200);
        /* gd.setStroke(2, Color.parseColor(StaticUtility.BORDERCOLOR));*/
    }
    //endregion

    //region Drawer
    public static void manageDrawer(boolean value) {
        if (value) {
            drawer.openDrawer(GravityCompat.START);
        } else {
            drawer.closeDrawer(GravityCompat.START);
        }
    }
    //endregion

    @Override
    public void onBackPressed() {
        if (checkback) {
            checkback = false;
            String status = "Transaction Cancelled!";
            bundle = new Bundle();
            PlaceOrderSuccessFragment placeOrderSuccessFragment = new PlaceOrderSuccessFragment();
            bundle.putString("payment_method", "2");
            bundle.putString("urlStatus", status);
            movetofragment(bundle, placeOrderSuccessFragment);
        } else {
            if (!isDashBoard) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    txtCatName.setVisibility(View.GONE);
                    imageCartBack.setVisibility(View.GONE);
                    imageOrderHistoryFilter.setVisibility(View.GONE);

                    imageNavigation.setVisibility(View.VISIBLE);
                    imageLogo.setVisibility(View.VISIBLE);
                    if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Cart_Active) != null) {
                        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Is_Cart_Active).equals("1")) {
                            frameLayoutCart.setVisibility(View.VISIBLE);
                        } else {
                            frameLayoutCart.setVisibility(View.GONE);
                        }
                    }
                    llBottomNavigation.setVisibility(View.VISIBLE);
                    cardviewBottomNavigation.setVisibility(View.VISIBLE);
                    toolbar.setVisibility(View.VISIBLE);


                    if (isCheckoutBack.equalsIgnoreCase("Checkout")) {
                        SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                        SharedPreference.ClearPreference(context, Global.Billing_Preference);
                    }

                    GetCartCount();
                    API();

                    editSearch.setText("");
                    isDashBoard = true;
                    super.onBackPressed();
                    if (ClearSharePreference) {
                        ClearSharePreference = false;
                        SharedPreference.ClearPreference(context, Global.IsClickPreference);
                    }
                }
            } else {
                if (PlaceOrderSuccessback) {
                    PlaceOrderSuccessback = false;
                    PlaceOrderSuccessFragment.isclickback = false;
                    Intent intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                    finish();
                    isDashBoard = true;
                } else {
                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        //region exit on backProcess
                        if (doubleBackToExitPressedOnce) {
                            System.exit(0);
                        }
                        this.doubleBackToExitPressedOnce = true;
                        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                doubleBackToExitPressedOnce = false;
                            }
                        }, 3000);
                        //endregion
                    }
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        switch (v.getId()) {
            case R.id.linearHome:
                imageHome.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                imageCatagory.setColorFilter(Color.parseColor("#919191"));
                imageWishlist.setColorFilter(Color.parseColor("#919191"));
                imageNotification.setColorFilter(Color.parseColor("#919191"));
                imageProfile.setColorFilter(Color.parseColor("#919191"));

                textHome.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                textNotification.setTextColor(Color.parseColor("#919191"));
                textCatagory.setTextColor(Color.parseColor("#919191"));
                textWishlist.setTextColor(Color.parseColor("#919191"));
                textProfile.setTextColor(Color.parseColor("#919191"));

                startActivity(new Intent(context, MainActivity.class));
                finish();
                isDashBoard = true;

                break;
            case R.id.linearCatagory:
                imageHome.setColorFilter(Color.parseColor("#919191"));
                imageCatagory.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                imageWishlist.setColorFilter(Color.parseColor("#919191"));
                imageNotification.setColorFilter(Color.parseColor("#919191"));
                imageProfile.setColorFilter(Color.parseColor("#919191"));

                textHome.setTextColor(Color.parseColor("#919191"));
                textNotification.setTextColor(Color.parseColor("#919191"));
                textCatagory.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                textWishlist.setTextColor(Color.parseColor("#919191"));
                textProfile.setTextColor(Color.parseColor("#919191"));

                MegaMenuCategoryFragment megaMenuCategoryFragment1 = new MegaMenuCategoryFragment();
                movetofragment(bundle, megaMenuCategoryFragment1);
                break;

            case R.id.linearNotification:
                imageHome.setColorFilter(Color.parseColor("#919191"));
                imageCatagory.setColorFilter(Color.parseColor("#919191"));
                imageWishlist.setColorFilter(Color.parseColor("#919191"));
                imageNotification.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                imageProfile.setColorFilter(Color.parseColor("#919191"));

                textHome.setTextColor(Color.parseColor("#919191"));
                textNotification.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                textCatagory.setTextColor(Color.parseColor("#919191"));
                textWishlist.setTextColor(Color.parseColor("#919191"));
                textProfile.setTextColor(Color.parseColor("#919191"));
                break;

            case R.id.linearWishlist:
                imageHome.setColorFilter(Color.parseColor("#919191"));
                imageCatagory.setColorFilter(Color.parseColor("#919191"));
                imageWishlist.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                imageNotification.setColorFilter(Color.parseColor("#919191"));
                imageProfile.setColorFilter(Color.parseColor("#919191"));

                textHome.setTextColor(Color.parseColor("#919191"));
                textNotification.setTextColor(Color.parseColor("#919191"));
                textCatagory.setTextColor(Color.parseColor("#919191"));
                textWishlist.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                textProfile.setTextColor(Color.parseColor("#919191"));
                if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
                    userID = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
                    if (!userID.equalsIgnoreCase("")) {
                        WishlistFragment wishlistFragment = new WishlistFragment();
                        movetofragment(bundle, wishlistFragment);
                    } else {

                        imageWishlist.setColorFilter(Color.parseColor("#919191"));
                        imageCatagory.setColorFilter(Color.parseColor("#919191"));
                        imageHome.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                        imageNotification.setColorFilter(Color.parseColor("#919191"));
                        imageProfile.setColorFilter(Color.parseColor("#919191"));

                        textWishlist.setTextColor(Color.parseColor("#919191"));
                        textNotification.setTextColor(Color.parseColor("#919191"));
                        textCatagory.setTextColor(Color.parseColor("#919191"));
                        textHome.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                        textProfile.setTextColor(Color.parseColor("#919191"));

                        /*Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();*/
                        SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                        Intent intent = new Intent(context, LoginActivity.class);
                        SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                        SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                        SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                        SharedPreference.ClearPreference(context, Global.Billing_Preference);
                        SharedPreference.ClearPreference(context, Global.ISCheck);
                        startActivity(intent);
                        /*finish();*/
                    }
                } else {

                    imageWishlist.setColorFilter(Color.parseColor("#919191"));
                    imageCatagory.setColorFilter(Color.parseColor("#919191"));
                    imageHome.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                    imageNotification.setColorFilter(Color.parseColor("#919191"));
                    imageProfile.setColorFilter(Color.parseColor("#919191"));

                    textWishlist.setTextColor(Color.parseColor("#919191"));
                    textNotification.setTextColor(Color.parseColor("#919191"));
                    textCatagory.setTextColor(Color.parseColor("#919191"));
                    textHome.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                    textProfile.setTextColor(Color.parseColor("#919191"));

                    /*Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();*/
                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                    Intent intent = new Intent(context, LoginActivity.class);
                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                    SharedPreference.ClearPreference(context, Global.ISCheck);
                    startActivity(intent);
                }
                break;

            case R.id.linearProfile:
                imageHome.setColorFilter(Color.parseColor("#919191"));
                imageCatagory.setColorFilter(Color.parseColor("#919191"));
                imageWishlist.setColorFilter(Color.parseColor("#919191"));
                imageNotification.setColorFilter(Color.parseColor("#919191"));
                imageProfile.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

                textHome.setTextColor(Color.parseColor("#919191"));
                textNotification.setTextColor(Color.parseColor("#919191"));
                textCatagory.setTextColor(Color.parseColor("#919191"));
                textWishlist.setTextColor(Color.parseColor("#919191"));
                textProfile.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
                    userID = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
                    if (!userID.equalsIgnoreCase("")) {
                        MyAccountFragment myAccountFragment = new MyAccountFragment();
                        movetofragment(bundle, myAccountFragment);
                    } else {

                        imageHome.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                        textHome.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

                        imageProfile.setColorFilter(Color.parseColor("#919191"));
                        imageCatagory.setColorFilter(Color.parseColor("#919191"));
                        imageWishlist.setColorFilter(Color.parseColor("#919191"));
                        imageNotification.setColorFilter(Color.parseColor("#919191"));

                        textProfile.setTextColor(Color.parseColor("#919191"));
                        textNotification.setTextColor(Color.parseColor("#919191"));
                        textCatagory.setTextColor(Color.parseColor("#919191"));
                        textWishlist.setTextColor(Color.parseColor("#919191"));

                        /*Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();*/
                        SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                        Intent intent = new Intent(context, LoginActivity.class);
                        SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                        SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                        SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                        SharedPreference.ClearPreference(context, Global.Billing_Preference);
                        SharedPreference.ClearPreference(context, Global.ISCheck);
                        startActivity(intent);
                        /*finish();*/
                    }
                } else {
                    imageHome.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                    textHome.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

                    imageProfile.setColorFilter(Color.parseColor("#919191"));
                    imageCatagory.setColorFilter(Color.parseColor("#919191"));
                    imageWishlist.setColorFilter(Color.parseColor("#919191"));
                    imageNotification.setColorFilter(Color.parseColor("#919191"));

                    textProfile.setTextColor(Color.parseColor("#919191"));
                    textNotification.setTextColor(Color.parseColor("#919191"));
                    textCatagory.setTextColor(Color.parseColor("#919191"));
                    textWishlist.setTextColor(Color.parseColor("#919191"));

                    /*Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();*/
                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                    Intent intent = new Intent(context, LoginActivity.class);
                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                    SharedPreference.ClearPreference(context, Global.ISCheck);
                    startActivity(intent);
                }
                break;

            case R.id.llLogout:
                LISessionManager.getInstance(getApplicationContext()).clearSession();
                pdkClient.getInstance().logout();
                if (mApp.hasAccessToken()) {
                    mApp.resetAccessToken();
                }
                try {
                    if (FacebookSdk.isInitialized()) {
                        LoginManager.getInstance().logOut();
                    }
                } catch (FacebookSdkNotInitializedException e) {
                    e.printStackTrace();
                }
                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                        new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                                //updateUI(false);
                            }
                        });
                Logout();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                break;

            case R.id.llLogIn:
                startActivity(new Intent(context, LoginActivity.class));
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                /*finish();*/
                break;

            case R.id.frameLayoutCart:
                bundle = new Bundle();
                bundle.putString("ActivityType", "MainActivity");
                CartFragment cartFragment = new CartFragment();
                movetofragment(bundle, cartFragment);
                break;

            case R.id.llOrderHistory:
                txtOrderHistory.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                imgOrderHistory.setImageResource(R.drawable.ic_circle);
                imgOrderHistory.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                txtMyAddress.setTextColor(Color.parseColor("#919191"));
                imgMyAddress.setImageResource(R.drawable.ic_circle);
                imgMyAddress.setColorFilter(Color.parseColor("#919191"));
                txtReviews.setTextColor(Color.parseColor("#919191"));
                imgReviews.setImageResource(R.drawable.ic_circle);
                imgReviews.setColorFilter(Color.parseColor("#919191"));
                txtFeedback.setTextColor(Color.parseColor("#919191"));
                imgFeedback.setImageResource(R.drawable.ic_circle);
                imgFeedback.setColorFilter(Color.parseColor("#919191"));
                txtHome.setTextColor(Color.parseColor("#919191"));
                imgHome.setImageResource(R.drawable.ic_circle);
                imgHome.setColorFilter(Color.parseColor("#919191"));
                textCatagory.setTextColor(Color.parseColor("#919191"));
                imgContactUs.setImageResource(R.drawable.ic_circle);
                imgContactUs.setColorFilter(Color.parseColor("#919191"));
                txtContactUs.setTextColor(Color.parseColor("#919191"));
               /* OrderHistoryFragment orderHistoryFragment = new OrderHistoryFragment();
                movetofragment(bundle, orderHistoryFragment);*/


                if (!userID.equalsIgnoreCase("")) {
                    Intent intentOrderHistory = new Intent(context, OrderHistoryActivity.class);
                    intentOrderHistory.putExtra("Redirect", "MainActivity");
                    startActivity(intentOrderHistory);
                } else {
                    /*Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();*/
                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                    Intent intent = new Intent(context, LoginActivity.class);
                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                    SharedPreference.ClearPreference(context, Global.ISCheck);
                    startActivity(intent);
                    finish();
                }

                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.closeDrawer(GravityCompat.END);
                }
                break;

            case R.id.llMyAddress:
                txtMyAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                imgMyAddress.setImageResource(R.drawable.ic_circle);
                imgMyAddress.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                txtOrderHistory.setTextColor(Color.parseColor("#919191"));
                imgOrderHistory.setImageResource(R.drawable.ic_circle);
                imgOrderHistory.setColorFilter(Color.parseColor("#919191"));
                txtReviews.setTextColor(Color.parseColor("#919191"));
                imgReviews.setImageResource(R.drawable.ic_circle);
                imgReviews.setColorFilter(Color.parseColor("#919191"));
                txtFeedback.setTextColor(Color.parseColor("#919191"));
                imgFeedback.setImageResource(R.drawable.ic_circle);
                imgFeedback.setColorFilter(Color.parseColor("#919191"));
                txtHome.setTextColor(Color.parseColor("#919191"));
                imgHome.setImageResource(R.drawable.ic_circle);
                imgHome.setColorFilter(Color.parseColor("#919191"));
                textCatagory.setTextColor(Color.parseColor("#919191"));
                imgContactUs.setImageResource(R.drawable.ic_circle);
                imgContactUs.setColorFilter(Color.parseColor("#919191"));
                txtContactUs.setTextColor(Color.parseColor("#919191"));

                if (!userID.equalsIgnoreCase("")) {
                    getAddressList("", "MainActivity", "", "");
                } else {
                    Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();
                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                    Intent intent = new Intent(context, LoginActivity.class);
                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                    SharedPreference.ClearPreference(context, Global.ISCheck);
                    startActivity(intent);
                    finish();
                }

                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.closeDrawer(GravityCompat.END);
                }
                break;
        }
    }

    //region ViewPager..
    private void getViewPager(final ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(context, img);
        viewPager.setAdapter(viewPagerAdapter);

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == img.length) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 500, 5000);
    }
    //endregion

    //region FOR silder API..
    private void Silder() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.GETSLIDER);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                try {
                    Log.d("Test", "onResponse object : " + response.toString());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            /*itemVendorStocks.clear();*/
                            relativeProgress.setVisibility(View.GONE);
                        }
                    });
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
//                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    JSONObject jsonObjectslider = response.getJSONObject("payload");
                                    if (jsonObjectslider.has("slides")) {
                                        Object object = jsonObjectslider.get("slides");
                                        if (!object.equals("")) {
                                            Slider.setVisibility(View.VISIBLE);
                                            JSONArray jsonArrayslider = jsonObjectslider.getJSONArray("slides");
                                            img = new String[jsonArrayslider.length()];
                                            for (int i = 0; i < jsonArrayslider.length(); i++) {
                                                JSONObject jsonObject = jsonArrayslider.getJSONObject(i);
                                                JSONArray array = jsonObject.getJSONArray("image_url");
                                                img[i] = String.valueOf(array.get(0));
                                            }
                                            getViewPager(Slider);
                                        } else {
                                            Slider.setVisibility(View.GONE);
                                        }
                                    } else {
                                        Slider.setVisibility(View.GONE);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in HomeFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR getBestSeller API..
    private void getBestSeller() {
        relativeProgress.setVisibility(View.VISIBLE);
        String[] key;
        String[] val;

        String user_id = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
            key = new String[]{"user_id"};
            val = new String[]{user_id};
        } else {
            key = new String[]{};
            val = new String[]{};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.getBestSeller);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                try {
                    Log.d("Test", "onResponse object : " + response.toString());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            /*itemVendorStocks.clear();*/
                            relativeProgress.setVisibility(View.GONE);
                        }
                    });
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    llBestSeller.setVisibility(View.VISIBLE);
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    if (jsonObjectPayload.has("bestseller")) {
                                        JSONArray jsonArraySeller = jsonObjectPayload.getJSONArray("bestseller");
                                        jsonArrayBestSeller = jsonArraySeller;
                                        AdapterBestSeller adapterHomeProduct = new AdapterBestSeller(context, jsonArraySeller, "bestSeller");
                                        recyclerviewBestSelling.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                        recyclerviewBestSelling.setAdapter(adapterHomeProduct);
                                        if (bestSellerPosition != 0) {
                                            recyclerviewBestSelling.scrollToPosition(bestSellerPosition);
                                            bestSellerPosition = 0;
                                        }
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in HomeFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR getWantedProduct API..
    private void getWantedProduct() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key;
        String[] val;

        String user_id = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
            key = new String[]{"user_id"};
            val = new String[]{user_id};
        } else {
            key = new String[]{};
            val = new String[]{};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.getWantedProduct);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                try {
                    Log.d("Test", "onResponse object : " + response.toString());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            /*itemVendorStocks.clear();*/
                            relativeProgress.setVisibility(View.GONE);
                        }
                    });
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    llMostWanted.setVisibility(View.VISIBLE);
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    if (jsonObjectPayload.has("mostWantedProducts")) {
                                        JSONArray jsonArrayWanted = jsonObjectPayload.getJSONArray("mostWantedProducts");
                                        jsonArrayMostWanted = jsonArrayWanted;
                                        AdapterMostWanted adapterMostWanted = new AdapterMostWanted(context, jsonArrayWanted, "mostwanted");
                                        recyclerviewMostwanted.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                        recyclerviewMostwanted.setAdapter(adapterMostWanted);
                                        if (mostWantedPosition != 0) {
                                            recyclerviewMostwanted.scrollToPosition(mostWantedPosition);
                                            mostWantedPosition = 0;
                                        }
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in HomeFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR getNewArrivals API..
    private void getNewArrivals() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key;
        String[] val;

        String user_id = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
            key = new String[]{"user_id"};
            val = new String[]{user_id};
        } else {
            key = new String[]{};
            val = new String[]{};
        }


        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.getNewArrivals);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                try {
                    Log.d("Test", "onResponse object : " + response.toString());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            /*itemVendorStocks.clear();*/
                            relativeProgress.setVisibility(View.GONE);
                        }
                    });
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    llArrivals.setVisibility(View.VISIBLE);
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    if (jsonObjectPayload.has("newArrivals")) {
                                        JSONArray jsonArrayNewArrivals = jsonObjectPayload.getJSONArray("newArrivals");
                                        jsonArrayArrivals = jsonArrayNewArrivals;
                                        AdapterNewArrivals adapterArrivals = new AdapterNewArrivals(context, jsonArrayNewArrivals, "newarrivals");
                                        recyclerviewNewArrivals.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                        recyclerviewNewArrivals.setAdapter(adapterArrivals);
                                        if (newArrivalPosition != 0) {
                                            recyclerviewNewArrivals.scrollToPosition(mostWantedPosition);
                                            newArrivalPosition = 0;
                                        }
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in HomeFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR getBenners API..
    private void getBenners() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.getBenner);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    if (jsonObjectPayload.has("images")) {
                                        JSONArray jsonArrayImages = jsonObjectPayload.getJSONArray("images");
                                        frameLayoutBenner.setVisibility(View.VISIBLE);
                                        jsonArrayBenner = jsonArrayImages;
                                        AdapterBanner adapterHomeProduct = new AdapterBanner(context, jsonArrayImages);
                                        recyclerviewBenner.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                        recyclerviewBenner.setAdapter(adapterHomeProduct);
                                    } else {
                                        frameLayoutBenner.setVisibility(View.GONE);
                                    }
                                }

                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in HomeFragment.java When parsing Error response.\n" + e.toString());
                                //Executing sendmail to send email
                                sm.execute();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in HomeFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR getCategory API..
    private void getCategory() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.getCategory);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    recyclerviewCategories.setVisibility(View.VISIBLE);
                                    JSONArray jsonArrayPayload = response.getJSONArray("payload");
                                    categories = new ArrayList<>();
                                    for (int i = 0; i < jsonArrayPayload.length(); i++) {
                                        JSONObject jsonObjectPayload = jsonArrayPayload.getJSONObject(i);
                                        String cat_name = jsonObjectPayload.getString("cat_name");
                                        String slug = jsonObjectPayload.getString("slug");
                                        JSONObject jsonObjectImage = jsonObjectPayload.getJSONObject("image_url");
                                        String images = jsonObjectImage.getString("main_image");
                                        categories.add(new Category(images, cat_name, slug));
                                       /* if (jsonObjectPayload.has("childs")) {
                                            JSONArray jsonArray = jsonObjectPayload.getJSONArray("childs");
                                            for (int j = 0; j < jsonArray.length(); j++) {
                                                JSONObject jsonObjectChild = jsonArray.getJSONObject(j);
                                                String cat_name = jsonObjectChild.getString("cat_name");
                                                String slug = jsonObjectChild.getString("slug");
                                                Object objImageUrl = jsonObjectChild.opt("image_url");
                                                String images = "";
                                                if (!objImageUrl.equals("")){
                                                    JSONObject jsonObjectImage1 = jsonObjectChild.getJSONObject("image_url");
                                                    images = jsonObjectImage1.getString("main_image");
                                                }
                                                categories.add(new Category(images, cat_name, slug));
                                            }
                                        } else {
                                            String cat_name = jsonObjectPayload.getString("cat_name");
                                            String slug = jsonObjectPayload.getString("slug");
                                            JSONObject jsonObjectImage = jsonObjectPayload.getJSONObject("image_url");
                                            String images = jsonObjectImage.getString("main_image");
                                            categories.add(new Category(images, cat_name, slug));
                                        }*/
                                    }
                                    if (categories.size() > 0) {
                                        AdapterCategories adapterCategories = new AdapterCategories(context, categories);
                                        recyclerviewCategories.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                        recyclerviewCategories.setAdapter(adapterCategories);
                                    } else {
                                        recyclerviewCategories.setVisibility(View.GONE);
                                    }



                                /*    if (jsonArrayPayload.length() > 0) {
                                        AdapterCategories adapterCategories = new AdapterCategories(context, jsonArrayPayload);
                                        recyclerviewCategories.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                        recyclerviewCategories.setAdapter(adapterCategories);
                                    } else {
                                        recyclerviewCategories.setVisibility(View.GONE);
                                    }*/
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in HomeFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR Logout API..
    private void Logout() {
        String usertoken = "";
        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
            usertoken = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERTOKEN);
        }
        String[] key = {"user_token"};
        String[] val = {usertoken};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.Logout);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                SharedPreference.ClearPreference(context, Global.ISCheck);
                                Intent intent = new Intent(context, LoginActivity.class);

                                startActivity(intent);
                                /*finish();*/
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(context, "User logged out successfully..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    SharedPreference.ClearPreference(context, Global.ISCheck);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    startActivity(intent);
                                    /*finish();*/
                                } else {
                                    Toast.makeText(context, "User logged out successfully..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    SharedPreference.ClearPreference(context, Global.ISCheck);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    startActivity(intent);
                                    /*finish();*/
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR GetCartCount API..
    private void GetCartCount() {

        String[] key = {};
        String[] val = {};

        String userId = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
        String SessionId = SharedPreference.GetPreference(context, Global.preferenceNameGuestUSer, Global.SessionId);
        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
            key = new String[]{"session_id", "user_id"};
            val = new String[]{"", userId};
        } else if (SharedPreference.GetPreference(context, Global.preferenceNameGuestUSer, Global.SessionId) != null) {
            key = new String[]{"session_id", "user_id"};
            val = new String[]{SessionId, ""};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.CartCount);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("ok")) {
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    String cart_items = jsonObject.getString("cart_items");
                                    String wishlist_cnt = jsonObject.getString("wishlist_cnt");
                                    if (Integer.parseInt(cart_items) > 0) {
                                        txtCardCount.setVisibility(View.VISIBLE);
                                        txtCardCount.setText(cart_items);
                                    } else {
                                        txtCardCount.setText(cart_items);
                                        txtCardCount.setVisibility(View.GONE);
                                    }
                                    if (Integer.parseInt(wishlist_cnt) > 0) {
                                        txtWishlistCount.setVisibility(View.VISIBLE);
                                        txtWishlistCount.setText(wishlist_cnt);
                                    } else {
                                        txtWishlistCount.setVisibility(View.GONE);
                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region AdapterBestSeller
    public class AdapterBestSeller extends RecyclerView.Adapter<AdapterBestSeller.Viewholder> {

        Context context;
        JSONArray jsonArray;
        String type;

        public AdapterBestSeller(Context context, JSONArray jsonArray, String type) {
            this.context = context;
            this.jsonArray = jsonArray;
            this.type = type;
        }

        @Override
        public AdapterBestSeller.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_product, viewGroup, false);
            return new AdapterBestSeller.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder viewholder, final int position) {
            try {

                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        viewholder.pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
                    }
                }

                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) viewholder.llProducts.getLayoutParams();
                if (position == 0) {
                    params.setMargins(15, 0, 5, 0);
                    viewholder.llProducts.setLayoutParams(params);
                } else if (position == getItemCount() - 1) {
                    params.setMargins(5, 0, 15, 0);
                    viewholder.llProducts.setLayoutParams(params);
                } else {
                    params.setMargins(5, 0, 5, 0);
                    viewholder.llProducts.setLayoutParams(params);
                }
                final JSONObject jsonObject = jsonArray.getJSONObject(position);
                String strRating = jsonObject.getString("avg_rating");
                float floatBasePrice = Float.parseFloat(jsonObject.getString("price"));
                int intBasePrice = Math.round(floatBasePrice);
                float floatSalePrice = Float.parseFloat(jsonObject.getString("sale_price"));
                int intSalePrice = Math.round(floatSalePrice);
                final String product_id = jsonObject.getString("product_id");
                String exists_in_wishlist = jsonObject.getString("exists_in_wishlist");
                viewholder.textProductName.setText(jsonObject.getString("name"));
                /*viewholder.imgProductPlaceholder.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));*/

                final String slug = jsonObject.getString("slug");
                final String name = jsonObject.getString("name");

                if (!strRating.equals("")) {
                    float rating = Float.parseFloat(strRating);
                    viewholder.reviewRatingbar.setVisibility(View.VISIBLE);
                    viewholder.reviewRatingbar.setRating(rating);
                } else {
                    float rating = Float.parseFloat("0");
                    viewholder.reviewRatingbar.setVisibility(View.VISIBLE);
                    viewholder.reviewRatingbar.setRating(rating);
                }

                if (intSalePrice > 0) {
                    if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("1")) {
                        viewholder.textProductSalePrice.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + jsonObject.getString("sale_price"));
                        viewholder.txt_product_base_price.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + jsonObject.getString("price"));
                    } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("2")) {
                        viewholder.textProductSalePrice.setText(jsonObject.getString("sale_price") + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        viewholder.txt_product_base_price.setText(jsonObject.getString("price") + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                    }
                } else {
                    if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("1")) {
                        viewholder.textProductSalePrice.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + jsonObject.getString("price"));
                        viewholder.txt_product_base_price.setVisibility(View.GONE);
                    } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("2")) {
                        viewholder.textProductSalePrice.setText(jsonObject.getString("price") + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        viewholder.txt_product_base_price.setVisibility(View.GONE);
                    }
                }

                if (is_wishlist.equals("1")) {
                    viewholder.imgWish.setVisibility(View.VISIBLE);
                } else {
                    viewholder.imgWish.setVisibility(View.GONE);
                }

                if (exists_in_wishlist.equals("0")) {
                    viewholder.imgWish.setImageResource(R.drawable.ic_heart);
                } else if (exists_in_wishlist.equals("1")) {
                    viewholder.imgWish.setImageResource(R.drawable.ic_select_heart);
                }

                viewholder.imgWish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
                            userID = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
                            if (!userID.equalsIgnoreCase("")) {
                                AddToWishList(product_id, viewholder.imgWish, position, jsonArray, recyclerviewBestSelling);
                            } else {
                                Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(context, LoginActivity.class);
                                SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                SharedPreference.ClearPreference(context, Global.ISCheck);
                                startActivity(intent);
                                /*finish();*/
                            }
                        } else {
                            Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, LoginActivity.class);
                            SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                            SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                            SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                            SharedPreference.ClearPreference(context, Global.Billing_Preference);
                            SharedPreference.ClearPreference(context, Global.ISCheck);
                            startActivity(intent);
                            /*finish();*/
                        }
                    }
                });

                viewholder.framelayoutProduct.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bestSellerPosition = position;
                       /* txtCatName.setVisibility(View.VISIBLE);
                        imageNavigation.setVisibility(View.GONE);
                        imageLogo.setVisibility(View.GONE);
                        txtCatName.setText("");*/
                        try {
                            String product_id = jsonObject.getString("product_id");
                            ProductDetailFragment productDetailFragment = new ProductDetailFragment();
                            bundle.putString("slug", slug);
                            bundle.putString("name", name);
                            bundle.putString("ProductID", product_id);
                            movetofragment(bundle, productDetailFragment);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

                /*JSONArray jsonArrayImages = jsonObject.getJSONArray("main_image");
                String images = String.valueOf(jsonArrayImages.get(0));*/

                JSONObject jsonObjectImages = jsonObject.getJSONObject("main_image");
                String images = jsonObjectImages.getString("main_image");

                //region Image
                String picUrl = null;
                try {
                    URL urla = null;
                    urla = new URL(images.replaceAll("%20", " "));
                    URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    // Capture position and set to the ImageView
                    Picasso.with(context)
                            .load(picUrl)
                            .into(viewholder.imageProduct, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                    viewholder.rlImgHolder.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }
                            });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
                //endregion

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return jsonArray.length();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            ImageView imageProduct, imgWish;
            TextView textProductName, textProductSalePrice, txt_product_base_price;
            FrameLayout framelayoutProduct;
            RatingBar reviewRatingbar, ratingBar;
            LinearLayout llProducts;
            RelativeLayout rlImgHolder;
            ProgressBar pbImgHolder;

            public Viewholder(View itemView) {
                super(itemView);
                framelayoutProduct = (FrameLayout) itemView.findViewById(R.id.framelayoutProduct);
                imageProduct = (ImageView) itemView.findViewById(R.id.imageProduct);
                imgWish = (ImageView) itemView.findViewById(R.id.imgWish);
                textProductName = (TextView) itemView.findViewById(R.id.textProductName);
                textProductSalePrice = (TextView) itemView.findViewById(R.id.textProductSalePrice);
                txt_product_base_price = (TextView) itemView.findViewById(R.id.txt_product_base_price);
                reviewRatingbar = (RatingBar) itemView.findViewById(R.id.reviewRatingbar);
                ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar);
                llProducts = (LinearLayout) itemView.findViewById(R.id.llProducts);
                rlImgHolder = (RelativeLayout) itemView.findViewById(R.id.rlImgHolder);
                pbImgHolder = (ProgressBar) itemView.findViewById(R.id.pbImgHolder);

                LayerDrawable stars = (LayerDrawable) reviewRatingbar.getProgressDrawable();
                stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                stars.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

                LayerDrawable starRating = (LayerDrawable) ratingBar.getProgressDrawable();
                starRating.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                starRating.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

                textProductName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                textProductSalePrice.setTypeface(Typefaces.TypefaceCalibri_bold(context));
                txt_product_base_price.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
                    textProductName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                    textProductSalePrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                }
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor) != "") {
                    txt_product_base_price.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                }
            }
        }

    }
    //endregion

    //region AdapterMostWanted
    public class AdapterMostWanted extends RecyclerView.Adapter<AdapterMostWanted.Viewholder> {

        Context context;
        JSONArray jsonArray;
        String type;

        public AdapterMostWanted(Context context, JSONArray jsonArray, String type) {
            this.context = context;
            this.jsonArray = jsonArray;
            this.type = type;
        }

        @Override
        public AdapterMostWanted.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_product, viewGroup, false);
            return new AdapterMostWanted.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder viewholder, final int position) {
            try {
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        viewholder.pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
                    }
                }

                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) viewholder.llProducts.getLayoutParams();
                if (position == 0) {
                    params.setMargins(15, 0, 5, 0);
                    viewholder.llProducts.setLayoutParams(params);
                } else if (position == getItemCount() - 1) {
                    params.setMargins(5, 0, 15, 0);
                    viewholder.llProducts.setLayoutParams(params);
                } else {
                    params.setMargins(5, 0, 5, 0);
                    viewholder.llProducts.setLayoutParams(params);
                }
                final JSONObject jsonObject = jsonArray.getJSONObject(position);
                String strRating = jsonObject.getString("avg_rating");
                float floatBasePrice = Float.parseFloat(jsonObject.getString("price"));
                int intBasePrice = Math.round(floatBasePrice);
                float floatSalePrice = Float.parseFloat(jsonObject.getString("sale_price"));
                int intSalePrice = Math.round(floatSalePrice);
                final String product_id = jsonObject.getString("product_id");
                String exists_in_wishlist = jsonObject.getString("exists_in_wishlist");
                viewholder.textProductName.setText(jsonObject.getString("name"));
                /*                viewholder.imgProductPlaceholder.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));*/
                final String slug = jsonObject.getString("slug");
                final String name = jsonObject.getString("name");

                if (!strRating.equals("")) {
                    float rating = Float.parseFloat(strRating);
                    viewholder.reviewRatingbar.setVisibility(View.VISIBLE);
                    viewholder.reviewRatingbar.setRating(rating);
                } else {
                    float rating = Float.parseFloat("0");
                    viewholder.reviewRatingbar.setVisibility(View.VISIBLE);
                    viewholder.reviewRatingbar.setRating(rating);
                }

                if (intSalePrice > 0) {
                    if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("1")) {
                        viewholder.textProductSalePrice.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + jsonObject.getString("sale_price"));
                        viewholder.txt_product_base_price.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + jsonObject.getString("price"));
                    } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("2")) {
                        viewholder.textProductSalePrice.setText(jsonObject.getString("sale_price") + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        viewholder.txt_product_base_price.setText(jsonObject.getString("price") + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                    }
                } else {
                    if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("1")) {
                        viewholder.textProductSalePrice.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + jsonObject.getString("price"));
                        viewholder.txt_product_base_price.setVisibility(View.GONE);
                    } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("2")) {
                        viewholder.textProductSalePrice.setText(jsonObject.getString("price") + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        viewholder.txt_product_base_price.setVisibility(View.GONE);
                    }
                }

                if (is_wishlist.equals("1")) {
                    viewholder.imgWish.setVisibility(View.VISIBLE);
                } else {
                    viewholder.imgWish.setVisibility(View.GONE);
                }

                if (exists_in_wishlist.equals("0")) {
                    viewholder.imgWish.setImageResource(R.drawable.ic_heart);
                } else if (exists_in_wishlist.equals("1")) {
                    viewholder.imgWish.setImageResource(R.drawable.ic_select_heart);
                }

                viewholder.imgWish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
                            userID = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
                            if (!userID.equalsIgnoreCase("")) {
                                AddToWishList(product_id, viewholder.imgWish, position, jsonArray, recyclerviewMostwanted);
                            } else {
                                Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(context, LoginActivity.class);
                                SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                SharedPreference.ClearPreference(context, Global.ISCheck);
                                startActivity(intent);
                                /*finish();*/
                            }
                        } else {
                            Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, LoginActivity.class);
                            SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                            SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                            SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                            SharedPreference.ClearPreference(context, Global.Billing_Preference);
                            SharedPreference.ClearPreference(context, Global.ISCheck);
                            startActivity(intent);
                            /*finish();*/
                        }
                    }
                });

                viewholder.framelayoutProduct.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mostWantedPosition = position;
                        txtCatName.setVisibility(View.VISIBLE);
                        imageNavigation.setVisibility(View.GONE);
                        imageLogo.setVisibility(View.GONE);
                        txtCatName.setText("");
                        try {
                            String product_id = jsonObject.getString("product_id");
                            ProductDetailFragment productDetailFragment = new ProductDetailFragment();
                            bundle.putString("slug", slug);
                            bundle.putString("name", name);
                            bundle.putString("ProductID", product_id);
                            movetofragment(bundle, productDetailFragment);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

              /*  JSONArray jsonArrayImages = jsonObject.getJSONArray("main_image");
                String images = String.valueOf(jsonArrayImages.get(0));*/

                JSONObject jsonObjectImages = jsonObject.getJSONObject("main_image");
                String images = jsonObjectImages.getString("main_image");

                //region Image
                String picUrl = null;
                try {
                    URL urla = null;
                    urla = new URL(images.replaceAll("%20", " "));
                    URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    // Capture position and set to the ImageView
                    Picasso.with(context)
                            .load(picUrl)
                            .into(viewholder.imageProduct, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                    viewholder.rlImgHolder.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }
                            });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
                //endregion

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return jsonArray.length();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            ImageView imageProduct, imgWish;
            TextView textProductName, textProductSalePrice, txt_product_base_price;
            FrameLayout framelayoutProduct;
            RatingBar reviewRatingbar, ratingBar;
            LinearLayout llProducts;
            RelativeLayout rlImgHolder;
            ProgressBar pbImgHolder;

            public Viewholder(View itemView) {
                super(itemView);
                framelayoutProduct = (FrameLayout) itemView.findViewById(R.id.framelayoutProduct);
                imageProduct = (ImageView) itemView.findViewById(R.id.imageProduct);
                imgWish = (ImageView) itemView.findViewById(R.id.imgWish);
                textProductName = (TextView) itemView.findViewById(R.id.textProductName);
                textProductSalePrice = (TextView) itemView.findViewById(R.id.textProductSalePrice);
                txt_product_base_price = (TextView) itemView.findViewById(R.id.txt_product_base_price);
                reviewRatingbar = (RatingBar) itemView.findViewById(R.id.reviewRatingbar);
                ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar);
                llProducts = (LinearLayout) itemView.findViewById(R.id.llProducts);
                rlImgHolder = (RelativeLayout) itemView.findViewById(R.id.rlImgHolder);
                pbImgHolder = (ProgressBar) itemView.findViewById(R.id.pbImgHolder);

                LayerDrawable stars = (LayerDrawable) reviewRatingbar.getProgressDrawable();
                stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                stars.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

                LayerDrawable starRating = (LayerDrawable) ratingBar.getProgressDrawable();
                starRating.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                starRating.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

                textProductName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                textProductSalePrice.setTypeface(Typefaces.TypefaceCalibri_bold(context));
                txt_product_base_price.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
                    textProductName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                    textProductSalePrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                }
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor) != "") {
                    txt_product_base_price.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                }
            }
        }

    }
    //endregion

    //region AdapterNewArrivals
    public class AdapterNewArrivals extends RecyclerView.Adapter<AdapterNewArrivals.Viewholder> {

        Context context;
        JSONArray jsonArray;
        String type;

        public AdapterNewArrivals(Context context, JSONArray jsonArray, String type) {
            this.context = context;
            this.jsonArray = jsonArray;
            this.type = type;
        }

        @Override
        public AdapterNewArrivals.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_product, viewGroup, false);
            return new AdapterNewArrivals.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder viewholder, final int position) {
            try {

                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        viewholder.pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
                    }
                }

                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) viewholder.llProducts.getLayoutParams();
                if (position == 0) {
                    params.setMargins(15, 0, 5, 0);
                    viewholder.llProducts.setLayoutParams(params);
                } else if (position == getItemCount() - 1) {
                    params.setMargins(5, 0, 15, 0);
                    viewholder.llProducts.setLayoutParams(params);
                } else {
                    params.setMargins(5, 0, 5, 0);
                    viewholder.llProducts.setLayoutParams(params);
                }
                final JSONObject jsonObject = jsonArray.getJSONObject(position);
                String strRating = jsonObject.getString("avg_rating");
                float floatBasePrice = Float.parseFloat(jsonObject.getString("price"));
                int intBasePrice = Math.round(floatBasePrice);
                float floatSalePrice = Float.parseFloat(jsonObject.getString("sale_price"));
                int intSalePrice = Math.round(floatSalePrice);
                final String product_id = jsonObject.getString("product_id");
                String exists_in_wishlist = jsonObject.getString("exists_in_wishlist");
                viewholder.textProductName.setText(jsonObject.getString("name"));
                /*viewholder.imgProductPlaceholder.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));*/
                final String slug = jsonObject.getString("slug");
                final String name = jsonObject.getString("name");

                if (!strRating.equals("")) {
                    float rating = Float.parseFloat(strRating);
                    viewholder.reviewRatingbar.setVisibility(View.VISIBLE);
                    viewholder.reviewRatingbar.setRating(rating);
                } else {
                    float rating = Float.parseFloat("0");
                    viewholder.reviewRatingbar.setVisibility(View.VISIBLE);
                    viewholder.reviewRatingbar.setRating(rating);
                }

                if (intSalePrice > 0) {
                    if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("1")) {
                        viewholder.textProductSalePrice.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + jsonObject.getString("sale_price"));
                        viewholder.txt_product_base_price.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + jsonObject.getString("price"));
                    } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("2")) {
                        viewholder.textProductSalePrice.setText(jsonObject.getString("sale_price") + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        viewholder.txt_product_base_price.setText(jsonObject.getString("price") + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                    }
                } else {
                    if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("1")) {
                        viewholder.textProductSalePrice.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + jsonObject.getString("price"));
                        viewholder.txt_product_base_price.setVisibility(View.GONE);
                    } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("2")) {
                        viewholder.textProductSalePrice.setText(jsonObject.getString("price") + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        viewholder.txt_product_base_price.setVisibility(View.GONE);
                    }
                }

                if (is_wishlist.equals("1")) {
                    viewholder.imgWish.setVisibility(View.VISIBLE);
                } else {
                    viewholder.imgWish.setVisibility(View.GONE);
                }

                if (exists_in_wishlist.equals("0")) {
                    viewholder.imgWish.setImageResource(R.drawable.ic_heart);
                } else if (exists_in_wishlist.equals("1")) {
                    viewholder.imgWish.setImageResource(R.drawable.ic_select_heart);
                }

                viewholder.imgWish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
                            userID = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
                            if (!userID.equalsIgnoreCase("")) {
                                AddToWishList(product_id, viewholder.imgWish, position, jsonArray, recyclerviewNewArrivals);
                            } else {
                                Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(context, LoginActivity.class);
                                SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                SharedPreference.ClearPreference(context, Global.ISCheck);
                                startActivity(intent);
                                /*finish();*/
                            }
                        } else {
                            Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, LoginActivity.class);
                            SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                            SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                            SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                            SharedPreference.ClearPreference(context, Global.Billing_Preference);
                            SharedPreference.ClearPreference(context, Global.ISCheck);
                            startActivity(intent);
                            /*finish();*/
                        }
                    }
                });

                viewholder.framelayoutProduct.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        newArrivalPosition = position;
                        txtCatName.setVisibility(View.VISIBLE);
                        imageNavigation.setVisibility(View.GONE);
                        imageLogo.setVisibility(View.GONE);
                        txtCatName.setText("");
                        try {
                            String product_id = jsonObject.getString("product_id");
                            ProductDetailFragment productDetailFragment = new ProductDetailFragment();
                            bundle.putString("slug", slug);
                            bundle.putString("name", name);
                            bundle.putString("ProductID", product_id);
                            movetofragment(bundle, productDetailFragment);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

              /*  JSONArray jsonArrayImages = jsonObject.getJSONArray("main_image");
                String images = String.valueOf(jsonArrayImages.get(0));*/

                JSONObject jsonObjectImages = jsonObject.getJSONObject("main_image");
                String images = jsonObjectImages.getString("main_image");

                //region Image
                String picUrl = null;
                try {
                    URL urla = null;
                    urla = new URL(images.replaceAll("%20", " "));
                    URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    // Capture position and set to the ImageView
                    Picasso.with(context)
                            .load(picUrl)
                            .into(viewholder.imageProduct, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                    viewholder.rlImgHolder.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }
                            });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
                //endregion

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return jsonArray.length();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            ImageView imageProduct, imgWish;
            TextView textProductName, textProductSalePrice, txt_product_base_price;
            FrameLayout framelayoutProduct;
            RatingBar reviewRatingbar, ratingBar;
            LinearLayout llProducts;
            RelativeLayout rlImgHolder;
            ProgressBar pbImgHolder;

            public Viewholder(View itemView) {
                super(itemView);
                framelayoutProduct = (FrameLayout) itemView.findViewById(R.id.framelayoutProduct);
                imageProduct = (ImageView) itemView.findViewById(R.id.imageProduct);
                imgWish = (ImageView) itemView.findViewById(R.id.imgWish);
                textProductName = (TextView) itemView.findViewById(R.id.textProductName);
                textProductSalePrice = (TextView) itemView.findViewById(R.id.textProductSalePrice);
                txt_product_base_price = (TextView) itemView.findViewById(R.id.txt_product_base_price);
                reviewRatingbar = (RatingBar) itemView.findViewById(R.id.reviewRatingbar);
                ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar);
                llProducts = (LinearLayout) itemView.findViewById(R.id.llProducts);
                rlImgHolder = (RelativeLayout) itemView.findViewById(R.id.rlImgHolder);
                pbImgHolder = (ProgressBar) itemView.findViewById(R.id.pbImgHolder);

                LayerDrawable stars = (LayerDrawable) reviewRatingbar.getProgressDrawable();
                stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                stars.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

                LayerDrawable starRating = (LayerDrawable) ratingBar.getProgressDrawable();
                starRating.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                starRating.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

                textProductName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                textProductSalePrice.setTypeface(Typefaces.TypefaceCalibri_bold(context));
                txt_product_base_price.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
                    textProductName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                    textProductSalePrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                }
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor) != "") {
                    txt_product_base_price.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                }
            }
        }

    }
    //endregion

    //region AdapterCategories
    public class AdapterCategories extends RecyclerView.Adapter<AdapterCategories.Viewholder> {

        Context context;
        ArrayList<Category> categories = new ArrayList<>();

        public AdapterCategories(Context context, ArrayList<Category> categories) {
            this.context = context;
            this.categories = categories;
        }

        @Override
        public AdapterCategories.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_home_category, viewGroup, false);
            return new AdapterCategories.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder viewholder, int position) {
            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    viewholder.pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
                }
            }
            final Category category = categories.get(position);
            viewholder.txtCategory.setText(category.getCategory_Name());
            String images = category.getCategory_image();

            //region Image
            String picUrl = null;
            try {
                URL urla = null;
                urla = new URL(images.replaceAll("%20", " "));
                URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                picUrl = String.valueOf(urin.toURL());
                // Capture position and set to the ImageView
                Picasso.with(context)
                        .load(picUrl)
                        .into(viewholder.img_Category, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                                viewholder.rlImgHolder.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }
                        });
            } catch (MalformedURLException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            } catch (URISyntaxException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            }
            //endregion

            if (position == categories.size() - 1) {
                viewholder.viewCategory.setVisibility(View.GONE);
            }

            viewholder.llCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String slug = category.getSlug();
                    String cat_name = category.getCategory_Name();
                    ProductListFragment productListFragment = new ProductListFragment();
                    bundle.putString("category_slug", slug);
                    bundle.putString("catName", cat_name);
                    movetofragment(bundle, productListFragment);
                    /*Intent intentProductList = new Intent(context, ProductListingActivity.class);
                    intentProductList.putExtra("category_slug", slug);
                    intentProductList.putExtra("catName", cat_name);
                    startActivity(intentProductList);*/
                }
            });
        }

        @Override
        public int getItemCount() {
            return categories.size();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            ImageView img_Category;
            TextView txtCategory;
            LinearLayout llCategory;
            View viewCategory;
            RelativeLayout rlImgHolder;
            ProgressBar pbImgHolder;

            public Viewholder(View itemView) {
                super(itemView);
                img_Category = (ImageView) itemView.findViewById(R.id.img_Category);
                txtCategory = (TextView) itemView.findViewById(R.id.txtCategory);
                llCategory = (LinearLayout) itemView.findViewById(R.id.llCategory);
                viewCategory = (View) itemView.findViewById(R.id.viewCategory);
                rlImgHolder = (RelativeLayout) itemView.findViewById(R.id.rlImgHolder);
                pbImgHolder = (ProgressBar) itemView.findViewById(R.id.pbImgHolder);

                txtCategory.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                txtCategory.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));

            }
        }

    }
    //endregion

    @Override
    protected void onResume() {
        super.onResume();

        GetCartCount();
        API();
        SetUserInfo();

        if (SharedPreference.GetPreference(this, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
            llLogout.setVisibility(View.VISIBLE);
            llLogIn.setVisibility(View.GONE);
        } else {
            llLogout.setVisibility(View.GONE);
            llLogIn.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void gotoChangeAddressCheckout(String AddressType, String AddressTypeName,
                                          String MethodCallFrom) {
        /*bundle = new Bundle();
        AddressListFragment addressListFragment = new AddressListFragment();
        bundle.putString("ActivityType", "MyAddressActivity");
        bundle.putString("AddressType", AddressType);
        bundle.putString("AddressTypeName", AddressTypeName);
        movetofragment(bundle, addressListFragment);*/
        getAddressList(AddressType, "CheckoutFragment", AddressTypeName, MethodCallFrom);
    }

    @Override
    public void gotoChangeAddressCheckout1(String AddressType, String AddressTypeName,
                                           String MethodCallFrom, String strShippingUserName,
                                           String strShippingAddress, String strShippingPhoneNo) {
        strShippinguserName = strShippingUserName;
        strShippingaddress = strShippingAddress;
        strShippingphoneno = strShippingPhoneNo;
        getAddressList(AddressType, "CheckoutFragment", AddressTypeName, MethodCallFrom);
    }


    @Override
    public void gotoSubmitFeedback(String product_id, String orderitem_id) {
        /*bundle = new Bundle();
        bundle.putString("orderitem_id", orderitem_id);
        bundle.putString("product_id", product_id);
        SubmitFeedbackFragment submitFeedbackFragment = new SubmitFeedbackFragment();
        movetofragment(bundle, submitFeedbackFragment);*/

        Intent intentSubmitFeedback = new Intent(context, SubmitFeedbackActivity.class);
        intentSubmitFeedback.putExtra("orderitem_id", orderitem_id);
        intentSubmitFeedback.putExtra("product_id", product_id);
        startActivity(intentSubmitFeedback);
    }

    @Override
    public void gotoCheckoutCart() {
        CheckoutFragment checkoutFragment = new CheckoutFragment();
        movetofragment(bundle, checkoutFragment);
    }

    @Override
    public void gotoReviews() {
        bundle = new Bundle();
       /* ReviewsFragment reviewsFragment = new ReviewsFragment();
        movetofragment(bundle, reviewsFragment);*/
        Intent intentReviews = new Intent(context, ReviewsActivity.class);
        startActivity(intentReviews);
    }

    @Override
    public void gotoOrderHistory() {
        /*OrderHistoryFragment orderHistoryFragment = new OrderHistoryFragment();
        movetofragment(bundle, orderHistoryFragment);*/
        Intent intentOrderHistory = new Intent(context, OrderHistoryActivity.class);
        intentOrderHistory.putExtra("Redirect", "MyAccount");
        startActivity(intentOrderHistory);
    }

    @Override
    public void gotoUserProfile() {
        /*ProfileFragment profileFragment = new ProfileFragment();
        movetofragment(bundle, profileFragment);*/
        Intent intentProfile = new Intent(context, ProfileActivity.class);
        startActivity(intentProfile);
    }

    @Override
    public void gotoChangepwd() {
        /*ChangepasswordFragment changepasswordFragment = new ChangepasswordFragment();
        movetofragment(bundle, changepasswordFragment);*/
        Intent intentChangepassword = new Intent(context, ChangepasswordActivity.class);
        startActivity(intentChangepassword);
    }

    @Override
    public void gotoMyAccountAddressListing() {
        getAddressList("", "MyAccountAddressListingActivity", "", "");
    }

    @Override
    public void gotoOrderSuccess(String payment_method, String urlStatus) {
        bundle = new Bundle();
        PlaceOrderSuccessFragment placeOrderSuccessFragment = new PlaceOrderSuccessFragment();
        bundle.putString("payment_method", payment_method);
        bundle.putString("urlStatus", urlStatus);
        movetofragment(bundle, placeOrderSuccessFragment);
    }

    @Override
    public void gotoPayment(String orderid, String amount, String Fanme, String phoneNo, String mMerchantKey, String mSalt, String Payumoneysuccessurl, String payumoneyfailureurl) {
        bundle = new Bundle();
        PaymentFragment paymentFragment = new PaymentFragment();
        bundle.putString("order_id", orderid);
        bundle.putString("amount", amount);
        bundle.putString("firstname", Fanme);
        bundle.putString("phone", phoneNo);
        bundle.putString("mMerchantKey", mMerchantKey);
        bundle.putString("mSalt", mSalt);
        bundle.putString("Payumoneysuccessurl", Payumoneysuccessurl);
        bundle.putString("payumoneyfailureurl", payumoneyfailureurl);
        movetofragment(bundle, paymentFragment);
    }

    @Override
    public void gotoCCAvence(Bundle bundle) {
        CCAvenueFragment ccAvenueFragment = new CCAvenueFragment();
        movetofragment(bundle, ccAvenueFragment);
    }

    @Override
    public void gotoProductListing(String category_slug, String CatName) {
        ProductListFragment productListFragment = new ProductListFragment();
        bundle.putString("category_slug", category_slug);
        bundle.putString("catName", CatName);
        movetofragment(bundle, productListFragment);
    }

    @Override
    public void GotoOrderDetail(String slug) {
        bundle = new Bundle();
        OrderDetailFragment orderDetailFragment = new OrderDetailFragment();
        bundle.putString("Order_Id", slug);
        movetofragment(bundle, orderDetailFragment);
    }

    @Override
    public void onAddToCartProduct() {
        qty = txtCardCount.getText().toString();
        if (qty.equals("")) {
            qty = "0";
        }
        qty = String.valueOf(Integer.parseInt(qty) + 1);
        txtCardCount.setVisibility(View.VISIBLE);
        txtCardCount.setText(qty);
    }

    @Override
    public void FilterClick(Boolean values) {
        if (values) {
            cardviewBottomNavigation.setVisibility(View.GONE);
            toolbar.setVisibility(View.GONE);
        } else {
            cardviewBottomNavigation.setVisibility(View.VISIBLE);
            toolbar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void OnProductClick(String slug, String Productname, String product_id) {
        ProductDetailFragment productDetailFragment = new ProductDetailFragment();
        bundle.putString("slug", slug);
        bundle.putString("name", Productname);
        bundle.putString("ProductID", product_id);
        movetofragment(bundle, productDetailFragment);
    }

    @Override
    public void GoToCartList() {
        CartFragment cartFragment = new CartFragment();
        movetofragment(bundle, cartFragment);
    }

    @Override
    public void WishlistCount() {
        wishlistcount = txtWishlistCount.getText().toString();
        if (wishlistcount.equals("")) {
            wishlistcount = "0";
        }
        wishlistcount = String.valueOf(Integer.parseInt(wishlistcount) + 1);
        txtWishlistCount.setVisibility(View.VISIBLE);
        txtWishlistCount.setText(wishlistcount);
        SharedPreference.CreatePreference(context, Global.WishlistCountPreference);
        SharedPreference.SavePreference(StaticUtility.WishlistCount, wishlistcount);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.length() >= 1) {
            getSearch(String.valueOf(s));
        } else {
            Global.HideSystemKeyboard(this);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public void wishlistcount() {
        wishlistcount = txtWishlistCount.getText().toString();
        if (wishlistcount.equals("")) {
            wishlistcount = "0";
        }
        wishlistcount = String.valueOf(Integer.parseInt(wishlistcount) + 1);
        txtWishlistCount.setVisibility(View.VISIBLE);
        txtWishlistCount.setText(wishlistcount);
        SharedPreference.CreatePreference(context, Global.WishlistCountPreference);
        SharedPreference.SavePreference(StaticUtility.WishlistCount, wishlistcount);
    }

    @Override
    public void wishlistcountRemove() {
        wishlistcount = txtWishlistCount.getText().toString();
        wishlistcount = String.valueOf(Integer.parseInt(wishlistcount) - 1);
        if (!wishlistcount.equalsIgnoreCase("0")) {
            txtWishlistCount.setVisibility(View.VISIBLE);
            txtWishlistCount.setText(wishlistcount);
        } else {
            txtWishlistCount.setVisibility(View.GONE);
            txtWishlistCount.setText("");
            /*SharedPreference.CreatePreference(context, Global.WishlistCountPreference);
            SharedPreference.SavePreference(StaticUtility.WishlistCount, wishlistcount);*/
        }
    }

    //region FOR AddToWishList API..
    private void AddToWishList(String product_id, final ImageView imageView, final int position,
                               final JSONArray jsonArray, final RecyclerView recyclerView) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"product_id"};
        String[] val = {product_id};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.AddToWishlist);
        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers1(context));
        } else {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers2(context));
        }
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                imageView.setImageResource(R.drawable.ic_select_heart);
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                wishlistcount();
                                JSONObject objStatus = jsonArray.optJSONObject(position);
                                String exists_in_wishlist = objStatus.getString("exists_in_wishlist");
                                objStatus.remove("exists_in_wishlist");
                                objStatus.put("exists_in_wishlist", "1");
                                recyclerView.getAdapter().notifyDataSetChanged();
                                recyclerView.getAdapter().notifyItemChanged(position);

                                /*getBestSeller();
                                getWantedProduct();
                                getNewArrivals();*/
                              /*  if (type.equalsIgnoreCase("bestseller")) {
                                    getBestSeller();
                                } else if (type.equalsIgnoreCase("mostwanted")) {
                                    getWantedProduct();
                                } else if (type.equalsIgnoreCase("newArrivals")) {
                                    getNewArrivals();
                                }*/

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(context, "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    startActivity(intent);
                                    /*finish();*/
                                } else if (strMessage.equals("Product already exists in Wishlist!")) {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                    imageView.setClickable(false);
                                } else {
                                    imageView.setClickable(true);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region For EventBus onEvent
    public void onEvent(String event) {
        openInternetAlertDialog(context, event);
    }
    //endregion

    //region FOR SHOW INTERNET CONNECTION DIALOG...
    public void openInternetAlertDialog(final Context mContext, String alertString) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.row_alert_dialog, null);
        final TextView tvAlertText = (TextView) row.findViewById(R.id.tvAlertText);
        final TextView tvTitle = (TextView) row.findViewById(R.id.tvTitle);
        final Button btnSettings = (Button) row.findViewById(R.id.btnSettings);
        final Button btnExit = (Button) row.findViewById(R.id.btnExit);

        tvTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        tvAlertText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        btnSettings.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        btnExit.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        btnExit.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        btnSettings.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        tvTitle.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

        tvAlertText.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        tvTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnSettings.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnExit.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

        try {
            if (alertString.equals("Not connected to Internet")) {
                if (i == 0) {
                    i = 1;
                    AlertDialog.Builder i_builder = new AlertDialog.Builder(mContext);
                    internetAlert = i_builder.create();
                    internetAlert.setCancelable(false);
                    internetAlert.setView(row);

                    if (internetAlert.isShowing()) {
                        internetAlert.dismiss();
                    } else {
                        internetAlert.show();
                    }

                    btnExit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            internetAlert.dismiss();
                            //FOR CLOSE APP...
                            System.exit(0);
                        }
                    });

                    btnSettings.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            /*internetAlert.dismiss();*/
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        }
                    });
                } else {
                    /*internetAlert.dismiss();*/
                }
            } else {
                i = 0;
                internetAlert.dismiss();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region ON ACTIVITY RESULT FOR DISMISS OR SHOW INTERNET ALERT DIALOG...
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (!Global.isNetworkAvailable(context)) {
                openInternetAlertDialog(context, "Not connected to Internet");
            } else {
                internetAlert.dismiss();
            }
        }
    }
    //endregion

    //region Refresh
    public void API() {
        if (is_bestselller.equals("1")) {
            getBestSeller();
        } else {
            llBestSeller.setVisibility(View.GONE);
        }
        if (is_mostwanted.equals("1")) {
            getWantedProduct();
        } else {
            llMostWanted.setVisibility(View.GONE);
        }
        if (is_arrivals.equals("1")) {
            getNewArrivals();
        } else {
            llArrivals.setVisibility(View.GONE);
        }
    }
    //endregion

    //region FOR getCMSListing API..
    private void getCMSListing() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetCMSListing);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    ArrayList<CMSPages> cmsPages = new ArrayList<>();
                                    JSONArray jsonArray = response.getJSONArray("payload");
                                    cmsPages.add(new CMSPages("Home", "home", "1"));
                                    cmsPages.add(new CMSPages("Feedback", "feedback", "0"));
                                    cmsPages.add(new CMSPages("Contact Us", "contact-us", "0"));
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.optJSONObject(i);
                                        String strTitle = jsonObject.optString("title");
                                        String strSlug = jsonObject.optString("slug");
                                        cmsPages.add(new CMSPages(strTitle, strSlug, "0"));
                                    }
                                    if (cmsPages.size() > 0) {
                                        recyclerviewCMS.setVisibility(View.VISIBLE);
                                        AdapterCMSList adapterCMSList = new AdapterCMSList(context, cmsPages);
                                        recyclerviewCMS.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                                        recyclerviewCMS.setAdapter(adapterCMSList);
                                    } else {
                                        recyclerviewCMS.setVisibility(View.GONE);

                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region ADAPTER
    public class AdapterCMSList extends RecyclerView.Adapter<AdapterCMSList.Viewholder> {

        Context context;
        JSONArray jsonArray;
        ArrayList<CMSPages> cmsPages = new ArrayList<>();
        int selectedPosition = -1;

        public AdapterCMSList(Context context, JSONArray jsonArray) {
            this.context = context;
            this.jsonArray = jsonArray;
        }

        public AdapterCMSList(Context context, ArrayList<CMSPages> cmsPages) {
            this.context = context;
            this.cmsPages = cmsPages;
        }

        @Override
        public AdapterCMSList.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_cms_list, viewGroup, false);
            return new AdapterCMSList.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder viewholder, final int position) {
            final CMSPages cmsPage = cmsPages.get(position);
            viewholder.txtCMS.setText(cmsPage.getTitle());
            if (cmsPage.getIs_Select().equalsIgnoreCase("1")) {
                viewholder.txtCMS.setTextSize(getResources().getDimension(R.dimen.txtmenu_select_textSize) / getResources().getDisplayMetrics().density);
                viewholder.llSelection.setVisibility(View.VISIBLE);
                viewholder.llCMS.setBackgroundColor(getResources().getColor(R.color.selectionColor));
                viewholder.llSelection.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                viewholder.txtCMS.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                viewholder.imgCMS.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
            } else {
                viewholder.llCMS.setBackgroundColor(getResources().getColor(R.color.colorcard));
                viewholder.txtCMS.setTextSize(getResources().getDimension(R.dimen.txtmenu_textSize) / getResources().getDisplayMetrics().density);
                viewholder.llSelection.setVisibility(View.GONE);
                viewholder.txtCMS.setTextColor(Color.parseColor("#919191"));
                viewholder.imgCMS.setColorFilter(Color.parseColor("#919191"));
            }


            viewholder.llCMS.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = 0; i < cmsPages.size(); i++) {
                        CMSPages cmsPage = cmsPages.get(i);
                        cmsPage.setIs_Select("0");
                    }
                    cmsPage.setIs_Select("1");
                    notifyDataSetChanged();
                    if (cmsPage.getSlug().equalsIgnoreCase("home")) {
                        startActivity(new Intent(context, MainActivity.class));
                        finish();
                        isDashBoard = true;
                    } else if (cmsPage.getSlug().equalsIgnoreCase("feedback")) {
                        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
                            userID = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
                            if (!userID.equalsIgnoreCase("")) {
                                clearBackStack();
                                FeedbackFragment feedbackFragment = new FeedbackFragment();
                                movetofragment(bundle, feedbackFragment);
                            } else {
                                getCMSListing();
                                /*Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();*/
                                Intent intent = new Intent(context, LoginActivity.class);
                                SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                SharedPreference.ClearPreference(context, Global.ISCheck);
                                startActivity(intent);
                                /*finish();*/
                            }
                        } else {
                            /*Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();*/
                            getCMSListing();
                            Intent intent = new Intent(context, LoginActivity.class);
                            SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                            SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                            SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                            SharedPreference.ClearPreference(context, Global.Billing_Preference);
                            SharedPreference.ClearPreference(context, Global.ISCheck);
                            startActivity(intent);
                        }
                    } else if (cmsPage.getSlug().equalsIgnoreCase("contact-us")) {
                        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
                            userID = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
                            if (!userID.equalsIgnoreCase("")) {
                                clearBackStack();
                                ContactUsFragment contactUsFragment = new ContactUsFragment();
                                movetofragment(bundle, contactUsFragment);
                            } else {
                                getCMSListing();
                                /*Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();*/
                                Intent intent = new Intent(context, LoginActivity.class);
                                SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                SharedPreference.ClearPreference(context, Global.ISCheck);
                                startActivity(intent);
                                /*finish();*/
                            }
                        } else {
                            /*Toast.makeText(context, "You have to Login!", Toast.LENGTH_SHORT).show();*/
                            getCMSListing();
                            Intent intent = new Intent(context, LoginActivity.class);
                            SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                            SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                            SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                            SharedPreference.ClearPreference(context, Global.Billing_Preference);
                            SharedPreference.ClearPreference(context, Global.ISCheck);
                            startActivity(intent);
                        }
                    } else {
                        clearBackStack();
                        CMSFragment cmsFragment = new CMSFragment();
                        bundle = new Bundle();
                        bundle.putString("slug", cmsPage.getSlug());
                        bundle.putString("title", cmsPage.getTitle());
                        movetofragment(bundle, cmsFragment);
                    }
                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            /*return jsonArray.length();*/
            return cmsPages.size();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            ImageView imgCMS;
            TextView txtCMS;
            LinearLayout llCMS;
            LinearLayout llSelection;

            public Viewholder(View itemView) {
                super(itemView);
                llCMS = (LinearLayout) itemView.findViewById(R.id.llCMS);
                imgCMS = (ImageView) itemView.findViewById(R.id.imgCMS);
                txtCMS = (TextView) itemView.findViewById(R.id.txtCMS);
                llSelection = (LinearLayout) itemView.findViewById(R.id.llSelection);

                txtCMS.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                txtCMS.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));

            }
        }

    }
    //endregion

    //region FOR getSearch API..
    private void getSearch(String search) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"productname", "offset", "limit"};
        String[] val = {search, "0", "20"};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetSearchResult);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                productItems.clear();
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    JSONArray jsonArray = jsonObject.getJSONArray("products");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObjectProduct = jsonArray.getJSONObject(i);
                                        String slug = jsonObjectProduct.getString("slug");
                                        String product_id = jsonObjectProduct.getString("product_id");
                                        String name = jsonObjectProduct.getString("name");

                                        productItem = new ProductItem();
                                        productItem.setProductName(name);
                                        productItem.setProductId(product_id);
                                        productItem.setSlug(slug);
                                        productItems.add(productItem);
                                    }
                                    AutoCompleteAdapter autoCompleteAdapterCity = new AutoCompleteAdapter(context, R.layout.row_product_items, productItems, editSearch);
                                    editSearch.setAdapter(autoCompleteAdapterCity);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region AUTO COMPLETE ADAPTER...
    public class AutoCompleteAdapter extends ArrayAdapter<ProductItem> {
        Context mContext;
        ArrayList<ProductItem> mDepartments;
        ArrayList<ProductItem> mDepartments_All;
        ArrayList<ProductItem> mDepartments_Suggestion;
        int mLayoutResourceId;
        private AutoCompleteTextView autoCompleteTextView;

        public AutoCompleteAdapter(Context context, int resource, ArrayList<ProductItem> departments, AutoCompleteTextView autoCompleteTextView) {
            super(context, resource, departments);
            this.mContext = context;
            this.mLayoutResourceId = resource;
            this.mDepartments = new ArrayList<>(departments);
            this.mDepartments_All = new ArrayList<>(departments);
            this.mDepartments_Suggestion = new ArrayList<>();
            this.autoCompleteTextView = autoCompleteTextView;
        }

        public int getCount() {
            return mDepartments.size();
        }

        public ProductItem getItem(int position) {
            return mDepartments.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                    convertView = inflater.inflate(mLayoutResourceId, parent, false);
                }
                final ProductItem department = getItem(position);
                TextView name = (TextView) convertView.findViewById(R.id.productName);
                TextView id = (TextView) convertView.findViewById(R.id.productId);
                name.setText(department.getProductName());
                name.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                id.setText(department.getProductId());

                autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ProductItem pi = getItem(position);
                        String strProductName = pi.getProductName();
                        String strProductId = pi.getProductId();
                        String strProductSlug = pi.getSlug();
                        hideKeyboard(MainActivity.this);
                        ProductDetailFragment productDetailFragment = new ProductDetailFragment();
                        bundle = new Bundle();
                        bundle.putString("slug", strProductSlug);
                        bundle.putString("name", strProductName);
                        bundle.putString("ProductID", strProductId);
                        movetofragment(bundle, productDetailFragment);
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                public String convertResultToString(Object resultValue) {
                    return ((ProductItem) resultValue).getProductName();
                }

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    if (constraint != null) {
                        mDepartments_Suggestion.clear();
                        for (ProductItem department : mDepartments_All) {
                            if (department.getProductName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                                mDepartments_Suggestion.add(department);
                            }
                        }
                        FilterResults filterResults = new FilterResults();
                        filterResults.values = mDepartments_Suggestion;
                        filterResults.count = mDepartments_Suggestion.size();
                        return filterResults;
                    } else {
                        return new FilterResults();
                    }
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    mDepartments.clear();
                    if (results != null && results.count > 0) {
                        // avoids unchecked cast warning when using mDepartments.addAll((ArrayList<Department>) results.values);
                        List<?> result = (List<?>) results.values;
                        for (Object object : result) {
                            if (object instanceof ProductItem) {
                                mDepartments.add((ProductItem) object);
                            }
                        }
                    } else if (constraint == null) {
                        // no filter, add entire original list back in
                        mDepartments.addAll(mDepartments_All);
                    }
                    notifyDataSetChanged();
                }
            };
        }
    }
    //endregion

    //region Keyboard Hide...
    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    //endregion

    //region FOR getAddressList API...
    public void getAddressList(final String type, final String ActivityType,
                               final String AddressTypeName, final String strMethodCall) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = new String[0];
        String[] val = new String[0];

        if (ActivityType.equalsIgnoreCase("CheckoutFragment")) {
            key = new String[]{"address_type"};
            val = new String[]{type};
        } else if (ActivityType.equalsIgnoreCase("MyAccountAddressListingActivity")) {
            key = new String[]{};
            val = new String[]{};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.AddressList);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONArray jsonArrayPayload = response.getJSONArray("payload");
                                    if (jsonArrayPayload.length() > 0) {
                                        addressLists = new ArrayList<>();
                                        for (int i = 0; i < jsonArrayPayload.length(); i++) {
                                            JSONObject jsonObjectPayload = jsonArrayPayload.getJSONObject(i);
                                            String Fname = jsonObjectPayload.getString("first_name");
                                            String Lname = jsonObjectPayload.getString("last_name");
                                            String address = jsonObjectPayload.getString("address");
                                            String pincode = jsonObjectPayload.getString("pincode");
                                            String city = jsonObjectPayload.getString("city");
                                            String state = jsonObjectPayload.getString("state");
                                            String phone_number = jsonObjectPayload.getString("phone_number");
                                            String useradress_id = jsonObjectPayload.getString("useradress_id");
                                            String landmark = jsonObjectPayload.getString("landmark");
                                            String country = jsonObjectPayload.getString("country");
                                            String address_type = jsonObjectPayload.getString("address_type");
                                            addressLists.add(new AddressList(Fname, Lname, address, city, pincode, state, phone_number, useradress_id, landmark, country, address_type));
                                        }
                                        if (ActivityType.equalsIgnoreCase("CheckoutFragment")) {
                                            Intent intent = new Intent(context, CheckoutAddressListActivity.class);
                                            bundle.putString("ActivityType", ActivityType);
                                            bundle.putString("AddressType", type);
                                            bundle.putString("AddressTypeName", AddressTypeName);
                                            bundle.putSerializable("AddressList", addressLists);
                                            intent.putExtra("BundleAddress", bundle);
                                            startActivity(intent);
                                        } else if (ActivityType.equalsIgnoreCase("MyAccountAddressListingActivity")) {
                                            Intent intent = new Intent(context, MyAccountAddressListActivity.class);
                                            bundle.putString("ActivityType", ActivityType);
                                            bundle.putString("AddressType", type);
                                            bundle.putString("AddressTypeName", AddressTypeName);
                                            bundle.putSerializable("AddressList", addressLists);
                                            intent.putExtra("BundleAddress", bundle);
                                            startActivity(intent);
                                            /*AddressListFragment addressListFragment = new AddressListFragment();
                                            bundle.putString("ActivityType", ActivityType);
                                            bundle.putString("AddressType", type);
                                            bundle.putString("AddressTypeName", AddressTypeName);
                                            bundle.putSerializable("AddressList", addressLists);
                                            movetofragment(bundle, addressListFragment);*/
                                        }
                                    } else {
                                        if (ActivityType.equalsIgnoreCase("CheckoutFragment")) {
                                            if (strMethodCall.equalsIgnoreCase("LastAddress")) {
                                                Intent intent = new Intent(context, CheckoutAddAddressActivity.class);
                                                intent.putExtra("ActivityType", ActivityType);
                                                intent.putExtra("AddressType", "Both");
                                                intent.putExtra("Type", "Checkout");
                                                startActivity(intent);
                                            } else if (strMethodCall.equalsIgnoreCase("ChangeAddress")) {
                                                Intent intent = new Intent(context, CheckoutAddAddressActivity.class);
                                                intent.putExtra("ActivityType", ActivityType);
                                                intent.putExtra("AddressType", AddressTypeName);
                                                intent.putExtra("Type", "Checkout");
                                                intent.putExtra("ShippingUserName", strShippinguserName);
                                                intent.putExtra("ShippingAddress", strShippingaddress);
                                                intent.putExtra("ShippingPhoneNo", strShippingphoneno);
                                                startActivity(intent);
                                            }
                                        } else {
                                            Intent intent = new Intent(context, ADDAddressesActivity.class);
                                            intent.putExtra("ActivityType", ActivityType);
                                            intent.putExtra("Type", "MainActivity");
                                            startActivity(intent);
                                        }
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(context, "You are already Logged in other device !", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    /*finish();*/
                                } else if (strMessage.equals("app-id and app-secret is required")) {
                                    Toast.makeText(context, "You have to Login First!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    /*finish();*/
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in HomeFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region SetUserInfo
    public static void SetUserInfo() {
        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
            userID = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID);
        }
        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USER_Profile_Picture) != null) {
            String strProfile_Image = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USER_Profile_Picture);
            if (!strProfile_Image.equalsIgnoreCase("")) {
                imgUser.setVisibility(View.GONE);
                circularImageViewUser.setVisibility(View.VISIBLE);
                try {
                    picUrl = String.valueOf(strProfile_Image);
                    urla = new URL(picUrl);
                    urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    Picasso.with(context)
                            .load(picUrl)
                            .into(circularImageViewUser, new Callback() {
                                @Override
                                public void onSuccess() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }

                                @Override
                                public void onError() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When parsing image url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
            } else {
                imgUser.setVisibility(View.VISIBLE);
                circularImageViewUser.setVisibility(View.GONE);
            }
        } else {
            imgUser.setVisibility(View.VISIBLE);
            circularImageViewUser.setVisibility(View.GONE);
        }

        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USER_EMAIL) != null) {
            txtUserEmail.setText(SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USER_EMAIL));
        } else {
            txtUserEmail.setText(R.string.guest_email_address);
        }

        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USER_Name) != null) {
            txtUserName.setText(SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USER_Name));
        } else {
            txtUserName.setText(R.string.guest_user);
        }
    }//endregion

    public void clearBackStack() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
            manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

}
