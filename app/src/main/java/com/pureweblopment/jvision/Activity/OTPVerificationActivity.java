package com.pureweblopment.jvision.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.pureweblopment.jvision.Global.Global;
import com.pureweblopment.jvision.Global.SendMail;
import com.pureweblopment.jvision.Global.SharedPreference;
import com.pureweblopment.jvision.Global.StaticUtility;
import com.pureweblopment.jvision.Global.Typefaces;
import com.pureweblopment.jvision.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.InitializationCallback;
import io.fabric.sdk.android.services.settings.AppSettingsData;
import okhttp3.Response;

public class OTPVerificationActivity extends AppCompatActivity implements View.OnClickListener {
    TextView txtViewVerifyLable, txtViewVerifyLable1, txtResendOTP;
    EditText OTPcode1, OTPcode2, OTPcode3, OTPcode4, OTPcode5, OTPcode6;
    Button btnContinue;
    Context context = OTPVerificationActivity.this;
    RelativeLayout relativeProgress;
    String usertonken, email, phone, CountryCode, otp;
    private TextWatcher textWatcher;
    private EventBus eventBus = EventBus.getDefault();
    private int i = 0;
    private AlertDialog internetAlert;
    TextView txtLoginNow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_otpverification);

        eventBus.register(context);

        Initialization();
        TypeFace();
        AppSettings();
        OnClickListener();

        email = getIntent().getStringExtra("userEmail");
        phone = getIntent().getStringExtra("userPhone");
        usertonken = getIntent().getStringExtra("userUserID");
        CountryCode = getIntent().getStringExtra("userCountyCode");

        txtViewVerifyLable1.setText("Please type verification code" + "\n" + "send to " + "(" + "+" + CountryCode + ") " + phone);

        chanageButton(btnContinue);
        chanageEditText(OTPcode1);
        chanageEditText(OTPcode2);
        chanageEditText(OTPcode3);
        chanageEditText(OTPcode4);
        chanageEditText(OTPcode5);
        chanageEditText(OTPcode6);

        //region TEXT WATCHER FOR EDIT_TEXT...
        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    if (OTPcode1.getText().hashCode() == s.hashCode()) {
                        OTPcode2.requestFocus();
                        otp = OTPcode1.getText().toString() + OTPcode2.getText().toString()
                                + OTPcode3.getText().toString() + OTPcode4.getText().toString()
                                + OTPcode5.getText().toString() + OTPcode6.getText().toString();
                        if (otp != null) {
                            if (!otp.isEmpty()) {
                                if (otp.length() == 6) {
                                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    inputMethodManager.hideSoftInputFromWindow(OTPcode6.getWindowToken(), 0);
                                    if (Global.isNetworkAvailable(context)) {
                                        VerifyOPTAPI();
                                    } else {
                                        openInternetAlertDialog(context, "Not connected to Internet");
                                    }
                                }
                            }
                        }
                    } else if (OTPcode2.getText().hashCode() == s.hashCode()) {
                        OTPcode3.requestFocus();
                        otp = OTPcode1.getText().toString() + OTPcode2.getText().toString()
                                + OTPcode3.getText().toString() + OTPcode4.getText().toString()
                                + OTPcode5.getText().toString() + OTPcode6.getText().toString();
                        if (otp != null) {
                            if (!otp.isEmpty()) {
                                if (otp.length() == 6) {
                                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    inputMethodManager.hideSoftInputFromWindow(OTPcode6.getWindowToken(), 0);
                                    if (Global.isNetworkAvailable(context)) {
                                        VerifyOPTAPI();
                                    } else {
                                        openInternetAlertDialog(context, "Not connected to Internet");
                                    }
                                }
                            }
                        }
                    } else if (OTPcode3.getText().hashCode() == s.hashCode()) {
                        OTPcode4.requestFocus();
                        otp = OTPcode1.getText().toString() + OTPcode2.getText().toString()
                                + OTPcode3.getText().toString() + OTPcode4.getText().toString()
                                + OTPcode5.getText().toString() + OTPcode6.getText().toString();
                        if (otp != null) {
                            if (!otp.isEmpty()) {
                                if (otp.length() == 6) {
                                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    inputMethodManager.hideSoftInputFromWindow(OTPcode6.getWindowToken(), 0);
                                    if (Global.isNetworkAvailable(context)) {
                                        VerifyOPTAPI();
                                    } else {
                                        openInternetAlertDialog(context, "Not connected to Internet");
                                    }
                                }
                            }
                        }
                    } else if (OTPcode4.getText().hashCode() == s.hashCode()) {
                        OTPcode5.requestFocus();
                        otp = OTPcode1.getText().toString() + OTPcode2.getText().toString()
                                + OTPcode3.getText().toString() + OTPcode4.getText().toString()
                                + OTPcode5.getText().toString() + OTPcode6.getText().toString();
                        if (otp != null) {
                            if (!otp.isEmpty()) {
                                if (otp.length() == 6) {
                                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    inputMethodManager.hideSoftInputFromWindow(OTPcode6.getWindowToken(), 0);
                                    if (Global.isNetworkAvailable(context)) {
                                        VerifyOPTAPI();
                                    } else {
                                        openInternetAlertDialog(context, "Not connected to Internet");
                                    }
                                }
                            }
                        }
                    } else if (OTPcode5.getText().hashCode() == s.hashCode()) {
                        OTPcode6.requestFocus();
                        otp = OTPcode1.getText().toString() + OTPcode2.getText().toString()
                                + OTPcode3.getText().toString() + OTPcode4.getText().toString()
                                + OTPcode5.getText().toString() + OTPcode6.getText().toString();
                        if (otp != null) {
                            if (!otp.isEmpty()) {
                                if (otp.length() == 6) {
                                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    inputMethodManager.hideSoftInputFromWindow(OTPcode6.getWindowToken(), 0);
                                    if (Global.isNetworkAvailable(context)) {
                                        VerifyOPTAPI();
                                    } else {
                                        openInternetAlertDialog(context, "Not connected to Internet");
                                    }
                                }
                            }
                        }
                    } else if (OTPcode6.getText().hashCode() == s.hashCode()) {
                        String strOtpOne = OTPcode1.getText().toString().trim();
                        String strOtpTwo = OTPcode2.getText().toString().trim();
                        String strOtpThree = OTPcode3.getText().toString().trim();
                        String strOtpFour = OTPcode4.getText().toString().trim();
                        String strOtpFive = OTPcode5.getText().toString().trim();
                        String strOtpSix = OTPcode6.getText().toString().trim();
                        //Toast.makeText(getContext(), strOtpOne + strOtpTwo + strOtpThree + strOtpFour + strOtpFive +strOtpSix, Toast.LENGTH_SHORT).show();
                        otp = strOtpOne + strOtpTwo + strOtpThree + strOtpFour + strOtpFive + strOtpSix;
                        if (otp != null) {
                            if (!otp.isEmpty()) {
                                if (otp.length() == 6) {
                                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    inputMethodManager.hideSoftInputFromWindow(OTPcode6.getWindowToken(), 0);
                                    if (Global.isNetworkAvailable(context)) {
                                        VerifyOPTAPI();
                                    } else {
                                        openInternetAlertDialog(context, "Not connected to Internet");
                                    }
                                }
                            }
                        }

                    }
                } else if (s.length() == 0) {
                    if (OTPcode6.getText().hashCode() == s.hashCode()) {
                        OTPcode5.requestFocus();
                    } else if (OTPcode5.getText().hashCode() == s.hashCode()) {
                        OTPcode4.requestFocus();
                    } else if (OTPcode4.getText().hashCode() == s.hashCode()) {
                        OTPcode3.requestFocus();
                    } else if (OTPcode3.getText().hashCode() == s.hashCode()) {
                        OTPcode2.requestFocus();
                    } else if (OTPcode2.getText().hashCode() == s.hashCode()) {
                        OTPcode1.requestFocus();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        //endregion

        OTPcode1.addTextChangedListener(textWatcher);
        OTPcode2.addTextChangedListener(textWatcher);
        OTPcode3.addTextChangedListener(textWatcher);
        OTPcode4.addTextChangedListener(textWatcher);
        OTPcode5.addTextChangedListener(textWatcher);
        OTPcode6.addTextChangedListener(textWatcher);
    }

    //region Initialization
    private void Initialization() {
        txtViewVerifyLable = (TextView) findViewById(R.id.txtViewVerifyLable);
        txtViewVerifyLable1 = (TextView) findViewById(R.id.txtViewVerifyLable1);
        txtResendOTP = (TextView) findViewById(R.id.txtResendOTP);
        txtLoginNow = (TextView) findViewById(R.id.txtLoginNow);
        OTPcode1 = (EditText) findViewById(R.id.OTPcode1);
        OTPcode2 = (EditText) findViewById(R.id.OTPcode2);
        OTPcode3 = (EditText) findViewById(R.id.OTPcode3);
        OTPcode4 = (EditText) findViewById(R.id.OTPcode4);
        OTPcode5 = (EditText) findViewById(R.id.OTPcode5);
        OTPcode6 = (EditText) findViewById(R.id.OTPcode6);
        btnContinue = (Button) findViewById(R.id.btnContinue);
        relativeProgress = (RelativeLayout) findViewById(R.id.relativeProgress);
    }
    //endregion

    //region TypeFace
    private void TypeFace() {
        btnContinue.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        OTPcode6.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        OTPcode5.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        OTPcode4.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        OTPcode3.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        OTPcode2.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        OTPcode1.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtResendOTP.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtViewVerifyLable1.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtViewVerifyLable.setTypeface(Typefaces.TypefaceCalibri_bold(context));
        txtLoginNow.setTypeface(Typefaces.TypefaceCalibri_bold(context));
    }
    //endregion

    //region AppSettings
    private void AppSettings() {
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            txtViewVerifyLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
            txtViewVerifyLable1.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
            txtResendOTP.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
            txtLoginNow.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
    }
    //endregion

    //region OnClickListener
    private void OnClickListener() {
        btnContinue.setOnClickListener(this);
        txtResendOTP.setOnClickListener(this);
        txtLoginNow.setOnClickListener(this);

    }
    //endregion

    //region chanageButton
    public void chanageButton(Button button) {
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor) != "") {
            button.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        }
//        button.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TEXTCOLOR)));
        button.setBackgroundResource(R.drawable.ic_button);
        GradientDrawable gd = (GradientDrawable) button.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
// gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        /* gd.setStroke(2, Color.parseColor(StaticUtility.BORDERCOLOR));*/
    }
    //endregion

    //region chanageEditText
    public void chanageEditText(EditText editText) {

        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor) != "") {
            editText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        }
        editText.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) editText.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        /*gd.setColor(Color.parseColor("#000000"));*/
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
    }
    //endregion

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnContinue:
                if (!TextUtils.isEmpty(OTPcode1.getText().toString())) {
                    if (!TextUtils.isEmpty(OTPcode2.getText().toString())) {
                        if (!TextUtils.isEmpty(OTPcode3.getText().toString())) {
                            if (!TextUtils.isEmpty(OTPcode4.getText().toString())) {
                                if (!TextUtils.isEmpty(OTPcode5.getText().toString())) {
                                    if (!TextUtils.isEmpty(OTPcode6.getText().toString())) {
                                        VerifyOPTAPI();
                                    } else {
                                        Toast.makeText(context, "Please enter OTP!", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(context, "Please enter OTP!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(context, "Please enter OTP!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, "Please enter OTP!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "Please enter OTP!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Please enter OTP!", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.txtResendOTP:
                OTPcode1.setText("");
                OTPcode2.setText("");
                OTPcode3.setText("");
                OTPcode4.setText("");
                OTPcode5.setText("");
                OTPcode6.setText("");
                sendOPTAPI();
                break;
            case R.id.txtLoginNow:
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    //region FOR Verify OPT API...
    private void VerifyOPTAPI() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"otp", "otp_token"};
        String[] val = {otp, usertonken};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.verifyOTP);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    Toast.makeText(context, "User Register Successfully..!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.putExtra("userEmail", email);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);

                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in OPTActivity.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
//                Toast.makeText(context, anError.toString(), Toast.LENGTH_SHORT).show();
            }

        });
    }
    //endregion

    //region FOR Send API...
    private void sendOPTAPI() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"user_token"};
        String[] val = {usertonken};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.ResendOTP);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in OPTActivity.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
//                Toast.makeText(context, anError.toString(), Toast.LENGTH_SHORT).show();
            }

        });
    }
    //endregion

    //region For EventBus onEvent
    public void onEvent(String event) {
        openInternetAlertDialog(context, event);
    }
    //endregion

    //region FOR SHOW INTERNET CONNECTION DIALOG...
    public void openInternetAlertDialog(final Context mContext, String alertString) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.row_alert_dialog, null);
        final TextView tvAlertText = (TextView) row.findViewById(R.id.tvAlertText);
        final TextView tvTitle = (TextView) row.findViewById(R.id.tvTitle);
        final Button btnSettings = (Button) row.findViewById(R.id.btnSettings);
        final Button btnExit = (Button) row.findViewById(R.id.btnExit);

        tvTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        tvAlertText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        btnSettings.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        btnExit.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        btnExit.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        btnSettings.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        tvTitle.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));


        tvAlertText.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        tvTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnSettings.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnExit.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

        try {
            if (alertString.equals("Not connected to Internet")) {
                if (i == 0) {
                    i = 1;
                    AlertDialog.Builder i_builder = new AlertDialog.Builder(mContext);
                    internetAlert = i_builder.create();
                    internetAlert.setCancelable(false);
                    internetAlert.setView(row);

                    if (internetAlert.isShowing()) {
                        internetAlert.dismiss();
                    } else {
                        internetAlert.show();
                    }

                    btnExit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            internetAlert.dismiss();
                            //FOR CLOSE APP...
                            System.exit(0);
                        }
                    });

                    btnSettings.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            /*internetAlert.dismiss();*/
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        }
                    });
                } else {
                    /*internetAlert.dismiss();*/
                }
            } else {
                i = 0;
                internetAlert.dismiss();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region ON ACTIVITY RESULT FOR DISMISS OR SHOW INTERNET ALERT DIALOG...
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (!Global.isNetworkAvailable(context)) {
                openInternetAlertDialog(context, "Not connected to Internet");
            } else {
                internetAlert.dismiss();
            }
        }
    }
    //endregion
}
