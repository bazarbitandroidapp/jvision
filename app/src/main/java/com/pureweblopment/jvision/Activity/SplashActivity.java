package com.pureweblopment.jvision.Activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.pureweblopment.jvision.Global.Global;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Executors;

import com.pureweblopment.jvision.Global.SendMail;
import com.pureweblopment.jvision.Global.SharedPreference;
import com.pureweblopment.jvision.Global.StaticUtility;

import com.pureweblopment.jvision.R;

import io.fabric.sdk.android.Fabric;
import okhttp3.Response;

public class SplashActivity extends AppCompatActivity {

    Context context = SplashActivity.this;
    CoordinatorLayout coodinator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_splash);

        coodinator = (CoordinatorLayout) findViewById(R.id.coodinator);
        if (SharedPreference.GetPreference(this, Global.preferenceNameGuestUSer,
                Global.SessionId) == null) {
            SharedPreference.CreatePreference(this, Global.preferenceNameGuestUSer);
            SharedPreference.SavePreference(Global.SessionId, Global.SessionId());
        }
        Global.DeviceInfo(context);
        if (Global.isNetworkAvailable(context)) {
            AppSetting();
        } else {
            Global.Toast(context, "Kindly check your internet connection...!");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    System.exit(0);
                }
            }, 5000);
        }

        //region If the device is having android oreo we will create a notification channel
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            String channelId = "fcm_default_channel";
            CharSequence channelName = "News";
            int importance = NotificationManager.IMPORTANCE_LOW;
            @SuppressLint("WrongConstant") NotificationChannel notificationChannel =
                    new NotificationChannel(channelId, channelName, importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(notificationChannel);
        }//endregion
    }

    //region FOR APP setting API...
    private void AppSetting() {
        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(
                StaticUtility.URL + StaticUtility.AppSetting + Global.queryStringUrl(context));
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                ApplicationSettings();
                FeatureSetting();
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");

                                if (strStatus.equals("ok")) {
                                    String is_live = "", force_update_title = "", force_update_msg = "",
                                            app_type = "", app_version = "";
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    if (jsonObject.has("app_type")) {
                                        app_type = jsonObject.getString("app_type");
                                    }
                                    if (jsonObject.has("app_version")) {
                                        app_version = jsonObject.getString("app_version");
                                    }
                                    if (jsonObject.has("app_setting")) {
                                        JSONObject jsonObjectAppSetting = jsonObject.getJSONObject("app_setting");
                                        if (jsonObjectAppSetting.has("is_live")) {
                                            is_live = jsonObjectAppSetting.getString("is_live");
                                        }
                                        if (jsonObjectAppSetting.has("force_update_title")) {
                                            force_update_title = jsonObjectAppSetting.getString("force_update_title");
                                        }
                                        if (jsonObjectAppSetting.has("force_update_msg"))
                                            force_update_msg = jsonObjectAppSetting.getString("force_update_msg");
                                    }

                                    if (is_live.equalsIgnoreCase("1")) {
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                                                startActivity(i);
                                                finish();
                                            }
                                        }, 3000);

                                    } else {
                                        new AlertDialog.Builder(context, R.style.MyDialogTheme)
                                                .setTitle(force_update_msg)
                                                .setMessage(force_update_title)
                                                .setCancelable(false)
                                                .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        // continue with delete
                                                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                                        try {
//                                                            Toast.makeText(getApplicationContext(), "App is in BETA version cannot update", Toast.LENGTH_SHORT).show();
                                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                                        } catch (ActivityNotFoundException anfe) {
                                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                                        }
                                                    }
                                                })
                                                .setIcon(android.R.drawable.ic_dialog_alert)
                                                .show();
                                    }

                                }
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                ApplicationSettings();
                FeatureSetting();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strMessage.equalsIgnoreCase("app-id and app-secret is required")) {
//                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                    Global.showSnackBar(coodinator, "Application is not active !");
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            System.exit(0);
                                        }
                                    }, 5000);

                                }
//                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in LoginActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR Application setting API...
    private void ApplicationSettings() {
        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(
                StaticUtility.URL + StaticUtility.ApplicationSettings);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");

                                if (strStatus.equals("ok")) {
                                    String strLogo = "", strGoogle = "", strFacebook = "",
                                            strInstagram = "", strLinkedinimage = "", strPinterestimage = "",
                                            strapplicationLogo = "", is_live = "", force_update_title = "", force_update_msg = "", CurrencyName = "", CurrencySign = "",
                                            CurrencyPosition = "", CurrencyCountry = "", CurrencyThousandSign = "",
                                            CurrencyDecimalSign = "", CurrencyDecimalPlace = "",
                                            app_type = "", app_version = "", strIsMegaMenuActivited = "";
//                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    if (jsonObject.has("logoimage")) {
                                        JSONArray Logo = jsonObject.getJSONArray("logoimage");
                                        strLogo = Logo.getString(0);
                                    }
                                    if (jsonObject.has("googleimage")) {
                                        JSONArray googleimage = jsonObject.getJSONArray("googleimage");
                                        strGoogle = googleimage.getString(0);
                                    }
                                    if (jsonObject.has("facebookimage")) {
                                        JSONArray facebookimage = jsonObject.getJSONArray("facebookimage");
                                        strFacebook = facebookimage.getString(0);
                                    }
                                    if (jsonObject.has("instagramimage")) {
                                        JSONArray instagramimage = jsonObject.getJSONArray("instagramimage");
                                        strInstagram = instagramimage.getString(0);
                                    }
                                    if (jsonObject.has("linkedinimage")) {
                                        JSONArray linkedinimage = jsonObject.getJSONArray("linkedinimage");
                                        strLinkedinimage = linkedinimage.getString(0);
                                    }
                                    if (jsonObject.has("pinterestimage")) {
                                        JSONArray pinterestimage = jsonObject.getJSONArray("pinterestimage");
                                        strPinterestimage = pinterestimage.getString(0);
                                    }
                                    if (jsonObject.has("applicationimage")) {
                                        JSONArray applicationimage = jsonObject.getJSONArray("applicationimage");
                                        strapplicationLogo = applicationimage.getString(0);
                                    }
                                    String strthemeprimarycolor = jsonObject.getString("themeprimarycolor");
                                    if (!strthemeprimarycolor.equals("")) {
                                        strthemeprimarycolor = "#" + strthemeprimarycolor;
                                    }
                                    String strthemedarkcolor = jsonObject.getString("themeprimarydarkcolor");
                                    if (!strthemedarkcolor.equals("")) {
                                        strthemedarkcolor = "#" + strthemedarkcolor;
                                    }
                                    String strtextcolor = jsonObject.getString("textprimarycolor");
                                    if (!strtextcolor.equals("")) {
                                        strtextcolor = "#" + strtextcolor;
                                    }
                                    String strtextlightcolor = jsonObject.getString("textlightcolor");
                                    if (!strtextlightcolor.equals("")) {
                                        strtextlightcolor = "#" + strtextlightcolor;
                                    }
                                    String strbtntextcolor = jsonObject.getString("buttontextcolor");
                                    if (!strbtntextcolor.equals("")) {
                                        strbtntextcolor = "#" + strbtntextcolor;
                                    }
                                    String is_slider_active = jsonObject.getString("is_slider_active");
                                    String is_cart_active = jsonObject.getString("is_cart_active");
                                    String is_order_active = jsonObject.getString("is_order_active");
                                    String is_wishlist_active = jsonObject.getString("is_wishlist_active");
                                    String is_arrival_active = jsonObject.getString("is_arrival_active");
                                    String is_seller_active = jsonObject.getString("is_seller_active");
                                    String is_mostwanted_active = jsonObject.getString("is_mostwanted_active");


                                    if (jsonObject.has("currencydata")) {
                                        JSONObject jsonObjectCurrency = jsonObject.getJSONObject("currencydata");
                                        CurrencyName = jsonObjectCurrency.getString("name");
                                        CurrencySign = jsonObjectCurrency.getString("sign");
                                        CurrencyPosition = jsonObjectCurrency.getString("sign_position");
                                        CurrencyCountry = jsonObjectCurrency.getString("country");
                                        CurrencyThousandSign = jsonObjectCurrency.getString("thousand_sign");
                                        CurrencyDecimalSign = jsonObjectCurrency.getString("decimal_sign");
                                        CurrencyDecimalPlace = jsonObjectCurrency.getString("decimal_place");
                                    }

                                    if (jsonObject.has("is_megamenu_active")) {
                                        strIsMegaMenuActivited = jsonObject.getString("is_megamenu_active");
                                    }


                                    SharedPreference.CreatePreference(context, Global.APPSetting_PREFERENCE);
                                    SharedPreference.SavePreference(StaticUtility.IMG_LOGO, strLogo);
                                    SharedPreference.SavePreference(StaticUtility.IMG_Facebook, strFacebook);
                                    SharedPreference.SavePreference(StaticUtility.IMG_google, strGoogle);
                                    SharedPreference.SavePreference(StaticUtility.IMG_insta, strInstagram);
                                    SharedPreference.SavePreference(StaticUtility.IMG_Linkedin, strLinkedinimage);
                                    SharedPreference.SavePreference(StaticUtility.IMG_pinterest, strPinterestimage);
                                    SharedPreference.SavePreference(StaticUtility.ThemePrimaryColor, strthemeprimarycolor);
                                    SharedPreference.SavePreference(StaticUtility.ThemePrimaryDarkColor, strthemedarkcolor);
                                    SharedPreference.SavePreference(StaticUtility.TextColor, strtextcolor);
                                    SharedPreference.SavePreference(StaticUtility.TextLightColor, strtextlightcolor);
                                    SharedPreference.SavePreference(StaticUtility.ButtonTextColor, strbtntextcolor);
                                    SharedPreference.SavePreference(StaticUtility.APP_LOGO, strapplicationLogo);
                                    SharedPreference.SavePreference(StaticUtility.Is_Slider_Active, is_slider_active);
                                    SharedPreference.SavePreference(StaticUtility.Is_Cart_Active, is_cart_active);
                                    SharedPreference.SavePreference(StaticUtility.Is_Order_Active, is_order_active);
                                    SharedPreference.SavePreference(StaticUtility.Is_Wishlist_Active, is_wishlist_active);
                                    SharedPreference.SavePreference(StaticUtility.Is_Arrival_Active, is_arrival_active);
                                    SharedPreference.SavePreference(StaticUtility.Is_Seller_Active, is_seller_active);
                                    SharedPreference.SavePreference(StaticUtility.Is_Mostwanted_Active, is_mostwanted_active);
                                    SharedPreference.SavePreference(StaticUtility.CurrencyName, CurrencyName);
                                    SharedPreference.SavePreference(StaticUtility.CurrencySign, CurrencySign);
                                    SharedPreference.SavePreference(StaticUtility.CurrencySignPosition, CurrencyPosition);
                                    SharedPreference.SavePreference(StaticUtility.Is_MEGAMENU_Active, strIsMegaMenuActivited);
                                    /*if (jsonObject.has("is_live")) {
                                        is_live = jsonObject.getString("is_live");
                                    }

                                    if (jsonObject.has("is_force_update")) {
                                        JSONObject jsonObjectForceUpdate = jsonObject.getJSONObject("is_force_update");
                                        force_update_title = jsonObjectForceUpdate.getString("force_update_title");
                                        force_update_msg = jsonObjectForceUpdate.getString("force_update_msg");
                                    }
                                    if (jsonObject.has("app_type")) {
                                        app_type = jsonObject.getString("app_type");
                                    }
                                    if (jsonObject.has("app_version")) {
                                        app_version = jsonObject.getString("app_version");
                                    }
                                    if (is_live.equalsIgnoreCase("1")) {
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                                                startActivity(i);
                                                finish();
                                            }
                                        }, 3000);
                                    } else {
                                        new AlertDialog.Builder(context, R.style.MyDialogTheme)
                                                .setTitle(force_update_msg)
                                                .setMessage(force_update_title)
                                                .setCancelable(false)
                                                .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        // continue with delete
                                                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                                        try {
//                                                            Toast.makeText(getApplicationContext(), "App is in BETA version cannot update", Toast.LENGTH_SHORT).show();
                                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                                        } catch (ActivityNotFoundException anfe) {
                                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                                        }
                                                    }
                                                })
                                                .setIcon(android.R.drawable.ic_dialog_alert)
                                                .show();
                                    }*/

                                }
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strMessage.equalsIgnoreCase("app-id and app-secret is required")) {
//                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                    Global.showSnackBar(coodinator, "Application is not active !");
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            System.exit(0);
                                        }
                                    }, 5000);

                                }
//                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in LoginActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    // region FOR Feature setting API...
    private void FeatureSetting() {
        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder =
                AndroidNetworking.post(StaticUtility.URL + StaticUtility.FeatureSetting);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            String strStatus = response.optString("status");
                            String strMessage = response.optString("message");
                            if (strStatus.equals("ok")) {
                                if (response.has("payload")) {
                                    JSONArray jsonArray = response.optJSONArray("payload");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.optJSONObject(i);
                                        String slug = jsonObject.optString("slug");
                                        if (slug.equalsIgnoreCase("social-media-logins")) {
                                            String strSoicalMediaLogin = jsonObject.optString("feature_textval");
                                            SharedPreference.CreatePreference(context, Global.APPSetting_PREFERENCE);
                                            SharedPreference.SavePreference(StaticUtility.Feature_Social_Media, strSoicalMediaLogin);
                                        }
                                    }
                                }
//                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strMessage.equalsIgnoreCase("app-id and app-secret is required")) {
//                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                    Global.showSnackBar(coodinator, "Application is not active !");
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            System.exit(0);
                                        }
                                    }, 5000);

                                }
//                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in LoginActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion
}
