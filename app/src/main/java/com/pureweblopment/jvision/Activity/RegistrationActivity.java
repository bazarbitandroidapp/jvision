package com.pureweblopment.jvision.Activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.pureweblopment.jvision.Global.Global;
import com.pureweblopment.jvision.Global.Typefaces;
import com.pureweblopment.jvision.Linkedin.APIHelper;
import com.pureweblopment.jvision.Linkedin.LISessionManager;
import com.pureweblopment.jvision.Linkedin.errors.LIApiError;
import com.pureweblopment.jvision.Linkedin.errors.LIAuthError;
import com.pureweblopment.jvision.Linkedin.listeners.ApiListener;
import com.pureweblopment.jvision.Linkedin.listeners.ApiResponse;
import com.pureweblopment.jvision.Linkedin.listeners.AuthListener;
import com.pureweblopment.jvision.Linkedin.utils.Scope;
import com.pureweblopment.jvision.Pdk.PDKCallback;
import com.pureweblopment.jvision.Pdk.PDKClient;
import com.pureweblopment.jvision.Pdk.PDKException;
import com.pureweblopment.jvision.Pdk.PDKResponse;
import com.pureweblopment.jvision.Pdk.PDKUser;
import com.pureweblopment.jvision.Pdk.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;

import com.pureweblopment.jvision.Global.SendMail;
import com.pureweblopment.jvision.Global.SharedPreference;
import com.pureweblopment.jvision.Global.StaticUtility;
import com.pureweblopment.jvision.Instagram.InstagramApp;
import com.pureweblopment.jvision.Model.SpinnerItem;
import com.pureweblopment.jvision.R;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;
import okhttp3.Response;

public class RegistrationActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {
    EditText editFirstName, editLastName, editEmail, editpassword, editConfirmPwd, editPhoneNo;
    ImageView imageviewFB, imageviewInsta, imageviewGmail, imageviewLinkedin, imageviewPinterest;
    Button btnRegistration;
    TextView txtLoginNow;
    private static String strCountryCode = null;
    ArrayList<SpinnerItem> countries;
    String countryName = null, countryCode = null;


    private String first_name = "", last_name = "", user_email = "", socialId = "";
    private String username, userEmail, social_type, user_insta_fullname, user_fb_email;

    private boolean isReceiverRegistered;
    private String TAG;
    private String TAGS;

    private SharedPreferences sharedPreferences;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    boolean sentToken;
    private CallbackManager callbackManager;
    LoginButton login_button;
    EditText editUserEmail;

    ImageView imageLogo;
    String strLOGO = "", strFB = "", strGmail = "", strinsta = "";
    String picUrl = null;

    private InstagramApp mApp;
    private HashMap<String, String> userInfoHashmap = new HashMap<String, String>();

    RelativeLayout relativeProgress;
    CoordinatorLayout coordinator;

    Context context = RegistrationActivity.this;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 100;

    AutoCompleteTextView actCountryCode;

    private EventBus eventBus = EventBus.getDefault();

    FrameLayout flCountryCode;

    private int i = 0;
    private AlertDialog internetAlert;

    PDKClient pdkClient;
    PDKUser user;
    private static boolean DEBUG = true;
    private final String USER_FIELDS = "id,image,counts,created_at,first_name,last_name,bio";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_registration);

        eventBus.register(context);

        pdkClient = PDKClient.configureInstance(this, StaticUtility.PinterestApp_ID);
        pdkClient.onConnect(this);

        ProgressBar progress = (ProgressBar) findViewById(R.id.progress);
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
            }
        }


        mApp = new InstagramApp(this, StaticUtility.CLIENT_ID,
                StaticUtility.CLIENT_SECRET, StaticUtility.CALLBACK_URL);
        mApp.setListener(new InstagramApp.OAuthAuthenticationListener() {
            @Override
            public void onSuccess() {
                mApp.fetchUserName(handler);

            }

            @Override
            public void onFail(String error) {
                Toast.makeText(RegistrationActivity.this, error, Toast.LENGTH_SHORT).show();
            }
        });

        Initialization();
        Typeface();
        ClickListener();

        actCountryCode.requestFocus();

        coordinator.setBackgroundColor(Color.parseColor("#FFFFFF"));

        //region Set App Logo
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.IMG_LOGO) != "") {
            strLOGO = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.IMG_LOGO);
            //region ImageLOGO
            try {
                URL urla = null;
            /*picUrl = picUrl.replace("[", "");
            picUrl = picUrl.replace("]", "").replace("\"", "");*/
                urla = new URL(strLOGO);
                URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                picUrl = String.valueOf(urin.toURL());
                // Capture position and set to the ImageView
                Picasso.with(context)
                        .load(picUrl)
                        .into(imageLogo, new Callback() {
                            @Override
                            public void onSuccess() {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onError() {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }
                        });
            } catch (MalformedURLException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            } catch (URISyntaxException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            }
            //endregion
        }
        //endregion

        //region Set Social media Linkedin
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Feature_Social_Media) != null) {
            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Feature_Social_Media).contains("linkedin")) {
                imageviewLinkedin.setVisibility(View.VISIBLE);
                /*if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.IMG_Facebook) != "") {
                    imageviewFB.setVisibility(View.VISIBLE);
                    strFB = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.IMG_Facebook);
                    //region ImageFB
                    try {
                        URL urla = null;
            *//*picUrl = picUrl.replace("[", "");
            picUrl = picUrl.replace("]", "").replace("\"", "");*//*
                        urla = new URL(strFB);
                        URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                        picUrl = String.valueOf(urin.toURL());
                        // Capture position and set to the ImageView
                        Picasso.with(context)
                                .load(picUrl)
                                .into(imageviewFB, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                    }

                                    @Override
              (MalformedURLExc                      public void onError() {
                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                    }
                                });
                    } catch eption e) {
                        e.printStackTrace();
                        //Creating SendMail object
                        SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                        //Executing sendmail to send email
                        sm.execute();
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                        //Creating SendMail object
                        SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                        //Executing sendmail to send email
                        sm.execute();
                    }
                    //endregion
                }else {
                    imageviewFB.setVisibility(View.GONE);
                }*/
            }
        } else {
            imageviewLinkedin.setVisibility(View.GONE);
        }
        //endregion

        //region Set Social Pinterest
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Feature_Social_Media) != null) {
            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Feature_Social_Media).contains("Pinterest")) {
                imageviewPinterest.setVisibility(View.VISIBLE);
                /*if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.IMG_Facebook) != "") {
                    imageviewFB.setVisibility(View.VISIBLE);
                    strFB = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.IMG_Facebook);
                    //region ImageFB
                    try {
                        URL urla = null;
            *//*picUrl = picUrl.replace("[", "");
            picUrl = picUrl.replace("]", "").replace("\"", "");*//*
                        urla = new URL(strFB);
                        URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                        picUrl = String.valueOf(urin.toURL());
                        // Capture position and set to the ImageView
                        Picasso.with(context)
                                .load(picUrl)
                                .into(imageviewFB, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                    }

                                    @Override
              (MalformedURLExc                      public void onError() {
                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                    }
                                });
                    } catch eption e) {
                        e.printStackTrace();
                        //Creating SendMail object
                        SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                        //Executing sendmail to send email
                        sm.execute();
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                        //Creating SendMail object
                        SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                        //Executing sendmail to send email
                        sm.execute();
                    }
                    //endregion
                }else {
                    imageviewFB.setVisibility(View.GONE);
                }*/
            }
        } else {
            imageviewPinterest.setVisibility(View.GONE);
        }
        //endregion

        //region Set Social media FB
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Feature_Social_Media) != null) {
            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Feature_Social_Media).contains("FB")) {
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.IMG_Facebook) != "") {
                    imageviewFB.setVisibility(View.VISIBLE);
                    strFB = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.IMG_Facebook);
                    //region ImageFB
                    try {
                        URL urla = null;
            /*picUrl = picUrl.replace("[", "");
            picUrl = picUrl.replace("]", "").replace("\"", "");*/
                        urla = new URL(strFB);
                        URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                        picUrl = String.valueOf(urin.toURL());
                        // Capture position and set to the ImageView
                        Picasso.with(context)
                                .load(picUrl)
                                .into(imageviewFB, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                    }

                                    @Override
                                    public void onError() {
                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                    }
                                });
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                        //Creating SendMail object
                        SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                        //Executing sendmail to send email
                        sm.execute();
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                        //Creating SendMail object
                        SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                        //Executing sendmail to send email
                        sm.execute();
                    }
                    //endregion
                } else {
                    imageviewFB.setVisibility(View.GONE);
                }
            }
        } else {
            imageviewFB.setVisibility(View.GONE);
        }//endregion

        //region Set Social media Google/Gmail
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Feature_Social_Media) != null) {
            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Feature_Social_Media).contains("Google")) {
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.IMG_google) != "") {
                    imageviewGmail.setVisibility(View.VISIBLE);
                    strGmail = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.IMG_google);
                    //region ImageGoogal
                    try {
                        URL urla = null;
            /*picUrl = picUrl.replace("[", "");
            picUrl = picUrl.replace("]", "").replace("\"", "");*/
                        urla = new URL(strGmail);
                        URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                        picUrl = String.valueOf(urin.toURL());
                        // Capture position and set to the ImageView
                        Picasso.with(context)
                                .load(picUrl)
                                .into(imageviewGmail, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                    }

                                    @Override
                                    public void onError() {
                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                    }
                                });
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                        //Creating SendMail object
                        SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                        //Executing sendmail to send email
                        sm.execute();
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                        //Creating SendMail object
                        SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                        //Executing sendmail to send email
                        sm.execute();
                    }
                    //endregion
                } else {
                    imageviewGmail.setVisibility(View.GONE);
                }
            }
        } else {
            imageviewGmail.setVisibility(View.GONE);
        }
        //endregion

        //region Set Social media Instagram
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Feature_Social_Media) != null) {
            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.Feature_Social_Media).contains("Instagram")) {
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.IMG_google) != "") {
                    imageviewInsta.setVisibility(View.VISIBLE);
                    strinsta = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.IMG_insta);
                    //region ImageInsta
                    try {
                        URL urla = null;
            /*picUrl = picUrl.replace("[", "");
            picUrl = picUrl.replace("]", "").replace("\"", "");*/
                        urla = new URL(strinsta);
                        URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                        picUrl = String.valueOf(urin.toURL());
                        // Capture position and set to the ImageView
                        Picasso.with(context)
                                .load(picUrl)
                                .into(imageviewInsta, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                    }

                                    @Override
                                    public void onError() {
                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                    }
                                });
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                        //Creating SendMail object
                        SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                        //Executing sendmail to send email
                        sm.execute();
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                        //Creating SendMail object
                        SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                        //Executing sendmail to send email
                        sm.execute();
                    }
                    //endregion
                } else {
                    imageviewInsta.setVisibility(View.GONE);
                }
            }
        } else {
            imageviewInsta.setVisibility(View.GONE);
        }
        //endregion

        chanageEditTextBorder(editFirstName);
        chanageEditTextBorder(editLastName);
        chanageEditTextBorder(editEmail);
        chanageEditTextBorder(editPhoneNo);
        chanageEditTextBorder(editpassword);
        chanageEditTextBorder(editConfirmPwd);
        chanageFrameLayout(flCountryCode);
        chanageButton(btnRegistration);

        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            txtLoginNow.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
        getCountryCodeAPI();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        editFirstName.setText("");
        editLastName.setText("");
        editEmail.setText("");

        first_name = getIntent().getStringExtra("firstname");
        last_name = getIntent().getStringExtra("lastname");
        user_email = getIntent().getStringExtra("Useremail");
        social_type = getIntent().getStringExtra("SocialType");
        socialId = getIntent().getStringExtra("SocialId");
        try {
            if (first_name != null) {
                editFirstName.setText(first_name);
            }
            if (last_name != null) {
                editLastName.setText(last_name);
            }
            if (user_email != null) {
                editEmail.setText(user_email);
                editEmail.setEnabled(false);
            } else {
                editEmail.setEnabled(true);
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    //region Typeface
    private void Typeface() {
        editFirstName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editLastName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editEmail.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editPhoneNo.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editpassword.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editConfirmPwd.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnRegistration.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtLoginNow.setTypeface(Typefaces.TypefaceCalibri_bold(context));
    }
    //endregion

    //region ClickListener
    private void ClickListener() {
        imageviewFB.setOnClickListener(this);
        imageviewInsta.setOnClickListener(this);
        imageviewGmail.setOnClickListener(this);
        btnRegistration.setOnClickListener(this);
        txtLoginNow.setOnClickListener(this);
        actCountryCode.setOnClickListener(this);
        imageviewLinkedin.setOnClickListener(this);
        imageviewPinterest.setOnClickListener(this);
    }
    //endregion

    //region Initialization
    private void Initialization() {
        imageviewLinkedin = (ImageView) findViewById(R.id.imageviewLinkedin);
        imageviewPinterest = (ImageView) findViewById(R.id.imageviewPinterest);
        imageviewGmail = (ImageView) findViewById(R.id.imageviewGmail);
        imageviewInsta = (ImageView) findViewById(R.id.imageviewInsta);
        imageviewFB = (ImageView) findViewById(R.id.imageviewFB);

        editFirstName = (EditText) findViewById(R.id.editFirstName);
        editLastName = (EditText) findViewById(R.id.editLastName);
        editEmail = (EditText) findViewById(R.id.editEmail);
        editUserEmail = (EditText) findViewById(R.id.editUserEmail);
        editpassword = (EditText) findViewById(R.id.editpassword);
        editConfirmPwd = (EditText) findViewById(R.id.editConfirmPwd);
        editPhoneNo = (EditText) findViewById(R.id.editPhoneNo);

        login_button = (LoginButton) findViewById(R.id.login_button);
        btnRegistration = (Button) findViewById(R.id.btnRegistration);

        relativeProgress = (RelativeLayout) findViewById(R.id.relativeProgress);

        txtLoginNow = (TextView) findViewById(R.id.txtLoginNow);

        imageLogo = (ImageView) findViewById(R.id.imageLogo);
        coordinator = (CoordinatorLayout) findViewById(R.id.coordinator);

        actCountryCode = (AutoCompleteTextView) findViewById(R.id.actCountryCode);
        flCountryCode = (FrameLayout) findViewById(R.id.flCountryCode);
    }
    //endregion


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageviewGmail:
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
            case R.id.imageviewInsta:
                mApp.authorize();
                break;
            case R.id.btnRegistration:
                if (Validation()) {
                    sendRegisterAPI();
                }
                break;
            case R.id.txtLoginNow:
                onBackPressed();
                finish();
                break;

            case R.id.imageviewLinkedin:
                loginLinkedin();
                break;

            case R.id.imageviewPinterest:
                PinterestAuthentication();
                break;

            case R.id.actCountryCode:
                actCountryCode.showDropDown();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LISessionManager.getInstance(getApplicationContext())
                .onActivityResult(this,
                        requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        pdkClient.onOauthResponse(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else if (requestCode == 1) {
            if (!Global.isNetworkAvailable(context)) {
                openInternetAlertDialog(context, "Not connected to Internet");
            } else {
                internetAlert.dismiss();
            }
        }
    }

    //region For handel gmail sign In
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            //Log.e(TAG, "display name: " + acct.getDisplayName());

            editFirstName.setText("");
            editLastName.setText("");
            editEmail.setText("");


            username = acct.getDisplayName();
            userEmail = acct.getEmail();
            socialId = acct.getId();
            social_type = "google";
            CheckSocialRegister(socialId, social_type, username, userEmail);

        }
    }
    //endregion

    //region SOCIAL MEDIA
    public void CheckSocialRegister(final String social_id, final String Social_type,
                                    final String userName, final String userEmail) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"social_id", "social_type", "email_id"};
        String[] val = {social_id, Social_type, userEmail};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.Checkregister);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                if (strStatus.equals("ok")) {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    String user_id = jsonObject.getString("user_id");
                                    JSONObject jsonObjectAuther = jsonObject.getJSONObject("authUser");
                                    String user_token = jsonObjectAuther.getString("user_token");
                                    String email = jsonObject.getString("email");
                                    String firstname = jsonObject.getString("firstname");

                                    Intent intentMain = new Intent(context, MainActivity.class);
                                    SharedPreference.CreatePreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.SavePreference(Global.USERID, user_id);
                                    SharedPreference.SavePreference(Global.USERTOKEN, user_token);
                                    SharedPreference.SavePreference(Global.USER_EMAIL, email);
                                    SharedPreference.SavePreference(Global.USER_Name, firstname);
                                    startActivity(intentMain);
                                    finish();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                String str = null, struserEmail = null;
                                //str = userName;
                                //struserEmail = userEmail;
                                if (userName.contains(" ")) {
                                    String[] str1 = userName.split(" ");
                                    editFirstName.setText(str1[0]);
                                    editLastName.setText(str1[1]);
                                } else {
                                    editFirstName.setText(userName);
                                    editLastName.setText("");
                                }
                                try {
                                    if (userEmail != null) {
                                        editEmail.setEnabled(false);
                                        editEmail.setText(struserEmail);
                                    } else {
                                        editEmail.setEnabled(true);
                                    }
                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT,
                                    "Getting error in RegistrationActivity.java When parsing" +
                                            " Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    //region Linkedin Social media intergration
    public void loginLinkedin() {
        final String url = "https://api.linkedin.com/v1/people/~:" +
                "(id,first-name,last-name,email-address,formatted-name,phone-numbers," +
                "picture-urls::(original))";
        LISessionManager.getInstance(getApplicationContext())
                .init(this, buildScope(), new AuthListener() {
                    @Override
                    public void onAuthSuccess() {

                        /*Toast.makeText(getApplicationContext(), "success" +
                                        LISessionManager
                                                .getInstance(getApplicationContext())
                                                .getSession().getAccessToken().toString(),
                                Toast.LENGTH_LONG).show();*/
                        APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
                        apiHelper.getRequest(RegistrationActivity.this, url, new ApiListener() {
                            @Override
                            public void onApiSuccess(ApiResponse result) {
                                try {
                                    JSONObject jsonObject = result.getResponseDataAsJson();
                                    String Fname = jsonObject.optString("firstName");
                                    String Lname = jsonObject.optString("lastName");
                                    String email = jsonObject.optString("emailAddress");
                                    String Social_id = jsonObject.optString("id");
                                    String Picture = jsonObject.optString("pictureUrl");
                                    social_type = "linkedin";
                                    CheckSocialRegister(Social_id,
                                            social_type, Fname + " " + Lname, email);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onApiError(LIApiError error) {
                                error.printStackTrace();
                            }
                        });

                    }

                    @Override
                    public void onAuthError(LIAuthError error) {

                        Toast.makeText(getApplicationContext(), "failed "
                                        + error.toString(),
                                Toast.LENGTH_LONG).show();
                    }
                }, true);
    }

    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS, Scope.W_SHARE);
    }
    //endregion

    //region Pinterest Authentication
    private void PinterestAuthentication() {
        List scopes = new ArrayList<String>();
        scopes.add(PDKClient.PDKCLIENT_PERMISSION_READ_PUBLIC);
        scopes.add(PDKClient.PDKCLIENT_PERMISSION_WRITE_PUBLIC);
        scopes.add(PDKClient.PDKCLIENT_PERMISSION_READ_RELATIONSHIPS);
        scopes.add(PDKClient.PDKCLIENT_PERMISSION_WRITE_RELATIONSHIPS);
        scopes.add(PDKClient.PDKCLIENT_PERMISSION_READ_PRIVATE);
        scopes.add(PDKClient.PDKCLIENT_PERMISSION_WRITE_PRIVATE);

        pdkClient.login(this, scopes, new PDKCallback() {
            @Override
            public void onSuccess(PDKResponse response) {
                finish();
                Log.d(getClass().getName(), response.getData().toString());
                getMe();
               /* String Fname = response.getUser().getFirstName();
                String Lname = response.getUser().getLastName();
                String Email = response.getUser().getUsername();
                String ID = response.getUser().getUid();
                CheckSocialRegister(ID, "Pinterest", Fname + " " + Lname, Email);*/
            }

            @Override
            public void onFailure(PDKException exception) {
                Log.e(getClass().getName(), exception.getDetailMessage());
            }
        });

    }
    //endregion

    //region get User Details
    private void getMe() {
        PDKClient.getInstance().getMe(USER_FIELDS, new PDKCallback() {
            @Override
            public void onSuccess(PDKResponse response) {
                editFirstName.setText("");
                editLastName.setText("");
                editEmail.setText("");
                if (DEBUG) log(String.format("status: %d", response.getStatusCode()));
                user = response.getUser();
                String Fname = user.getFirstName();
                String Lname = user.getLastName();
                String Email = user.getUsername();
                String ID = user.getUid();
                if (!Lname.equalsIgnoreCase("")) {
                    CheckSocialRegister(ID, "Pinterest", Fname + " " + Lname, Email);
                } else {
                    CheckSocialRegister(ID, "Pinterest", Fname, Email);
                }
            }

            @Override
            public void onFailure(PDKException exception) {
                if (DEBUG) log(exception.getDetailMessage());
                Toast.makeText(RegistrationActivity.this, "/me Request failed", Toast.LENGTH_SHORT).show();
            }
        });
    }//endregion

    private void log(String msg) {
        if (!Utils.isEmpty(msg))
            Log.d(getClass().getName(), msg);
    }

    public void Login() {
        PDKClient.getInstance().getPath("me/", new PDKCallback() {
            @Override
            public void onSuccess(PDKResponse response) {
                Log.d("Response", response.toString());
                String Fname = response.getUser().getFirstName();
                String Lname = response.getUser().getLastName();
                String Email = response.getUser().getUsername();
                String ID = response.getUser().getUid();
                CheckSocialRegister(ID, "Pinterest", Fname + " " + Lname, Email);
            }

            @Override
            public void onFailure(PDKException exception) {
                Log.d("exception", exception.toString());
            }
        });
    }

    //region For FacebookWith Login
    @Override
    protected void onResume() {
        super.onResume();
        callbackManager = CallbackManager.Factory.create();
        login_button = (LoginButton) findViewById(R.id.login_button);
        assert login_button != null;
        login_button.setReadPermissions("email");

        Intent intent = getIntent();
        String email = intent.getStringExtra("email");
        editUserEmail.setText(email);

        imageviewFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // progressDialog = new ProgressDialog(Login.this);
                //progressDialog.setMessage("Loading...");
                //progressDialog.show();

                login_button.performClick();

                login_button.setPressed(true);

                login_button.invalidate();

                login_button.registerCallback(callbackManager, mCallBack);

                login_button.setPressed(false);

                login_button.invalidate();

            }
        });
        callbackManager = CallbackManager.Factory.create();
    }

    private FacebookCallback<LoginResult> mCallBack = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            //progressDialog.dismiss();

            // App code
            //Profile profile = Profile.getCurrentProfile();
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            Log.e("response: ", response + "");
                            try {
                                //String str = object.toString();

                                editFirstName.setText("");
                                editLastName.setText("");
                                editEmail.setText("");

                                user_fb_email = object.getString("email");
                                socialId = object.getString("id");
                                username = object.getString("name");
                                social_type = "facebook";
                                CheckSocialRegister(socialId, social_type, username, user_fb_email);
                                /*user = new User();
                                user.facebookID = object.getString("id");
                                //user.email = object.getString("email");
                                //user.email="atlmavani@ymail.com";
                                user.name = object.getString("name");
                                user.gender = object.getString("gender");
                                PrefUtils.setCurrentUser(user,Login.this);*/

                            } catch (Exception e) {
                                e.printStackTrace();
                                //Creating SendMail object
                                SendMail sm = new SendMail(RegistrationActivity.this, Global.TOEMAIL, Global.SUBJECT, "Getting error in Register.java When parsing response from facebook.\n" + e.toString());
                                //Executing sendmail to send email
                                sm.execute();
                            }
                            /*TimeStamp=Security.getCurrentTimeStamp();
                            ipv4=Security.getIPAddress(true);
                            //SaltMd5=Security.md5(TimeStamp);
                            // SaltMd5="1";
                            //encryptedData=Security.encrypt(SaltMd5);
                            encryptedData=Security.tokenencrypt(TimeStamp);
                            //decryptedData=Security.decrypt(encryptedData, Salt);
                            String username[]=user.name.split(" ");
                            data.put("firstname", username[0]);
                            data.put("lastname", username[1]);
                            data.put("email", "");
                            data.put("gender", user.gender);
                            data.put("dob", " ");
                            data.put("mobile", " ");
                            data.put("current_city", " ");
                            data.put("fb_id", user.facebookID);
                            data.put("g_id", " ");
                            data.put("profile_img", " ");
                            data.put("occupation", " ");
                            data.put("time_stamp", TimeStamp);
                            data.put("token", encryptedData);
                            data.put("client_ip", ipv4);
                            new UserLogin().execute();*/
                        }

                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender, birthday");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
            //progressDialog.dismiss();
        }

        @Override
        public void onError(FacebookException e) {
            e.printStackTrace();
            //progressDialog.dismiss();
            //Creating SendMail object
            SendMail sm = new SendMail(RegistrationActivity.this, Global.TOEMAIL, Global.SUBJECT, "Getting error in RegistrationActivity.java When parsing response from facebook.\n" + e.toString());
            //Executing sendmail to send email
            sm.execute();
        }
    };
    //endregion

    //region Instagram
    private Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == InstagramApp.WHAT_FINALIZE) {

                editFirstName.setText("");
                editLastName.setText("");
                editEmail.setText("");

                userInfoHashmap = mApp.getUserInfo();
                socialId = userInfoHashmap.get("id");
                user_insta_fullname = userInfoHashmap.get("full_name");
                social_type = "instagram";
                CheckSocialRegister(socialId, social_type, user_insta_fullname, "");
            } else if (msg.what == InstagramApp.WHAT_FINALIZE) {
                Toast.makeText(RegistrationActivity.this, "Check your network.",
                        Toast.LENGTH_SHORT).show();
            }
            return false;
        }
    });
    //endregion

    //region FOR Registration API...
    private void sendRegisterAPI() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"firstname", "lastname", "email", "password", "confirmpassword", "phoneno", "socialmediaid",
                "social_type", "countrycodes"};
        String[] val = {editFirstName.getText().toString(), editLastName.getText().toString(), editEmail.getText().toString(),
                editpassword.getText().toString(), editConfirmPwd.getText().toString(), editPhoneNo.getText().toString(),
                socialId, social_type, actCountryCode.getText().toString()};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.Registration);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
//                                Toast.makeText(context, strMessage , Toast.LENGTH_SHORT).show();
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObject = response.getJSONObject("payload");

                                    Intent intent = new Intent(context, OTPVerificationActivity.class);
                                    intent.putExtra("userEmail", editEmail.getText().toString());
                                    intent.putExtra("userPhone", editPhoneNo.getText().toString());
                                    intent.putExtra("userCountyCode", actCountryCode.getText().toString());
                                    intent.putExtra("userUserID", jsonObject.getString("user_token"));
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
//                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in RegisActivity.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region Validation
    public Boolean Validation() {
        Boolean valid = false;
        if (!TextUtils.isEmpty(editFirstName.getText().toString())) {
            editFirstName.setError(null);
            if (!TextUtils.isEmpty(editLastName.getText().toString())) {
                editLastName.setError(null);
                if (!TextUtils.isEmpty(editEmail.getText().toString())) {
                    if (Global.isValidEmail(editEmail.getText())) {
                        editEmail.setError(null);
                        if (!TextUtils.isEmpty(editpassword.getText().toString())) {
                            editpassword.setError(null);
                            if (editpassword.length() >= 6) {
                                editpassword.setError(null);
                                if (!TextUtils.isEmpty(editConfirmPwd.getText().toString())) {
                                    editConfirmPwd.setError(null);
                                    if (editConfirmPwd.length() >= 6) {
                                        editpassword.setError(null);
                                        if (editConfirmPwd.getText().toString().equals(editpassword.getText().toString())) {
                                            if (!TextUtils.isEmpty(editPhoneNo.getText().toString())) {
                                                if (editPhoneNo.length() >= 10) {
                                                    valid = true;
                                                } else {
                                                    editPhoneNo.setError("Please enter valid Phone number..!");
                                                }
                                            } else {
                                                editPhoneNo.setError("Please enter user Phone number..!");
                                            }
                                        } else {
                                            editConfirmPwd.setError("Confirm Password not match..!");
                                        }
                                    } else {
                                        editConfirmPwd.setError("Password must have at least 6 character..!");
                                    }
                                } else {
                                    editConfirmPwd.setError("Please enter Confirm Password..!");
                                }
                            } else {
                                editpassword.setError("Password must have at least 6 character..!");
                            }
                        } else {
                            editpassword.setError("Please enter Password..!");
                        }
                    } else {
                        editEmail.setError("Please enter Valid Email Address..!");
                    }
                } else {
                    editEmail.setError("Please enter user Email Address..!");
                }
            } else {
                editLastName.setError("Please enter user Last name..!");
            }
        } else {
            editFirstName.setError("Please enter user First name..!");
        }
        return valid;
    }
    //endregion

    //region chanage FrameLayout Border
    public void chanageFrameLayout(FrameLayout flCountryCode) {
        actCountryCode.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        flCountryCode.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) flCountryCode.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setCornerRadius(60);
        gd.setColor(Color.parseColor("#FFFFFF"));
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
    }
    //endregion

    //region chanageEditTextBorder
    public void chanageEditTextBorder(EditText editText) {
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
            editText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        }
        editText.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) editText.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#FFFFFF"));
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
    }
    //endregion

    //region chanageButton
    public void chanageButton(Button button) {
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor) != "") {
            button.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        }
//        button.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TEXTCOLOR)));
        button.setBackgroundResource(R.drawable.ic_button);
        GradientDrawable gd = (GradientDrawable) button.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
// gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        /* gd.setStroke(2, Color.parseColor(StaticUtility.BORDERCOLOR));*/
    }
    //endregion

    //region FOR GET Country Code...
    private void getCountryCodeAPI() {

        String[] key = {};
        String[] val = {};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.getCountryCode);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
//                                Toast.makeText(context, strMessage , Toast.LENGTH_SHORT).show();

                                if (strMessage.equals("OK")) {
                                    JSONArray payloadArray = response.getJSONArray("payload");
                                    countries = new ArrayList<>();
                                    for (int i = 0; i < payloadArray.length(); i++) {
                                        JSONObject object = payloadArray.getJSONObject(i);
                                        countryName = object.getString("name");
                                        countryCode = object.getString("code");
                                        countries.add(new SpinnerItem(countryName, countryCode, i));
                                    }
                                    if (countries.size() > 0) {
                                        AutoCompleteForCountryCodeAdapter autoCompleteForCountryCodeAdapter = new AutoCompleteForCountryCodeAdapter(context, R.layout.row_items, countries, actCountryCode);
                                        actCountryCode.setAdapter(autoCompleteForCountryCodeAdapter);
                                    }
                                }
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in RegisActivity.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
//                Toast.makeText(context, anError.toString(), Toast.LENGTH_SHORT).show();
            }

        });
    }
    //endregion

    //region AUTO COMPLETE ADAPTER FOR COUNTRY CODE...
    public class AutoCompleteForCountryCodeAdapter extends ArrayAdapter<SpinnerItem> {
        Context mContext;
        ArrayList<SpinnerItem> mDepartments;
        ArrayList<SpinnerItem> mDepartments_All;
        ArrayList<SpinnerItem> mDepartments_Suggestion;
        int mLayoutResourceId;
        private AutoCompleteTextView autoCompleteTextView;

        public AutoCompleteForCountryCodeAdapter(Context context, int resource, ArrayList<SpinnerItem> departments,
                                                 AutoCompleteTextView autoCompleteTextView) {
            super(context, resource, departments);
            this.mContext = context;
            this.mLayoutResourceId = resource;
            this.mDepartments = new ArrayList<>(departments);
            this.mDepartments_All = new ArrayList<>(departments);
            this.mDepartments_Suggestion = new ArrayList<>();
            this.autoCompleteTextView = autoCompleteTextView;
        }

        public int getCount() {
            return mDepartments.size();
        }

        public SpinnerItem getItem(int position) {
            return mDepartments.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                    convertView = inflater.inflate(mLayoutResourceId, parent, false);
                }
                final SpinnerItem department = getItem(position);
                TextView name = (TextView) convertView.findViewById(R.id.ItemName);
                TextView id = (TextView) convertView.findViewById(R.id.ItemId);
                name.setText(department.getCountrycode());
                name.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                /*id.setText(department.getCountryName());*/

                autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        SpinnerItem pi = getItem(position);
                        strCountryCode = pi.getCountrycode();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                public String convertResultToString(Object resultValue) {
                    return ((SpinnerItem) resultValue).getCountrycode();
                }

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    if (constraint != null) {
                        mDepartments_Suggestion.clear();
                        for (SpinnerItem department : mDepartments_All) {
                            if (department.getCountrycode().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                                mDepartments_Suggestion.add(department);
                            }
                        }
                        FilterResults filterResults = new FilterResults();
                        filterResults.values = mDepartments_Suggestion;
                        filterResults.count = mDepartments_Suggestion.size();
                        return filterResults;
                    } else {
                        return new FilterResults();
                    }
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    mDepartments.clear();
                    if (results != null && results.count > 0) {
                        // avoids unchecked cast warning when using mDepartments.addAll((ArrayList<Department>) results.values);
                        List<?> result = (List<?>) results.values;
                        for (Object object : result) {
                            if (object instanceof SpinnerItem) {
                                mDepartments.add((SpinnerItem) object);
                            }
                        }
                    } else if (constraint == null) {
                        // no filter, add entire original list back in
                        mDepartments.addAll(mDepartments_All);
                    }
                    notifyDataSetChanged();
                }
            };
        }
    }
    //endregion

    //region For EventBus onEvent
    public void onEvent(String event) {
        openInternetAlertDialog(context, event);
    }
    //endregion

    //region FOR SHOW INTERNET CONNECTION DIALOG...
    public void openInternetAlertDialog(final Context mContext, String alertString) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.row_alert_dialog, null);
        final TextView tvAlertText = (TextView) row.findViewById(R.id.tvAlertText);
        final TextView tvTitle = (TextView) row.findViewById(R.id.tvTitle);
        final Button btnSettings = (Button) row.findViewById(R.id.btnSettings);
        final Button btnExit = (Button) row.findViewById(R.id.btnExit);

        tvTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        tvAlertText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        btnSettings.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        btnExit.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        btnExit.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        btnSettings.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        tvTitle.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));

        tvAlertText.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        tvTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnSettings.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnExit.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

        if (alertString.equals("Not connected to Internet")) {
            if (i == 0) {
                i = 1;
                AlertDialog.Builder i_builder = new AlertDialog.Builder(mContext);
                internetAlert = i_builder.create();
                internetAlert.setCancelable(false);
                internetAlert.setView(row);

                if (internetAlert.isShowing()) {
                    internetAlert.dismiss();
                } else {
                    internetAlert.show();
                }

                btnExit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        internetAlert.dismiss();
                        //FOR CLOSE APP...
                        System.exit(0);
                    }
                });

                btnSettings.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        /*internetAlert.dismiss();*/
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                    }
                });
            } else {
                /*internetAlert.dismiss();*/
            }
        } else {
            i = 0;
            internetAlert.dismiss();
        }
    }
    //endregion

}
