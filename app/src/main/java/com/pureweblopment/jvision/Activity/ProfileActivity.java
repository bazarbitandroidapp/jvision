package com.pureweblopment.jvision.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.pureweblopment.jvision.Global.Global;
import com.pureweblopment.jvision.Global.SendMail;
import com.pureweblopment.jvision.Global.SharedPreference;
import com.pureweblopment.jvision.Global.StaticUtility;
import com.pureweblopment.jvision.Global.Typefaces;
import com.pureweblopment.jvision.Model.Country;
import com.pureweblopment.jvision.Model.SpinnerItem;
import com.pureweblopment.jvision.Model.State;
import com.pureweblopment.jvision.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;
import de.hdodenhof.circleimageview.CircleImageView;
import eu.janmuller.android.simplecropimage.CropImage;
import io.fabric.sdk.android.Fabric;
import okhttp3.Response;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    Context context = ProfileActivity.this;

    RelativeLayout relativeProgress;
    CircleImageView circularImageViewUser;
    Button btnSubmit, btnClear;

    EditText editFirstName, editLastName, editEmail, editPhoneNo, editAddress, editCity, editPincode;
    TextView txtGender;
    RadioGroup radioGroupGender;
    RadioButton radioBtnMale, radioBtnFemail;

    TextView txtDOB;

    Spinner spinnerCountry, spinnerState;
    String strCountry = "", strState = "";

    CoordinatorLayout coodinator;

    String picUrl = null;
    private static URL urla = null;
    private static URI urin = null;

    String strFname, strLastname, strEmail, strPhoneNo, strGender, strDOB, strAddress1, strAddress2, strCity, strPincode, strProfileImage = "";

    private static String strImagePath = null, strFilename = null;
    static boolean ImageUser = false;
    private static JSONObject jsonObjectImage;

    boolean isclear = true;

    ImageView imageCartBack;
    TextView txtCatName;
    ProgressBar progress;

    CircleImageView circularImageViewUser1;
    TextView txtUserName;

    private File mFileTemp;
    public static final int REQUEST_CODE_INTERNT_DIALOG = 1;
    public static final int REQUEST_CODE_GALLERY = 2;
    public static final int REQUEST_CODE_TAKE_PICTURE = 3;
    public static final int REQUEST_CODE_CROP_IMAGE = 4;

    public String TEMP_PHOTO_FILE_NAME;
    String imgstring = null, fileName = "";

    AutoCompleteTextView actCountry, actState;

    private Country country;
    private State state;
    ArrayList<Country> countries = new ArrayList<>();
    ArrayList<State> states = new ArrayList<>();

    LinearLayout llGender;

    ImageView imgUser;

    FrameLayout flCountry, flState;

    //Internet Alert
    public EventBus eventBus = EventBus.getDefault();
    public static int i = 0;
    public static AlertDialog internetAlert;

    FrameLayout flToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_profile);
        eventBus.register(this);

        Initialization();
        TypeFace();
        OnClickListener();

        txtCatName.setText(R.string.myaccount);

        imageCartBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    onBackPressed();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });

        changeButton(btnSubmit);
        changeButton(btnClear);
        changeEditTextBorder(editFirstName);
        changeEditTextBorder(editLastName);
        changeEditTextBorder(editEmail);
        changeEditTextBorder(editPhoneNo);
        changeTextViewBorder(txtDOB);
        changeEditTextBorder(editAddress);
        changeEditTextBorder(editCity);
        changeEditTextBorder(editPincode);
        changeLinearBorder(llGender);
        changeFramelayoutBorder(flCountry);
        changeFramelayoutBorder(flState);
        changeRadioButtonColor(radioBtnMale);
        changeRadioButtonColor(radioBtnFemail);

        flToolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

        getUserLastInfo();

        actCountry.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    actState.setEnabled(true);
                    getStateAPI();
                } else {
                    actState.setEnabled(false);
                    actState.setText("");
                    actState.setHint("Select State");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    //region Initialization
    private void Initialization() {
        progress = (ProgressBar) findViewById(R.id.progress);
        imageCartBack = (ImageView) findViewById(R.id.imageCartBack);
        circularImageViewUser1 = (CircleImageView) findViewById(R.id.circularImageViewUser);
        txtUserName = (TextView) findViewById(R.id.txtUserName);
        txtCatName = (TextView) findViewById(R.id.txtCatName);
        relativeProgress = (RelativeLayout) findViewById(R.id.relativeProgress);
        circularImageViewUser = (CircleImageView) findViewById(R.id.circularImageViewUser);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnClear = (Button) findViewById(R.id.btnClear);
        editFirstName = (EditText) findViewById(R.id.editFirstName);
        editLastName = (EditText) findViewById(R.id.editLastName);
        editEmail = (EditText) findViewById(R.id.editEmail);
        editPhoneNo = (EditText) findViewById(R.id.editPhoneNo);
        txtDOB = (TextView) findViewById(R.id.txtDOB);
        editAddress = (EditText) findViewById(R.id.editAddress);
        editCity = (EditText) findViewById(R.id.editCity);
        editPincode = (EditText) findViewById(R.id.editPincode);
        txtGender = (TextView) findViewById(R.id.txtGender);
        radioGroupGender = (RadioGroup) findViewById(R.id.radioGroupGender);
        radioBtnMale = (RadioButton) findViewById(R.id.radioBtnMale);
        radioBtnFemail = (RadioButton) findViewById(R.id.radioBtnFemail);
        coodinator = (CoordinatorLayout) findViewById(R.id.coodinator);
        llGender = (LinearLayout) findViewById(R.id.llGender);

        actCountry = (AutoCompleteTextView) findViewById(R.id.actCountry);
        actState = (AutoCompleteTextView) findViewById(R.id.actState);
        actCountry.setThreshold(1);
        actState.setThreshold(1);

        imgUser = (ImageView) findViewById(R.id.imgUser);

        flCountry = (FrameLayout) findViewById(R.id.flCountry);
        flState = (FrameLayout) findViewById(R.id.flState);
        flToolbar = (FrameLayout) findViewById(R.id.flToolbar);

    }
    //endregion

    //region TypeFace
    private void TypeFace() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

        txtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        btnSubmit.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnClear.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editFirstName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editLastName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editEmail.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editPhoneNo.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtDOB.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editCity.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editPincode.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtGender.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        radioBtnMale.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        radioBtnFemail.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        actCountry.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        actState.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));

    }
    //endregion

    //region OnClickListener
    private void OnClickListener() {
        circularImageViewUser.setOnClickListener(this);
        btnClear.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        txtDOB.setOnClickListener(this);
        radioGroupGender.setOnCheckedChangeListener(this);
    }
    //endregion

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.circularImageViewUser:
                ImageUser = true;
                Image_Picker_Dialog();
                break;

            case R.id.btnSubmit:
                if (Validation()) {
                    UpdateProfile();
                }
                break;

            case R.id.btnClear:
                editFirstName.setText("");
                editLastName.setText("");
                editCity.setText("");
                editPincode.setText("");
                editPhoneNo.setText("");
                txtDOB.setText("");
                editAddress.setText("");
                actCountry.setText("");
                actState.setText("");
                actCountry.setHint("Select Country");
                actState.setHint("Select State");
                isclear = false;
                getCountryAPI();
                break;

            case R.id.actCountry:
                actCountry.showDropDown();
                break;

            case R.id.actState:
                actState.showDropDown();
                break;

            case R.id.txtDOB:
                Global.DateDialog newFragment = new Global.DateDialog(txtDOB, false);
                newFragment.show(getSupportFragmentManager(), "datePicker");
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        if (radioGroup.equals(radioGroupGender)) {
            switch (radioGroupGender.getCheckedRadioButtonId()) {
                case R.id.radioBtnMale:
                    if (radioBtnMale.isChecked()) {
                        radioBtnFemail.setChecked(false);
                        strGender = radioBtnMale.getText().toString();
                    }
                    break;
                case R.id.radioBtnFemail:
                    if (radioBtnFemail.isChecked()) {
                        radioBtnMale.setChecked(false);
                        strGender = radioBtnFemail.getText().toString();
                    }
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {
            case 1:

                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);

                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);

                boolean isCamera = perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
                boolean isStorage = perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
                boolean isStorageWrite = perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;

                if (isCamera && isStorage && isStorageWrite)
                    cameraIntent();
                else
                    Toast.makeText(context, "Please grant both permission to work camera properly!!", Toast.LENGTH_SHORT).show();
                break;

            case 2:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    galleryIntent();
                else
                    Toast.makeText(context, "Storage permission denied!!", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    //region FOR IMAGE PICKER DIALOG...
    public void Image_Picker_Dialog() {
        final CharSequence[] items = {"Take Photo", "Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select Image");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    dialog.dismiss();
                    if (checkPermission()) {
                        cameraIntent();
                    }
                } else if (item == 1) {
                    dialog.dismiss();
                    if (checkPermissionGallery()) {
                        galleryIntent();
                    }
                } else {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    //endregion

    //region CheckPermission
    private boolean checkPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int permissionCamera = ContextCompat.checkSelfPermission(context,
                    Manifest.permission.CAMERA);
            int readPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
            int writePermission = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);

            List<String> listPermissionsNeeded = new ArrayList<>();
            if (permissionCamera != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }
            if (readPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (writePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }

            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 1);
                return false;
            } else {
                return true;
            }
        }
        return true;
    }
    //endregion

    //region cameraIntent
    private void cameraIntent() {

        setTempFilePath();

        Intent intent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment
                    .getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
//                mImageCaptureUri = Uri.fromFile(mFileTemp);
                mImageCaptureUri = FileProvider.getUriForFile(context,
                        context.getPackageName(), mFileTemp);
            }
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {

        }
    }
    //endregion

    //region setTempFilePath
    private void setTempFilePath() {
        TEMP_PHOTO_FILE_NAME = "profile_photo_" + System.currentTimeMillis() + ".jpg";
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) || !Environment.isExternalStorageRemovable()) {
            if (context.getExternalCacheDir() != null) {
                mFileTemp = new File(context.getExternalCacheDir().getPath(), TEMP_PHOTO_FILE_NAME); // most likely your null value
            }
        } else {
            if (context.getCacheDir() != null) {
                mFileTemp = new File(context.getCacheDir().getPath(), TEMP_PHOTO_FILE_NAME);
            }
        }
    }
    //endregion

    //region CheckPermissionGallery
    private boolean checkPermissionGallery() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                return false;

            } else {
                return true;
            }
        }
        return true;
    }
    //endregion

    //region galleryIntent
    private void galleryIntent() {

        setTempFilePath();

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent,
                REQUEST_CODE_GALLERY);
    }
    //endregion

    //region startCropImage
    private void startCropImage() {
        try {
            Intent intent = new Intent(context, CropImage.class);
            try {
                intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
            }catch (NullPointerException e){
                e.printStackTrace();
            }
            intent.putExtra(CropImage.SCALE, true);

            intent.putExtra(CropImage.ASPECT_X, 2);
            intent.putExtra(CropImage.ASPECT_Y, 2);

            startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }
    //endregion

    //region copyStream
    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }
    //endregion

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        Bitmap bitmap;

        switch (requestCode) {
            case REQUEST_CODE_GALLERY:
                try {
                    InputStream inputStream = getContentResolver().openInputStream(
                            data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                    copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();

                    startCropImage();

                } catch (Exception e) {
                }
                break;
            case REQUEST_CODE_TAKE_PICTURE:

                startCropImage();

                break;

            case REQUEST_CODE_INTERNT_DIALOG:

                if (!Global.isNetworkAvailable(context)) {
                    openInternetAlertDialog(context, "Not connected to Internet");
                } else {
                    internetAlert.dismiss();
                }

                break;
            case REQUEST_CODE_CROP_IMAGE:
                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                if (path == null) {
                    return;
                }
                String fileNameSegments[] = path.split("/");
                fileName = fileNameSegments[fileNameSegments.length - 1];

                bitmap = Global.decodeFile(new File(path), 400, 400);

                ByteArrayOutputStream proofbyteArrayOutputStream1 = new ByteArrayOutputStream();
                if (bitmap != null) {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, proofbyteArrayOutputStream1);
                    byte[] proofbyteArray1 = proofbyteArrayOutputStream1.toByteArray();
                    imgstring = Base64.encodeToString(proofbyteArray1, Base64.DEFAULT);
                    circularImageViewUser.setImageBitmap(bitmap);
                    ImagePath(bitmap, imgstring, fileName);
                    imgUser.setVisibility(View.GONE);

                } else {
                    imgUser.setVisibility(View.VISIBLE);
                    Toast.makeText(context, "Problem in getting image.Please try again!!", Toast.LENGTH_SHORT).show();
                }

                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    //region FOR GET Country API...
    private void getCountryAPI() {
        String[] key = {};
        String[] val = {};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.Countries);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        countries.clear();
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("OK")) {
                                    JSONArray payloadArray = response.getJSONArray("payload");
                                    for (int i = 0; i < payloadArray.length(); i++) {
                                        JSONObject jsonObjectProduct = payloadArray.getJSONObject(i);
                                        String CountryCode = jsonObjectProduct.getString("country_id");
                                        String Countryname = jsonObjectProduct.getString("country_name");

                                        country = new Country();
                                        country.setCountryName(Countryname);
                                        country.setCountryId(CountryCode);
                                        countries.add(country);
                                    }
                                    AutoCompleteForCountryAdapter autoCompleteForCountryAdapter = new AutoCompleteForCountryAdapter(context, R.layout.row_items, countries, actCountry);
                                    actCountry.setAdapter(autoCompleteForCountryAdapter);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                if (strMessage.equalsIgnoreCase("Invalid Credentials")) {
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ProfileActivity.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
//                Toast.makeText(context, anError.toString(), Toast.LENGTH_SHORT).show();
            }

        });
    }
    //endregion

    //region AUTO COMPLETE ADAPTER For COUNTRY...
    public class AutoCompleteForCountryAdapter extends ArrayAdapter<Country> {
        Context mContext;
        ArrayList<Country> mDepartments;
        ArrayList<Country> mDepartments_All;
        ArrayList<Country> mDepartments_Suggestion;
        int mLayoutResourceId;
        private AutoCompleteTextView autoCompleteTextView;

        public AutoCompleteForCountryAdapter(Context context, int resource, ArrayList<Country> departments, AutoCompleteTextView autoCompleteTextView) {
            super(context, resource, departments);
            this.mContext = context;
            this.mLayoutResourceId = resource;
            this.mDepartments = new ArrayList<>(departments);
            this.mDepartments_All = new ArrayList<>(departments);
            this.mDepartments_Suggestion = new ArrayList<>();
            this.autoCompleteTextView = autoCompleteTextView;
        }

        public int getCount() {
            return mDepartments.size();
        }

        public Country getItem(int position) {
            return mDepartments.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                    convertView = inflater.inflate(mLayoutResourceId, parent, false);
                }
                final Country department = getItem(position);
                TextView name = (TextView) convertView.findViewById(R.id.ItemName);
                TextView id = (TextView) convertView.findViewById(R.id.ItemId);
                name.setText(department.getCountryName());
                name.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                /*id.setText(department.getCountryName());*/

                autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Country pi = getItem(position);
                        strCountry = pi.getCountryName();
                        if (!strCountry.equals("")) {
                            actState.setEnabled(true);
                            getStateAPI();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                public String convertResultToString(Object resultValue) {
                    return ((Country) resultValue).getCountryName();
                }

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    if (constraint != null) {
                        mDepartments_Suggestion.clear();
                        for (Country department : mDepartments_All) {
                            if (department.getCountryName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                                mDepartments_Suggestion.add(department);
                            }
                        }
                        FilterResults filterResults = new FilterResults();
                        filterResults.values = mDepartments_Suggestion;
                        filterResults.count = mDepartments_Suggestion.size();
                        return filterResults;
                    } else {
                        return new FilterResults();
                    }
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    mDepartments.clear();
                    if (results != null && results.count > 0) {
                        // avoids unchecked cast warning when using mDepartments.addAll((ArrayList<Department>) results.values);
                        List<?> result = (List<?>) results.values;
                        for (Object object : result) {
                            if (object instanceof Country) {
                                mDepartments.add((Country) object);
                            }
                        }
                    } else if (constraint == null) {
                        // no filter, add entire original list back in
                        mDepartments.addAll(mDepartments_All);
                    }
                    notifyDataSetChanged();
                }
            };
        }
    }
    //endregion

    //region FOR GET State API...
    private void getStateAPI() {
        String[] key = {"country_name"};
        String[] val = {actCountry.getText().toString()};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.States);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        states.clear();
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("OK")) {
                                    JSONArray payloadArray = response.getJSONArray("payload");
                                    for (int i = 1; i < payloadArray.length(); i++) {
                                        JSONObject jsonObjectProduct = payloadArray.getJSONObject(i);
                                        String country_id = jsonObjectProduct.getString("country_id");
                                        String StateId = jsonObjectProduct.getString("state_id");
                                        String Statename = jsonObjectProduct.getString("state_name");
                                        String status = jsonObjectProduct.getString("status");

                                        state = new State();
                                        state.setStateName(Statename);
                                        state.setStateId(StateId);
                                        state.setCountry_id(country_id);
                                        state.setStatus(status);
                                        states.add(state);
                                    }
                                    AutoCompleteForStateAdapter autoCompleteForStateAdapter = new AutoCompleteForStateAdapter(context, R.layout.row_items, states, actState);
                                    actState.setAdapter(autoCompleteForStateAdapter);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                if (strMessage.equalsIgnoreCase("Invalid Credentials")) {
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ProfileActivity.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region AUTO COMPLETE ADAPTER FOR STATE...
    public class AutoCompleteForStateAdapter extends ArrayAdapter<State> {
        Context mContext;
        ArrayList<State> mDepartments;
        ArrayList<State> mDepartments_All;
        ArrayList<State> mDepartments_Suggestion;
        int mLayoutResourceId;
        private AutoCompleteTextView autoCompleteTextView;

        public AutoCompleteForStateAdapter(Context context, int resource, ArrayList<State> departments, AutoCompleteTextView autoCompleteTextView) {
            super(context, resource, departments);
            this.mContext = context;
            this.mLayoutResourceId = resource;
            this.mDepartments = new ArrayList<>(departments);
            this.mDepartments_All = new ArrayList<>(departments);
            this.mDepartments_Suggestion = new ArrayList<>();
            this.autoCompleteTextView = autoCompleteTextView;
        }

        public int getCount() {
            return mDepartments.size();
        }

        public State getItem(int position) {
            return mDepartments.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                    convertView = inflater.inflate(mLayoutResourceId, parent, false);
                }
                final State department = getItem(position);
                TextView name = (TextView) convertView.findViewById(R.id.ItemName);
                TextView id = (TextView) convertView.findViewById(R.id.ItemId);
                name.setText(department.getStateName());
                name.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                /*id.setText(department.getCountryName());*/

                autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        State pi = getItem(position);
                        strState = pi.getStateName();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                public String convertResultToString(Object resultValue) {
                    return ((State) resultValue).getStateName();
                }

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    if (constraint != null) {
                        mDepartments_Suggestion.clear();
                        for (State department : mDepartments_All) {
                            if (department.getStateName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                                mDepartments_Suggestion.add(department);
                            }
                        }
                        FilterResults filterResults = new FilterResults();
                        filterResults.values = mDepartments_Suggestion;
                        filterResults.count = mDepartments_Suggestion.size();
                        return filterResults;
                    } else {
                        return new FilterResults();
                    }
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    mDepartments.clear();
                    if (results != null && results.count > 0) {
                        // avoids unchecked cast warning when using mDepartments.addAll((ArrayList<Department>) results.values);
                        List<?> result = (List<?>) results.values;
                        for (Object object : result) {
                            if (object instanceof State) {
                                mDepartments.add((State) object);
                            }
                        }
                    } else if (constraint == null) {
                        // no filter, add entire original list back in
                        mDepartments.addAll(mDepartments_All);
                    }
                    notifyDataSetChanged();
                }
            };
        }
    }
    //endregion

    //region Filter
    private String filter(ArrayList<SpinnerItem> models, Spinner spinner, String query) {
        String filtered = "";
        try {
            final String lowerCaseQuery = query.toLowerCase();
            String selectedQuery = "British Indian Ocean Territory";
            final String strSelectedQuery = selectedQuery.toLowerCase();

            for (int i = 1; i < models.size(); i++) {
                final String text = models.get(i).getCountryname().toLowerCase();
                if (text.contains(lowerCaseQuery))
                    if (text.contains(strSelectedQuery)) {
                        spinner.setSelection(i - 1);
                    } else {
                        spinner.setSelection(i);
                    }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return filtered;
    }
    //endregion

    //region FOR getUserLastInfo API..
    private void getUserLastInfo() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetLastUserInfo);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    strFname = jsonObjectPayload.getString("firstname");
                                    strLastname = jsonObjectPayload.getString("lastname");
                                    strEmail = jsonObjectPayload.getString("email");
                                    strPhoneNo = jsonObjectPayload.getString("phoneno");
                                    strGender = jsonObjectPayload.getString("gender");
                                    strDOB = jsonObjectPayload.getString("birthdate");
                                    strAddress1 = jsonObjectPayload.getString("address1");
                                    strAddress2 = jsonObjectPayload.getString("address2");
                                    strCountry = jsonObjectPayload.getString("country");
                                    strState = jsonObjectPayload.getString("state");
                                    strCity = jsonObjectPayload.getString("city");
                                    strPincode = jsonObjectPayload.getString("postalcode");
                                    JSONArray jsonArray = jsonObjectPayload.getJSONArray("profilepicture");
                                    if (jsonArray.length() > 0) {
                                        strProfileImage = jsonObjectPayload.getJSONArray("profilepicture").getString(0);
                                    }

                                    if (!strFname.equals("")) {
                                        editFirstName.setText(strFname);
                                    } else {
                                        editFirstName.setText("");
                                    }
                                    if (!strLastname.equals("")) {
                                        editLastName.setText(strLastname);
                                    } else {
                                        editLastName.setText("");
                                    }
                                    if (!strEmail.equals("")) {
                                        editEmail.setEnabled(false);
                                        editEmail.setText(strEmail);
                                    } else {
                                        editEmail.setEnabled(true);
                                        editEmail.setText("");
                                    }
                                    if (!strPhoneNo.equals("")) {
                                        editPhoneNo.setText(strPhoneNo);
                                    } else {
                                        editPhoneNo.setText("");
                                    }
                                    if (!strCountry.equals("")) {
                                        actCountry.setText(strCountry);
                                        getStateAPI();
                                    } else {
                                        actCountry.setHint("Select Country");
                                    }
                                    if (!strState.equals("")) {
                                        actState.setText(strState);
                                    } else {
                                        actState.setHint("Select State");
                                        actState.setEnabled(false);
                                    }
                                    if (!strGender.equals("")) {
                                        if (strGender.equalsIgnoreCase("male")) {
                                            radioBtnMale.setChecked(true);
                                            radioBtnFemail.setChecked(false);
                                        } else {
                                            radioBtnFemail.setChecked(true);
                                            radioBtnMale.setChecked(false);
                                        }
                                    } else {
                                        radioBtnMale.setChecked(true);
                                    }
                                    if (!strDOB.equals("")) {
                                        txtDOB.setText(Global.changeDateFormate(strDOB, "yyyy-MM-dd", "dd-MM-yyyy"));
                                    } else {
                                        txtDOB.setHint("Date Of Birth");
                                    }
                                    if (!strAddress1.equals("") || !strAddress2.equals("")) {
                                        editAddress.setText(strAddress1 + " " + strAddress2);
                                    } else {
                                        editAddress.setText("");
                                    }
                                    if (!strCity.equals("")) {
                                        editCity.setText(strCity);
                                    } else {
                                        editCity.setText("");
                                    }
                                    if (!strPincode.equals("")) {
                                        editPincode.setText(strPincode);
                                    } else {
                                        editPincode.setText("");
                                    }
                                    if (!strProfileImage.equals("")) {
                                        imgUser.setVisibility(View.GONE);
                                        circularImageViewUser.setVisibility(View.VISIBLE);
//                                        String str = strProfileImage.replace("[", "").replace("]", "").replace("\"", "");
                                        Picasso.with(context)
                                                .load(strProfileImage)
                                                .into(circularImageViewUser, new com.squareup.picasso.Callback() {
                                                    @Override
                                                    public void onSuccess() {
                                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                                    }

                                                    @Override
                                                    public void onError() {
                                                        //holder.pbHome.setVisibility(View.INVISIBLE);
                                                    }
                                                });
                                    } else {
                                        /*circularImageViewUser.setImageResource(R.drawable.ic_user_profile);*/
                                        imgUser.setVisibility(View.VISIBLE);
                                        circularImageViewUser.setVisibility(View.VISIBLE);
                                    }
                                    SharedPreference.CreatePreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.SavePreference(Global.USER_Profile_Picture, strProfileImage);
                                    SharedPreference.SavePreference(Global.USER_Name, strFname + " " + strLastname);
                                    MainActivity.SetUserInfo();
                                }
                                getCountryAPI();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(context, "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ProfileActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region changeButton
    public void changeButton(Button button) {

        button.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        button.setBackgroundResource(R.drawable.ic_button);
        GradientDrawable gd = (GradientDrawable) button.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        /* gd.setStroke(2, Color.parseColor(StaticUtility.BORDERCOLOR));*/
    }
    //endregion

    //region changeEditTextBorder
    public void changeEditTextBorder(EditText editText) {
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
            editText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        }
        editText.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) editText.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#FFFFFF"));
        gd.setCornerRadius(60);
        gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
    }
    //endregion

    //region changeTextViewBorder
    public void changeTextViewBorder(TextView textView) {
        textView.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        textView.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) textView.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#FFFFFF"));
        gd.setCornerRadius(60);
        gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
    }
    //endregion

    // region change FramelayoutBorder Border
    public void changeFramelayoutBorder(FrameLayout frameLayout) {
        frameLayout.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) frameLayout.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#FFFFFF"));
        gd.setCornerRadius(60);
        gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
    }
    //endregion

    // region changeLinearBorder
    public void changeLinearBorder(LinearLayout linearLayout) {
        txtGender.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        linearLayout.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) linearLayout.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#FFFFFF"));
        /*gd.setColor(Color.parseColor("#000000"));*/
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
    }
    //endregion

    // region changeRadioButtonColor
    @SuppressLint("NewApi")
    public void changeRadioButtonColor(RadioButton radioButton) {
        radioButton.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        radioButton.setButtonTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor))));
    }
    //endregion

    //region Validation
    public Boolean Validation() {
        Boolean valid = false;
        if (!TextUtils.isEmpty(editFirstName.getText().toString())) {
            editFirstName.setError(null);
            if (!TextUtils.isEmpty(editLastName.getText().toString())) {
                editLastName.setError(null);
                if (!TextUtils.isEmpty(editPhoneNo.getText().toString())) {
                    editPhoneNo.setError(null);
                    if (editPhoneNo.length() >= 10) {
                        editPhoneNo.setError(null);
                        if (!TextUtils.isEmpty(txtDOB.getText().toString())) {
                            txtDOB.setError(null);
                            if (!TextUtils.isEmpty(editAddress.getText().toString())) {
                                editAddress.setError(null);
                                if (!TextUtils.isEmpty(editCity.getText().toString())) {
                                    editCity.setError(null);
                                    if (!TextUtils.isEmpty(editPincode.getText().toString())) {
                                        editPincode.setError(null);
                                        if (!TextUtils.isEmpty(actCountry.getText().toString())) {
                                            actCountry.setError(null);
                                            if (!TextUtils.isEmpty(actState.getText().toString())) {
                                                actState.setError(null);
                                                valid = true;
                                            } else {
                                                actState.setError("please Select State..!");
                                            }
                                        } else {
                                            actCountry.setError("Please Select Country..!");
                                        }

                                    } else {
                                        editPincode.setError("Please Enter Pincode..!");
                                    }
                                } else {
                                    editCity.setError("Please Enter City..!");
                                }
                            } else {
                                editAddress.setError("Please Select Address..!");
                            }
                        } else {
                            Toast.makeText(context, "Please Select User Date of Birth...!", Toast.LENGTH_SHORT).show();
//                            editDOB.setError("Please Select User Date of Birth...!");
                        }
                    } else {
                        editPhoneNo.setError("Please Enter Valid Phone Number...!");
                    }
                } else {
                    editPhoneNo.setError("Please Enter User Phone Number...!");
                }
            } else {
                editLastName.setError("Please Enter Last Name...!");
            }
        } else {
            editFirstName.setError("Please Enter First Name...!");
        }
        return valid;
    }
    //endregion

    //region IMAGE SET OF USER
    public void ImagePath(Bitmap bitmap, String strImageBase64, String FileName) {
        String type = null;
        if (bitmap == null) {
            circularImageViewUser.setImageResource(R.drawable.ic_user_profile);
        } else {
            circularImageViewUser.setImageBitmap(bitmap);
            if (FileName != null) {
                strFilename = FileName;
                String extension = MimeTypeMap.getFileExtensionFromUrl(strFilename);
                if (extension != null) {
                    type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                    strImagePath = "data:" + type + ";base64," + strImageBase64;
                }
            }

            /*else {
                strFilename = "profile_photo_" + System.currentTimeMillis() + ".jpg";
                String extension = MimeTypeMap.getFileExtensionFromUrl(strFilename);
                if (extension != null) {
                    type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                    strImagePath = "data:" + type + ";base64," + strImageBase64;
                }
            }*/

            try {
                jsonObjectImage = new JSONObject();
                jsonObjectImage.put(strFilename, strImagePath);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    //endregion

    //region FOR UpdateProfile API...
    private void UpdateProfile() {
        relativeProgress.setVisibility(View.VISIBLE);
        String[] key, val;
        if (ImageUser) {
            key = new String[]{"firstname", "lastname", "phoneno", "pincode", "gender", "address", "country", "state",
                    "city", "dateofbirth", "old_image"};
            val = new String[]{editFirstName.getText().toString(), editLastName.getText().toString(),
                    editPhoneNo.getText().toString(), editPincode.getText().toString(), strGender, editAddress.getText().toString(),
                    actCountry.getText().toString(), actState.getText().toString(), editCity.getText().toString(), Global.changeDateFormate(txtDOB.getText().toString(), "dd-MM-yyyy", "yyyy-MM-dd"), ""};
        } else {
            key = new String[]{"firstname", "lastname", "phoneno", "pincode", "gender", "address", "country", "state",
                    "city", "dateofbirth", "old_image"};
            val = new String[]{editFirstName.getText().toString(), editLastName.getText().toString(),
                    editPhoneNo.getText().toString(), editPincode.getText().toString(), strGender, editAddress.getText().toString(),
                    actCountry.getText().toString(), actState.getText().toString(), editCity.getText().toString(), Global.changeDateFormate(txtDOB.getText().toString(), "dd-MM-yyyy", "yyyy-MM-dd"), strProfileImage};
        }
        JSONObject jsonObject = Global.bodyParameter(key, val);
        try {
            jsonObject.put("profilepicture", jsonObjectImage);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.UpdateProfile);
        postRequestBuilder.addJSONObjectBody(jsonObject)
                .addHeaders(Global.headers1(context));
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {

                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("OK")) {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                    SharedPreference.CreatePreference(context, Global.LOGIN_PREFERENCE);
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    Object object = jsonObjectPayload.get("profileimage");
                                    if (!object.equals("")) {
                                        JSONArray jsonArrayImages = jsonObjectPayload.getJSONArray("profileimage");
                                        strProfileImage = String.valueOf(jsonArrayImages.get(0));
                                        if (!strProfileImage.equalsIgnoreCase("")) {
                                            SharedPreference.SavePreference(Global.USER_Profile_Picture, strProfileImage);
                                        }
                                    }
                                    MainActivity.SetUserInfo();
                                    MainActivity.manageBackPress(true);
                                    String fullname = jsonObjectPayload.getString("fullname");
                                    if (!fullname.equalsIgnoreCase("")) {
                                        SharedPreference.SavePreference(Global.USER_Name, fullname);
                                    }
                                    Intent intent = new Intent(context, MainActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    MainActivity.manageBackPress(true);
                                    Toast.makeText(context, "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ProfileActivity.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
//                Toast.makeText(context, anError.toString(), Toast.LENGTH_SHORT).show();
            }

        });
    }
    //endregion

    //region For EventBus onEvent
    public void onEvent(String event) {
        openInternetAlertDialog(context, event);
    }
    //endregion

    //region FOR SHOW INTERNET CONNECTION DIALOG...
    public void openInternetAlertDialog(final Context mContext, String alertString) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.row_alert_dialog, null);
        final TextView tvAlertText = (TextView) row.findViewById(R.id.tvAlertText);
        final TextView tvTitle = (TextView) row.findViewById(R.id.tvTitle);
        final Button btnSettings = (Button) row.findViewById(R.id.btnSettings);
        final Button btnExit = (Button) row.findViewById(R.id.btnExit);

        tvTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        tvAlertText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        btnSettings.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        btnExit.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        btnExit.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        btnSettings.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        tvTitle.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));


        tvAlertText.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        tvTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnSettings.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnExit.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

        try {
            if (alertString.equals("Not connected to Internet")) {
                if (i == 0) {
                    i = 1;
                    AlertDialog.Builder i_builder = new AlertDialog.Builder(mContext);
                    internetAlert = i_builder.create();
                    internetAlert.setCancelable(false);
                    internetAlert.setView(row);

                    if (internetAlert.isShowing()) {
                        internetAlert.dismiss();
                    } else {
                        internetAlert.show();
                    }

                    btnExit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            internetAlert.dismiss();
                            //FOR CLOSE APP...
                            System.exit(0);
                        }
                    });

                    btnSettings.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            /*internetAlert.dismiss();*/
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        }
                    });
                } else {
                    /*internetAlert.dismiss();*/
                }
            } else {
                i = 0;
                internetAlert.dismiss();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    //endregion
}
