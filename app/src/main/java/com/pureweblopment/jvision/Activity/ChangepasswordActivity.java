package com.pureweblopment.jvision.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.pureweblopment.jvision.Global.Global;
import com.pureweblopment.jvision.Global.SendMail;
import com.pureweblopment.jvision.Global.SharedPreference;
import com.pureweblopment.jvision.Global.StaticUtility;
import com.pureweblopment.jvision.Global.Typefaces;
import com.pureweblopment.jvision.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;
import okhttp3.Response;

public class ChangepasswordActivity extends AppCompatActivity implements View.OnClickListener {

    Context context = ChangepasswordActivity.this;

    ImageView imageCartBack;
    TextView txtCatName;
    ProgressBar progress;
    FrameLayout flToolbar;

    EditText editOldpassword, editNewpassword, editConfirmPwd;
    RelativeLayout relativeProgress;

    Button btnChangePWD;

    //Internet Alert
    public EventBus eventBus = EventBus.getDefault();
    public static int i = 0;
    public static AlertDialog internetAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        super.onCreate(savedInstanceState);
        eventBus.register(this);

        setContentView(R.layout.activity_changepassword);
        progress = (ProgressBar) findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

        Initialization();
        TypeFace();
        OnClickListener();

        txtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        flToolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtCatName.setText(R.string.changepwd);

        imageCartBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    onBackPressed();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });

        changeButton(btnChangePWD);
        changeEditTextBorder(editNewpassword);
        changeEditTextBorder(editConfirmPwd);
        changeEditTextBorder(editOldpassword);
    }

    //region Initialization
    private void Initialization() {
        flToolbar = (FrameLayout) findViewById(R.id.flToolbar);
        imageCartBack = (ImageView) findViewById(R.id.imageCartBack);
        txtCatName = (TextView) findViewById(R.id.txtCatName);
        editOldpassword = (EditText) findViewById(R.id.editOldpassword);
        editNewpassword = (EditText) findViewById(R.id.editNewpassword);
        editConfirmPwd = (EditText) findViewById(R.id.editConfirmPwd);
        btnChangePWD = (Button) findViewById(R.id.btnChangePWD);
        relativeProgress = (RelativeLayout) findViewById(R.id.relativeProgress);

    }
    //endregion

    //region TypeFace
    private void TypeFace() {
        editOldpassword.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editNewpassword.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editConfirmPwd.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnChangePWD.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
    }
    //endregion

    //region OnClickListener
    private void OnClickListener() {
        btnChangePWD.setOnClickListener(this);
    }
    //endregion

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnChangePWD:
                if (Validation()) {
                    ChangePassword();
                }
                break;
        }
    }

    //region Validation
    public Boolean Validation() {
        Boolean valid = false;
        if (!TextUtils.isEmpty(editOldpassword.getText().toString())) {
            editOldpassword.setError(null);
            if (!TextUtils.isEmpty(editNewpassword.getText().toString())) {
                editNewpassword.setError(null);
                if (editNewpassword.length() >= 6) {
                    editNewpassword.setError(null);
                    if (!TextUtils.isEmpty(editConfirmPwd.getText().toString())) {
                        editConfirmPwd.setError(null);
                        if (editConfirmPwd.length() >= 6) {
                            editNewpassword.setError(null);
                            if (!editOldpassword.getText().toString().equalsIgnoreCase(editNewpassword.getText().toString())) {
                                if (editConfirmPwd.getText().toString().equals(editNewpassword.getText().toString())) {
                                    valid = true;
                                } else {
                                    editConfirmPwd.setError("Confirm Password not match..!");
                                }
                            } else {
                                Toast.makeText(context, "New Password should not be same as old password..!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            editConfirmPwd.setError("Password must have at least 6 character..!");
                        }
                    } else {
                        editConfirmPwd.setError("Please enter Confirm Password..!");
                    }
                } else {
                    editNewpassword.setError("Password must have at least 6 character..!");
                }
            } else {
                editNewpassword.setError("Please enter New Password..!");
            }
        } else {
            editOldpassword.setError("Please enter Old Password..!");
        }
        return valid;
    }
    //endregion

    //region FOR ChangePassword...
    private void ChangePassword() {
        relativeProgress.setVisibility(View.VISIBLE);
        String[] key = {"password", "newpassword", "confirmpassword"};
        String[] val = {editOldpassword.getText().toString(), editNewpassword.getText().toString(),
                editConfirmPwd.getText().toString()};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.ChangePWD);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            String strStatus = response.optString("status");
                            String strMessage = response.optString("message");
                            if (strStatus.equalsIgnoreCase("ok")) {
                                editOldpassword.setText("");
                                editNewpassword.setText("");
                                editConfirmPwd.setText("");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                SharedPreference.ClearPreference(context, Global.ISCheck);
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.putExtra("Redirect", "myaccount");
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                        Intent.FLAG_ACTIVITY_NEW_TASK |
                                        Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }
                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    MainActivity.manageBackPress(true);
                                    Toast.makeText(context, "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ChangePassword.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region chanageButton
    public void changeButton(Button button) {
        button.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        button.setBackgroundResource(R.drawable.ic_button);
        GradientDrawable gd = (GradientDrawable) button.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        /* gd.setStroke(2, Color.parseColor(StaticUtility.BORDERCOLOR));*/
    }
    //endregion

    //region chanageEditTextBorder
    public void changeEditTextBorder(EditText editText) {
        editText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editText.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) editText.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#FFFFFF"));
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
    }
    //endregion

    //region For EventBus onEvent
    public void onEvent(String event) {
        openInternetAlertDialog(context, event);
    }
    //endregion

    //region FOR SHOW INTERNET CONNECTION DIALOG...
    public void openInternetAlertDialog(final Context mContext, String alertString) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.row_alert_dialog, null);
        final TextView tvAlertText = (TextView) row.findViewById(R.id.tvAlertText);
        final TextView tvTitle = (TextView) row.findViewById(R.id.tvTitle);
        final Button btnSettings = (Button) row.findViewById(R.id.btnSettings);
        final Button btnExit = (Button) row.findViewById(R.id.btnExit);

        tvTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        tvAlertText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        btnSettings.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        btnExit.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        btnExit.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        btnSettings.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        tvTitle.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));


        tvAlertText.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        tvTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnSettings.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnExit.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

        try {
            if (alertString.equals("Not connected to Internet")) {
                if (i == 0) {
                    i = 1;
                    AlertDialog.Builder i_builder = new AlertDialog.Builder(mContext);
                    internetAlert = i_builder.create();
                    internetAlert.setCancelable(false);
                    internetAlert.setView(row);

                    if (internetAlert.isShowing()) {
                        internetAlert.dismiss();
                    } else {
                        internetAlert.show();
                    }

                    btnExit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            internetAlert.dismiss();
                            //FOR CLOSE APP...
                            System.exit(0);
                        }
                    });

                    btnSettings.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            /*internetAlert.dismiss();*/
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        }
                    });
                } else {
                    /*internetAlert.dismiss();*/
                }
            } else {
                i = 0;
                internetAlert.dismiss();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region ON ACTIVITY RESULT FOR DISMISS OR SHOW INTERNET ALERT DIALOG...
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (!Global.isNetworkAvailable(context)) {
                openInternetAlertDialog(context, "Not connected to Internet");
            } else {
                internetAlert.dismiss();
            }
        }
    }
    //endregion
}
