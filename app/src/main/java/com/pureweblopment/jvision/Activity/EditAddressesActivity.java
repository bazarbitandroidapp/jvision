package com.pureweblopment.jvision.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.pureweblopment.jvision.Global.Global;
import com.pureweblopment.jvision.Global.SendMail;
import com.pureweblopment.jvision.Global.SharedPreference;
import com.pureweblopment.jvision.Global.StaticUtility;
import com.pureweblopment.jvision.Global.Typefaces;
import com.pureweblopment.jvision.Model.Country;
import com.pureweblopment.jvision.Model.SpinnerItem;
import com.pureweblopment.jvision.Model.State;
import com.pureweblopment.jvision.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;
import okhttp3.Response;


public class EditAddressesActivity extends AppCompatActivity
        implements View.OnClickListener {

    Context context = EditAddressesActivity.this;

    TextView txtAddressType;
    Button btnUpdate;

    EditText editFirstName, editLastName, editAddress, editLandmark, editPincode, editCity, editPhoneNo;

    String strFname, strLname, strAddress, strLandmark, strPincode, strCity, strPhoneNo,
            strAddressType, struseradressid, Position;

    String strCountry = "", strState = "";

    RelativeLayout relativeProgress;
    CoordinatorLayout coodinator;

    LinearLayout llPaymentHeader, llToolbar;
    String ActivityType;

    AutoCompleteTextView actCountry, actState;

    private Country country;
    private State state;
    ArrayList<Country> countries = new ArrayList<>();
    ArrayList<State> states = new ArrayList<>();

    TextView txtAddressName;

    FrameLayout flCountry, flState;
    ImageView imageCartBack;

    //Internet Alert
    public EventBus eventBus = EventBus.getDefault();
    public static int i = 0;
    public static AlertDialog internetAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_edit_addresses);

        eventBus.register(this);

        ProgressBar progress = (ProgressBar) findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

        ActivityType = getIntent().getStringExtra("ActivityType");

        Initialization();
        TypeFace();
        OnClickListener();
        AppSetting();

        if (ActivityType.equalsIgnoreCase("CheckoutFragment")) {
            try {
                llPaymentHeader.setVisibility(View.VISIBLE);
                llToolbar.setVisibility(View.VISIBLE);
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                    llPaymentHeader.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                }
            } catch (NullPointerException e) {

            }
        } else {
            llPaymentHeader.setVisibility(View.GONE);
            llToolbar.setVisibility(View.VISIBLE);
        }

        strFname = getIntent().getStringExtra("Fname");
        strLname = getIntent().getStringExtra("Lname");
        strAddress = getIntent().getStringExtra("Address");
        strLandmark = getIntent().getStringExtra("Landmark");
        strPincode = getIntent().getStringExtra("Pincode");
        strCountry = getIntent().getStringExtra("Country");
        strState = getIntent().getStringExtra("State");
        strCity = getIntent().getStringExtra("City");
        strPhoneNo = getIntent().getStringExtra("Phoneno");
        strAddressType = getIntent().getStringExtra("addressType");
        struseradressid = getIntent().getStringExtra("useradress_id");
        Position = getIntent().getStringExtra("Position");

        chanageEditTextBorder(editFirstName);
        chanageEditTextBorder(editLastName);
        chanageEditTextBorder(editAddress);
        chanageEditTextBorder(editLandmark);
        chanageEditTextBorder(editPincode);
        chanageEditTextBorder(editCity);
        chanageEditTextBorder(editPhoneNo);
        chanageButton(btnUpdate);
        chanageFramelayoutBorder(flCountry, actCountry);
        chanageFramelayoutBorder(flState, actState);

        if (strAddressType.equals("0")) {
            txtAddressType.setVisibility(View.VISIBLE);
            txtAddressType.setText("Shipping Address");
        } else if (strAddressType.equals("1")) {
            txtAddressType.setVisibility(View.VISIBLE);
            txtAddressType.setText("Billing Address");
        } else {
            txtAddressType.setVisibility(View.GONE);
        }

        editFirstName.setText(strFname);
        editLastName.setText(strLname);
        editAddress.setText(strAddress);
        editLandmark.setText(strLandmark);
        editPincode.setText(strPincode);
        editCity.setText(strCity);
        editPhoneNo.setText(strPhoneNo);
        actCountry.setText(strCountry);
        actState.setText(strState);

        getCountryAPI();
        if (actCountry.length() > 0) {
            actState.setEnabled(true);
            getStateAPI();
        } else {
            actState.setEnabled(false);
        }

        actCountry.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    actState.setEnabled(true);
                    getStateAPI();
                } else {
                    actState.setEnabled(false);
                    actState.setText("");
                    actState.setHint("Select State");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    //region Initialization
    private void Initialization() {
        editFirstName = (EditText) findViewById(R.id.editFirstName);
        editLastName = (EditText) findViewById(R.id.editLastName);
        editAddress = (EditText) findViewById(R.id.editAddress);
        editLandmark = (EditText) findViewById(R.id.editLandmark);
        editPincode = (EditText) findViewById(R.id.editPincode);
        editCity = (EditText) findViewById(R.id.editCity);
        editPhoneNo = (EditText) findViewById(R.id.editPhoneNo);

        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        txtAddressType = (TextView) findViewById(R.id.txtAddressType);
        relativeProgress = (RelativeLayout) findViewById(R.id.relativeProgress);

        coodinator = (CoordinatorLayout) findViewById(R.id.coodinator);
        llPaymentHeader = (LinearLayout) findViewById(R.id.llPaymentHeader);
        llToolbar = (LinearLayout) findViewById(R.id.llToolbar);

        actCountry = (AutoCompleteTextView) findViewById(R.id.actCountry);
        actState = (AutoCompleteTextView) findViewById(R.id.actState);

        txtAddressName = (TextView) findViewById(R.id.txtAddressName);

        flCountry = (FrameLayout) findViewById(R.id.flCountry);
        flState = (FrameLayout) findViewById(R.id.flState);

        imageCartBack = (ImageView) findViewById(R.id.imageCartBack);
    }
    //endregion

    //region TypeFace
    private void TypeFace() {
        editFirstName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editLastName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editLandmark.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editPincode.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editCity.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editPhoneNo.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnUpdate.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtAddressType.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtAddressName.setTypeface(Typefaces.TypefaceCalibri_bold(context));
    }
    //endregion

    //region OnClickListener
    private void OnClickListener() {
        btnUpdate.setOnClickListener(this);
        actCountry.setOnClickListener(this);
        actState.setOnClickListener(this);
        imageCartBack.setOnClickListener(this);
    }
    //endregion

    //region AppSetting
    private void AppSetting() {
        editFirstName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editLastName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editLandmark.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editPincode.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editCity.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editPhoneNo.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtAddressType.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));

        llToolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        llPaymentHeader.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtAddressName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        imageCartBack.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

    }
    //endregion


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnUpdate:
                if (Validation()) {
                    InputMethodManager imm = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(
                            btnUpdate.getWindowToken(), 0);
                    EditAddress();
                }
                break;

            case R.id.actCountry:
                actCountry.showDropDown();
                break;

            case R.id.actState:
                actState.showDropDown();
                break;
            case R.id.imageCartBack:
                onBackPressed();
                finish();
                break;
        }
    }

    //region FOR GET Country API...
    private void getCountryAPI() {
        String[] key = {};
        String[] val = {};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.Countries);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
//                                Toast.makeText(context, strMessage , Toast.LENGTH_SHORT).show();
                                countries = new ArrayList<>();
                                if (strStatus.equalsIgnoreCase("OK")) {
                                    JSONArray payloadArray = response.getJSONArray("payload");
                                    for (int i = 0; i < payloadArray.length(); i++) {
                                        JSONObject jsonObjectProduct = payloadArray.getJSONObject(i);
                                        String CountryCode = jsonObjectProduct.getString("country_id");
                                        String Countryname = jsonObjectProduct.getString("country_name");

                                        country = new Country();
                                        country.setCountryName(Countryname);
                                        country.setCountryId(CountryCode);
                                        countries.add(country);
                                    }
                                    if (countries.size() > 0) {
                                        AutoCompleteForCountryAdapter autoCompleteForCountryAdapter = new AutoCompleteForCountryAdapter(context, R.layout.row_items, countries, actCountry);
                                        actCountry.setAdapter(autoCompleteForCountryAdapter);
                                    }
                                }
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    MainActivity.manageBackPress(true);
                                    Toast.makeText(context, "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    /*finish();*/
                                } else {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in EditAddressesFragment.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
//                Toast.makeText(context, anError.toString(), Toast.LENGTH_SHORT).show();
            }

        });
    }
    //endregion

    //region AUTO COMPLETE ADAPTER For COUNTRY...
    public class AutoCompleteForCountryAdapter extends ArrayAdapter<Country> {
        Context mContext;
        ArrayList<Country> mDepartments;
        ArrayList<Country> mDepartments_All;
        ArrayList<Country> mDepartments_Suggestion;
        int mLayoutResourceId;
        private AutoCompleteTextView autoCompleteTextView;

        public AutoCompleteForCountryAdapter(Context context, int resource, ArrayList<Country> departments, AutoCompleteTextView autoCompleteTextView) {
            super(context, resource, departments);
            this.mContext = context;
            this.mLayoutResourceId = resource;
            this.mDepartments = new ArrayList<>(departments);
            this.mDepartments_All = new ArrayList<>(departments);
            this.mDepartments_Suggestion = new ArrayList<>();
            this.autoCompleteTextView = autoCompleteTextView;
        }

        public int getCount() {
            return mDepartments.size();
        }

        public Country getItem(int position) {
            return mDepartments.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                    convertView = inflater.inflate(mLayoutResourceId, parent, false);
                }
                final Country department = getItem(position);
                TextView name = (TextView) convertView.findViewById(R.id.ItemName);
                TextView id = (TextView) convertView.findViewById(R.id.ItemId);
                name.setText(department.getCountryName());
                name.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                /*id.setText(department.getCountryName());*/

                autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Country pi = getItem(position);
                        strCountry = pi.getCountryName();
                        if (!strCountry.equals("")) {
                            getStateAPI();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                public String convertResultToString(Object resultValue) {
                    return ((Country) resultValue).getCountryName();
                }

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    if (constraint != null) {
                        mDepartments_Suggestion.clear();
                        for (Country department : mDepartments_All) {
                            if (department.getCountryName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                                mDepartments_Suggestion.add(department);
                            }
                        }
                        FilterResults filterResults = new FilterResults();
                        filterResults.values = mDepartments_Suggestion;
                        filterResults.count = mDepartments_Suggestion.size();
                        return filterResults;
                    } else {
                        return new FilterResults();
                    }
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    mDepartments.clear();
                    if (results != null && results.count > 0) {
                        // avoids unchecked cast warning when using mDepartments.addAll((ArrayList<Department>) results.values);
                        List<?> result = (List<?>) results.values;
                        for (Object object : result) {
                            if (object instanceof Country) {
                                mDepartments.add((Country) object);
                            }
                        }
                    } else if (constraint == null) {
                        // no filter, add entire original list back in
                        mDepartments.addAll(mDepartments_All);
                    }
                    notifyDataSetChanged();
                }
            };
        }
    }
    //endregion

    //region FOR GET State API...
    private void getStateAPI() {
        String[] key = {"country_name"};
        String[] val = {actCountry.getText().toString()};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.States);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
//                                Toast.makeText(context, strMessage , Toast.LENGTH_SHORT).show();
                                if (strStatus.equalsIgnoreCase("OK")) {
                                    JSONArray payloadArray = response.getJSONArray("payload");
                                    for (int i = 1; i < payloadArray.length(); i++) {
                                        JSONObject jsonObjectProduct = payloadArray.getJSONObject(i);
                                        String country_id = jsonObjectProduct.getString("country_id");
                                        String StateId = jsonObjectProduct.getString("state_id");
                                        String Statename = jsonObjectProduct.getString("state_name");
                                        String status = jsonObjectProduct.getString("status");

                                        state = new State();
                                        state.setStateName(Statename);
                                        state.setStateId(StateId);
                                        state.setCountry_id(country_id);
                                        state.setStatus(status);
                                        states.add(state);
                                    }
                                    if (states.size() > 0) {
                                        AutoCompleteForStateAdapter autoCompleteForStateAdapter = new AutoCompleteForStateAdapter(context, R.layout.row_items, states, actState);
                                        actState.setAdapter(autoCompleteForStateAdapter);
                                    }
                                }
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in EditAddressesFragment.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
//                Toast.makeText(context, anError.toString(), Toast.LENGTH_SHORT).show();
            }

        });
    }
    //endregion

    //region AUTO COMPLETE ADAPTER FOR STATE...
    public class AutoCompleteForStateAdapter extends ArrayAdapter<State> {
        Context mContext;
        ArrayList<State> mDepartments;
        ArrayList<State> mDepartments_All;
        ArrayList<State> mDepartments_Suggestion;
        int mLayoutResourceId;
        private AutoCompleteTextView autoCompleteTextView;

        public AutoCompleteForStateAdapter(Context context, int resource, ArrayList<State> departments, AutoCompleteTextView autoCompleteTextView) {
            super(context, resource, departments);
            this.mContext = context;
            this.mLayoutResourceId = resource;
            this.mDepartments = new ArrayList<>(departments);
            this.mDepartments_All = new ArrayList<>(departments);
            this.mDepartments_Suggestion = new ArrayList<>();
            this.autoCompleteTextView = autoCompleteTextView;
        }

        public int getCount() {
            return mDepartments.size();
        }

        public State getItem(int position) {
            return mDepartments.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                    convertView = inflater.inflate(mLayoutResourceId, parent, false);
                }
                final State department = getItem(position);
                TextView name = (TextView) convertView.findViewById(R.id.ItemName);
                TextView id = (TextView) convertView.findViewById(R.id.ItemId);
                name.setText(department.getStateName());
                name.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                /*id.setText(department.getCountryName());*/

                autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        State pi = getItem(position);
                        strState = pi.getStateName();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                public String convertResultToString(Object resultValue) {
                    return ((State) resultValue).getStateName();
                }

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    if (constraint != null) {
                        mDepartments_Suggestion.clear();
                        for (State department : mDepartments_All) {
                            if (department.getStateName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                                mDepartments_Suggestion.add(department);
                            }
                        }
                        FilterResults filterResults = new FilterResults();
                        filterResults.values = mDepartments_Suggestion;
                        filterResults.count = mDepartments_Suggestion.size();
                        return filterResults;
                    } else {
                        return new FilterResults();
                    }
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    mDepartments.clear();
                    if (results != null && results.count > 0) {
                        // avoids unchecked cast warning when using mDepartments.addAll((ArrayList<Department>) results.values);
                        List<?> result = (List<?>) results.values;
                        for (Object object : result) {
                            if (object instanceof State) {
                                mDepartments.add((State) object);
                            }
                        }
                    } else if (constraint == null) {
                        // no filter, add entire original list back in
                        mDepartments.addAll(mDepartments_All);
                    }
                    notifyDataSetChanged();
                }
            };
        }
    }
    //endregion

    //region Filter
    private String filter(ArrayList<SpinnerItem> models, Spinner spinner, String query) {
        final String lowerCaseQuery = query.toLowerCase();
        String selectedQuery = "British Indian Ocean Territory";
        final String strSelectedQuery = selectedQuery.toLowerCase();

        String filtered = "";
        for (int i = 1; i < models.size(); i++) {
            final String text = models.get(i).getCountryname().toLowerCase();
            if (text.contains(lowerCaseQuery))
                if (text.contains(strSelectedQuery)) {
                    spinner.setSelection(i - 1);
                } else {
                    spinner.setSelection(i);
                }
        }
        return filtered;
    }
    //endregion

    //region FOR Edit Address...
    private void EditAddress() {
        relativeProgress.setVisibility(View.VISIBLE);
        String[] key = {};
        String[] val = {};
        key = new String[]{"useraddress_id", "first_name", "last_name", "address", "landmark", "pincode", "country",
                "state", "city", "phone_number"};
        val = new String[]{struseradressid, editFirstName.getText().toString(), editLastName.getText().toString(), editAddress.getText().toString(),
                editLandmark.getText().toString(), editPincode.getText().toString(), strCountry, strState, editCity.getText().toString(),
                editPhoneNo.getText().toString()};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.EditAddress);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("ok")) {
                                    onBackPressed();
                                    finish();
                                    if (ActivityType.equalsIgnoreCase("CheckoutFragment")) {
                                        CheckoutAddressListActivity.EditAddresses(editFirstName.getText().toString(),
                                                editLastName.getText().toString(),
                                                editAddress.getText().toString(),
                                                editLandmark.getText().toString(),
                                                editPincode.getText().toString(),
                                                strCountry, strState,
                                                editCity.getText().toString(),
                                                editPhoneNo.getText().toString(),
                                                ActivityType, struseradressid, Position);
                                    } else {
                                        MyAccountAddressListActivity.EditAddresses(editFirstName.getText().toString(),
                                                editLastName.getText().toString(),
                                                editAddress.getText().toString(),
                                                editLandmark.getText().toString(),
                                                editPincode.getText().toString(),
                                                strCountry, strState,
                                                editCity.getText().toString(),
                                                editPhoneNo.getText().toString(),
                                                ActivityType, struseradressid, Position);
                                    }

                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
//                                    getActivity().onBackPressed();
                                    /*if (ActivityType.equalsIgnoreCase("MainActivity")) {
                                        mListener = (EditAddressesFragment.OnFragmentInteractionListener) context;
                                        mListener.gotoAddressListing();
                                    } else if(ActivityType.equalsIgnoreCase("MyAddressActivity")){
                                        mListener = (EditAddressesFragment.OnFragmentInteractionListener) context;
                                        if (strAddressType.equals("0")) {
                                            mListener.gotoAddressListingCheckout("0", "Shipping Address");
                                        } else if (strAddressType.equals("1")) {
                                            mListener.gotoAddressListingCheckout("1", "Billing Address");
                                        }
                                    }else {
                                        mListener = (EditAddressesFragment.OnFragmentInteractionListener) context;
                                        mListener.gotoMyAccountAddressListing();
                                    }*/
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(context, "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    /*finish();*/
                                } else {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in EditAddressesFragment.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
//                Toast.makeText(context, anError.toString(), Toast.LENGTH_SHORT).show();
            }

        });
    }
    //endregion

    //region Validation
    public Boolean Validation() {
        Boolean valid = false;
        if (!TextUtils.isEmpty(editFirstName.getText().toString())) {
            editFirstName.setError(null);
            if (!TextUtils.isEmpty(editLastName.getText().toString())) {
                editLastName.setError(null);
                if (!TextUtils.isEmpty(editAddress.getText().toString())) {
                    editAddress.setError(null);
                    if (!TextUtils.isEmpty(editLandmark.getText().toString())) {
                        editLandmark.setError(null);
                        if (!TextUtils.isEmpty(editPincode.getText().toString())) {
                            editPincode.setError(null);
                            if (!TextUtils.isEmpty(editCity.getText().toString())) {
                                editCity.setError(null);
                                if (!TextUtils.isEmpty(editPhoneNo.getText().toString())) {
                                    editPhoneNo.setError(null);
                                    if (editPhoneNo.length() >= 10) {
                                        editPhoneNo.setError(null);
                                        if (!strCountry.equals("Please Select")) {
                                            if (!strState.equals("Please Select")) {
                                                valid = true;
                                            } else {
                                                Toast.makeText(context, "Please Select State", Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            Toast.makeText(context, "Please Select Country", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        editPhoneNo.setError("Please Enter Valid Phone Number!");
                                    }
                                } else {
                                    editPhoneNo.setError("Please Enter Phone Number!");
                                }
                            } else {
                                editCity.setError("Please Enter City!");
                            }
                        } else {
                            editPincode.setError("Please Enter Pincode!");
                        }
                    } else {
                        editLandmark.setError("Please Enter LandMark!");
                    }
                } else {
                    editAddress.setError("Please Enter Address!");
                }
            } else {
                editLastName.setError("Please Enter User Last Name!");
            }
        } else {
            editFirstName.setError("Please Enter User First Name!");
        }
        return valid;
    }
    //endregion

    //region chanageEditTextBorder
    public void chanageEditTextBorder(EditText editText) {
        editText.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) editText.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#FFFFFF"));
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
    }
    //endregion

    //region chanageButton
    public void chanageButton(Button button) {
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor) != "") {
            button.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        }
        button.setBackgroundResource(R.drawable.ic_button);
        GradientDrawable gd = (GradientDrawable) button.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        /* gd.setStroke(2, Color.parseColor(StaticUtility.BORDERCOLOR));*/
    }
    //endregion

    //region chanageFramelayoutBorder
    public void chanageFramelayoutBorder(FrameLayout frameLayout, AutoCompleteTextView autoCompleteTextView) {
        autoCompleteTextView.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        frameLayout.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) frameLayout.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setCornerRadius(60);
        gd.setColor(Color.parseColor("#FFFFFF"));
        gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

    }
    //endregion

    //region For EventBus onEvent
    public void onEvent(String event) {
        openInternetAlertDialog(context, event);
    }
    //endregion

    //region FOR SHOW INTERNET CONNECTION DIALOG...
    public void openInternetAlertDialog(final Context mContext, String alertString) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.row_alert_dialog, null);
        final TextView tvAlertText = (TextView) row.findViewById(R.id.tvAlertText);
        final TextView tvTitle = (TextView) row.findViewById(R.id.tvTitle);
        final Button btnSettings = (Button) row.findViewById(R.id.btnSettings);
        final Button btnExit = (Button) row.findViewById(R.id.btnExit);

        tvTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        tvAlertText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        btnSettings.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        btnExit.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        btnExit.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        btnSettings.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        tvTitle.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));


        tvAlertText.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        tvTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnSettings.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnExit.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

        try {
            if (alertString.equals("Not connected to Internet")) {
                if (i == 0) {
                    i = 1;
                    AlertDialog.Builder i_builder = new AlertDialog.Builder(mContext);
                    internetAlert = i_builder.create();
                    internetAlert.setCancelable(false);
                    internetAlert.setView(row);

                    if (internetAlert.isShowing()) {
                        internetAlert.dismiss();
                    } else {
                        internetAlert.show();
                    }

                    btnExit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            internetAlert.dismiss();
                            //FOR CLOSE APP...
                            System.exit(0);
                        }
                    });

                    btnSettings.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            /*internetAlert.dismiss();*/
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        }
                    });
                } else {
                    /*internetAlert.dismiss();*/
                }
            } else {
                i = 0;
                internetAlert.dismiss();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region ON ACTIVITY RESULT FOR DISMISS OR SHOW INTERNET ALERT DIALOG...
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (!Global.isNetworkAvailable(context)) {
                openInternetAlertDialog(context, "Not connected to Internet");
            } else {
                internetAlert.dismiss();
            }
        }
    }
    //endregion

}
