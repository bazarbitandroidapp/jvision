package com.pureweblopment.jvision.Fragment;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.pureweblopment.jvision.Activity.MainActivity;
import com.pureweblopment.jvision.Global.Global;
import com.pureweblopment.jvision.Global.SharedPreference;
import com.pureweblopment.jvision.Global.Typefaces;
import com.pureweblopment.jvision.Model.ChildCategory;
import com.pureweblopment.jvision.Model.SubCategory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.Executors;

import com.pureweblopment.jvision.Global.SendMail;
import com.pureweblopment.jvision.Global.StaticUtility;
import com.pureweblopment.jvision.Model.MegaMenu;

import com.pureweblopment.jvision.R;

import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MegaMenuCategoryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MegaMenuCategoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MegaMenuCategoryFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ExpandableListView expantableListview;
    RelativeLayout relativeProgress;

    LinearLayout llBottomNavigation;
    ImageView imageCartBack, imageNavigation, imageLogo;
    FrameLayout frameLayoutCart;
    TextView txtCatName;

    ArrayList<MegaMenu> megaMenus;
    ArrayList<ChildCategory> childCategories;
    ArrayList<SubCategory> subCategories;
    HashMap<MegaMenu, ArrayList<ChildCategory>> listHashMap;
    HashMap<ChildCategory, ArrayList<SubCategory>> listHashMapThird;
    String menuname;
    String[] stringsParent, stringsChilds, stringsSubChilds;
    String[] stringsSlugChilds, stringsSlugSubChilds;

    LinkedHashMap<String, String[]> thirdLevel = new LinkedHashMap<>();
    ArrayList<String[]> secondLevel = new ArrayList<>();
    ArrayList<LinkedHashMap<String, String[]>> firstLeveldata = new ArrayList<>();
    LinkedHashMap<String, String[]> SlugthirdLevel = new LinkedHashMap<>();
    ArrayList<String[]> SlugsecondLevel = new ArrayList<>();
    ArrayList<LinkedHashMap<String, String[]>> SlugfirstLeveldata = new ArrayList<>();
    boolean isthridleveldata = true;

    private OnFragmentInteractionListener mListener;

    int count = 0;
    int width;


    public MegaMenuCategoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MegaMenuCategoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MegaMenuCategoryFragment newInstance(String param1, String param2) {
        MegaMenuCategoryFragment fragment = new MegaMenuCategoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        MainActivity.manageBackPress(false);
        View view = inflater.inflate(R.layout.fragment_mega_menu_category, container, false);

        secondLevel.clear();
        firstLeveldata.clear();
        SlugsecondLevel.clear();
        SlugfirstLeveldata.clear();
        count = 0;

        ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

        imageCartBack = (ImageView) getActivity().findViewById(R.id.imageCartBack);
        imageNavigation = (ImageView) getActivity().findViewById(R.id.imageNavigation);
        imageLogo = (ImageView) getActivity().findViewById(R.id.imageLogo);

        frameLayoutCart = (FrameLayout) getActivity().findViewById(R.id.frameLayoutCart);
        txtCatName = (TextView) getActivity().findViewById(R.id.txtCatName);
        llBottomNavigation = (LinearLayout) getActivity().findViewById(R.id.llBottomNavigation);

        imageCartBack.setVisibility(View.GONE);
        txtCatName.setVisibility(View.GONE);

        llBottomNavigation.setVisibility(View.VISIBLE);
        imageNavigation.setVisibility(View.VISIBLE);
        imageLogo.setVisibility(View.VISIBLE);
        frameLayoutCart.setVisibility(View.VISIBLE);

        Initialization(view);

        getMegaMenu();
        return view;
    }

    //region Initialization
    private void Initialization(View view) {
        expantableListview = (ExpandableListView) view.findViewById(R.id.expantableListview);
        relativeProgress = (RelativeLayout) view.findViewById(R.id.relativeProgress);
//        expantableListview.setGroupIndicator(null);
//        expantableListview.setIndicatorBounds(expantableListview.getRight()-GetDipsFromPixel(35), width-GetDipsFromPixel(5));
    }
    //endregion

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void gotoProductListing(String category_slug, String CatName);
    }

    //region FOR getMegaMenu API..
    private void getMegaMenu() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetMegaMenu);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    int a = 0;
//                                    listHashMap = new HashMap<>();
                                    JSONArray jsonArrayPayload = response.getJSONArray("payload");
//                                    megaMenus = new ArrayList<>();
                                    stringsParent = new String[jsonArrayPayload.length()];
                                    for (int i = 0; i < jsonArrayPayload.length(); i++) {
                                        JSONObject jsonObjectPayload = jsonArrayPayload.getJSONObject(i);
                                        int megamenu_fk = Integer.parseInt(jsonObjectPayload.getString("z_megamenuid_fk"));
                                        if (megamenu_fk > 0) {
                                            menuname = jsonObjectPayload.getString("menuname");
//                                            a++;
                                            if (jsonObjectPayload.has("child")) {
//                                                childCategories = new ArrayList<>();
                                                JSONArray jsonArrayChild = jsonObjectPayload.getJSONArray("child");
                                                stringsChilds = new String[jsonArrayChild.length()];
                                                stringsSlugChilds = new String[jsonArrayChild.length()];
                                                thirdLevel = new LinkedHashMap<>();
                                                SlugthirdLevel = new LinkedHashMap<>();
                                                for (int j = 0; j < jsonArrayChild.length(); j++) {
//                                                    secondLevel = new ArrayList<>();
                                                    JSONObject jsonObjectChild = jsonArrayChild.getJSONObject(j);
                                                    String childcategoryname = jsonObjectChild.getString("categoryname");
                                                    String childslug = jsonObjectChild.getString("slug");
                                                    stringsChilds[j] = childcategoryname;
                                                    stringsSlugChilds[j] = childslug;
                                                    if (jsonArrayChild.length() - 1 == j) {
                                                        secondLevel.add(stringsChilds);
                                                        SlugsecondLevel.add(stringsSlugChilds);
                                                    }
//                                                    secondLevel1.add();
//                                                    childCategories.add(new ChildCategory(childcategoryname, childslug, jsonArraySubCategory));
                                                    JSONArray jsonArraySubCategory = jsonObjectChild.getJSONArray("subcat");
                                                    stringsSubChilds = new String[jsonArraySubCategory.length()];
                                                    stringsSlugSubChilds = new String[jsonArraySubCategory.length()];
                                                    if (jsonArraySubCategory.length() > 0) {
//                                                        subCategories = new ArrayList<>();
                                                        for (int k = 0; k < jsonArraySubCategory.length(); k++) {
                                                            JSONObject jsonObjectSubCategory = jsonArraySubCategory.getJSONObject(k);
                                                            String name = jsonObjectSubCategory.getString("name");
                                                            String slug = jsonObjectSubCategory.getString("slug");
                                                            String category_id = jsonObjectSubCategory.getString("category_id");
                                                            stringsSubChilds[k] = name;
                                                            stringsSlugSubChilds[k] = slug;
//                                                            subCategories.add(new SubCategory(name, slug, category_id));
                                                        }
                                                    }
                                                    thirdLevel.put(stringsChilds[j], stringsSubChilds);
                                                    SlugthirdLevel.put(stringsSlugChilds[j], stringsSlugSubChilds);
                                                   /* stringsChilds =  new String[]{" "};
                                                    stringsSubChilds =  new String[]{" "};*/
                                                }
                                            }
                                        /* megaMenus.add(new MegaMenu(menuname, childCategories));
                                            listHashMap.put(megaMenus.get(a), childCategories);
                                            a++;*/
                                            stringsParent[a] = menuname;
                                            firstLeveldata.add(a, thirdLevel);
                                            SlugfirstLeveldata.add(a, SlugthirdLevel);
                                            a++;
                                            ThreeLevelListAdapter threeLevelListAdapterAdapter = new
                                                    ThreeLevelListAdapter(getContext(), stringsParent, secondLevel, firstLeveldata,
                                                    SlugsecondLevel, SlugfirstLeveldata);
                                            expantableListview.setAdapter(threeLevelListAdapterAdapter);
                                        } else {
                                            count++;
                                        }

                                    }
                                    /*if (megaMenus.size() > 0) {
                                        expantableListview.setVisibility(View.VISIBLE);
                                        ExpandableListAdapter expandableListAdapter = new ExpandableListAdapter(getContext(), megaMenus, listHashMap);
                                        expantableListview.setAdapter(expandableListAdapter);
                                    }*/
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in MegaMenuCategoryFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region ThreeLevelListAdapter
    public class ThreeLevelListAdapter extends BaseExpandableListAdapter {

        String[] parentHeaders;
        ArrayList<String[]> secondLevel;
        private Context context;
        ArrayList<LinkedHashMap<String, String[]>> data;
        ArrayList<String[]> SlugsecondLevel;
        ArrayList<LinkedHashMap<String, String[]>> Slugdata;

        public ThreeLevelListAdapter(Context context, String[] parentHeader, ArrayList<String[]> secondLevel,
                                     ArrayList<LinkedHashMap<String, String[]>> data, ArrayList<String[]> SlugsecondLevel,
                                     ArrayList<LinkedHashMap<String, String[]>> Slugdata) {
            this.context = context;
            this.parentHeaders = parentHeader;
            this.secondLevel = secondLevel;
            this.data = data;
            this.SlugsecondLevel = SlugsecondLevel;
            this.Slugdata = Slugdata;
        }

        @Override
        public int getGroupCount() {
            return parentHeaders.length - count;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return 1;

        }

        @Override
        public Object getGroup(int groupPosition) {

            return groupPosition;
        }

        @Override
        public Object getChild(int group, int child) {
            return child;
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_first, null);
            TextView text = (TextView) convertView.findViewById(R.id.rowParentText);
            text.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
                text.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            }
            text.setText(this.parentHeaders[groupPosition]);
            if (groupPosition % 2 == 0) {
                text.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            } else {
                text.setBackgroundColor(getContext().getResources().getColor(R.color.colorMegamenu));
            }

            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

            final SecondLevelExpandableListView secondLevelELV = new SecondLevelExpandableListView(context);
            String[] headers = secondLevel.get(groupPosition);
            String[] headersSlug = SlugsecondLevel.get(groupPosition);
            ArrayList<String[]> childData = new ArrayList<>();
            HashMap<String, String[]> secondLevelData = data.get(groupPosition);
            ArrayList<String[]> childDataSlug = new ArrayList<>();
            HashMap<String, String[]> secondLevelDataSlug = Slugdata.get(groupPosition);

            for (String key : secondLevelData.keySet()) {
                childData.add(secondLevelData.get(key));
            }

            for (String key : secondLevelDataSlug.keySet()) {
                childDataSlug.add(secondLevelDataSlug.get(key));
            }

           /* if(childData.size() > 0)
            {
                isthridleveldata = true;
            }else {
                isthridleveldata = false;
            }
*/
            secondLevelELV.setAdapter(new SecondLevelAdapter(context, headers, childData, headersSlug, childDataSlug));
            secondLevelELV.setGroupIndicator(null);
            secondLevelELV.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                int previousGroup = -1;

                @Override
                public void onGroupExpand(int groupPosition) {
                    if (groupPosition != previousGroup)
                        secondLevelELV.collapseGroup(previousGroup);
                    previousGroup = groupPosition;
                }
            });

            return secondLevelELV;
        }


        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }
    //endregion

    //region SecondLevelAdapter
    public class SecondLevelAdapter extends BaseExpandableListAdapter {

        private Context context;
        List<String[]> data;
        List<String[]> dataSlug;
        String[] headers;
        String[] headersSlug;

        public SecondLevelAdapter(Context context, String[] headers, List<String[]> data,
                                  String[] headersSlug, List<String[]> dataSlug) {
            this.context = context;
            this.data = data;
            this.headers = headers;
            this.headersSlug = headersSlug;
            this.dataSlug = dataSlug;
        }

        @Override
        public Object getGroup(int groupPosition) {

            return headers[groupPosition];
        }

        public Object getGroupSlug(int groupPosition) {

            return headersSlug[groupPosition];
        }

        @Override
        public int getGroupCount() {

            return headers.length;
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_second, null);
            TextView text = (TextView) convertView.findViewById(R.id.rowSecondText);
            final ImageView imagedown = (ImageView) convertView.findViewById(R.id.imgdown);
            text.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
                text.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            }
            final String groupText = getGroup(groupPosition).toString();
            final String groupTextSlug = getGroupSlug(groupPosition).toString();
//            Toast.makeText(getContext(), groupTextSlug, Toast.LENGTH_SHORT).show();
            text.setText(groupText);

            try {
                if (data.get(groupPosition).length > 0) {
                    if (isExpanded) {
                        imagedown.setImageResource(R.drawable.ic_arrow_up1);
                    } else {
                        imagedown.setVisibility(View.VISIBLE);
                    }
                } else {
                    imagedown.setVisibility(View.GONE);
                    text.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mListener = (OnFragmentInteractionListener) getContext();
                            mListener.gotoProductListing(groupTextSlug, groupText);
//                        Toast.makeText(getContext(), groupText , Toast.LENGTH_SHORT).show();
                            secondLevel.clear();
                            firstLeveldata.clear();
                            SlugsecondLevel.clear();
                            SlugfirstLeveldata.clear();
                            count = 0;
                        }
                    });
                }
            } catch (IndexOutOfBoundsException exception) {
                exception.printStackTrace();
            }

            return convertView;
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            String[] childData;
            childData = data.get(groupPosition);
            return childData[childPosition];
        }

        public Object getChildSlug(int groupPosition, int childPosition) {
            String[] childDataSlug;
            childDataSlug = dataSlug.get(groupPosition);
            return childDataSlug[childPosition];
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_third, null);
            final TextView textView = (TextView) convertView.findViewById(R.id.rowThirdText);
            textView.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
                textView.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            }
            String[] childArray = data.get(groupPosition);
            String[] childArraySlug = dataSlug.get(groupPosition);
            final String text = childArray[childPosition];
            final String textSlug = childArraySlug[childPosition];
            textView.setText(text);
//            Toast.makeText(getContext(), textSlug, Toast.LENGTH_SHORT).show();
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener = (OnFragmentInteractionListener) getContext();
                    mListener.gotoProductListing(textSlug, text);
//                    Toast.makeText(getContext(), textSlug, Toast.LENGTH_SHORT).show();
                    secondLevel.clear();
                    firstLeveldata.clear();
                    SlugsecondLevel.clear();
                    SlugfirstLeveldata.clear();
                    count = 0;
                }
            });
            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            String[] children = data.get(groupPosition);
            return children.length;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }
    //endregion

    //region SecondLevelExpandableListView
    public class SecondLevelExpandableListView extends ExpandableListView {

        public SecondLevelExpandableListView(Context context) {
            super(context);
        }

        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            //999999 is a size in pixels. ExpandableListView requires a maximum height in order to do measurement calculations.
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(999999, MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    //endregion

    public int GetDipsFromPixel(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

}
