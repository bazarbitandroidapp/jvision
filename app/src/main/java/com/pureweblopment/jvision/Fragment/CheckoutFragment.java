package com.pureweblopment.jvision.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.pureweblopment.jvision.Activity.ADDAddressesActivity;
import com.pureweblopment.jvision.Activity.CheckoutAddAddressActivity;
import com.pureweblopment.jvision.Activity.RegistrationActivity;
import com.pureweblopment.jvision.Model.SpinnerItem;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import com.pureweblopment.jvision.Activity.LoginActivity;
import com.pureweblopment.jvision.Activity.MainActivity;
import com.pureweblopment.jvision.Global.Global;
import com.pureweblopment.jvision.Global.SendMail;
import com.pureweblopment.jvision.Global.SharedPreference;
import com.pureweblopment.jvision.Global.StaticUtility;
import com.pureweblopment.jvision.Global.Typefaces;
import com.pureweblopment.jvision.R;
import com.pureweblopment.jvision.Utility.AvenuesParams;

import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CheckoutFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CheckoutFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CheckoutFragment extends Fragment implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    TextView txtTotalAmount;
    String price, items;
    TextView txtNextpaymentnifo, txtPaymentTitle;
    ImageView imgCompletedAddress, imgCompletedPaymane, imgCompletedSuccess;
    private static TextView UserNameForShipping, txtShippingAdd, txtShippingChange, txtShippingLastAddress, txtShippingPhoneNo,
            txtShippingAddress;
    private static TextView txtUserNameForBilling, txtBillingAdd, txtBillingChange, txtBillingLastAddress, txtBillingPhoneNo,
            txtBillingAddress;
    CheckBox checkBilling;
    LinearLayout llBillingAddress, llAddBillingAddress, llAddShippingAddress, llLastAddress, llCardPayment;
    RelativeLayout relativeProgress;
    Bundle bundle;
    String ischeck = "no";
    RecyclerView recyclerviewCartItems;
    private static String strShippingFname = "", strShippingLname = "", strShippingAddress = "", strShippingLandmark = "",
            strShippingPincode = "", strShippingCountry = "", strShippingState = "", strShippingCity = "",
            strShippingPhoneno = "";
    private static String strBillingFname = "", strBillingLname = "", strBillingAddress = "", strBillingLandmark = "",
            strBillingPincode = "", strBillingCountry = "", strBillingState = "", strBillingCity = "",
            strBillingPhoneno = "";
    public static boolean isBillingAddress = true;

    LinearLayout llPromocode;
    TextView txtPromocode;
    ImageView imagePromocode;

    LinearLayout llCODCharges, llShippingCharges, llDiscount;

    RadioGroup radioGroupPayment;
    RadioButton radioCOD, radioPayumoney, radioCCAvenue, radioPaypal;

    private AlertDialog SelectAlert;
    String promocode = "";
    TextView txtPromocodeTitle, txtPromocodeSelect;
    ImageView imagecancelPromocode;

    CardView cartPromocodeset, cartPromocode;

    TextView txtSubTotalTitle, txtSubTotal, txtDiscountTitle, txtDiscount, txtShippingChargesTitle, txtShippingCharges,
            txtCODChargesTitle, txtCODCharges, txtTotalTitle, txtTotal;

    String selectPayment;
    String Payumoneymerchantid = "", Payumoneysecretkey = "", Payumoneysuccessurl = "", payumoneyfailureurl = "";
    String CCavenuemerchantid = "", CCavenueWorkingkey = "", CCavenueCurrency = "", CCavenueAccessCode = "",
            CCAvenuseRedirectURL = "", CCAvenueCancelURL = "", CCAvenueRSAKeyURL = "", PaypalMode = "", Paypalusername = "",
            PaypalPassword = "";
    String totalAmount = "";
    ImageView imageAddressCartBack;

    Boolean isSameAsShippingcheck = false;

    ImageView imageCartBack, imageNavigation, imageLogo;
    FrameLayout frameLayoutCart;
    TextView txtCatName;
    LinearLayout llBottomNavigation, llcheckout, llPromocodeSet;
    CardView cardviewBottomNavigation;
    String PaymentMethod = "";
    private AlertDialog OTPVerificationDialog;

    ArrayList<SpinnerItem> countries;
    private static String strCountry = null;
    private TextWatcher textWatcher;
    String otp, usertonken = "";
    public static boolean isLastShippingAddress = false, isLastBillingAddress = false;
    public static boolean isShippingAddressChange = false, isBillingAddressChange = false;

    private boolean isSendOTP = false;
    private boolean isVerifyOTP = false;
    CardView cvTotal;
    private EditText OTPcode1, OTPcode2, OTPcode3, OTPcode4, OTPcode5, OTPcode6;
    public CheckoutFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CheckoutFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CheckoutFragment newInstance(String param1, String param2) {
        CheckoutFragment fragment = new CheckoutFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MainActivity.manageBackPress(false);
        MainActivity.isCheckoutBack = "Checkout";
        View view = inflater.inflate(R.layout.fragment_checkout, container, false);

        ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

        imageCartBack = (ImageView) getActivity().findViewById(R.id.imageCartBack);
        imageNavigation = (ImageView) getActivity().findViewById(R.id.imageNavigation);
        imageLogo = (ImageView) getActivity().findViewById(R.id.imageLogo);

        frameLayoutCart = (FrameLayout) getActivity().findViewById(R.id.frameLayoutCart);
        txtCatName = (TextView) getActivity().findViewById(R.id.txtCatName);
        llBottomNavigation = (LinearLayout) getActivity().findViewById(R.id.llBottomNavigation);
        cardviewBottomNavigation = (CardView) getActivity().findViewById(R.id.cardviewBottomNavigation);

        imageCartBack.setVisibility(View.VISIBLE);
        txtCatName.setVisibility(View.VISIBLE);
        txtCatName.setText(R.string.myaddress);

        imageNavigation.setVisibility(View.GONE);
        imageLogo.setVisibility(View.GONE);
        frameLayoutCart.setVisibility(View.GONE);
//        llBottomNavigation.setVisibility(View.GONE);
        cardviewBottomNavigation.setVisibility(View.GONE);
        txtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        imageCartBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    getActivity().onBackPressed();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });

        Initialization(view);
        OnClickListener();
        TypeFace();
        AppSetting();

        chanageLinearBorder(llPromocodeSet);
        changeRadioButtonColor(radioCCAvenue);
        changeRadioButtonColor(radioPaypal);
        changeRadioButtonColor(radioPayumoney);
        changeRadioButtonColor(radioCOD);

        if (SharedPreference.GetPreference(getContext(), Global.CheckoutTotalPrice, StaticUtility.strTotalPrice) != null) {
            price = SharedPreference.GetPreference(getContext(), Global.CheckoutTotalPrice, StaticUtility.strTotalPrice);
        }
        if (SharedPreference.GetPreference(getContext(), Global.CheckoutTotalPrice, StaticUtility.strTotalItems) != null) {
            items = SharedPreference.GetPreference(getContext(), Global.CheckoutTotalPrice, StaticUtility.strTotalItems);
        }

        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CurrencySignPosition) != null) {
            if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CurrencySignPosition).equals("1")) {
                txtTotalAmount.setText(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                        StaticUtility.CurrencySign) + " " + String.valueOf(price) + "( " + items + " items" + ")");
            }
        }

        SetShippingAddress();
        SetBillingAddress();

        /*if (SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingUserName) != null) {
            txtUserNameForBilling.setText(SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingUserName));
            txtBillingLastAddress.setText(SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingAddress));
            txtBillingPhoneNo.setText(SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillinggPhoneno));
            strBillingPincode = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingPincode);
            strBillingFname = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingFname);
            strBillingLname = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingLname);
            strBillingAddress = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingAddress1);
            strBillingLandmark = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingLandmark);
            strBillingCountry = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingCountry);
            strBillingState = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingState);
            strBillingCity = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingCity);
            strBillingPhoneno = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillinggPhoneno);
        }*/

        if (SharedPreference.GetPreference(getContext(), Global.ISPromocode, StaticUtility.strIsPromocode) != null) {
            cartPromocodeset.setVisibility(View.VISIBLE);
            cartPromocode.setVisibility(View.GONE);
            txtPromocodeSelect.setText(SharedPreference.GetPreference(getContext(), Global.ISPromocode, StaticUtility.strIsPromocode));
        } else {
            cartPromocode.setVisibility(View.VISIBLE);
            cartPromocodeset.setVisibility(View.GONE);
        }
        CartItems();

        if (radioCOD.isChecked()) {
            txtNextpaymentnifo.setText(R.string.placeorder);
        } else {
            txtNextpaymentnifo.setText(R.string.nextpaymentingo);
        }

        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.Is_Order_Active) != null) {
            String isorderactive = SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.Is_Order_Active);
            if (isorderactive.equals("1")) {
                txtNextpaymentnifo.setVisibility(View.VISIBLE);
            } else {
                txtNextpaymentnifo.setVisibility(View.GONE);
            }
        }

        if (SharedPreference.GetPreference(getContext(), Global.ISCheck, StaticUtility.strIscheck) != null) {
            String isSameAsShipping = SharedPreference.GetPreference(getContext(), Global.ISCheck, StaticUtility.strIscheck);
            if (isSameAsShipping.equals("yes")) {
                checkBilling.setChecked(true);
            } else {
                checkBilling.setChecked(false);
            }
        }

        return view;
    }

    //region AppSetting
    @SuppressLint("NewApi")
    private void AppSetting() {
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != null) {
            UserNameForShipping.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtShippingAdd.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtShippingChange.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtShippingPhoneNo.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtTotalAmount.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtUserNameForBilling.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtBillingAdd.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtBillingChange.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtBillingPhoneNo.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtPaymentTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtTotal.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtTotalTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtShippingAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtBillingAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        }
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor) != "") {
            txtNextpaymentnifo.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        }
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            txtNextpaymentnifo.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor) != "") {
            txtPromocode.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            txtPromocodeTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            txtPromocodeSelect.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            txtBillingLastAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            txtCODCharges.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            txtCODChargesTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            txtShippingCharges.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            txtShippingChargesTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            txtDiscount.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            txtDiscountTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            txtSubTotal.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            txtSubTotalTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            txtShippingLastAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        }
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            llcheckout.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
            imagecancelPromocode.setColorFilter(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            checkBilling.setButtonTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

    }
    //endregion

    //region OnClickListener
    private void OnClickListener() {
        checkBilling.setOnClickListener(this);
        txtShippingAdd.setOnClickListener(this);
        txtBillingAdd.setOnClickListener(this);
        txtShippingChange.setOnClickListener(this);
        txtBillingChange.setOnClickListener(this);
        txtNextpaymentnifo.setOnClickListener(this);
        llPromocode.setOnClickListener(this);
        imagecancelPromocode.setOnClickListener(this);
        radioGroupPayment.setOnCheckedChangeListener(this);
    }
    //endregion

    //region TypeFace
    private void TypeFace() {
        UserNameForShipping.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtShippingAdd.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtShippingChange.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtShippingLastAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtShippingPhoneNo.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));

        txtUserNameForBilling.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtBillingAdd.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtBillingChange.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtBillingLastAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtBillingPhoneNo.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));

        txtShippingAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtBillingAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtNextpaymentnifo.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
        txtPromocode.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtPromocodeTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtPromocodeSelect.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtPaymentTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));

        txtTotal.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtTotalTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtCODCharges.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtCODChargesTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtShippingCharges.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtShippingChargesTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtDiscount.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtDiscountTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtSubTotal.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtSubTotalTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtTotalAmount.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
    }
    //endregion

    //region Initialization
    private void Initialization(View view) {
        UserNameForShipping = (TextView) view.findViewById(R.id.UserNameForShipping);
        txtShippingAdd = (TextView) view.findViewById(R.id.txtShippingAdd);
        txtShippingChange = (TextView) view.findViewById(R.id.txtShippingChange);
        txtShippingLastAddress = (TextView) view.findViewById(R.id.txtShippingLastAddress);
        txtShippingPhoneNo = (TextView) view.findViewById(R.id.txtShippingPhoneNo);

        txtUserNameForBilling = (TextView) view.findViewById(R.id.txtUserNameForBilling);
        txtBillingAdd = (TextView) view.findViewById(R.id.txtBillingAdd);
        txtBillingChange = (TextView) view.findViewById(R.id.txtBillingChange);
        txtBillingLastAddress = (TextView) view.findViewById(R.id.txtBillingLastAddress);
        txtBillingPhoneNo = (TextView) view.findViewById(R.id.txtBillingPhoneNo);

        checkBilling = (CheckBox) view.findViewById(R.id.checkBilling);
        llBillingAddress = (LinearLayout) view.findViewById(R.id.llBillingAddress);
        llAddBillingAddress = (LinearLayout) view.findViewById(R.id.llAddBillingAddress);
        llAddShippingAddress = (LinearLayout) view.findViewById(R.id.llAddShippingAddress);
        llLastAddress = (LinearLayout) view.findViewById(R.id.llLastAddress);
        relativeProgress = (RelativeLayout) view.findViewById(R.id.relativeProgress);

        txtShippingAddress = (TextView) view.findViewById(R.id.txtShippingAddress);
        txtBillingAddress = (TextView) view.findViewById(R.id.txtBillingAddress);

        recyclerviewCartItems = (RecyclerView) view.findViewById(R.id.recyclerviewCartItems);

        llPromocode = (LinearLayout) view.findViewById(R.id.llPromocode);
        txtPromocode = (TextView) view.findViewById(R.id.txtPromocode);
        imagePromocode = (ImageView) view.findViewById(R.id.imagePromocode);

        cartPromocodeset = (CardView) view.findViewById(R.id.cartPromocodeset);
        cartPromocode = (CardView) view.findViewById(R.id.cartPromocode);

        txtPromocodeTitle = (TextView) view.findViewById(R.id.txtPromocodeTitle);
        txtPromocodeSelect = (TextView) view.findViewById(R.id.txtPromocodeSelect);
        imagecancelPromocode = (ImageView) view.findViewById(R.id.imagecancelPromocode);

        radioGroupPayment = (RadioGroup) view.findViewById(R.id.radioGroupPayment);
        radioCOD = (RadioButton) view.findViewById(R.id.radioCOD);
        radioPayumoney = (RadioButton) view.findViewById(R.id.radioPayumoney);
        radioCCAvenue = (RadioButton) view.findViewById(R.id.radioCCAvenue);
        radioPaypal = (RadioButton) view.findViewById(R.id.radioPaypal);

        txtPaymentTitle = (TextView) view.findViewById(R.id.txtPaymentTitle);

        txtSubTotalTitle = (TextView) view.findViewById(R.id.txtSubTotalTitle);
        txtSubTotal = (TextView) view.findViewById(R.id.txtSubTotal);
        txtDiscountTitle = (TextView) view.findViewById(R.id.txtDiscountTitle);
        txtDiscount = (TextView) view.findViewById(R.id.txtDiscount);
        txtShippingChargesTitle = (TextView) view.findViewById(R.id.txtShippingChargesTitle);
        txtShippingCharges = (TextView) view.findViewById(R.id.txtShippingCharges);
        txtCODChargesTitle = (TextView) view.findViewById(R.id.txtCODChargesTitle);
        txtCODCharges = (TextView) view.findViewById(R.id.txtCODCharges);
        txtTotalTitle = (TextView) view.findViewById(R.id.txtTotalTitle);
        txtTotal = (TextView) view.findViewById(R.id.txtTotal);

        txtNextpaymentnifo = (TextView) view.findViewById(R.id.txtNextpaymentnifo);
        llCardPayment = (LinearLayout) view.findViewById(R.id.llCardPayment);
        imgCompletedSuccess = (ImageView) view.findViewById(R.id.imgCompletedSuccess);
        imgCompletedPaymane = (ImageView) view.findViewById(R.id.imgCompletedPaymane);
        imgCompletedAddress = (ImageView) view.findViewById(R.id.imgCompletedAddress);

        txtTotalAmount = (TextView) view.findViewById(R.id.txtTotalAmount);
        llcheckout = (LinearLayout) view.findViewById(R.id.llcheckout);

        llPromocodeSet = (LinearLayout) view.findViewById(R.id.llPromocodeSet);
        llCODCharges = (LinearLayout) view.findViewById(R.id.llCODCharges);
        llShippingCharges = (LinearLayout) view.findViewById(R.id.llShippingCharges);
        llDiscount = (LinearLayout) view.findViewById(R.id.llDiscount);

        cvTotal = (CardView) view.findViewById(R.id.cvTotal);

    }
    //endregion

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.checkBilling:
                SharedPreference.CreatePreference(getContext(), Global.ISCheck);
                if (((CheckBox) v).isChecked()) {
                    llBillingAddress.setVisibility(View.GONE);
                    ischeck = "no";
                    SharedPreference.SavePreference(StaticUtility.strIscheck, "no");
                    checkBilling.setChecked(true);
                    isSameAsShippingcheck = false;
                } else {
                    llBillingAddress.setVisibility(View.VISIBLE);
                    checkBilling.setChecked(false);
                    isSameAsShippingcheck = true;
                    ischeck = "yes";
                    SharedPreference.SavePreference(StaticUtility.strIscheck, "yes");
                    if (SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingUserName) != null) {
                        txtUserNameForBilling.setText(SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingUserName));
                        txtBillingLastAddress.setText(SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingAddress));
                        txtBillingPhoneNo.setText(SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingPhoneno));
                        strBillingPincode = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingPincode);
                        strBillingFname = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingFname);
                        strBillingLname = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingLname);
                        strBillingAddress = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingAddress1);
                        strBillingLandmark = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingLandmark);
                        strBillingCountry = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingCountry);
                        strBillingState = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingState);
                        strBillingCity = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingCity);
                        strBillingPhoneno = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingPhoneno);
                    } else {
                        if (isBillingAddress) {
                            txtUserNameForBilling.setText("");
                            txtBillingLastAddress.setText("");
                            txtBillingPhoneNo.setText("");
                        }
                        if (TextUtils.isEmpty(txtUserNameForBilling.getText().toString())) {
                            LastOrderBillingAddress();
                        }
                    }
                }
                break;
            case R.id.txtShippingAdd:
                llCardPayment.setVisibility(View.VISIBLE);
                Intent intentShippingAdd = new Intent(getActivity(), CheckoutAddAddressActivity.class);
                intentShippingAdd.putExtra("ActivityType", "MyAddressActivity");
                intentShippingAdd.putExtra("AddressType", "Shipping Address");
                intentShippingAdd.putExtra("Type", "Checkout");
                getActivity().startActivity(intentShippingAdd);
                break;

            case R.id.txtBillingAdd:
                llCardPayment.setVisibility(View.VISIBLE);
                Intent intentBillingAdd = new Intent(getActivity(), CheckoutAddAddressActivity.class);
                intentBillingAdd.putExtra("ActivityType", "MyAddressActivity");
                intentBillingAdd.putExtra("AddressType", "Billing Address");
                intentBillingAdd.putExtra("Type", "Checkout");
                intentBillingAdd.putExtra("ShippingUserName", UserNameForShipping.getText().toString());
                intentBillingAdd.putExtra("ShippingAddress", txtShippingLastAddress.getText().toString());
                intentBillingAdd.putExtra("ShippingPhoneNo", txtShippingPhoneNo.getText().toString());
                getActivity().startActivity(intentBillingAdd);
                break;
            case R.id.txtShippingChange:
                llCardPayment.setVisibility(View.VISIBLE);
                mListener = (OnFragmentInteractionListener) getContext();
                mListener.gotoChangeAddressCheckout("0", "Shipping Address", "ChangeAddress");
                break;
            case R.id.txtBillingChange:
                llCardPayment.setVisibility(View.VISIBLE);
                mListener = (OnFragmentInteractionListener) getContext();
                if (!UserNameForShipping.getText().equals("")) {
                    mListener.gotoChangeAddressCheckout1("1", "Billing Address",
                            "ChangeAddress", UserNameForShipping.getText().toString(),
                            txtShippingLastAddress.getText().toString(), txtShippingPhoneNo.getText().toString());
                } else {
                    mListener.gotoChangeAddressCheckout("1", "Billing Address", "ChangeAddress");
                }
                break;
            case R.id.txtNextpaymentnifo:
                CheckPincode(strShippingPincode);
                break;
            case R.id.llPromocode:
                PromocodeDialog();
                break;
            case R.id.imagecancelPromocode:
                cartPromocodeset.setVisibility(View.GONE);
                cartPromocode.setVisibility(View.VISIBLE);
                RemovePromocde();
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (group.getCheckedRadioButtonId()) {
            case R.id.radioCOD:
                if (radioCOD.isChecked()) {
                    selectPayment = radioCOD.getText().toString();
                    radioCOD.setChecked(true);
                    radioPayumoney.setChecked(false);
                    radioCCAvenue.setChecked(false);
                    radioPaypal.setChecked(false);
                    txtNextpaymentnifo.setText(R.string.placeorder);
                    PaymentMethod = "COD";
                }
                break;
            case R.id.radioPayumoney:
                if (radioPayumoney.isChecked()) {
                    selectPayment = radioPayumoney.getText().toString();
                    radioPayumoney.setChecked(true);
                    radioCOD.setChecked(false);
                    radioCCAvenue.setChecked(false);
                    radioPaypal.setChecked(false);
                    txtNextpaymentnifo.setText(R.string.nextpaymentingo);
                    PaymentMethod = "PayUMoney";
                }
                break;
            case R.id.radioCCAvenue:
                if (radioCCAvenue.isChecked()) {
                    selectPayment = radioCCAvenue.getText().toString();
                    radioCCAvenue.setChecked(true);
                    radioCOD.setChecked(false);
                    radioPayumoney.setChecked(false);
                    radioPaypal.setChecked(false);
                    txtNextpaymentnifo.setText(R.string.nextpaymentingo);
                    PaymentMethod = "CCA";
                }
                break;
            case R.id.radioPaypal:
                if (radioPaypal.isChecked()) {
                    selectPayment = radioPaypal.getText().toString();
                    radioPaypal.setChecked(true);
                    radioCOD.setChecked(false);
                    radioPayumoney.setChecked(false);
                    radioCCAvenue.setChecked(false);
                    PaymentMethod = "PayPal";
                }
                break;
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void gotoChangeAddressCheckout(String AddressType, String AddressTypeName, String MethodCallFrom);

        void gotoChangeAddressCheckout1(String AddressType, String AddressTypeName,
                                        String MethodCallFrom, String strShippingUserName, String strShippingAddress, String strShippingPhoneNo);

        void gotoOrderSuccess(String payment_method, String urlStatus);

        void gotoPayment(String orderid, String amount, String Fanme, String phoneNo, String mMerchantKey, String mSalt,
                         String Payumoneysuccessurl, String payumoneyfailureurl);

        void gotoCCAvence(Bundle bundle);
    }

    @Override
    public void onResume() {
        super.onResume();
        llCardPayment.setVisibility(View.VISIBLE);
        if (!strShippingPincode.equalsIgnoreCase("")) {
            GetshippingchargeDelivery();
        }
        if (isBillingAddressChange) {
            /*checkBilling.setChecked(true);*/
            SetBillingAddress();
        } else {
            checkBilling.setChecked(true);
            llBillingAddress.setVisibility(View.GONE);
        }
        if (isShippingAddressChange) {
            llCardPayment.setVisibility(View.VISIBLE);
            isShippingAddressChange = false;
            SetShippingAddress();
        }

    }


    //region SetShippingAddress
    public void SetShippingAddress() {
        if (SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingUserName) != null) {
            UserNameForShipping.setText(SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingUserName));
            txtShippingLastAddress.setText(SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingAddress));
            txtShippingPhoneNo.setText(SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingPhoneno));

            strShippingPincode = SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strPincode);
            strShippingPhoneno = SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingPhoneno);
            strShippingFname = SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingFname);
            strShippingLname = SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingLname);
            strShippingAddress = SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingAddress1);
            strShippingLandmark = SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingLandmark);
            strShippingCountry = SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingCountry);
            strShippingState = SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingState);
            strShippingCity = SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingCity);
            GetshippingchargeDelivery();
        } else {
            UserNameForShipping.setText("");
            txtShippingLastAddress.setText("");
            txtShippingPhoneNo.setText("");
            LastOrderShippingAddress();
        }

        /*if (SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.IsSelectAddress) != null) {
            if (SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.IsSelectAddress).equalsIgnoreCase("2")) {
                if (SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingUserName) != null) {
                    UserNameForShipping.setText(SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingUserName));
                    txtShippingLastAddress.setText(SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingAddress));
                    txtShippingPhoneNo.setText(SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingPhoneno));
                    strShippingPincode = SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strPincode);
                    strShippingPhoneno = SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingPhoneno);
                    strShippingFname = SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingFname);
                    strShippingLname = SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingLname);
                    strShippingAddress = SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingAddress1);
                    strShippingLandmark = SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingLandmark);
                    strShippingCountry = SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingCountry);
                    strShippingState = SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingState);
                    strShippingCity = SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.strShippingCity);
                    GetshippingchargeDelivery();
                } else {
                    LastOrderShippingAddress();
                }
            } else if (SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.IsSelectAddress).equalsIgnoreCase("1")) {
                if (isLastShippingAddress) {
                    LastOrderShippingAddress();
                }
            } else if (SharedPreference.GetPreference(getContext(), Global.Shipping_Preference, StaticUtility.IsSelectAddress).equalsIgnoreCase("0")) {
                LastOrderShippingAddress();
            }
        } else {
            LastOrderShippingAddress();
            if(!strShippingPincode.equalsIgnoreCase("")){
                GetshippingchargeDelivery();
            }
        }*/
    }//endregion

    //region SetBillingAddress
    public void SetBillingAddress() {
        if (ischeck.equals("yes")) {
            llBillingAddress.setVisibility(View.VISIBLE);
            checkBilling.setChecked(false);
            isSameAsShippingcheck = true;
            ischeck = "yes";
            if (SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingUserName) != null) {
                txtUserNameForBilling.setText(SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingUserName));
                txtBillingLastAddress.setText(SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingAddress));
                txtBillingPhoneNo.setText(SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingPhoneno));
                strBillingPincode = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingPincode);
                strBillingFname = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingFname);
                strBillingLname = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingLname);
                strBillingAddress = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingAddress1);
                strBillingLandmark = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingLandmark);
                strBillingCountry = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingCountry);
                strBillingState = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingState);
                strBillingCity = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingCity);
                strBillingPhoneno = SharedPreference.GetPreference(getContext(), Global.Billing_Preference, StaticUtility.strBillingPhoneno);
            } else {
                if (isBillingAddress) {
                    txtUserNameForBilling.setText("");
                    txtBillingLastAddress.setText("");
                    txtBillingPhoneNo.setText("");
                }
                if (TextUtils.isEmpty(txtUserNameForBilling.getText().toString())) {
                    LastOrderBillingAddress();
                }
            }
        }

    }
    //endregion

    //region FOR LastOrderShippingAddress API..
    private void LastOrderShippingAddress() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.LastOrderAddress);
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers1(getContext()));
        }
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    if (jsonObject.has("first_name")) {
                                        if (!jsonObject.getString("first_name").equals("")) {
                                            cvTotal.setVisibility(View.VISIBLE);
                                            isLastShippingAddress = true;
                                            strShippingFname = jsonObject.getString("first_name");
                                            strShippingLname = jsonObject.getString("last_name");
                                            strShippingAddress = jsonObject.getString("address");
                                            strShippingLandmark = jsonObject.getString("landmark");
                                            strShippingPincode = jsonObject.getString("pincode");
                                            strShippingCountry = jsonObject.getString("country");
                                            strShippingState = jsonObject.getString("state");
                                            strShippingCity = jsonObject.getString("city");
                                            strShippingPhoneno = jsonObject.getString("phone_number");
                                            UserNameForShipping.setText(jsonObject.getString("first_name") + " " + jsonObject.getString("last_name"));
                                            txtShippingLastAddress.setText(jsonObject.getString("address") + "," + "\n" +
                                                    jsonObject.getString("city") + " - " + jsonObject.getString("pincode") + "," + "\n" + jsonObject.getString("state"));
                                            txtShippingPhoneNo.setText(jsonObject.getString("phone_number"));
                                            GetshippingchargeDelivery();
                                        } else {
                                            isLastShippingAddress = false;
                                            mListener = (OnFragmentInteractionListener) getContext();
                                            mListener.gotoChangeAddressCheckout("0", "Shipping Address", "LastAddress");
                                        }
                                    } else {
                                        isLastShippingAddress = false;
                                        mListener = (OnFragmentInteractionListener) getContext();
                                        mListener.gotoChangeAddressCheckout("0", "Shipping Address", "LastAddress");
                                        cvTotal.setVisibility(View.GONE);

                                       /* Intent intent = new Intent(getActivity(), ADDAddressesActivity.class);
                                        intent.putExtra("ActivityType", "MyAddressActivity");
                                        intent.putExtra("AddressType", "Shipping Address");
                                        getActivity().startActivity(intent);*/
                                       /* mListener = (OnFragmentInteractionListener) getContext();
                                        mListener.gotoChangeAddressCheckout("0", "Shipping Address");*/
                                    }

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in CheckOutFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR LastOrderBillingAddress API..
    private void LastOrderBillingAddress() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.LastOrderAddress);
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers1(getContext()));
        }
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                JSONObject jsonObject = response.getJSONObject("payload");
                                if (strStatus.equals("ok")) {
                                    if (jsonObject.has("bill_first_name")) {
                                        if (!jsonObject.getString("bill_first_name").equals("")) {
                                            isLastBillingAddress = true;
                                            strBillingFname = jsonObject.getString("bill_first_name");
                                            strBillingLname = jsonObject.getString("bill_last_name");
                                            strBillingAddress = jsonObject.getString("bill_address");
                                            strBillingLandmark = jsonObject.getString("bill_landmark");
                                            strBillingPincode = jsonObject.getString("bill_pincode");
                                            strBillingCountry = jsonObject.getString("bill_country");
                                            strBillingState = jsonObject.getString("bill_state");
                                            strBillingCity = jsonObject.getString("bill_city");
                                            strBillingPhoneno = jsonObject.getString("bill_phone_number");
                                            txtUserNameForBilling.setText(jsonObject.getString("bill_first_name") + " " + jsonObject.getString("bill_last_name"));
                                            txtBillingLastAddress.setText(jsonObject.getString("bill_address") + "," + "\n" +
                                                    jsonObject.getString("bill_city") + " - " + jsonObject.getString("bill_pincode") + "," + "\n" + jsonObject.getString("bill_state"));
                                            txtBillingPhoneNo.setText(jsonObject.getString("bill_phone_number"));
                                        } else {
                                            isLastBillingAddress = false;
                                            mListener = (OnFragmentInteractionListener) getContext();
                                            if (!UserNameForShipping.getText().equals("")) {
                                                mListener.gotoChangeAddressCheckout1("1", "Billing Address",
                                                        "ChangeAddress", UserNameForShipping.getText().toString(),
                                                        txtShippingLastAddress.getText().toString(), txtShippingPhoneNo.getText().toString());
                                            } else {
                                                mListener.gotoChangeAddressCheckout("1", "Billing Address", "LastAddress");
                                            }
                                            /*mListener.gotoChangeAddressCheckout("1", "Billing Address", "LastAddress");*/
                                        }
                                    } else {
                                        isLastBillingAddress = false;
                                        mListener = (OnFragmentInteractionListener) getContext();
                                        if (!UserNameForShipping.getText().equals("")) {
                                            mListener.gotoChangeAddressCheckout1("1", "Billing Address",
                                                    "ChangeAddress", UserNameForShipping.getText().toString(),
                                                    txtShippingLastAddress.getText().toString(), txtShippingPhoneNo.getText().toString());
                                        } else {
                                            mListener.gotoChangeAddressCheckout("1", "Billing Address", "LastAddress");
                                        }
                                        /*mListener.gotoChangeAddressCheckout("1", "Billing Address", "LastAddress");*/
                                    }

                                    /*else {
                                        llBillingAddress.setVisibility(View.GONE);
                                        Intent intent = new Intent(getActivity(), ADDAddressesActivity.class);
                                        intent.putExtra("ActivityType", "MyAddressActivity");
                                        intent.putExtra("AddressType", "Billing Address");
                                        getActivity().startActivity(intent);
                                        *//*mListener = (OnFragmentInteractionListener) getContext();
                                        mListener.gotoChangeAddressCheckout("1", "Billing Address");*//*
                                    }*/

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in CheckoutFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    // region FOR GetshippingchargeDelivery API..
    private void GetshippingchargeDelivery() {
        relativeProgress.setVisibility(View.VISIBLE);
        String[] key = {};
        String[] val = {};
        String userId = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        if (userId != null) {
            key = new String[]{"session_id", "user_id", "pincode", "city"};
            val = new String[]{"", userId, strShippingPincode, ""};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.Getshipping_charge_Delivery);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    GetCartTotal();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException | NullPointerException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in CheckOutFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    // region FOR GetCartTotal API..
    private void GetCartTotal() {
        relativeProgress.setVisibility(View.VISIBLE);
        String[] key = {};
        String[] val = {};
        String userId = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        if (userId != null) {
            key = new String[]{"session_id", "user_id"};
            val = new String[]{"", userId};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetCartTotal);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    cvTotal.setVisibility(View.VISIBLE);
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    String total_price = jsonObject.getString("total_price");
                                    String sub_total = jsonObject.getString("sub_total");
                                    String promocode_discount = jsonObject.getString("promocode_discount");
                                    String priceint = promocode_discount.replace(",", "");
//                                    String floatPrice = new DecimalFormat("##,###").format(Float.valueOf(promocode_discount));
                                    String shipping_price = jsonObject.getString("shipping_price");
                                    String shipping_price_cod = jsonObject.getString("shipping_price_cod");

                                    totalAmount = total_price;

                                    if (!promocode_discount.equals("")) {
                                        float floatPromocode = Float.parseFloat(priceint);
                                        int intPromocode = Math.round(floatPromocode);
                                        if (intPromocode > 0) {
                                            if (SharedPreference.GetPreference(getContext(), Global.ISPromocode, StaticUtility.strIsPromocode) != null) {
                                                cartPromocodeset.setVisibility(View.VISIBLE);
                                                cartPromocode.setVisibility(View.GONE);
                                                txtPromocodeSelect.setText(SharedPreference.GetPreference(getContext(), Global.ISPromocode, StaticUtility.strIsPromocode));
                                            } else {
                                                cartPromocode.setVisibility(View.VISIBLE);
                                                cartPromocodeset.setVisibility(View.GONE);
                                            }
                                        } else {
                                            cartPromocode.setVisibility(View.VISIBLE);
                                            cartPromocodeset.setVisibility(View.GONE);
                                        }
                                    }

                                    if (promocode_discount.equalsIgnoreCase("0.00")) {
                                        llDiscount.setVisibility(View.GONE);
                                    } else {
                                        llDiscount.setVisibility(View.VISIBLE);
                                    }

                                    if (shipping_price.equalsIgnoreCase("0.00")) {
                                        llShippingCharges.setVisibility(View.GONE);
                                    } else {
                                        llShippingCharges.setVisibility(View.VISIBLE);
                                    }

                                    if (shipping_price_cod.equalsIgnoreCase("0.00")) {
                                        llCODCharges.setVisibility(View.GONE);
                                    } else {
                                        llCODCharges.setVisibility(View.VISIBLE);
                                    }

                                    if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CurrencySignPosition) != null) {
                                        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                StaticUtility.CurrencySignPosition).equals("1")) {
                                            txtSubTotal.setText(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                    StaticUtility.CurrencySign) + ". " + sub_total);
                                            txtDiscount.setText(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                    StaticUtility.CurrencySign) + ". " + promocode_discount);
                                            txtShippingCharges.setText(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                    StaticUtility.CurrencySign) + ". " + shipping_price);
                                            txtCODCharges.setText(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                    StaticUtility.CurrencySign) + ". " + shipping_price_cod);
                                            txtTotal.setText(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                    StaticUtility.CurrencySign) + ". " + total_price);
                                        } else {
                                            txtDiscount.setText(promocode_discount + ". " + SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                    StaticUtility.CurrencySign));
                                            txtShippingCharges.setText(shipping_price + ". " + SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                    StaticUtility.CurrencySign));
                                            txtCODCharges.setText(shipping_price_cod + ". " + SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                    StaticUtility.CurrencySign));
                                            txtSubTotal.setText(sub_total + ". " + SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                    StaticUtility.CurrencySign));
                                            txtTotal.setText(total_price + ". " + SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                    StaticUtility.CurrencySign));
                                        }
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException | NullPointerException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in CheckoutFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR CartItems API..
    private void CartItems() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key;
        String[] val;

        String userId = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        String SessionId = SharedPreference.GetPreference(getActivity(), Global.preferenceNameGuestUSer, Global.SessionId);
        if (userId != null) {
            key = new String[]{"session_id", "user_id"};
            val = new String[]{"", userId};
        } else {
            key = new String[]{"session_id", "user_id"};
            val = new String[]{SessionId, ""};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetCartItems);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    if (jsonObjectPayload.has("cart_items")) {
                                        JSONArray jsonArraycartitems = jsonObjectPayload.getJSONArray("cart_items");
                                        if (jsonArraycartitems.length() > 0) {
                                            recyclerviewCartItems.setVisibility(View.VISIBLE);
                                            AdapterCart adapterCart = new AdapterCart(getContext(), jsonArraycartitems);
                                            recyclerviewCartItems.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                            recyclerviewCartItems.setAdapter(adapterCart);
                                        } else {
                                            recyclerviewCartItems.setVisibility(View.GONE);
                                        }
                                    }
                                }
                                PaymentGetway();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in CheckoutFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region AdapterCart
    public class AdapterCart extends RecyclerView.Adapter<AdapterCart.Viewholder> {

        Context context;
        JSONArray jsonArray;

        public AdapterCart(Context context, JSONArray jsonArray) {
            this.context = context;
            this.jsonArray = jsonArray;
        }

        @Override
        public AdapterCart.Viewholder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_cart_checkout_items, viewGroup, false);
            return new AdapterCart.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder holder, int position) {
            try {
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        holder.pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
                    }
                }
                final JSONObject jsonObject = jsonArray.getJSONObject(position);
                final JSONObject jsonObjectProductData = jsonObject.getJSONObject("product_data");

                holder.txtProductName.setText(jsonObjectProductData.getString("name"));
                holder.txtProductDes.setText(jsonObjectProductData.getString("short_description"));

                holder.txtQunty.setText("Quantity :- " + jsonObject.getString("product_quantity"));

                float floatBasePrice = Float.parseFloat(jsonObjectProductData.getString("price"));
                int intBasePrice = Math.round(floatBasePrice);
                float floatSalePrice = Float.parseFloat(jsonObjectProductData.getString("sale_price"));
                int intSalePrice = Math.round(floatSalePrice);

                if (intSalePrice > 0) {
                    if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("1")) {
                        holder.textProductSalePrice.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + jsonObjectProductData.getString("sale_price"));
                        holder.txt_product_base_price.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + jsonObjectProductData.getString("price"));
                    } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("2")) {
                        holder.textProductSalePrice.setText(jsonObjectProductData.getString("sale_price") + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        holder.txt_product_base_price.setText(jsonObjectProductData.getString("price") + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                    }
                } else {
                    if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("1")) {
                        holder.textProductSalePrice.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + jsonObjectProductData.getString("price"));
                        holder.txt_product_base_price.setVisibility(View.GONE);
                    } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("2")) {
                        holder.textProductSalePrice.setText(jsonObjectProductData.getString("price") + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        holder.txt_product_base_price.setVisibility(View.GONE);
                    }
                }
               /* JSONArray jsonArrayImages = jsonObjectProductData.getJSONArray("main_image");
                String image = String.valueOf(jsonArrayImages.get(0));*/

                JSONObject jsonObjectImages = jsonObjectProductData.getJSONObject("main_image");
                String image = jsonObjectImages.getString("main_image");

                //region Image
                String picUrl = null;
                try {
                    URL urla = null;
                /*images = images.replace("[", "");
                image = image.replace("]", "");*/
                    urla = new URL(image.replaceAll("%20", " "));
                    URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    // Capture position and set to the ImageView
                    Picasso.with(context)
                            .load(picUrl)
                            .into(holder.imageProduct, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                    holder.rlImgHolder.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }
                            });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in CheckoutFragment.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in CheckoutFragment.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
                //endregion

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return jsonArray.length();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            ImageView imageProduct;
            TextView txtProductName, txtProductDes, textProductSalePrice,
                    txt_product_base_price, txtQunty;
            RelativeLayout rlImgHolder;
            ProgressBar pbImgHolder;

            public Viewholder(View itemView) {
                super(itemView);
                imageProduct = (ImageView) itemView.findViewById(R.id.imageProduct);

                txtProductName = (TextView) itemView.findViewById(R.id.txtProductName);
                txtProductDes = (TextView) itemView.findViewById(R.id.txtProductDes);
                textProductSalePrice = (TextView) itemView.findViewById(R.id.textProductSalePrice);
                txt_product_base_price = (TextView) itemView.findViewById(R.id.txt_product_base_price);
                txtQunty = (TextView) itemView.findViewById(R.id.txtQunty);
                rlImgHolder = (RelativeLayout) itemView.findViewById(R.id.rlImgHolder);
                pbImgHolder = (ProgressBar) itemView.findViewById(R.id.pbImgHolder);

                txtProductName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                txtProductDes.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                textProductSalePrice.setTypeface(Typefaces.TypefaceCalibri_bold(context));
                txt_product_base_price.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                txtQunty.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
                    txtQunty.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                    txtProductName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                    textProductSalePrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                }
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor) != "") {
                    txtProductDes.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                    txt_product_base_price.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                }
            }
        }
    }
    //endregion

    //region FOR DIALOG...
    public void PromocodeDialog() {

        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.row_show_popup_promocode, null, false);
        // find the Textview in the popup layout
        final TextView txtpromocode = (TextView) inflatedView.findViewById(R.id.txtpromocode);
        final ImageView imgCancel = (ImageView) inflatedView.findViewById(R.id.imgCancel);
        final EditText edtPromocode = (EditText) inflatedView.findViewById(R.id.edtPromocode);
        final Button btnApply = (Button) inflatedView.findViewById(R.id.btnApply);
        txtpromocode.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        edtPromocode.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
            txtpromocode.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            edtPromocode.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        SelectAlert = builder.create();
        SelectAlert.setCancelable(true);
        SelectAlert.setView(inflatedView);
        SelectAlert.setCanceledOnTouchOutside(true);
        if (SelectAlert.isShowing()) {
            SelectAlert.dismiss();
        } else {
            SelectAlert.show();
        }

        chanageEditTextBorder(edtPromocode);
        chanageButton(btnApply);

        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectAlert.dismiss();
            }
        });

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promocode = edtPromocode.getText().toString();
                ValidPromocode(edtPromocode.getText().toString());
                SelectAlert.dismiss();
            }
        });
        SelectAlert.show();
    }
    //endregion

    //region FOR ValidPromocode API..
    private void ValidPromocode(final String promocode) {
        relativeProgress.setVisibility(View.VISIBLE);

        String userid = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);

        String[] key = {"session_id", "user_id", "promocode"};
        String[] val = {"", userid, promocode};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.ApplyPromocode);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Global.HideSystemKeyboard(getActivity());
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    cartPromocodeset.setVisibility(View.VISIBLE);
                                    cartPromocode.setVisibility(View.GONE);
                                    txtPromocodeSelect.setText(promocode);
                                    SharedPreference.CreatePreference(getContext(), Global.ISPromocode);
                                    SharedPreference.SavePreference(StaticUtility.strIsPromocode, promocode);
                                }
                                GetshippingchargeDelivery();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Global.HideSystemKeyboard(getActivity());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                                cartPromocodeset.setVisibility(View.GONE);
                                cartPromocode.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in CheckoutFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR RemovePromocde API..
    private void RemovePromocde() {
        relativeProgress.setVisibility(View.VISIBLE);

        String userid = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);

        String[] key = {"user_id"};
        String[] val = {userid};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.RemovePromocode);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                cartPromocode.setVisibility(View.VISIBLE);
                                cartPromocodeset.setVisibility(View.GONE);
                                if (strStatus.equals("ok")) {
                                    SharedPreference.ClearPreference(getContext(), Global.ISPromocode);
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                                GetshippingchargeDelivery();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                                cartPromocodeset.setVisibility(View.GONE);
                                cartPromocode.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in CheckoutFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR PaymentGetway API..
    private void PaymentGetway() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetPaymentGetway);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");

                                    JSONObject jsonObjectCodDetail = jsonObjectPayload.getJSONObject("cod_detail");
                                    String CODstatus = jsonObjectCodDetail.getString("status");
                                    if (CODstatus.equals("1")) {
                                        radioCOD.setVisibility(View.VISIBLE);
                                        PaymentMethod = "COD";
                                    } else {
                                        radioCOD.setVisibility(View.GONE);
                                        PaymentMethod = "";
                                    }
                                    JSONObject jsonObjectPayumoneyDetail = jsonObjectPayload.getJSONObject("payumoney_detail");
                                    Payumoneymerchantid = jsonObjectPayumoneyDetail.getString("merchantid");
                                    Payumoneysecretkey = jsonObjectPayumoneyDetail.getString("secretkey");
                                    Payumoneysuccessurl = jsonObjectPayumoneyDetail.getString("payumoneysuccessurl");
                                    payumoneyfailureurl = jsonObjectPayumoneyDetail.getString("payumoneyfailureurl");
                                    String Payumoneystatus = jsonObjectPayumoneyDetail.getString("status");
                                    if (Payumoneystatus.equals("1")) {
                                        radioPayumoney.setVisibility(View.VISIBLE);
                                    } else {
                                        radioPayumoney.setVisibility(View.GONE);
                                    }
                                    JSONObject jsonObjectCCavenueDetail = jsonObjectPayload.getJSONObject("ccavenue_detail");
                                    CCavenuemerchantid = jsonObjectCCavenueDetail.getString("merchant_id");
                                    CCavenueWorkingkey = jsonObjectCCavenueDetail.getString("workingkey");
                                    CCavenueCurrency = jsonObjectCCavenueDetail.getString("currency");
                                    CCavenueAccessCode = jsonObjectCCavenueDetail.getString("accesscode");
                                    CCAvenuseRedirectURL = jsonObjectCCavenueDetail.getString("redirecturl");
                                    CCAvenueCancelURL = jsonObjectCCavenueDetail.getString("cancelurl");
                                    CCAvenueRSAKeyURL = jsonObjectCCavenueDetail.getString("rsakeyurl");
                                    String CCavenueStatus = jsonObjectCCavenueDetail.getString("status");
                                    if (CCavenueStatus.equals("1")) {
                                        radioCCAvenue.setVisibility(View.VISIBLE);
                                    } else {
                                        radioCCAvenue.setVisibility(View.GONE);
                                    }
                                    JSONObject jsonObjectPaypal = jsonObjectPayload.getJSONObject("paypal_detail");
                                    PaypalMode = jsonObjectPaypal.getString("mode");
                                    Paypalusername = jsonObjectPaypal.getString("username");
                                    PaypalPassword = jsonObjectPaypal.getString("password");
                                    String PaypalStatus = jsonObjectPaypal.getString("status");
                                    if (PaypalStatus.equals("1")) {
                                        radioPaypal.setVisibility(View.VISIBLE);
                                    } else {
                                        radioPaypal.setVisibility(View.GONE);
                                    }

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in checkoutfragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR PlaceOrder..
    private void PlaceOrder(final String paymentMethod) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key;
        String[] val;

        if (paymentMethod.equals("1")) {
            key = new String[]{"firstname", "lastname", "address", "landmark", "pincode",
                    "country", "state", "city", "phonenumber",
                    "billing_firstname", "billing_lastname", "billing_address",
                    "billing_landmark", "billing_pincode", "billing_country",
                    "billing_state", "billing_city", "billing_phonenumber",
                    "payment_method", "billingflag"};
            val = new String[]{strShippingFname, strShippingLname, strShippingAddress,
                    strShippingLandmark, strShippingPincode, strShippingCountry,
                    strShippingState, strShippingCity, strShippingPhoneno,
                    strBillingFname, strBillingLname, strBillingAddress,
                    strBillingLandmark, strBillingPincode, strBillingCountry,
                    strBillingState, strBillingCity, strBillingPhoneno,
                    paymentMethod, "1"};
        } else {
            key = new String[]{"firstname", "lastname", "address", "landmark", "pincode",
                    "country", "state", "city", "phonenumber",
                    "billing_firstname", "billing_lastname", "billing_address",
                    "billing_landmark", "billing_pincode", "billing_country",
                    "billing_state", "billing_city", "billing_phonenumber",
                    "payment_method", "billingflag"};
            val = new String[]{strShippingFname, strShippingLname, strShippingAddress,
                    strShippingLandmark, strShippingPincode, strShippingCountry,
                    strShippingState, strShippingCity, strShippingPhoneno,
                    strBillingFname, strBillingLname, strBillingAddress,
                    strBillingLandmark, strBillingPincode, strBillingCountry,
                    strBillingState, strBillingCity, strBillingPhoneno,
                    paymentMethod, "1"};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(
                StaticUtility.URL + StaticUtility.PlaceOrder);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                SharedPreference.ClearPreference(getContext(), Global.CheckoutTotalPrice);
                                SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                                SharedPreference.ClearPreference(getContext(), Global.ISPromocode);
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    String orderid = jsonObject.getString("orderid");
                                    String order_id_key = jsonObject.getString("order_id_key");
//                                    String order_id_key = Catpital(jsonObject.getString("order_id_key"));
                                    if (paymentMethod.equals("1")) {
                                        mListener = (OnFragmentInteractionListener) getContext();
                                        mListener.gotoOrderSuccess("1", "Transaction Successfully!");
                                    } else if (paymentMethod.equals("2")) {
                                        mListener = (OnFragmentInteractionListener) getContext();
                                        mListener.gotoPayment(orderid, totalAmount, strShippingFname, strShippingPhoneno,
                                                Payumoneymerchantid, Payumoneysecretkey, Payumoneysuccessurl,
                                                payumoneyfailureurl);
                                    } else if (paymentMethod.equals("4")) {
                                        Bundle bundle = new Bundle();
                                        bundle.putString(AvenuesParams.ACCESS_CODE, CCavenueAccessCode);
                                        bundle.putString(AvenuesParams.MERCHANT_ID, CCavenuemerchantid);
                                        bundle.putString(AvenuesParams.ORDER_ID, order_id_key);
//                                        bundle.putString(AvenuesParams.ORDER_ID, "A4652335553546B585502ad5f4a5a");
                                        bundle.putString(AvenuesParams.CURRENCY, CCavenueCurrency);
                                        bundle.putString(AvenuesParams.AMOUNT, totalAmount);
                                        bundle.putString(AvenuesParams.REDIRECT_URL, CCAvenuseRedirectURL);
                                        bundle.putString(AvenuesParams.CANCEL_URL, CCAvenueCancelURL);
                                        bundle.putString(AvenuesParams.RSA_KEY_URL, CCAvenueRSAKeyURL);

                                        mListener = (OnFragmentInteractionListener) getContext();
                                        mListener.gotoCCAvence(bundle);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in checkoutfragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR PlaceSameAsShippingOrder..
    private void PlaceSameAsShippingOrder(final String paymentMethod) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key;
        String[] val;

        if (paymentMethod.equals("1")) {
            key = new String[]{"firstname", "lastname", "address", "landmark", "pincode",
                    "country", "state", "city", "phonenumber",
                    "billing_firstname", "billing_lastname", "billing_address",
                    "billing_landmark", "billing_pincode", "billing_country",
                    "billing_state", "billing_city", "billing_phonenumber",
                    "payment_method", "billingflag"};
            val = new String[]{strShippingFname, strShippingLname, strShippingAddress, strShippingLandmark, strShippingPincode,
                    strShippingCountry, strShippingState, strShippingCity, strShippingPhoneno,
                    strShippingFname, strShippingLname, strShippingAddress, strShippingLandmark, strShippingPincode,
                    strShippingCountry, strShippingState, strShippingCity, strShippingPhoneno, paymentMethod, "0"};
        } else {
            key = new String[]{"firstname", "lastname", "address", "landmark", "pincode",
                    "country", "state", "city", "phonenumber",
                    "billing_firstname", "billing_lastname", "billing_address",
                    "billing_landmark", "billing_pincode", "billing_country",
                    "billing_state", "billing_city", "billing_phonenumber",
                    "payment_method", "billingflag"};
            val = new String[]{strShippingFname, strShippingLname, strShippingAddress, strShippingLandmark, strShippingPincode,
                    strShippingCountry, strShippingState, strShippingCity, strShippingPhoneno,
                    strShippingFname, strShippingLname, strShippingAddress, strShippingLandmark, strShippingPincode,
                    strShippingCountry, strShippingState, strShippingCity, strShippingPhoneno, paymentMethod, "0"};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.PlaceOrder);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                SharedPreference.ClearPreference(getContext(), Global.CheckoutTotalPrice);
                                SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                                SharedPreference.ClearPreference(getContext(), Global.ISPromocode);
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    String orderid = jsonObject.getString("orderid");
//                                    String order_id_key = Catpital(jsonObject.getString("order_id_key"));
                                    String order_id_key = jsonObject.getString("order_id_key");

                                    if (paymentMethod.equals("1")) {
                                        mListener = (OnFragmentInteractionListener) getContext();
                                        mListener.gotoOrderSuccess("1", "Transaction Successfully!");
                                    } else if (paymentMethod.equals("2")) {
                                        mListener = (OnFragmentInteractionListener) getContext();
                                        mListener.gotoPayment(orderid, totalAmount, strShippingFname, strShippingPhoneno,
                                                Payumoneymerchantid, Payumoneysecretkey, Payumoneysuccessurl, payumoneyfailureurl);
                                    } else if (paymentMethod.equals("4")) {
                                        Bundle bundle = new Bundle();
                                        bundle.putString(AvenuesParams.ACCESS_CODE, CCavenueAccessCode);
                                        bundle.putString(AvenuesParams.MERCHANT_ID, CCavenuemerchantid);
//                                        bundle.putString(AvenuesParams.ORDER_ID, "a4652335553546a585502ad5f4a5a5");
                                        bundle.putString(AvenuesParams.ORDER_ID, order_id_key);
                                        bundle.putString(AvenuesParams.CURRENCY, CCavenueCurrency);
                                        bundle.putString(AvenuesParams.AMOUNT, totalAmount);
                                        bundle.putString(AvenuesParams.REDIRECT_URL, CCAvenuseRedirectURL);
                                        bundle.putString(AvenuesParams.CANCEL_URL, CCAvenueCancelURL);
                                        bundle.putString(AvenuesParams.RSA_KEY_URL, CCAvenueRSAKeyURL);

                                        mListener = (OnFragmentInteractionListener) getContext();
                                        mListener.gotoCCAvence(bundle);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in checkoutfragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR CheckPincode API..
    private void CheckPincode(String Pincode) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"pincode"};
        String[] val = {Pincode};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetCheckPincode);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    if (jsonObject.getString("count").equals("1")) {
                                        if (!isSendOTP) {
                                            isSendOTP = true;
                                            SendOTPDialog();
                                        }
//                                      Toast.makeText(getContext(), "Delivery Available", Toast.LENGTH_SHORT).show();
                                       /* if (isSameAsShippingcheck) {
                                            if (PaymentMethod.equalsIgnoreCase("COD")) {
                                                PlaceOrder("1");
                                            } else if (PaymentMethod.equalsIgnoreCase("PayUMoney")) {
                                                PlaceOrder("2");
                                            } else if (PaymentMethod.equalsIgnoreCase("CCA")) {
                                                PlaceOrder("4");
                                            } else if (PaymentMethod.equalsIgnoreCase("PayPal")) {
                                                PlaceOrder("3");
                                            }
                                        } else {
                                            if (PaymentMethod.equalsIgnoreCase("COD")) {
                                                PlaceSameAsShippingOrder("1");
                                            } else if (PaymentMethod.equalsIgnoreCase("PayUMoney")) {
                                                PlaceSameAsShippingOrder("2");
                                            } else if (PaymentMethod.equalsIgnoreCase("CCA")) {
                                                PlaceSameAsShippingOrder("4");
                                            } else if (PaymentMethod.equalsIgnoreCase("PayPal")) {
                                                PlaceSameAsShippingOrder("3");
                                            }
                                        }*/

                                    } else {
                                        imgCompletedAddress.setVisibility(View.GONE);
                                        txtNextpaymentnifo.setEnabled(true);
                                        Toast.makeText(getContext(), "Delivery Not Available !", Toast.LENGTH_SHORT).show();

                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in checkoutfragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region chanageEditTextBorder
    public void chanageEditTextBorder(EditText editText) {
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
            editText.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        }
        editText.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) editText.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#FFFFFF"));
        /*gd.setColor(Color.parseColor("#000000"));*/
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(50);
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
    }
    //endregion

    //region chanageButton
    public void chanageButton(Button button) {
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor) != "") {
            button.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        }
//        button.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TEXTCOLOR)));
        button.setBackgroundResource(R.drawable.ic_button);
        GradientDrawable gd = (GradientDrawable) button.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(50);
        /* gd.setStroke(2, Color.parseColor(StaticUtility.BORDERCOLOR));*/
    }
    //endregion

    // region chanageLinearBorder
    public void chanageLinearBorder(LinearLayout linearLayout) {
        linearLayout.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) linearLayout.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#FFFFFF"));
        /*gd.setColor(Color.parseColor("#000000"));*/
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
    }
    //endregion

    //region FOR SendOTPDialog...
    public void SendOTPDialog() {
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.row_otp_send, null, false);
        // find the Textview in the popup layout
        final EditText editPhoneNo;
        final Button btnSendOTP;
        TextView txtOtpTitle;
        ImageView imgCancel;
        FrameLayout flCountryCode;
        final AutoCompleteTextView actCountryCode;
        txtOtpTitle = (TextView) inflatedView.findViewById(R.id.txtOtpTitle);
        editPhoneNo = (EditText) inflatedView.findViewById(R.id.editPhoneNo);
        btnSendOTP = (Button) inflatedView.findViewById(R.id.btnSendOTP);
        actCountryCode = (AutoCompleteTextView) inflatedView.findViewById(R.id.actCountryCode);
        imgCancel = (ImageView) inflatedView.findViewById(R.id.imgCancel);
        flCountryCode = (FrameLayout) inflatedView.findViewById(R.id.flCountryCode);
        editPhoneNo.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        btnSendOTP.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
            txtOtpTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        }
        btnSendOTP.setOnClickListener(this);
        chanageButton(btnSendOTP);
        chanageEditTextBorder(editPhoneNo);
        chanageFramlayoutBorder(flCountryCode, actCountryCode);
        getCountryCodeAPI(actCountryCode);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        OTPVerificationDialog = builder.create();
        OTPVerificationDialog.setCancelable(false);
        OTPVerificationDialog.setView(inflatedView);
        OTPVerificationDialog.setCanceledOnTouchOutside(false);
        if (OTPVerificationDialog.isShowing()) {
            OTPVerificationDialog.dismiss();
        } else {
            OTPVerificationDialog.show();
        }

        btnSendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!TextUtils.isEmpty(editPhoneNo.getText().toString())) {
                    editPhoneNo.setError(null);
                    if (editPhoneNo.length() >= 10) {
                        editPhoneNo.setError(null);
                        OTPVerificationDialog.dismiss();
                        Global.HideSystemKeyboard(getActivity());
                        sendOPTAPI(actCountryCode.getText().toString(), editPhoneNo.getText().toString(), true);
                    } else {
                        editPhoneNo.setError("Please Enter Valid Mobile Number...!");
                    }
                } else {
                    editPhoneNo.setError("Please Enter Mobile Number...!");
                }


            }
        });

        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OTPVerificationDialog.dismiss();
                isSendOTP = false;
            }
        });

        OTPVerificationDialog.show();
    }
    //endregion

    //region FOR OTPVerificationtDialog...
    public void OTPVerificationtDialog(final String strCountryCode,
                                       final String strMoblieNo, boolean resendotp) {
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.row_otp_verification, null, false);
        // find the Textview in the popup layout
        final Button btnContinue, btnResendOtp;
        TextView txtViewVerifyLable, txtViewVerifyLable1;
        ImageView imgCancel;
        txtViewVerifyLable = (TextView) inflatedView.findViewById(R.id.txtViewVerifyLable);
        txtViewVerifyLable1 = (TextView) inflatedView.findViewById(R.id.txtViewVerifyLable1);
        OTPcode1 = (EditText) inflatedView.findViewById(R.id.OTPcode1);
        OTPcode2 = (EditText) inflatedView.findViewById(R.id.OTPcode2);
        OTPcode3 = (EditText) inflatedView.findViewById(R.id.OTPcode3);
        OTPcode4 = (EditText) inflatedView.findViewById(R.id.OTPcode4);
        OTPcode5 = (EditText) inflatedView.findViewById(R.id.OTPcode5);
        OTPcode6 = (EditText) inflatedView.findViewById(R.id.OTPcode6);
        btnContinue = (Button) inflatedView.findViewById(R.id.btnContinue);
        btnResendOtp = (Button) inflatedView.findViewById(R.id.btnResendOtp);
        imgCancel = (ImageView) inflatedView.findViewById(R.id.imgCancel);
        OTPcode1.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        OTPcode2.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        OTPcode3.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        OTPcode4.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        OTPcode5.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        OTPcode6.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        btnContinue.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
        btnResendOtp.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
            txtViewVerifyLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        }
        txtViewVerifyLable1.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));

        txtViewVerifyLable1.setText("Please type verification code" + "\n" + "send to " + "(" + "+" + strCountryCode + ") " + strMoblieNo);
        chanageButton(btnContinue);
        chanageButton(btnResendOtp);
        chanageEditText(OTPcode1);
        chanageEditText(OTPcode2);
        chanageEditText(OTPcode3);
        chanageEditText(OTPcode4);
        chanageEditText(OTPcode5);
        chanageEditText(OTPcode6);


        //region TEXT WATCHER FOR EDIT_TEXT...
        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    if (OTPcode1.getText().hashCode() == s.hashCode()) {
                        OTPcode2.requestFocus();
                        otp = OTPcode1.getText().toString() + OTPcode2.getText().toString()
                                + OTPcode3.getText().toString() + OTPcode4.getText().toString()
                                + OTPcode5.getText().toString() + OTPcode6.getText().toString();
                        if (otp != null) {
                            if (!otp.isEmpty()) {
                                if (otp.length() == 6) {
                                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                    inputMethodManager.hideSoftInputFromWindow(OTPcode6.getWindowToken(), 0);
                                    if (Global.isNetworkAvailable(getContext())) {
                                        VerifyOPTAPI();
                                    }
                                }
                            }
                        }
                    } else if (OTPcode2.getText().hashCode() == s.hashCode()) {
                        OTPcode3.requestFocus();
                        otp = OTPcode1.getText().toString() + OTPcode2.getText().toString()
                                + OTPcode3.getText().toString() + OTPcode4.getText().toString()
                                + OTPcode5.getText().toString() + OTPcode6.getText().toString();
                        if (otp != null) {
                            if (!otp.isEmpty()) {
                                if (otp.length() == 6) {
                                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                    inputMethodManager.hideSoftInputFromWindow(OTPcode6.getWindowToken(), 0);
                                    if (Global.isNetworkAvailable(getContext())) {
                                        VerifyOPTAPI();
                                    }
                                }
                            }
                        }
                    } else if (OTPcode3.getText().hashCode() == s.hashCode()) {
                        OTPcode4.requestFocus();
                        otp = OTPcode1.getText().toString() + OTPcode2.getText().toString()
                                + OTPcode3.getText().toString() + OTPcode4.getText().toString()
                                + OTPcode5.getText().toString() + OTPcode6.getText().toString();
                        if (otp != null) {
                            if (!otp.isEmpty()) {
                                if (otp.length() == 6) {
                                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                    inputMethodManager.hideSoftInputFromWindow(OTPcode6.getWindowToken(), 0);
                                    if (Global.isNetworkAvailable(getContext())) {
                                        VerifyOPTAPI();
                                    }
                                }
                            }
                        }
                    } else if (OTPcode4.getText().hashCode() == s.hashCode()) {
                        OTPcode5.requestFocus();
                        otp = OTPcode1.getText().toString() + OTPcode2.getText().toString()
                                + OTPcode3.getText().toString() + OTPcode4.getText().toString()
                                + OTPcode5.getText().toString() + OTPcode6.getText().toString();
                        if (otp != null) {
                            if (!otp.isEmpty()) {
                                if (otp.length() == 6) {
                                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                    inputMethodManager.hideSoftInputFromWindow(OTPcode6.getWindowToken(), 0);
                                    if (Global.isNetworkAvailable(getContext())) {
                                        VerifyOPTAPI();
                                    }
                                }
                            }
                        }
                    } else if (OTPcode5.getText().hashCode() == s.hashCode()) {
                        OTPcode6.requestFocus();
                        otp = OTPcode1.getText().toString() + OTPcode2.getText().toString()
                                + OTPcode3.getText().toString() + OTPcode4.getText().toString()
                                + OTPcode5.getText().toString() + OTPcode6.getText().toString();
                        if (otp != null) {
                            if (!otp.isEmpty()) {
                                if (otp.length() == 6) {
                                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                    inputMethodManager.hideSoftInputFromWindow(OTPcode6.getWindowToken(), 0);
                                    if (Global.isNetworkAvailable(getContext())) {
                                        VerifyOPTAPI();
                                    }
                                }
                            }
                        }
                    } else if (OTPcode6.getText().hashCode() == s.hashCode()) {
                        otp = OTPcode1.getText().toString() + OTPcode2.getText().toString()
                                + OTPcode3.getText().toString() + OTPcode4.getText().toString()
                                + OTPcode5.getText().toString() + OTPcode6.getText().toString();
                        if (otp != null) {
                            if (!otp.isEmpty()) {
                                if (otp.length() == 6) {
                                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                    inputMethodManager.hideSoftInputFromWindow(OTPcode6.getWindowToken(), 0);
                                    if (Global.isNetworkAvailable(getContext())) {
                                        VerifyOPTAPI();
                                    }
                                }
                            }
                        }
                    }
                } else if (s.length() == 0) {
                    if (OTPcode6.getText().hashCode() == s.hashCode()) {
                        OTPcode5.requestFocus();
                    } else if (OTPcode5.getText().hashCode() == s.hashCode()) {
                        OTPcode4.requestFocus();
                    } else if (OTPcode4.getText().hashCode() == s.hashCode()) {
                        OTPcode3.requestFocus();
                    } else if (OTPcode3.getText().hashCode() == s.hashCode()) {
                        OTPcode2.requestFocus();
                    } else if (OTPcode2.getText().hashCode() == s.hashCode()) {
                        OTPcode1.requestFocus();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        //endregion

        OTPcode1.addTextChangedListener(textWatcher);
        OTPcode2.addTextChangedListener(textWatcher);
        OTPcode3.addTextChangedListener(textWatcher);
        OTPcode4.addTextChangedListener(textWatcher);
        OTPcode5.addTextChangedListener(textWatcher);
        OTPcode6.addTextChangedListener(textWatcher);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        OTPVerificationDialog = builder.create();
        OTPVerificationDialog.setCancelable(false);
        OTPVerificationDialog.setView(inflatedView);
        OTPVerificationDialog.setCanceledOnTouchOutside(false);
        if (OTPVerificationDialog.isShowing()) {
            OTPVerificationDialog.dismiss();
        } else {
            OTPVerificationDialog.show();
        }

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(OTPcode1.getText().toString())) {
                    if (!TextUtils.isEmpty(OTPcode2.getText().toString())) {
                        if (!TextUtils.isEmpty(OTPcode3.getText().toString())) {
                            if (!TextUtils.isEmpty(OTPcode4.getText().toString())) {
                                if (!TextUtils.isEmpty(OTPcode5.getText().toString())) {
                                    if (!TextUtils.isEmpty(OTPcode6.getText().toString())) {
                                        Global.HideSystemKeyboard(getActivity());
                                        VerifyOPTAPI();
                                    } else {
                                        Toast.makeText(getContext(), "Please enter OTP!", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(getContext(), "Please enter OTP!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), "Please enter OTP!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getContext(), "Please enter OTP!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getContext(), "Please enter OTP!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), "Please enter OTP!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ResendOPTAPI();
                OTPcode1.setText("");
                OTPcode2.setText("");
                OTPcode3.setText("");
                OTPcode4.setText("");
                OTPcode5.setText("");
                OTPcode6.setText("");
                OTPcode1.requestFocus();
                sendOPTAPI(strCountryCode, strMoblieNo, false);
            }
        });

        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OTPVerificationDialog.dismiss();
                isVerifyOTP = false;
            }
        });

        OTPVerificationDialog.show();
    }
    //endregion

    //region chanageEditText
    public void chanageEditText(EditText editText) {

        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor) != "") {
            editText.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        }
        editText.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) editText.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#FFFFFF"));
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
    }
    //endregion

    //region chanage Framelayout Border
    public void chanageFramlayoutBorder(FrameLayout frameLayout, AutoCompleteTextView autoCompleteTextView) {
        autoCompleteTextView.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        frameLayout.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) frameLayout.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setCornerRadius(60);
        gd.setColor(Color.parseColor("#FFFFFF"));
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
    }
    //endregion

    // region changeRadioButtonColor
    @SuppressLint("NewApi")
    public void changeRadioButtonColor(RadioButton radioButton) {
        radioButton.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            radioButton.setButtonTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor))));
        }
    }
    //endregion

    //region FOR GET Country Code...
    private void getCountryCodeAPI(final AutoCompleteTextView actCountryCode) {

        String[] key = {};
        String[] val = {};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.getCountryCode);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
//                                Toast.makeText(context, strMessage , Toast.LENGTH_SHORT).show();

                                if (strMessage.equals("OK")) {
                                    JSONArray payloadArray = response.getJSONArray("payload");
                                    countries = new ArrayList<>();
                                    for (int i = 0; i < payloadArray.length(); i++) {
                                        JSONObject object = payloadArray.getJSONObject(i);
                                        String countryName = object.getString("name");
                                        String countryCode = object.getString("code");
                                        countries.add(new SpinnerItem(countryName, countryCode, i));
                                    }
                                    if (countries.size() > 0) {
                                        AutoCompleteForCountryCodeAdapter autoCompleteForCountryCodeAdapter = new AutoCompleteForCountryCodeAdapter(getContext(), R.layout.row_items, countries, actCountryCode);
                                        actCountryCode.setAdapter(autoCompleteForCountryCodeAdapter);
                                    }
                                }
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in RegisActivity.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
//                Toast.makeText(context, anError.toString(), Toast.LENGTH_SHORT).show();
            }

        });
    }
    //endregion

    //region AUTO COMPLETE ADAPTER FOR COUNTRY CODE...
    public class AutoCompleteForCountryCodeAdapter extends ArrayAdapter<SpinnerItem> {
        Context mContext;
        ArrayList<SpinnerItem> mDepartments;
        ArrayList<SpinnerItem> mDepartments_All;
        ArrayList<SpinnerItem> mDepartments_Suggestion;
        int mLayoutResourceId;
        private AutoCompleteTextView autoCompleteTextView;

        public AutoCompleteForCountryCodeAdapter(Context context, int resource, ArrayList<SpinnerItem> departments,
                                                 AutoCompleteTextView autoCompleteTextView) {
            super(context, resource, departments);
            this.mContext = context;
            this.mLayoutResourceId = resource;
            this.mDepartments = new ArrayList<>(departments);
            this.mDepartments_All = new ArrayList<>(departments);
            this.mDepartments_Suggestion = new ArrayList<>();
            this.autoCompleteTextView = autoCompleteTextView;
        }

        public int getCount() {
            return mDepartments.size();
        }

        public SpinnerItem getItem(int position) {
            return mDepartments.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                    convertView = inflater.inflate(mLayoutResourceId, parent, false);
                }
                final SpinnerItem department = getItem(position);
                TextView name = (TextView) convertView.findViewById(R.id.ItemName);
                TextView id = (TextView) convertView.findViewById(R.id.ItemId);
                name.setText(department.getCountrycode());
                name.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                /*id.setText(department.getCountryName());*/

                autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        SpinnerItem pi = getItem(position);
                        strCountry = pi.getCountrycode();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                public String convertResultToString(Object resultValue) {
                    return ((SpinnerItem) resultValue).getCountrycode();
                }

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    if (constraint != null) {
                        mDepartments_Suggestion.clear();
                        for (SpinnerItem department : mDepartments_All) {
                            if (department.getCountrycode().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                                mDepartments_Suggestion.add(department);
                            }
                        }
                        FilterResults filterResults = new FilterResults();
                        filterResults.values = mDepartments_Suggestion;
                        filterResults.count = mDepartments_Suggestion.size();
                        return filterResults;
                    } else {
                        return new FilterResults();
                    }
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    mDepartments.clear();
                    if (results != null && results.count > 0) {
                        // avoids unchecked cast warning when using mDepartments.addAll((ArrayList<Department>) results.values);
                        List<?> result = (List<?>) results.values;
                        for (Object object : result) {
                            if (object instanceof SpinnerItem) {
                                mDepartments.add((SpinnerItem) object);
                            }
                        }
                    } else if (constraint == null) {
                        // no filter, add entire original list back in
                        mDepartments.addAll(mDepartments_All);
                    }
                    notifyDataSetChanged();
                }
            };
        }
    }
    //endregion

    //region FOR sendOPT API...
    private void sendOPTAPI(final String Countycode, final String MobileNo, final boolean resend) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"otp_confirm_country_code", "confirm_order_mobile"};
        String[] val = {Countycode, MobileNo};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.SendOTP);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    isSendOTP = false;
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    usertonken = jsonObject.getString("order_otp_id");
                                    if (!isVerifyOTP) {
                                        isVerifyOTP = true;
                                        if (resend) {
                                            OTPVerificationtDialog(Countycode, MobileNo, resend);
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);

                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in OPTActivity.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
//                Toast.makeText(context, anError.toString(), Toast.LENGTH_SHORT).show();
            }

        });
    }
    //endregion

    //region FOR VerifyOPTAPI API...
    private void VerifyOPTAPI() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"verify_order_otp", "otp_to_verify"};
        String[] val = {otp, usertonken};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.PlaceOrderVerifyOTP);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    isVerifyOTP = false;
                                    OTPVerificationDialog.dismiss();
                                    imgCompletedAddress.setVisibility(View.VISIBLE);
                                    if (isSameAsShippingcheck) {
                                        if (PaymentMethod.equalsIgnoreCase("COD")) {
                                            PlaceOrder("1");
                                        } else if (PaymentMethod.equalsIgnoreCase("PayUMoney")) {
                                            PlaceOrder("2");
                                        } else if (PaymentMethod.equalsIgnoreCase("CCA")) {
                                            PlaceOrder("4");
                                        } else if (PaymentMethod.equalsIgnoreCase("PayPal")) {
                                            PlaceOrder("3");
                                        }
                                    } else {
                                        if (PaymentMethod.equalsIgnoreCase("COD")) {
                                            PlaceSameAsShippingOrder("1");
                                        } else if (PaymentMethod.equalsIgnoreCase("PayUMoney")) {
                                            PlaceSameAsShippingOrder("2");
                                        } else if (PaymentMethod.equalsIgnoreCase("CCA")) {
                                            PlaceSameAsShippingOrder("4");
                                        } else if (PaymentMethod.equalsIgnoreCase("PayPal")) {
                                            PlaceSameAsShippingOrder("3");
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);

                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                isVerifyOTP = false;
                                OTPcode1.setText("");
                                OTPcode2.setText("");
                                OTPcode3.setText("");
                                OTPcode4.setText("");
                                OTPcode5.setText("");
                                OTPcode6.setText("");
                                OTPcode1.requestFocus();
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in OPTActivity.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
//                Toast.makeText(context, anError.toString(), Toast.LENGTH_SHORT).show();
            }

        });
    }
    //endregion

    public static void FirstShippingAddress(Bundle bundle) {
        strShippingFname = bundle.getString("ShippingFname");
        strShippingLname = bundle.getString("ShippingLname");
        strShippingAddress = bundle.getString("ShippingAddress");
        strShippingLandmark = bundle.getString("ShippingLandmark");
        strShippingPincode = bundle.getString("ShippingPincode");
        strShippingCountry = bundle.getString("ShippingCountry");
        strShippingState = bundle.getString("ShippingState");
        strShippingCity = bundle.getString("ShippingCity");
        strShippingPhoneno = bundle.getString("ShippingPhoneNo");

        UserNameForShipping.setText(strShippingFname + " " + strShippingLname);
        txtShippingLastAddress.setText(strShippingAddress + " , " + "\n" +
                strShippingCity + " - " + strShippingPincode + "," + "\n" + strShippingState + " .");
        txtShippingPhoneNo.setText(strShippingPhoneno);
    }

    public static void FirstBillingAddress(Bundle bundle) {
        strBillingFname = bundle.getString("BillingFname");
        strBillingLname = bundle.getString("BillingLname");
        strBillingAddress = bundle.getString("BillingAddress");
        strBillingLandmark = bundle.getString("BillingLandmark");
        strBillingPincode = bundle.getString("BillingPincode");
        strBillingCountry = bundle.getString("BillingCountry");
        strBillingState = bundle.getString("BillingState");
        strBillingCity = bundle.getString("BillingCity");
        strBillingPhoneno = bundle.getString("BillingPhoneNo");

        txtUserNameForBilling.setText(strBillingFname + " " + strBillingLname);
        txtBillingLastAddress.setText(strBillingAddress + " , " + "\n" +
                strBillingCity + " - " + strBillingPincode + "," + "\n" + strBillingState + " .");
        txtBillingPhoneNo.setText(strBillingPhoneno);
    }
}
