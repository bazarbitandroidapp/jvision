package com.pureweblopment.jvision.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.pureweblopment.jvision.Activity.LoginActivity;
import com.pureweblopment.jvision.Activity.MainActivity;
import com.pureweblopment.jvision.Activity.OrderHistoryActivity;
import com.pureweblopment.jvision.Global.Global;
import com.pureweblopment.jvision.Global.SharedPreference;
import com.pureweblopment.jvision.Global.Typefaces;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.concurrent.Executors;

import com.pureweblopment.jvision.Global.SendMail;
import com.pureweblopment.jvision.Global.StaticUtility;

import com.pureweblopment.jvision.R;

import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OrderDetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OrderDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OrderDetailFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    Bundle bundle;

    ImageView imageCartBack, imageNavigation, imageLogo;
    FrameLayout frameLayoutCart;
    TextView txtCatName;
    LinearLayout llBottomNavigation;

    RelativeLayout relativeProgress;

    TextView txtOrderDateTitle, txtOrderDate, txtOrderIDTitle, txtOrderID, txtOrderTotalTitle, txtOrderTotal;
    TextView txtShippingAddressTitle, txtShippingUserName, txtShippingAddress, txtShippingPhoneNo,
            txtBillingAddressTitle, txtBillingUserName, txtBillingAddress, txtBillingPhoneNo;
    TextView txtDiscountTitle, txtDiscount, txtShippingChargesTitle, txtShippingCharges,
            txtCODChargesTitle, txtCODCharges, txtTotalTitle, txtTotal, txtItemTotal, txtItemTotalTitle,
            txtPaymentInfoTitle, txtPaymentMethod;
    Boolean clickable = true;
    String itemorderid, product_id;
    int paymentType, intOrderStatus, intDeliveryDays;
    private AlertDialog ReturnCOD, ReturnOnline, Reviews;

    CardView cardviewBottomNavigation;

    RecyclerView recyclerviewOrderHistoryProduct;
    String selectradio;

    String order_datetime;
    String slug;
    LinearLayout llToolbar, llDiscount, llShippingCharges, llCODCharges;
    int intOrderPaymentStatus = 0;

    public OrderDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OrderDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OrderDetailFragment newInstance(String param1, String param2) {
        OrderDetailFragment fragment = new OrderDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_detail, container, false);

        ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

        Initialization(view);
        TypeFace();
        AppSettings();

        txtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        txtCatName.setText(R.string.orderDetail);

        imageCartBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        llToolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

        bundle = getArguments();
        String slug = bundle.getString("Order_Id");

        SingleOrderDetail(slug);

        return view;
    }

    //region Initialization....
    private void Initialization(View view) {
        relativeProgress = (RelativeLayout) view.findViewById(R.id.relativeProgress);
        txtOrderDateTitle = (TextView) view.findViewById(R.id.txtOrderDateTitle);
        txtOrderDate = (TextView) view.findViewById(R.id.txtOrderDate);
        txtOrderIDTitle = (TextView) view.findViewById(R.id.txtOrderIDTitle);
        txtOrderID = (TextView) view.findViewById(R.id.txtOrderID);
        txtOrderTotalTitle = (TextView) view.findViewById(R.id.txtOrderTotalTitle);
        txtOrderTotal = (TextView) view.findViewById(R.id.txtOrderTotal);
        txtShippingAddressTitle = (TextView) view.findViewById(R.id.txtShippingAddressTitle);
        txtShippingUserName = (TextView) view.findViewById(R.id.txtShippingUserName);
        txtShippingAddress = (TextView) view.findViewById(R.id.txtShippingAddress);
        txtShippingPhoneNo = (TextView) view.findViewById(R.id.txtShippingPhoneNo);
        txtBillingAddressTitle = (TextView) view.findViewById(R.id.txtBillingAddressTitle);
        txtBillingUserName = (TextView) view.findViewById(R.id.txtBillingUserName);
        txtBillingAddress = (TextView) view.findViewById(R.id.txtBillingAddress);
        txtBillingPhoneNo = (TextView) view.findViewById(R.id.txtBillingPhoneNo);

        txtDiscountTitle = (TextView) view.findViewById(R.id.txtDiscountTitle);
        txtDiscount = (TextView) view.findViewById(R.id.txtDiscount);
        txtShippingChargesTitle = (TextView) view.findViewById(R.id.txtShippingChargesTitle);
        txtShippingCharges = (TextView) view.findViewById(R.id.txtShippingCharges);
        txtCODChargesTitle = (TextView) view.findViewById(R.id.txtCODChargesTitle);
        txtCODCharges = (TextView) view.findViewById(R.id.txtCODCharges);
        txtTotalTitle = (TextView) view.findViewById(R.id.txtTotalTitle);
        txtTotal = (TextView) view.findViewById(R.id.txtTotal);
        txtItemTotalTitle = (TextView) view.findViewById(R.id.txtItemTotalTitle);
        txtItemTotal = (TextView) view.findViewById(R.id.txtItemTotal);
        txtPaymentInfoTitle = (TextView) view.findViewById(R.id.txtPaymentInfoTitle);
        txtPaymentMethod = (TextView) view.findViewById(R.id.txtPaymentMethod);

        llToolbar = (LinearLayout) view.findViewById(R.id.llToolbar);

        txtCatName = (TextView) view.findViewById(R.id.txtCatName);
        imageCartBack = (ImageView) view.findViewById(R.id.imageCartBack);

        recyclerviewOrderHistoryProduct = (RecyclerView) view.findViewById(R.id.recyclerviewOrderHistoryProduct);
        llDiscount = (LinearLayout) view.findViewById(R.id.llDiscount);
        llShippingCharges = (LinearLayout) view.findViewById(R.id.llShippingCharges);
        llCODCharges = (LinearLayout) view.findViewById(R.id.llCODCharges);
    }
    //endregion

    //region TypeFace...
    private void TypeFace() {
        txtOrderTotal.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtOrderTotalTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtOrderID.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtOrderIDTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtOrderDate.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtOrderDateTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));

        txtBillingPhoneNo.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtBillingAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtBillingUserName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtBillingAddressTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtShippingPhoneNo.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtShippingAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtShippingUserName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtShippingAddressTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));

        txtTotal.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtTotalTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtCODCharges.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtCODChargesTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtShippingCharges.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtShippingChargesTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtDiscount.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtDiscountTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtItemTotal.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtItemTotalTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtPaymentInfoTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtPaymentMethod.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
    }
    //endregions

    //region AppSettings....
    private void AppSettings() {
        txtOrderTotal.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtOrderID.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtOrderDate.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtOrderDateTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtOrderIDTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtOrderTotalTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtShippingAddressTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtBillingAddressTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtBillingUserName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtShippingUserName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtShippingAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtShippingPhoneNo.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtBillingAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtBillingPhoneNo.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));

        txtTotal.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtTotalTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtCODCharges.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtCODChargesTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtShippingCharges.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtShippingChargesTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtDiscount.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtDiscountTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtItemTotalTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtItemTotal.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtPaymentInfoTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtPaymentMethod.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        llToolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
    }
    //endregion

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    //region FOR SingleOrderDetail...
    private void SingleOrderDetail(String slug) {
        relativeProgress.setVisibility(View.VISIBLE);
        String[] key = {"slug"};
        String[] val = {slug};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.SingleOrderDetail);
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers1(getContext()));
        } else {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers2(getContext()));
        }
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("ok")) {
                                    JSONArray jsonArrayPayload = response.getJSONArray("payload");
                                    JSONObject jsonObjectPayload = jsonArrayPayload.getJSONObject(0);
                                    txtOrderDate.setText(Global.changeDateFormate(jsonObjectPayload.getString("order_datetime"),
                                            "yyyy-MM-dd hh:mm:ss", "dd-MMM-yyyy"));
                                    txtOrderID.setText(jsonObjectPayload.getString("order_id"));
                                    order_datetime = jsonObjectPayload.getString("order_datetime");
                                    intOrderPaymentStatus = Integer.parseInt(jsonObjectPayload.getString("order_payment_status"));

                                    String strPrice = jsonObjectPayload.getString("order_product_amount");
                                    if(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                            StaticUtility.CurrencySignPosition) != null) {
                                        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                StaticUtility.CurrencySignPosition).equals("1")) {
                                            txtOrderTotal.setText(SharedPreference.GetPreference(getContext(),
                                                    Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " +
                                                    strPrice);
                                        } else if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                StaticUtility.CurrencySignPosition).equals("2")) {
                                            txtOrderTotal.setText(strPrice + ". " +
                                                    SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                                        }
                                    }else {
                                        txtOrderTotal.setText(strPrice);
                                    }
                                    String product_discount = jsonObjectPayload.getString("order_total_discount");
                                    String shipping_tax = jsonObjectPayload.getString("order_shipping_amount");
                                    String shipping_price_cod = jsonObjectPayload.getString("order_cod_amount");
                                    String total_price = jsonObjectPayload.getString("order_amount");
                                    String item_price = jsonObjectPayload.getString("order_product_amount");
                                    String order_payment_type_display = jsonObjectPayload.getString("order_payment_type_display");
                                    txtPaymentMethod.setText("(" + order_payment_type_display + ")");

                                    if (product_discount.equalsIgnoreCase("0.00")) {
                                        llDiscount.setVisibility(View.GONE);
                                    } else {
                                        llDiscount.setVisibility(View.VISIBLE);
                                    }
                                    if (shipping_tax.equalsIgnoreCase("0.00")) {
                                        llShippingCharges.setVisibility(View.GONE);
                                    } else {
                                        llShippingCharges.setVisibility(View.VISIBLE);
                                    }
                                    if (shipping_price_cod.equalsIgnoreCase("0.00")) {
                                        llCODCharges.setVisibility(View.GONE);
                                    } else {
                                        llCODCharges.setVisibility(View.VISIBLE);
                                    }

                                    if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                            StaticUtility.CurrencySignPosition).equals("1")) {
                                        txtDiscount.setText(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                StaticUtility.CurrencySign) + ". " + product_discount);
                                        txtShippingCharges.setText(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                StaticUtility.CurrencySign) + ". " + shipping_tax);
                                        txtCODCharges.setText(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                StaticUtility.CurrencySign) + ". " + shipping_price_cod);
                                        txtTotal.setText(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                StaticUtility.CurrencySign) + ". " + total_price);
                                        txtItemTotal.setText(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                StaticUtility.CurrencySign) + ". " + item_price);
                                    } else {
                                        txtDiscount.setText(product_discount + ". " + SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                StaticUtility.CurrencySign));
                                        txtShippingCharges.setText(shipping_tax + ". " + SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                StaticUtility.CurrencySign));
                                        txtCODCharges.setText(shipping_price_cod + ". " + SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                StaticUtility.CurrencySign));
                                        txtTotal.setText(total_price + ". " + SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                StaticUtility.CurrencySign));
                                        txtItemTotal.setText(item_price + ". " + SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                StaticUtility.CurrencySign));
                                    }

                                    paymentType = Integer.parseInt(jsonObjectPayload.getString("order_payment_type"));

                                    JSONArray jsonArrayOrderDetail = jsonObjectPayload.getJSONArray("orderitems");
                                    for (int i = 0; i < jsonArrayOrderDetail.length(); i++) {
                                        JSONObject jsonObjectOrderDetail = jsonArrayOrderDetail.getJSONObject(i);
                                        String fullname = jsonObjectOrderDetail.getString("fullname");
                                        String phone_number = jsonObjectOrderDetail.getString("phone_number");
                                        String address = jsonObjectOrderDetail.getString("address");
                                        String state = jsonObjectOrderDetail.getString("state");
                                        String city = jsonObjectOrderDetail.getString("city");
                                        String pincode = jsonObjectOrderDetail.getString("pincode");
                                        String billing_fullname = jsonObjectOrderDetail.getString("billing_fullname");
                                        String billing_phonenumber = jsonObjectOrderDetail.getString("billing_phonenumber");
                                        String billing_address = jsonObjectOrderDetail.getString("billing_address");
                                        String billing_state = jsonObjectOrderDetail.getString("billing_state");
                                        String billing_city = jsonObjectOrderDetail.getString("billing_city");
                                        String billing_pincode = jsonObjectOrderDetail.getString("billing_pincode");

                                        txtShippingUserName.setText(fullname);
                                        txtShippingAddress.setText(address + "\n" + city + " - " + pincode + "," + "\n" + state + ",");
                                        txtShippingPhoneNo.setText(phone_number);

                                        if (!billing_fullname.equals("")) {
                                            txtBillingUserName.setText(billing_fullname);
                                        }
                                        if (!billing_phonenumber.equals("")) {
                                            txtBillingPhoneNo.setText(billing_phonenumber);
                                        }
                                        if (!billing_address.equals("")) {
                                            txtBillingAddress.setText(billing_address + "\n" + billing_city + " - " + billing_pincode + "," + "\n" + billing_state + ",");
                                        }
                                    }
                                    AdapterOrderHistoryProduct adapterOrderHistoryProduct = new AdapterOrderHistoryProduct(getContext(), jsonArrayOrderDetail);
                                    recyclerviewOrderHistoryProduct.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                    recyclerviewOrderHistoryProduct.setAdapter(adapterOrderHistoryProduct);
                                }
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    MainActivity.manageBackPress(true);
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in OrderDetailFragment.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region AdapterOrderHistoryProduct
    public class AdapterOrderHistoryProduct extends RecyclerView.Adapter<AdapterOrderHistoryProduct.Viewholder> {

        Context context;
        JSONArray jsonArray;

        public AdapterOrderHistoryProduct(Context context, JSONArray jsonArray) {
            this.context = context;
            this.jsonArray = jsonArray;
        }

        @Override
        public AdapterOrderHistoryProduct.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_orderhistory_product, viewGroup, false);
            return new AdapterOrderHistoryProduct.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder holder, final int position) {
            try {
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        holder.pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
                    }
                }
                final JSONObject jsonObjectOrderDetail = jsonArray.getJSONObject(position);
                String name = jsonObjectOrderDetail.getString("name");
                String quantity = jsonObjectOrderDetail.getString("quantity");
                slug = jsonObjectOrderDetail.getString("slug");
                holder.txtOrderDetailProductName.setText(name);
                holder.txtProducDate.setText(Global.changeDateFormate(order_datetime, "yyyy-MM-dd hh:mm:ss", "dd-MMM-yyyy"));
                holder.txtOrderDetailQty.setText(quantity);
                String reviewdetail_id = jsonObjectOrderDetail.getString("reviewdetail_id");

                float floatOrderPrice = Float.parseFloat(jsonObjectOrderDetail.getString("price"));
                int intOrderPrice = Math.round(floatOrderPrice);
                if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                        StaticUtility.CurrencySignPosition).equals("1")) {
                    holder.txtProductPrice.setText(SharedPreference.GetPreference(getContext(),
                            Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + jsonObjectOrderDetail.getString("price"));
                } else if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                        StaticUtility.CurrencySignPosition).equals("2")) {
                    holder.txtProductPrice.setText(jsonObjectOrderDetail.getString("price") + ". " +
                            SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                }

                int intReturn_days = Integer.parseInt(jsonObjectOrderDetail.getString("return_days"));
                String order_item_status = jsonObjectOrderDetail.getString("order_item_status");
                intOrderStatus = Integer.parseInt(jsonObjectOrderDetail.getString("order_item_status"));
                String DeliveryDays = jsonObjectOrderDetail.getString("DeliveryDays");
                if (!DeliveryDays.equals("")) {
                    intDeliveryDays = Integer.parseInt(DeliveryDays);
                } else {
                    intDeliveryDays = 0;
                }

                if (reviewdetail_id.equalsIgnoreCase("")) {
                    holder.txtAddReviews.setVisibility(View.VISIBLE);
                    holder.llOrderStatus.setVisibility(View.VISIBLE);
                } else {
                    holder.txtAddReviews.setVisibility(View.GONE);
                    holder.llOrderStatus.setVisibility(View.GONE);
                }

                if (intOrderStatus >= 0) {
                    if (paymentType > 1 && intOrderStatus != 1) {
                        if (intOrderStatus == 0 && intOrderPaymentStatus == 0) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("cancelled order");
                            holder.txtProductReturn.setEnabled(false);
                            clickable = false;
                        } else if (intOrderStatus == 5) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("cancelled order");
                            holder.txtProductReturn.setEnabled(false);
                            clickable = false;
                        } else if (intOrderStatus == 4) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            if (intReturn_days > 0 && intDeliveryDays <= intReturn_days) {
                                holder.txtProductReturn.setText("Return Order");
                                holder.txtProductReturn.setClickable(true);
                            } else {
                                holder.txtProductReturn.setText("order delivered");
                                holder.txtProductReturn.setEnabled(false);
                            }
                        } else if (intOrderStatus == 6) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("order returned");
                            holder.txtProductReturn.setEnabled(false);
                        } else if (intOrderStatus == 7) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("order refund");
                            holder.txtProductReturn.setEnabled(false);
                        } else if (intOrderStatus == 3) {
                            holder.txtProductReturn.setVisibility(View.GONE);
                        } else {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("cancel");
                            holder.txtProductReturn.setEnabled(true);
                            clickable = true;
                        }
                    } else {
                        if (intOrderStatus == 5) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("cancelled order");
                            holder.txtProductReturn.setEnabled(false);
                            clickable = false;
                        } else if (intOrderStatus == 4) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            if (intReturn_days > 0 && intDeliveryDays <= intReturn_days) {
                                holder.txtProductReturn.setText("Return Order");
                                holder.txtProductReturn.setClickable(true);
                            } else {
                                holder.txtProductReturn.setText("order delivered");
                                holder.txtProductReturn.setEnabled(false);
                            }
                        } else if (intOrderStatus == 6) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("order returned");
                            holder.txtProductReturn.setEnabled(false);
                        } else if (intOrderStatus == 7) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("order refund");
                            holder.txtProductReturn.setEnabled(false);
                        } else if (intOrderStatus == 3) {
                            holder.txtProductReturn.setVisibility(View.GONE);
                        } else {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("cancel");
                            holder.txtProductReturn.setEnabled(true);
                            clickable = true;
                        }
                    }

                    /*if (paymentType > 1 && intOrderStatus != 1) {
                        if (intOrderStatus == 5) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("cancelled order");
                            holder.txtProductReturn.setEnabled(false);
                            clickable = false;
                        } else if (intOrderStatus == 4) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            if (intReturn_days > 0 && intDeliveryDays <= intReturn_days) {
                                holder.txtProductReturn.setText("Return Order");
                                holder.txtProductReturn.setClickable(true);
                            } else {
                                holder.txtProductReturn.setText("order delivered");
                                holder.txtProductReturn.setEnabled(false);
                            }
                        } else if (intOrderStatus == 6) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("order returned");
                            holder.txtProductReturn.setEnabled(false);
                        } else if (intOrderStatus == 7) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("order refund");
                            holder.txtProductReturn.setEnabled(false);
                        } else if (intOrderStatus == 3) {
                            holder.txtProductReturn.setVisibility(View.GONE);
                        } else {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("cancel");
                            holder.txtProductReturn.setEnabled(true);
                            clickable = true;
                        }
                    } else {
                        if (intOrderStatus == 5) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("cancelled order");
                            holder.txtProductReturn.setEnabled(false);
                            clickable = false;
                        } else if (intOrderStatus == 4) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            if (intReturn_days > 0 && intDeliveryDays <= intReturn_days) {
                                holder.txtProductReturn.setText("Return Order");
                                holder.txtProductReturn.setClickable(true);
                            } else {
                                holder.txtProductReturn.setText("order delivered");
                                holder.txtProductReturn.setEnabled(false);
                            }
                        } else if (intOrderStatus == 6) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("order returned");
                            holder.txtProductReturn.setEnabled(false);
                        } else if (intOrderStatus == 7) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("order refund");
                            holder.txtProductReturn.setEnabled(false);
                        } else if (intOrderStatus == 3) {
                            holder.txtProductReturn.setVisibility(View.GONE);
                        } else {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("cancel");
                            holder.txtProductReturn.setEnabled(true);
                            clickable = true;
                        }
                    }*/
                }

                itemorderid = jsonObjectOrderDetail.getString("orderitem_id");
                product_id = jsonObjectOrderDetail.getString("product_id");

                holder.txtProductReturn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (holder.txtProductReturn.getText().toString().
                                equalsIgnoreCase("cancel")) {
                            if (clickable) {
                                try {
                                    final String itemorderid = jsonObjectOrderDetail.getString("orderitem_id");
                                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                                    builder1.setMessage("Are you sure you want to cancel this order?");
                                    builder1.setCancelable(true);

                                    builder1.setPositiveButton(
                                            "Yes",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                    if (paymentType > 1) {
                                                        try {
                                                            String itemorderid = jsonObjectOrderDetail.getString("orderitem_id");
                                                            String product_id = jsonObjectOrderDetail.getString("product_id");
                                                            OnlineOrderCancelDialog(holder.txtProductReturn,
                                                                    itemorderid, product_id, position);
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }

                                                    }else {
                                                        CancelOrder(holder.txtProductReturn,
                                                                itemorderid, position);
                                                    }
                                                }
                                            });

                                    builder1.setNegativeButton(
                                            "No",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });

                                    AlertDialog alert11 = builder1.create();
                                    alert11.show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        } else {
                            if (paymentType > 1) {
                                try {
                                    String itemorderid = jsonObjectOrderDetail.getString("orderitem_id");
                                    String product_id = jsonObjectOrderDetail.getString("product_id");
                                    OnlinePaymentDialog(holder.txtProductReturn, itemorderid, product_id, position);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                try {
                                    String itemorderid = jsonObjectOrderDetail.getString("orderitem_id");
                                    String product_id = jsonObjectOrderDetail.getString("product_id");
                                    CODDialog(holder.txtProductReturn, itemorderid, product_id, position);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                });
                holder.txtAddReviews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            String itemorderid = jsonObjectOrderDetail.getString("orderitem_id");
                            String product_id = jsonObjectOrderDetail.getString("product_id");
                            ADDReviewsDialog(holder.txtAddReviews, itemorderid, product_id);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

              /*  JSONArray jsonArrayImage = jsonObjectOrderDetail.getJSONArray("main_image");
                String image = jsonArrayImage.get(0).toString();*/

                JSONObject jsonObjectImage = jsonObjectOrderDetail.getJSONObject("main_image");
                String image = jsonObjectImage.getString("main_image");

                //region Image
                String picUrl = null;
                try {
                    URL urla = null;
                    urla = new URL(image.replaceAll("%20", " "));
                    URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    // Capture position and set to the ImageView
                    Picasso.with(getContext())
                            .load(picUrl)
                            .into(holder.imageOrderDetail, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    holder.rlImgHolder.setVisibility(View.GONE);
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }

                                @Override
                                public void onError() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }
                            });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in OrderDetailFragment.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in OrderDetailFragment.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
                //endregion

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return jsonArray.length();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            ImageView imageOrderDetail;
            TextView txtOrderDetailProductName, txtQtyTitle, txtOrderDetailQty, txtProducDate, txtProductPrice,
                    txtProductReturn, txtAddReviews;
            LinearLayout llOrderStatus;
            RelativeLayout rlImgHolder;
            ProgressBar pbImgHolder;

            public Viewholder(View itemView) {
                super(itemView);
                txtOrderDetailProductName = (TextView) itemView.findViewById(R.id.txtOrderDetailProductName);
                txtQtyTitle = (TextView) itemView.findViewById(R.id.txtQtyTitle);
                txtOrderDetailQty = (TextView) itemView.findViewById(R.id.txtOrderDetailQty);
                txtProducDate = (TextView) itemView.findViewById(R.id.txtProducDate);
                txtProductPrice = (TextView) itemView.findViewById(R.id.txtProductPrice);
                txtProductReturn = (TextView) itemView.findViewById(R.id.txtProductReturn);
                txtAddReviews = (TextView) itemView.findViewById(R.id.txtAddReviews);
                imageOrderDetail = (ImageView) itemView.findViewById(R.id.imageOrderDetail);
                llOrderStatus = (LinearLayout) itemView.findViewById(R.id.llOrderStatus);
                rlImgHolder = (RelativeLayout) itemView.findViewById(R.id.rlImgHolder);
                pbImgHolder = (ProgressBar) itemView.findViewById(R.id.pbImgHolder);

                txtOrderDetailProductName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
                txtQtyTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
                txtOrderDetailQty.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
                txtProducDate.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
                txtProductPrice.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
                txtProductReturn.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
                txtAddReviews.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));

                txtQtyTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                txtProducDate.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                txtOrderDetailProductName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txtOrderDetailQty.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txtProductPrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txtProductReturn.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txtAddReviews.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));

            }
        }
    }
    //endregion

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void RefreshListing(int itemPositon, String ItemStatus);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    //region FOR CancelOrder API..
    private void CancelOrder(final TextView txtProductReturn, String itemorderid,
                             final int position) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"itemorderid", "status"};
        String[] val = {itemorderid, "5"};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.CancelOrder);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    txtProductReturn.setText("cancelled order");
                                    txtProductReturn.setEnabled(false);
                                    /*recyclerviewOrderHistoryProduct.getAdapter().notifyDataSetChanged();*/
                                    mListener = (OnFragmentInteractionListener) getContext();
                                    mListener.RefreshListing(position, "5");
//                                    txtProductReturn.setClickable(false);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    MainActivity.manageBackPress(true);
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in OrderDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR Cancel Online Order API..
    private void CancelOnlineOrder(String holderName, String BankName, String BranchName, String ACNo, String IFSCode,
                           String reason, final TextView txtProductReturn, String itemorderid, String product_id, final int position) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"itemorderid", "order_id", "product_id", "account_holder_name", "bank_name", "branch_name",
                "account_no", "ifsc_code", "reason", "status", "operation_type"};
        String[] val = {itemorderid, txtOrderID.getText().toString(), product_id, holderName, BankName, BranchName, ACNo,
                IFSCode, reason, "5", "online"};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.CancelOnlineOrder);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    txtProductReturn.setText("cancelled order");
                                    txtProductReturn.setEnabled(false);
                                    mListener = (OnFragmentInteractionListener) getContext();
                                    mListener.RefreshListing(position, "5");
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    MainActivity.manageBackPress(true);
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in OrderDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR OnlinePaymentCancelDialog...
    public void OnlineOrderCancelDialog(final TextView txtProductReturn,
                                          final String itemorderid, final String product_id,
                                          final int position) {

        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.row_online_order_cancel_dialog, null, false);
        // find the Textview in the popup layout
        final EditText editAccountHolderName, editBankName, editBranchName, editAccountNo, editIFSCCode, editReason;
        final Button btnSubmit, btnCancel;
        final TextView txtCODTitle;
        txtCODTitle = (TextView) inflatedView.findViewById(R.id.txtCODTitle);
        editAccountHolderName = (EditText) inflatedView.findViewById(R.id.editAccountHolderName);
        editBankName = (EditText) inflatedView.findViewById(R.id.editBankName);
        editBranchName = (EditText) inflatedView.findViewById(R.id.editBranchName);
        editAccountNo = (EditText) inflatedView.findViewById(R.id.editAccountNo);
        editIFSCCode = (EditText) inflatedView.findViewById(R.id.editIFSCCode);
        editReason = (EditText) inflatedView.findViewById(R.id.editReason);
        btnSubmit = (Button) inflatedView.findViewById(R.id.btnSubmit);
        btnCancel = (Button) inflatedView.findViewById(R.id.btnCancel);
        editAccountHolderName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editBankName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editBranchName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editAccountNo.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editIFSCCode.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editReason.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        btnSubmit.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        btnCancel.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        btnSubmit.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        chanageButton(btnSubmit);
        chanageButton(btnCancel);
        txtCODTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        /*editAccountHolderName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editBankName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editBranchName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editAccountNo.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editIFSCCode.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editReason.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));*/

        chanageEditTextBorder(editAccountHolderName);
        chanageEditTextBorder(editBankName);
        chanageEditTextBorder(editBranchName);
        chanageEditTextBorder(editAccountNo);
        chanageEditTextBorder(editIFSCCode);
        chanageEditTextBorder(editReason);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        ReturnCOD = builder.create();
        ReturnCOD.setCancelable(false);
        ReturnCOD.setView(inflatedView);
        ReturnCOD.setCanceledOnTouchOutside(true);
        if (ReturnCOD.isShowing()) {
            ReturnCOD.dismiss();
        } else {
            ReturnCOD.show();
        }

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editAccountHolderName.setText("");
                editBankName.setText("");
                editBranchName.setText("");
                editAccountNo.setText("");
                editIFSCCode.setText("");
                editReason.setText("");
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(editAccountHolderName.getText().toString())) {
                    editAccountHolderName.setError(null);
                    if (!TextUtils.isEmpty(editBankName.getText().toString())) {
                        editBankName.setError(null);
                        if (!TextUtils.isEmpty(editBranchName.getText().toString())) {
                            editBranchName.setError(null);
                            if (!TextUtils.isEmpty(editAccountNo.getText().toString())) {
                                editAccountNo.setError(null);
                                if (!TextUtils.isEmpty(editIFSCCode.getText().toString())) {
                                    editIFSCCode.setError(null);
                                    if (!TextUtils.isEmpty(editReason.getText().toString())) {
                                        editReason.setError(null);
                                        ReturnCOD.dismiss();
                                        CancelOnlineOrder(editAccountHolderName.getText().toString(), editBankName.getText().toString(), editBranchName.getText().toString(),
                                                editAccountNo.getText().toString(), editIFSCCode.getText().toString(),
                                                editReason.getText().toString(), txtProductReturn
                                                , itemorderid, product_id, position);
                                    } else {
                                        editReason.setError("Please Enter Reason...!");
                                    }
                                } else {
                                    editIFSCCode.setError("Please Enter IFSC CODE...!");
                                }
                            } else {
                                editAccountNo.setError("Please Enter Account Number...!");
                            }
                        } else {
                            editBranchName.setError("Please Enter Bank Name...!");
                        }
                    } else {
                        editBankName.setError("Please Enter Bank Name...!");
                    }
                } else {
                    editAccountHolderName.setError("Please Enter Account Holder Name...!");
                }

            }
        });
        ReturnCOD.show();
    }
    //endregion

    //region FOR CODDialog...
    public void CODDialog(final TextView txtProductReturn, final String itemorderid, final String product_id, final int position) {

        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.row_return_cod_dialog, null, false);
        // find the Textview in the popup layout
        final EditText editAccountHolderName, editBankName, editBranchName, editAccountNo, editIFSCCode, editReason;
        final Button btnSubmit;
        final TextView txtCODTitle;
        txtCODTitle = (TextView) inflatedView.findViewById(R.id.txtCODTitle);
        editAccountHolderName = (EditText) inflatedView.findViewById(R.id.editAccountHolderName);
        editBankName = (EditText) inflatedView.findViewById(R.id.editBankName);
        editBranchName = (EditText) inflatedView.findViewById(R.id.editBranchName);
        editAccountNo = (EditText) inflatedView.findViewById(R.id.editAccountNo);
        editIFSCCode = (EditText) inflatedView.findViewById(R.id.editIFSCCode);
        editReason = (EditText) inflatedView.findViewById(R.id.editReason);
        btnSubmit = (Button) inflatedView.findViewById(R.id.btnSubmit);
        editAccountHolderName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editBankName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editBranchName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editAccountNo.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editIFSCCode.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editReason.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        btnSubmit.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        btnSubmit.setOnClickListener(this);
        chanageButton(btnSubmit);
        txtCODTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        /*editAccountHolderName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editBankName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editBranchName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editAccountNo.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editIFSCCode.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editReason.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));*/

        chanageEditTextBorder(editAccountHolderName);
        chanageEditTextBorder(editBankName);
        chanageEditTextBorder(editBranchName);
        chanageEditTextBorder(editAccountNo);
        chanageEditTextBorder(editIFSCCode);
        chanageEditTextBorder(editReason);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        ReturnCOD = builder.create();
        ReturnCOD.setCancelable(false);
        ReturnCOD.setView(inflatedView);
        ReturnCOD.setCanceledOnTouchOutside(true);
        if (ReturnCOD.isShowing()) {
            ReturnCOD.dismiss();
        } else {
            ReturnCOD.show();
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(editAccountHolderName.getText().toString())) {
                    editAccountHolderName.setError(null);
                    if (!TextUtils.isEmpty(editBankName.getText().toString())) {
                        editBankName.setError(null);
                        if (!TextUtils.isEmpty(editBranchName.getText().toString())) {
                            editBranchName.setError(null);
                            if (!TextUtils.isEmpty(editAccountNo.getText().toString())) {
                                editAccountNo.setError(null);
                                if (!TextUtils.isEmpty(editIFSCCode.getText().toString())) {
                                    editIFSCCode.setError(null);
                                    if (!TextUtils.isEmpty(editReason.getText().toString())) {
                                        editReason.setError(null);
                                        ReturnCOD.dismiss();
                                        ReturnCOD(editAccountHolderName.getText().toString(), editBankName.getText().toString(), editBranchName.getText().toString(),
                                                editAccountNo.getText().toString(), editIFSCCode.getText().toString(),
                                                editReason.getText().toString(), txtProductReturn
                                                , itemorderid, product_id, position);
                                    } else {
                                        editReason.setError("Please Enter Reason...!");
                                    }
                                } else {
                                    editIFSCCode.setError("Please Enter IFSC CODE...!");
                                }
                            } else {
                                editAccountNo.setError("Please Enter Account Number...!");
                            }
                        } else {
                            editBranchName.setError("Please Enter Bank Name...!");
                        }
                    } else {
                        editBankName.setError("Please Enter Bank Name...!");
                    }
                } else {
                    editAccountHolderName.setError("Please Enter Account Holder Name...!");
                }

            }
        });
        ReturnCOD.show();
    }
    //endregion

    //region FOR ReturnCOD API..
    private void ReturnCOD(String holderName, String BankName, String BranchName, String ACNo, String IFSCode,
                           String reason, final TextView txtProductReturn, String itemorderid, String product_id, final int position) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"itemorder_id", "order_id", "product_id", "account_holder_name", "bank_name", "branch_name",
                "account_no", "ifsc_code", "reason"};
        String[] val = {itemorderid, txtOrderID.getText().toString(), product_id, holderName, BankName, BranchName, ACNo,
                IFSCode, reason};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.ReturnCODOrder);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    txtProductReturn.setText("Order Returned");
                                    txtProductReturn.setClickable(false);
                                    mListener = (OnFragmentInteractionListener) getContext();
                                    mListener.RefreshListing(position, "6");
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    MainActivity.manageBackPress(true);
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in OrderDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR OnlinePaymentDialog...
    public void OnlinePaymentDialog(final TextView txtProductReturn, final String itemorderid, final String product_id, final int position) {
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.row_return_online_payment_dialog, null, false);
        // find the Textview in the popup layout
        final EditText editReason;
        final Button btnSubmit;
        TextView txtCODTitle;
        txtCODTitle = (TextView) inflatedView.findViewById(R.id.txtCODTitle);
        editReason = (EditText) inflatedView.findViewById(R.id.editReason);
        btnSubmit = (Button) inflatedView.findViewById(R.id.btnSubmit);
        editReason.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        btnSubmit.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtCODTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editReason.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        btnSubmit.setOnClickListener(this);
        chanageButton(btnSubmit);
        chanageEditTextBorder(editReason);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        ReturnOnline = builder.create();
        ReturnOnline.setCancelable(false);
        ReturnOnline.setView(inflatedView);
        ReturnOnline.setCanceledOnTouchOutside(true);
        if (ReturnOnline.isShowing()) {
            ReturnOnline.dismiss();
        } else {
            ReturnOnline.show();
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!TextUtils.isEmpty(editReason.getText().toString())) {
                    editReason.setError(null);
                    ReturnOnline.dismiss();
                    ReturnOnlinePayment(editReason.getText().toString(), txtProductReturn, itemorderid, product_id, position);
                } else {
                    editReason.setError("Please Enter Reason...!");
                }


            }
        });
        ReturnOnline.show();
    }
    //endregion

    //region FOR ReturnOnlinePayment API..
    private void ReturnOnlinePayment(String reason, final TextView txtProductReturn, String itemorderid, String product_id, final int position) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"itemorder_id", "order_id", "product_id", "reason"};
        String[] val = {itemorderid, txtOrderID.getText().toString(), product_id, reason};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.ReturnNetOrder);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    txtProductReturn.setText("Order Returned");
                                    txtProductReturn.setClickable(false);
                                    mListener = (OnFragmentInteractionListener) getContext();
                                    mListener.RefreshListing(position, "6");
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    MainActivity.manageBackPress(true);
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in OrderDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR ADDReviewsDialog...
    public void ADDReviewsDialog(final TextView txtAddReviews, final String itemId, final String ProductID) {

        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View inflatedView = layoutInflater.inflate(R.layout.row_add_review, null, false);
        final EditText editUserName, editUserEmail, editUserTitle, editUserDes;
        final TextView txtRecomment;
        final Button btnSubmit;
        final RatingBar reviewRatingbar;
        final RadioGroup radioGroupRecomme;
        final RadioButton radioYes, radioNo;
        final TextView txtCODTitle;
        txtCODTitle = (TextView) inflatedView.findViewById(R.id.txtCODTitle);
        reviewRatingbar = (RatingBar) inflatedView.findViewById(R.id.reviewRatingbar1);
        editUserName = (EditText) inflatedView.findViewById(R.id.editUserName);
        editUserEmail = (EditText) inflatedView.findViewById(R.id.editUserEmail);
        editUserTitle = (EditText) inflatedView.findViewById(R.id.editUserTitle);
        editUserDes = (EditText) inflatedView.findViewById(R.id.editUserDes);
        txtRecomment = (TextView) inflatedView.findViewById(R.id.txtRecomment);
        btnSubmit = (Button) inflatedView.findViewById(R.id.btnSubmit);
        radioGroupRecomme = (RadioGroup) inflatedView.findViewById(R.id.radioGroupRecomme);
        radioYes = (RadioButton) inflatedView.findViewById(R.id.radioYes);
        radioNo = (RadioButton) inflatedView.findViewById(R.id.radioNo);
        editUserName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editUserEmail.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editUserTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editUserDes.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtRecomment.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        btnSubmit.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        btnSubmit.setOnClickListener(this);
        editUserEmail.setEnabled(false);
        chanageButton(btnSubmit);

        chanageEditTextBorder(editUserDes);
        chanageEditTextBorder(editUserName);
        chanageEditTextBorder(editUserEmail);
        chanageEditTextBorder(editUserTitle);
        changeRadioButtonColor(radioYes);
        changeRadioButtonColor(radioNo);

        txtCODTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editUserName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editUserEmail.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editUserTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editUserDes.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtRecomment.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));

        LayerDrawable stars = (LayerDrawable) reviewRatingbar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);

        String name = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_Name);
        String email = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_EMAIL);
        editUserName.setText(name);
        editUserEmail.setText(email);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        Reviews = builder.create();
        Reviews.setCancelable(false);
        Reviews.setView(inflatedView);
        Reviews.setCanceledOnTouchOutside(true);
        if (Reviews.isShowing()) {
            Reviews.dismiss();
        } else {
            Reviews.show();
        }

        radioGroupRecomme.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (radioGroupRecomme.getCheckedRadioButtonId()) {
                    case R.id.radioYes:
                        if (radioYes.isChecked()) {
                            selectradio = "1";
                            radioNo.setChecked(false);

                        }
                        break;
                    case R.id.radioNo:
                        if (radioNo.isChecked()) {
                            selectradio = "0";
                            radioYes.setChecked(false);
                        }
                        break;
                }
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(editUserName.getText().toString())) {
                    editUserName.setError(null);
                    if (!TextUtils.isEmpty(editUserEmail.getText().toString())) {
                        editUserEmail.setError(null);
                        if (!TextUtils.isEmpty(editUserTitle.getText().toString())) {
                            editUserTitle.setError(null);
                            if (!TextUtils.isEmpty(editUserDes.getText().toString())) {
                                editUserDes.setError(null);
                                if (reviewRatingbar.getRating() != 0.0) {
                                    Reviews.dismiss();
                                    SubmitReviews(editUserDes.getText().toString(), editUserEmail.getText().toString(),
                                            editUserName.getText().toString(), editUserTitle.getText().toString(),
                                            String.valueOf(reviewRatingbar.getRating()), selectradio, txtAddReviews, itemId, ProductID);
                                } else {
                                    Toast.makeText(getContext(), "Rating can\'t be black!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                editUserDes.setError("Please Enter Description...!");
                            }
                        } else {
                            editUserTitle.setError("Please Enter Title...!");
                        }
                    } else {
                        editUserEmail.setError("Please Enter Email Id...!");
                    }
                } else {
                    editUserName.setError("Please Enter User Name...!");
                }

            }
        });
        Reviews.show();
    }
    //endregion

    //region FOR SubmitReviews API..
    private void SubmitReviews(String des, String email, String name, String title, String rating, String recomment,
                               final TextView txtAddReviews, String itemId, String ProductId) {
        relativeProgress.setVisibility(View.VISIBLE);

        String userid = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);

        String[] key = {"description", "email", "itemorder_id", "name", "product_id", "rating", "title", "user_id", "recommendthisproject"};
        String[] val = {des, email, itemId, name, ProductId, rating, title, userid, recomment};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.SubmitReview);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    txtAddReviews.setVisibility(View.GONE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    MainActivity.manageBackPress(true);
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region chanageButton
    public void chanageButton(Button button) {
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor) != "") {
            button.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        }
        button.setBackgroundResource(R.drawable.ic_button);
        GradientDrawable gd = (GradientDrawable) button.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(50);
        /* gd.setStroke(2, Color.parseColor(StaticUtility.BORDERCOLOR));*/
    }
    //endregion

    //region chanageEditTextBorder
    public void chanageEditTextBorder(EditText editText) {
        editText.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editText.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) editText.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#FFFFFF"));
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

    }
    //endregion

    // region changeRadioButtonColor
    @SuppressLint("NewApi")
    public void changeRadioButtonColor(RadioButton radioButton) {
        radioButton.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        radioButton.setButtonTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor))));
    }
    //endregion
}
