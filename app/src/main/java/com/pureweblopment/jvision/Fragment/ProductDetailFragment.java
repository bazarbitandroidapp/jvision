package com.pureweblopment.jvision.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.pureweblopment.jvision.Activity.LoginActivity;
import com.pureweblopment.jvision.Activity.MainActivity;
import com.pureweblopment.jvision.Adapter.AdapterReviewsList;
import com.pureweblopment.jvision.Adapter.ViewPagerAdapter;
import com.pureweblopment.jvision.Global.Global;
import com.pureweblopment.jvision.Global.SendMail;
import com.pureweblopment.jvision.Global.SharedPreference;
import com.pureweblopment.jvision.Global.StaticUtility;
import com.pureweblopment.jvision.Global.Typefaces;
import com.pureweblopment.jvision.Model.DefaultVariation;
import com.pureweblopment.jvision.Model.Reviews;
import com.pureweblopment.jvision.Model.Variations;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;

import com.pureweblopment.jvision.R;

import io.fabric.sdk.android.services.settings.AppSettingsData;
import me.relex.circleindicator.CircleIndicator;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProductDetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProductDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

public class ProductDetailFragment extends Fragment implements View.OnClickListener, TextWatcher {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    Bundle bundle;

    ViewPager ViewPagerSlider;
    TextView txtAddtoWishlist, txtAddtoCart, txtProductName, txtProductSortDes, txt_product_base_price,
            textProductSalePrice, txtTitleVariation, txtisAvalible, txtTitleVariationColor;
    RatingBar reviewRatingbar;
    LinearLayout llCard, llVariation, llReviews;

    String[] img;
    int currentPage = 0;
    Timer timer;

    RelativeLayout relativeProgress;

    CircleIndicator indicator;
    String strSlug = "";
    String termsName = "", strSelectVariation = "", strVariationSlug = "";
    Spinner spinnerVariation;

    TextView txtReturndays, txtCashondelivery, txtExpectedDelivery, txtCheckOption;
    ImageView imageExpectedDelivery, imageCashondelivery, imageReturndays;

    TextView txtProductDetail, txtProductDetailTitle, txtRelatedProduct, txtReviewsTitle;

    EditText editTextPincode;
    Button btnCheck;

    int intSalePrice, intBasePrice;

    JSONArray jsonArrayMainImage, jsonArrayGelleryImage;

    RecyclerView recyclerviewReletedProduct, recyclerviewReviews;

    String is_wishlist = "", strProductID = "", strProductName = "", strMainProductID = "";
    int Stock_Status = 0, quantity = 0;

    ArrayList<Reviews> reviewses;

    ImageView imageCartBack, imageNavigation, imageLogo;
    FrameLayout frameLayoutCart;
    TextView txtCatName;
    LinearLayout llBottomNavigation;
    RecyclerView recyclerviewVariation;
    float floatRating;

    private OnFragmentInteractionListener mListener;
    JSONArray jsonArrayVariation = null;
    String productVariationid = "", Variationattritem_name = "";
    ArrayList<String> productIDs = new ArrayList<>(), variationAttribute = new ArrayList<>();

    ArrayList<DefaultVariation> defaultVariations;
    int VariationObject = 0;
    CardView cardviewBottomNavigation;
    String strPincodeValidation = "";
    View viewVariation;
    FrameLayout framLayoutProductDetail;
    FragmentManager fragmentManager;

    boolean flag = false;
    int count = 0;

    JSONArray attrArray = new JSONArray();
    JSONArray attrtermArray = new JSONArray();

    String[] strArrayAttr, strArrayAttrTerm, strAttrTerm;

    ArrayList<String> arrayListAttribute;
    ArrayList<String> arrayListAttributeTerm;

    String strPincodeMessage = "Please enter pincode and check delivery available or not!";

    LinearLayout llRelatedProduct;

    public ProductDetailFragment() {
    }

    JSONArray jsonArrayRelatedProduct;

    public static ProductDetailFragment newInstance(String param1, String param2) {
        ProductDetailFragment fragment = new ProductDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MainActivity.manageBackPress(false);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product_detail, container, false);

        ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

        bundle = getArguments();
        strSlug = bundle.getString("slug");
        strProductName = bundle.getString("name");
//        strProductID = bundle.getString("ProductID");

        imageCartBack = (ImageView) getActivity().findViewById(R.id.imageCartBack);
        imageNavigation = (ImageView) getActivity().findViewById(R.id.imageNavigation);
        imageLogo = (ImageView) getActivity().findViewById(R.id.imageLogo);

        frameLayoutCart = (FrameLayout) getActivity().findViewById(R.id.frameLayoutCart);
        txtCatName = (TextView) getActivity().findViewById(R.id.txtCatName);
        llBottomNavigation = (LinearLayout) getActivity().findViewById(R.id.llBottomNavigation);
        cardviewBottomNavigation = (CardView) getActivity().findViewById(R.id.cardviewBottomNavigation);

        txtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        imageCartBack.setVisibility(View.VISIBLE);
        frameLayoutCart.setVisibility(View.VISIBLE);
        txtCatName.setVisibility(View.VISIBLE);
        txtCatName.setText(strProductName);

        imageNavigation.setVisibility(View.GONE);
        imageLogo.setVisibility(View.GONE);
//        llBottomNavigation.setVisibility(View.GONE);
        cardviewBottomNavigation.setVisibility(View.GONE);

        imageCartBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    getActivity().onBackPressed();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });

        MainActivity.ClearSharePreference = true;

        Initialization(view);
        TypeFace();
        OnClickListener();
        AppSettings();

        chanageEditTextBorder(editTextPincode);
        chanageButton(btnCheck);

        is_wishlist = SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active);

        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.Is_Cart_Active).equals("1")) {
            llCard.setVisibility(View.VISIBLE);
            txtAddtoCart.setVisibility(View.VISIBLE);
            frameLayoutCart.setVisibility(View.VISIBLE);
        } else {
            txtAddtoCart.setVisibility(View.GONE);
            frameLayoutCart.setVisibility(View.GONE);
        }

        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active).equals("1")) {
            llCard.setVisibility(View.VISIBLE);
            txtAddtoWishlist.setVisibility(View.VISIBLE);
        } else {
            txtAddtoWishlist.setVisibility(View.GONE);
        }

        getProductDetails(strSlug);

        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, StaticUtility.sPincode) != null) {
            String strPincode = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, StaticUtility.sPincode);
            editTextPincode.setText(strPincode);
        }

        return view;
    }

    //region Initialization
    private void Initialization(View view) {
        ViewPagerSlider = (ViewPager) view.findViewById(R.id.ViewPagerSlider);
        txtAddtoWishlist = (TextView) view.findViewById(R.id.txtAddtoWishlist);
        txtAddtoCart = (TextView) view.findViewById(R.id.txtAddtoCart);
        llCard = (LinearLayout) view.findViewById(R.id.llCard);
        relativeProgress = (RelativeLayout) view.findViewById(R.id.relativeProgress);
        indicator = (CircleIndicator) view.findViewById(R.id.indicator);

        txtProductName = (TextView) view.findViewById(R.id.txtProductName);
        txtProductSortDes = (TextView) view.findViewById(R.id.txtProductSortDes);
        txt_product_base_price = (TextView) view.findViewById(R.id.txt_product_base_price);
        textProductSalePrice = (TextView) view.findViewById(R.id.textProductSalePrice);
//        txtTitleVariation = (TextView) view.findViewById(R.id.txtTitleVariation);
//        txtTitleVariationColor = (TextView) view.findViewById(R.id.txtTitleVariationColor);

        txtisAvalible = (TextView) view.findViewById(R.id.txtisAvalible);
        txtCheckOption = (TextView) view.findViewById(R.id.txtCheckOption);
        txtRelatedProduct = (TextView) view.findViewById(R.id.txtRelatedProduct);
        txtReviewsTitle = (TextView) view.findViewById(R.id.txtReviewsTitle);

        editTextPincode = (EditText) view.findViewById(R.id.editTextPincode);
        btnCheck = (Button) view.findViewById(R.id.btnCheck);

        llVariation = (LinearLayout) view.findViewById(R.id.llVariation);
        llReviews = (LinearLayout) view.findViewById(R.id.llReviews);

        spinnerVariation = (Spinner) view.findViewById(R.id.spinnerVariation);

        txtReturndays = (TextView) view.findViewById(R.id.txtReturndays);
        txtCashondelivery = (TextView) view.findViewById(R.id.txtCashondelivery);
        txtExpectedDelivery = (TextView) view.findViewById(R.id.txtExpectedDelivery);

        imageExpectedDelivery = (ImageView) view.findViewById(R.id.imageExpectedDelivery);
        imageCashondelivery = (ImageView) view.findViewById(R.id.imageCashondelivery);
        imageReturndays = (ImageView) view.findViewById(R.id.imageReturndays);

        txtProductDetail = (TextView) view.findViewById(R.id.txtProductDetail);
        txtProductDetailTitle = (TextView) view.findViewById(R.id.txtProductDetailTitle);
        viewVariation = (View) view.findViewById(R.id.viewVariation);

        recyclerviewReletedProduct = (RecyclerView) view.findViewById(R.id.recyclerviewReletedProduct);
        recyclerviewReviews = (RecyclerView) view.findViewById(R.id.recyclerviewReviews);
      /*  recyclerviewSize = (RecyclerView) view.findViewById(R.id.recyclerviewSize);
        recyclerviewColor = (RecyclerView) view.findViewById(R.id.recyclerviewColor);*/
        recyclerviewVariation = (RecyclerView) view.findViewById(R.id.recyclerviewVariation);
        framLayoutProductDetail = (FrameLayout) view.findViewById(R.id.framLayoutProductDetail);

        reviewRatingbar = (RatingBar) view.findViewById(R.id.reviewRatingbar);
        LayerDrawable stars = (LayerDrawable) reviewRatingbar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

        llRelatedProduct = (LinearLayout) view.findViewById(R.id.llRelatedProduct);
    }
    //endregion

    //region OnClickListener
    private void OnClickListener() {
        txtAddtoCart.setOnClickListener(this);
        txtAddtoWishlist.setOnClickListener(this);
        btnCheck.setOnClickListener(this);
        editTextPincode.addTextChangedListener(this);
    }
    //endregion

    //region TypeFace
    private void TypeFace() {
        txtAddtoCart.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
        txtAddtoWishlist.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
        txtProductName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtProductSortDes.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txt_product_base_price.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        textProductSalePrice.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
//        txtTitleVariation.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
//        txtTitleVariationColor.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtExpectedDelivery.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtCashondelivery.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtReturndays.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtProductDetail.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtProductDetailTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtRelatedProduct.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtReviewsTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
    }
    //endregion

    //region AppSettings
    private void AppSettings() {
        txtProductName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        textProductSalePrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txt_product_base_price.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtProductSortDes.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
//        txtTitleVariation.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TEXTCOLOR)));
//        txtTitleVariationColor.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TEXTCOLOR)));
        txtReturndays.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtCashondelivery.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtExpectedDelivery.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtProductDetailTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtProductDetail.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
//        txtAddtoCart.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.EDITTEXTCOLOR)));
//        txtAddtoWishlist.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.EDITTEXTCOLOR)));

        txtAddtoCart.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtAddtoCart.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
//        txtAddtoWishlist.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtAddtoWishlist.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtCheckOption.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtRelatedProduct.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtReviewsTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
    }
    //endregion

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtAddtoCart:
                if (txtAddtoCart.getText().toString().equalsIgnoreCase("add to cart")) {
                    if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, StaticUtility.sPincode) != null) {
                        String strPincode = SharedPreference.GetPreference(getContext(),
                                Global.LOGIN_PREFERENCE, StaticUtility.sPincode);
                        if (strPincode.equalsIgnoreCase(editTextPincode.getText().toString())) {
                            txtisAvalible.setText(getString(R.string.delivery_available));
                            txtisAvalible.setTextColor(Color.parseColor("#008000"));
                            imageCashondelivery.setImageResource(R.drawable.ic_right);
                            if (!strProductID.equalsIgnoreCase("")) {
                                AddToCart(strProductID);
                            } else {
                                AddToCart(strMainProductID);
                            }
                        } else {
                            Toast.makeText(getContext(), strPincodeMessage, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getContext(), strPincodeMessage, Toast.LENGTH_SHORT).show();
                    }
                } else if (txtAddtoCart.getText().toString().equalsIgnoreCase("notify me")) {
                    if (!strProductID.equalsIgnoreCase("")) {
                        CheckItemInNofityMe(strProductID);
                    } else {
                        CheckItemInNofityMe(strMainProductID);
                    }
                } else if (txtAddtoCart.getText().toString().equalsIgnoreCase("go to cart")) {
                    productIDs.clear();
                    variationAttribute.clear();
                    SharedPreference.ClearPreference(getContext(), Global.IsClickPreference);
                    mListener = (OnFragmentInteractionListener) getContext();
                    mListener.GoToCartList();
                }
                break;
            case R.id.txtAddtoWishlist:
                if (!strProductID.equalsIgnoreCase("")) {
                    AddToWishListProduct(strProductID);
                } else {
                    AddToWishListProduct(strMainProductID);
                }
                break;
            case R.id.btnCheck:
                if (!TextUtils.isEmpty(editTextPincode.getText().toString())) {
                    CheckPincode();
                } else {
                    Toast.makeText(getContext(), "Pincode Required!", Toast.LENGTH_SHORT).show();
                }
                hideKeyboard(getActivity());
                break;
        }

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.length() > 0) {
            strPincodeMessage = getString(R.string.check_delivery_available_or_not);
        } else if (s.length() == 0) {
            txtisAvalible.setVisibility(View.GONE);
            strPincodeMessage = getString(R.string.please_enter_pincode_and_check_delivery_available_or_not);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {


    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void onAddToCartProduct();

        void GoToCartList();

        void WishlistCount();
    }

    //region ViewPager...
    private void getViewPager(final ViewPager viewPager) {
//        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getActivity(), sliderses);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getContext(), img);
        viewPager.setAdapter(viewPagerAdapter);
        if (img.length > 1) {
            indicator.setVisibility(View.VISIBLE);
            indicator.setViewPager(viewPager);
        } else {
            indicator.setVisibility(View.GONE);
        }

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == img.length) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 500, 3000);
    }
    //endregion

    //region FOR getProductDetails API...
    private void getProductDetails(final String slug) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key;
        String[] val;

        String user_id = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID) != null) {
            key = new String[]{"slug", "user_id"};
            val = new String[]{slug, user_id};
        } else {
            key = new String[]{"slug", "user_id"};
            val = new String[]{slug, ""};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetSigalProduct);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
//                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");

                                    if (jsonObjectPayload.has("products")) {

                                        JSONObject jsonObjectProducts = jsonObjectPayload.getJSONObject("products");
                                        String product_type = jsonObjectProducts.getString("product_type");
                                        strProductName = jsonObjectProducts.getString("name");
                                        String strShortDescription = jsonObjectProducts.getString("short_description");
                                        /*txtCatName.setText(strProductName);*/
                                        txtProductName.setText(strProductName);
                                        txtProductSortDes.setText(strShortDescription);

                                        String expected_delivery_time = jsonObjectProducts.getString("expected_delivery_time");
                                        String returnDays = jsonObjectProducts.getString("return_days");
                                        String Description = jsonObjectProducts.getString("description");
//                                        strProductID = jsonObjectProducts.getString("product_id");
                                        strMainProductID = jsonObjectProducts.getString("product_id");
                                        /*quantity = Integer.parseInt(jsonObjectProducts.getString("quantity"));*/

                                        if (!expected_delivery_time.equals("")) {
                                            txtExpectedDelivery.setText("Expected Delivery, " + expected_delivery_time + " Days");
                                            imageExpectedDelivery.setImageResource(R.drawable.ic_right);
                                        } else {
                                            txtExpectedDelivery.setText("Expected Delivery, ");
                                            imageExpectedDelivery.setImageResource(R.drawable.ic_worrng);
                                        }
                                        if (!returnDays.equals("")) {
                                            txtReturndays.setText(returnDays + " Days Returns Available");
                                            imageReturndays.setImageResource(R.drawable.ic_right);
                                        } else {
                                            txtReturndays.setText("Days Returns Available");
                                            imageReturndays.setImageResource(R.drawable.ic_worrng);
                                        }
                                        txtProductDetail.setText(Description);

                                        if (product_type.equalsIgnoreCase("variable")) {
                                            getDefualtVariationData(strMainProductID);
                                            JSONArray jsonArrayVariations = jsonObjectProducts.getJSONArray("variations");
                                            for (int i = 0; i < jsonArrayVariations.length(); i++) {
                                                JSONObject jsonObjectVariations = jsonArrayVariations.getJSONObject(i);
                                                String defaultvariation = jsonObjectVariations.getString("defaultvariation");
                                                if (defaultvariation.equals("1")) {
                                                    Stock_Status = Integer.parseInt(jsonObjectVariations.getString("stock_status"));
                                                    quantity = Integer.parseInt(jsonObjectVariations.getString("quantity"));
                                                    strProductID = jsonObjectVariations.getString("product_id");
                                                    strProductName = jsonObjectVariations.getString("name");
                                                    /*txtCatName.setText(strProductName);*/
                                                    float floatSalePrice = Float.parseFloat(jsonObjectVariations.getString("price"));
                                                    intBasePrice = Math.round(floatSalePrice);
                                                    float floatBasePrice = Float.parseFloat(jsonObjectVariations.getString("sale_price"));
                                                    intSalePrice = Math.round(floatBasePrice);

                                                    if (intSalePrice > 0) {
                                                        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                                StaticUtility.CurrencySignPosition).equals("1")) {
                                                            textProductSalePrice.setText(SharedPreference.GetPreference(getContext(),
                                                                    Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + jsonObjectVariations.getString("sale_price"));
                                                            txt_product_base_price.setText(SharedPreference.GetPreference(getContext(),
                                                                    Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + jsonObjectVariations.getString("price"));
                                                        } else if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                                StaticUtility.CurrencySignPosition).equals("2")) {
                                                            textProductSalePrice.setText(jsonObjectVariations.getString("sale_price") + ". " +
                                                                    SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                                                            txt_product_base_price.setText(jsonObjectVariations.getString("price") + ". " +
                                                                    SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                                                        }
                                                    } else {
                                                        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                                StaticUtility.CurrencySignPosition).equals("1")) {
                                                            textProductSalePrice.setText(SharedPreference.GetPreference(getContext(),
                                                                    Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + jsonObjectVariations.getString("price"));
                                                            txt_product_base_price.setVisibility(View.GONE);
                                                        } else if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                                StaticUtility.CurrencySignPosition).equals("2")) {
                                                            textProductSalePrice.setText(jsonObjectVariations.getString("price") + " " +
                                                                    SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                                                            txt_product_base_price.setVisibility(View.GONE);
                                                        }
                                                    }

                                                    Object objProducts = jsonObjectVariations.get("gallery_images");
                                                    if (!objProducts.equals("")) {
                                                        jsonArrayGelleryImage = jsonObjectVariations.getJSONArray("gallery_images");
                                                        JSONObject jsonObjectImages = jsonObjectVariations.getJSONObject("main_image");
                                                        String images = jsonObjectImages.getString("main_image");
                                                        img = new String[jsonArrayGelleryImage.length() + 1];
                                                        img[0] = images;
                                                        for (int j = 0; j < jsonArrayGelleryImage.length(); j++) {
                                                            JSONObject jsonObjectGelleryImages = jsonArrayGelleryImage.getJSONObject(j);
                                                            String GelleryImages = jsonObjectGelleryImages.getString("main_image");
                                                            img[j + 1] = GelleryImages;
                                                        }
                                                    } else {
                                                        img = new String[1];
                                                        JSONObject jsonObjectImages = jsonObjectVariations.getJSONObject("main_image");
                                                        String images = jsonObjectImages.getString("main_image");
                                                        img[0] = images;
                                                    }
                                                }
                                            }

                                            if (jsonObjectProducts.has("usedVariation")) {
                                                llVariation.setVisibility(View.VISIBLE);
                                                viewVariation.setVisibility(View.VISIBLE);
                                                jsonArrayVariation = jsonObjectProducts.getJSONArray("usedVariation");
                                                /*AdapterAttributeTitle adapterAttributeTitle = new AdapterAttributeTitle(getContext(), jsonArrayVariation);
                                                recyclerviewVariation.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                                recyclerviewVariation.setAdapter(adapterAttributeTitle);*/
                                            } else {
                                                llVariation.setVisibility(View.GONE);
                                                viewVariation.setVisibility(View.GONE);
                                            }
                                        } else {
                                            Stock_Status = Integer.parseInt(jsonObjectProducts.getString("stock_status"));
                                            quantity = Integer.parseInt(jsonObjectProducts.getString("quantity"));
                                            float floatSalePrice = Float.parseFloat(jsonObjectProducts.getString("price"));
                                            intBasePrice = Math.round(floatSalePrice);
                                            float floatBasePrice = Float.parseFloat(jsonObjectProducts.getString("sale_price"));
                                            intSalePrice = Math.round(floatBasePrice);

                                            if (intSalePrice > 0) {
                                                if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                        StaticUtility.CurrencySignPosition).equals("1")) {
                                                    textProductSalePrice.setText(SharedPreference.GetPreference(getContext(),
                                                            Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + String.valueOf(intSalePrice));
                                                    txt_product_base_price.setText(SharedPreference.GetPreference(getContext(),
                                                            Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + String.valueOf(intBasePrice));
                                                } else if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                        StaticUtility.CurrencySignPosition).equals("2")) {
                                                    textProductSalePrice.setText(String.valueOf(intSalePrice) + ". " +
                                                            SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                                                    txt_product_base_price.setText(String.valueOf(intBasePrice) + ". " +
                                                            SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                                                }
                                            } else {
                                                if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                        StaticUtility.CurrencySignPosition).equals("1")) {
                                                    textProductSalePrice.setText(SharedPreference.GetPreference(getContext(),
                                                            Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + String.valueOf(intBasePrice));
                                                    txt_product_base_price.setVisibility(View.GONE);
                                                } else if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                        StaticUtility.CurrencySignPosition).equals("2")) {
                                                    textProductSalePrice.setText(String.valueOf(intBasePrice) + " " +
                                                            SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                                                    txt_product_base_price.setVisibility(View.GONE);
                                                }
                                            }
                                            Object objProducts = jsonObjectProducts.get("gallery_images");
                                            if (!objProducts.equals("")) {
                                                jsonArrayGelleryImage = jsonObjectProducts.getJSONArray("gallery_images");
                                                JSONObject jsonObjectImages = jsonObjectProducts.getJSONObject("main_image");
                                                String images = jsonObjectImages.getString("main_image");
                                                img = new String[jsonArrayGelleryImage.length() + 1];
                                                img[0] = images;
                                                for (int i = 0; i < jsonArrayGelleryImage.length(); i++) {
                                                    JSONObject jsonObjectGelleryImages = jsonArrayGelleryImage.getJSONObject(i);
                                                    String GelleryImages = jsonObjectGelleryImages.getString("main_image");
                                                    img[i + 1] = GelleryImages;
                                                }
                                            } else {
                                                img = new String[1];
                                                JSONObject jsonObjectImages = jsonObjectProducts.getJSONObject("main_image");
                                                String images = jsonObjectImages.getString("main_image");
                                                img[0] = images;
                                            }
                                        }
                                    }

                                    if (Stock_Status == 0 || quantity <= 0) {
                                        txtAddtoCart.setText("notify me");
                                    } else {
                                        if (!strProductID.equalsIgnoreCase("")) {
                                            CheckItemInCart(strProductID);
                                        } else {
                                            CheckItemInCart(strMainProductID);
                                        }
                                    }

                                }
                                getViewPager(ViewPagerSlider);


                                RelatedProducts(slug);
                                getReviews(slug);
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }


                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region ADAPTER AdapterAttributeTitle...
    public class AdapterAttributeTitle extends RecyclerView.Adapter<AdapterAttributeTitle.Viewholder> {

        Context context;
        JSONArray jsonArray;

        public AdapterAttributeTitle(Context context, JSONArray jsonArray) {
            this.context = context;
            this.jsonArray = jsonArray;
        }

        @Override
        public AdapterAttributeTitle.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_attribute_title, viewGroup, false);
            return new AdapterAttributeTitle.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder viewholder, final int position) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(position);
                VariationObject = jsonArray.length();
                viewholder.txtTitleVariation.setText("select" + " " + jsonObject.getString("name"));
                JSONArray jsonArray = jsonObject.getJSONArray("terms");
                AdapterAttribute adapterAttribute = new AdapterAttribute(getContext(),
                        jsonArray, position, jsonArray.length());
                viewholder.recyclerviewVariation.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                viewholder.recyclerviewVariation.setAdapter(adapterAttribute);

                if (position == VariationObject - 1) {
                    viewholder.viewVariation.setVisibility(View.GONE);
                }
                if (position != 0) {
                    count++;
                } else {
                    count = 0;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return jsonArray.length();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            TextView txtTitleVariation;
            RecyclerView recyclerviewVariation;
            View viewVariation;

            public Viewholder(View itemView) {
                super(itemView);
                txtTitleVariation = (TextView) itemView.findViewById(R.id.txtTitleVariation);
                recyclerviewVariation = (RecyclerView) itemView.findViewById(R.id.recyclerviewVariation);
                viewVariation = (View) itemView.findViewById(R.id.viewVariation);
                txtTitleVariation.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                txtTitleVariation.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            }
        }

    }
    //endregion

    //region ADAPTER AdapterAttribute...
    public class AdapterAttribute extends RecyclerView.Adapter<AdapterAttribute.Viewholder> {
        Context context;
        JSONArray jsonArray;
        boolean isClick = false;
        int Position, arraySize;
        String attributetTerm = "";

        public AdapterAttribute(Context context, JSONArray jsonArray, int Position, int arraySize) {
            this.context = context;
            this.jsonArray = jsonArray;
            this.arraySize = arraySize;
            this.Position = Position;
            arrayListAttribute = new ArrayList<>();
            arrayListAttributeTerm = new ArrayList<>();
        }

        @Override
        public AdapterAttribute.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_variation, viewGroup, false);
            return new AdapterAttribute.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder viewholder, final int position) {
            try {
                final JSONObject jsonObject = jsonArray.getJSONObject(position);
                viewholder.txtVariationName.setText(jsonObject.getString("attritem_name"));
                String name = jsonObject.getString("name");
                String attritem_name = jsonObject.getString("attritem_name");
                String productid = jsonObject.getString("z_productid_fk");
                String z_proattritemrelid_pk = jsonObject.getString("z_proattritemrelid_pk");
                String z_attributeid_pk = jsonObject.getString("z_attributeid_pk");
                String z_attributetermid_fk = jsonObject.getString("z_attributetermid_fk");

                attributetTerm = attributetTerm + "," + z_attributetermid_fk;
                strAttrTerm[Position] = attributetTerm;

                /*if (SharedPreference.GetPreference(getContext(), Global.IsClickPreference, StaticUtility.ArrayAttribut) != null) {
                    attrArray = new JSONArray(SharedPreference.GetPreference(getContext(), Global.IsClickPreference, StaticUtility.ArrayAttribut));
                    attrtermArray = new JSONArray(SharedPreference.GetPreference(getContext(), Global.IsClickPreference, StaticUtility.ArrayAttributTerm));
                    for (int i = 0; i < attrArray.length(); i++) {
                        arrayListAttribute.add(String.valueOf(attrArray.get(i)));
                        arrayListAttributeTerm.add(String.valueOf(attrtermArray.get(i)));
                    }
                }*/

                if (arrayListAttribute.size() > 0) {
                    if (arrayListAttribute.contains(z_attributeid_pk)) {
                        if (arrayListAttributeTerm.contains(z_attributetermid_fk)) {
                            chanageTextViewBorder(viewholder.txtVariationName);
                        } else {
                            chanageTextViewBorder1(viewholder.txtVariationName);
                        }
                    }
                } else {
                    chanageTextViewBorder1(viewholder.txtVariationName);
                    if (defaultVariations.size() > 0) {
                        for (int i = 0; i < defaultVariations.size(); i++) {
                            if (defaultVariations.get(i).getAttritem_name().contains(attritem_name)) {
                                chanageTextViewBorder(viewholder.txtVariationName);
                            }
                        }
                    }
                }


                viewholder.llVariation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            arrayListAttribute = new ArrayList<>();
                            arrayListAttributeTerm = new ArrayList<>();

                            JSONObject jsonObject1 = jsonArray.getJSONObject(position);
                            chanageTextViewBorder(viewholder.txtVariationName);
                            /*strArrayAttr[position] = jsonObject1.optString("z_attributeid_pk");*/
                            String Name = jsonObject.optString("attritem_name");
                            arrayListAttribute.add(jsonObject.optString("z_attributeid_pk"));
                            arrayListAttributeTerm.add(jsonObject.optString("z_attributetermid_fk"));
                            attrArray = new JSONArray();
                            attrtermArray = new JSONArray();
                            for (int j = 0; j < strAttrTerm.length; j++) {
                                if (strAttrTerm[j].contains(jsonObject.optString("z_attributetermid_fk"))) {
                                    strArrayAttrTerm[j] = jsonObject.optString("z_attributetermid_fk");
                                }
                            }
                            for (int i = 0; i < strArrayAttr.length; i++) {
                                attrArray.put(strArrayAttr[i]);
                                attrtermArray.put(strArrayAttrTerm[i]);
                            }
                            notifyDataSetChanged();
                            /*SharedPreference.CreatePreference(getContext(), Global.IsClickPreference);
                            SharedPreference.SavePreference(StaticUtility.ArrayAttribut, attrArray.toString());
                            SharedPreference.SavePreference(StaticUtility.ArrayAttributTerm, attrtermArray.toString());*/
                            getVariationSelection(String.valueOf(VariationObject));
//                            Toast.makeText(context, Name + " " + attrArray.toString() + attrtermArray.toString(), Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return jsonArray.length();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            TextView txtVariationName;
            LinearLayout llVariation;
            ImageView image_not_available;
            FrameLayout frameLayoutVariation;

            public Viewholder(View itemView) {
                super(itemView);
                txtVariationName = (TextView) itemView.findViewById(R.id.txtVariationName);
                llVariation = (LinearLayout) itemView.findViewById(R.id.llVariation);
                image_not_available = (ImageView) itemView.findViewById(R.id.image_not_available);
                frameLayoutVariation = (FrameLayout) itemView.findViewById(R.id.frameLayoutVariation);

                txtVariationName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

                txtVariationName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            }
        }
    }
    //endregion

    //region FOR getVariationSelection API...
    private void getVariationSelection(String Count) {
        relativeProgress.setVisibility(View.VISIBLE);

        JSONObject jsonObjectParam = new JSONObject();

        try {
            jsonObjectParam.put("count", Count);
            jsonObjectParam.put("product_id", strMainProductID);
            jsonObjectParam.put("attrArr", attrArray);
            jsonObjectParam.put("attrtermArr", attrtermArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.DisableVariationOption);
        postRequestBuilder.addJSONObjectBody(jsonObjectParam)
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    txtAddtoWishlist.setVisibility(View.VISIBLE);
                                    JSONObject jsonObject = response.optJSONObject("payload");
                                    if (jsonObject.has("product_id")) {
                                        String product_id = jsonObject.optString("product_id");
                                        if (!product_id.equalsIgnoreCase("")) {
                                            getVariationData(product_id);
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                /*Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();*/
                                txtAddtoCart.setText("out of stock");
                                txtAddtoWishlist.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR getVariationData API...
    private void getVariationData(String productid) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = new String[]{"z_productid_pk"};
        String[] val = new String[]{productid};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetVariationData);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    strProductID = jsonObjectPayload.getString("product_id");
                                    int Stock_Status = Integer.parseInt(jsonObjectPayload.getString("stock_status"));
                                    int quantity = Integer.parseInt(jsonObjectPayload.getString("quantity"));

                                    if (Stock_Status == 0 || quantity <= 0) {
                                        txtAddtoCart.setText("notify me");
                                    } else {
                                        if (!strProductID.equalsIgnoreCase("")) {
                                            CheckItemInCart(strProductID);
                                        } else {
                                            CheckItemInCart(strMainProductID);
                                        }
                                    }
                                    //region gallery_images
//                                    jsonArrayMainImage = jsonObjectPayload.getJSONArray("main_image");
                                    Object objProducts = jsonObjectPayload.get("gallery_images");
                                    if (!objProducts.equals("")) {
                                        JSONArray jsonArrayGelleryImage = jsonObjectPayload.getJSONArray("gallery_images");
                                        JSONObject jsonObjectImages = jsonObjectPayload.getJSONObject("main_image");
                                        String images = jsonObjectImages.getString("main_image");
                                        img = new String[jsonArrayGelleryImage.length() + 1];
//                                                img[0] = String.valueOf(jsonObjectProducts.getJSONArray("main_image").get(0));
                                        img[0] = images;
                                        for (int i = 0; i < jsonArrayGelleryImage.length(); i++) {
                                            JSONObject jsonObjectGelleryImages = jsonArrayGelleryImage.getJSONObject(i);
                                            String GelleryImages = jsonObjectGelleryImages.getString("main_image");
                                            img[i + 1] = GelleryImages;
//                                                img[i + 1] = String.valueOf(jsonObjectProducts.getJSONArray("gallery_images").get(i));
                                               /* for (int i = 0; i < jsonObjectProducts.getJSONArray("gallery_images").length(); i++) {
                                                    img[i + 1] = String.valueOf(jsonObjectProducts.getJSONArray("gallery_images").get(i));
                                                }*/
                                        }
                                    } else {
                                        img = new String[1];
                                        JSONObject jsonObjectImages = jsonObjectPayload.getJSONObject("main_image");
                                        String images = jsonObjectImages.getString("main_image");
                                        img[0] = images;
//                                            img[0] = String.valueOf(jsonObjectProducts.getJSONArray("main_image").get(0));
                                    }
                                    //endregion

                                    float floatSalePrice = Float.parseFloat(jsonObjectPayload.getString("price"));
                                    intBasePrice = Math.round(floatSalePrice);
                                    float floatBasePrice = Float.parseFloat(jsonObjectPayload.getString("sale_price"));
                                    intSalePrice = Math.round(floatBasePrice);

                                    if (intSalePrice > 0) {
                                        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                StaticUtility.CurrencySignPosition).equals("1")) {
                                            txt_product_base_price.setVisibility(View.VISIBLE);
                                            textProductSalePrice.setText(SharedPreference.GetPreference(getContext(),
                                                    Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + String.valueOf(intSalePrice));
                                            txt_product_base_price.setText(SharedPreference.GetPreference(getContext(),
                                                    Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + String.valueOf(intBasePrice));
                                        } else if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                StaticUtility.CurrencySignPosition).equals("2")) {
                                            txt_product_base_price.setVisibility(View.VISIBLE);
                                            textProductSalePrice.setText(String.valueOf(intSalePrice) + ". " +
                                                    SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                                            txt_product_base_price.setText(String.valueOf(intBasePrice) + ". " +
                                                    SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                                        }
                                    } else {
                                        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                StaticUtility.CurrencySignPosition).equals("1")) {
                                            textProductSalePrice.setText(SharedPreference.GetPreference(getContext(),
                                                    Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + String.valueOf(intBasePrice));
                                            txt_product_base_price.setVisibility(View.GONE);
                                        } else if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                StaticUtility.CurrencySignPosition).equals("2")) {
                                            textProductSalePrice.setText(String.valueOf(intBasePrice) + ". " +
                                                    SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                                            txt_product_base_price.setVisibility(View.GONE);
                                        }
                                    }
                                }
                                /*CheckItemInCart(strProductID);*/
                                getViewPager(ViewPagerSlider);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR CheckPincode API..
    private void CheckPincode() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"pincode"};
        String[] val = {editTextPincode.getText().toString()};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetCheckPincode);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    if (jsonObject.getString("count").equals("1")) {
                                        txtisAvalible.setVisibility(View.VISIBLE);
                                        txtisAvalible.setText(getString(R.string.delivery_available));
                                        /*strPincodeValidation = "Delivery Available";*/
                                        txtisAvalible.setTextColor(Color.parseColor("#008000"));
                                        imageCashondelivery.setImageResource(R.drawable.ic_right);
                                        SharedPreference.CreatePreference(getContext(), Global.LOGIN_PREFERENCE);
                                        SharedPreference.SavePreference(StaticUtility.sPincode, editTextPincode.getText().toString());
                                    } else {
                                        txtisAvalible.setVisibility(View.VISIBLE);
                                        txtisAvalible.setText(getString(R.string.delivery_not_available));
                                        txtisAvalible.setTextColor(Color.parseColor("#FF0000"));
                                        imageCashondelivery.setImageResource(R.drawable.ic_worrng);
                                        /*strPincodeValidation = "Delivery Not Available";*/
                                        strPincodeMessage = getString(R.string.please_enter_pincode_onther_pincode_because_delivery_not_available);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR RelatedProducts API..
    private void RelatedProducts(String slug) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key;
        String[] val;

        String user_id = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID) != null) {
            key = new String[]{"slug", "user_id"};
            val = new String[]{slug, user_id};
        } else {
            key = new String[]{"slug", "user_id"};
            val = new String[]{slug, ""};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.RelatedProduct);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {

                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    if (jsonObjectPayload.has("relatedproducts")) {
                                        jsonArrayRelatedProduct = jsonObjectPayload.getJSONArray("relatedproducts");
                                        if (jsonArrayRelatedProduct.length() > 0) {
                                            llRelatedProduct.setVisibility(View.VISIBLE);
                                            AdapterRelatedProduct adapterRelatedProduct = new AdapterRelatedProduct(getContext(), jsonArrayRelatedProduct);
                                            recyclerviewReletedProduct.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                                            recyclerviewReletedProduct.setAdapter(adapterRelatedProduct);
                                        } else {
                                            llRelatedProduct.setVisibility(View.GONE);
                                        }
                                    }
                                }
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                llRelatedProduct.setVisibility(View.GONE);
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                /*Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();*/
                            }
                        } catch (JSONException | NullPointerException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region ADAPTER
    public class AdapterRelatedProduct extends RecyclerView.Adapter<AdapterRelatedProduct.Viewholder> {

        Context context;
        JSONArray jsonArray;

        public AdapterRelatedProduct(Context context, JSONArray jsonArray) {
            this.context = context;
            this.jsonArray = jsonArray;
        }

        @Override
        public AdapterRelatedProduct.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_product, viewGroup, false);
            return new AdapterRelatedProduct.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder viewholder, int position) {
            try {
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        viewholder.pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
                    }
                }
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) viewholder.llProducts.getLayoutParams();
                if (position == 0) {
                    params.setMargins(15, 0, 5, 0);
                    viewholder.llProducts.setLayoutParams(params);
                } else if (position == getItemCount() - 1) {
                    params.setMargins(5, 0, 15, 0);
                    viewholder.llProducts.setLayoutParams(params);
                } else {
                    params.setMargins(5, 0, 5, 0);
                    viewholder.llProducts.setLayoutParams(params);
                }

                JSONObject jsonObject = jsonArray.getJSONObject(position);
                String strRating = jsonObject.getString("avg_rating");
                float floatSalePrice = Float.parseFloat(jsonObject.getString("price"));
                intBasePrice = Math.round(floatSalePrice);
                float floatBasePrice = Float.parseFloat(jsonObject.getString("sale_price"));
                intSalePrice = Math.round(floatBasePrice);
                final String product_id = jsonObject.getString("product_id");
                final String slug = jsonObject.getString("slug");
                String exists_in_wishlist = jsonObject.getString("exists_in_wishlist");
                final String productName = jsonObject.getString("name");
                viewholder.textProductName.setText(jsonObject.getString("name"));

                if (!strRating.equals("")) {
                    float rating = Float.parseFloat(strRating);
                    viewholder.reviewRatingbar.setVisibility(View.VISIBLE);
                    viewholder.reviewRatingbar.setRating(rating);
                } else {
                    float rating = Float.parseFloat("0");
                    viewholder.reviewRatingbar.setVisibility(View.VISIBLE);
                    viewholder.reviewRatingbar.setRating(rating);
                }

                if (intSalePrice > 0) {
                    if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("1")) {
                        viewholder.textProductSalePrice.setText(SharedPreference.GetPreference(getContext(),
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + jsonObject.getString("sale_price"));
                        viewholder.txt_product_base_price.setText(SharedPreference.GetPreference(getContext(),
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + jsonObject.getString("price"));
                    } else if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("2")) {
                        viewholder.textProductSalePrice.setText(jsonObject.getString("sale_price") + ". " +
                                SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        viewholder.txt_product_base_price.setText(jsonObject.getString("price") + ". " +
                                SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                    }
                } else {
                    if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("1")) {
                        viewholder.textProductSalePrice.setText(SharedPreference.GetPreference(getContext(),
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + jsonObject.getString("price"));
                        viewholder.txt_product_base_price.setVisibility(View.GONE);
                    } else if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("2")) {
                        viewholder.textProductSalePrice.setText(jsonObject.getString("price") + " " +
                                SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        viewholder.txt_product_base_price.setVisibility(View.GONE);
                    }
                }

              /*  JSONArray jsonArrayImages = jsonObject.getJSONArray("main_image");
                String images = String.valueOf(jsonArrayImages.get(0));*/

                JSONObject jsonObjectImages = jsonObject.getJSONObject("main_image");
                String images = jsonObjectImages.getString("main_image");

                //region Image
                String picUrl = null;
                try {
                    URL urla = null;
                /*images = images.replace("[", "");
                image = image.replace("]", "");*/
                    urla = new URL(images.replaceAll("%20", " "));
                    URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    // Capture position and set to the ImageView
                    Picasso.with(context)
                            .load(picUrl)
                            .into(viewholder.imageProduct, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                    viewholder.rlImgHolder.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }
                            });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment ViewPagerAdapter.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
                //endregion

                if (is_wishlist.equals("1")) {
                    viewholder.imgWish.setVisibility(View.VISIBLE);
                } else {
                    viewholder.imgWish.setVisibility(View.GONE);
                }

              /*  if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
                    CheckWishList(product_id, viewholder.imgWish);
                }*/

                if (exists_in_wishlist.equals("0")) {
                    viewholder.imgWish.setImageResource(R.drawable.ic_heart);
                } else if (exists_in_wishlist.equals("1")) {
                    viewholder.imgWish.setImageResource(R.drawable.ic_select_heart);
                }

                viewholder.imgWish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AddToWishList(product_id, viewholder.imgWish);
                    }
                });

                viewholder.framelayoutProduct.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ProductDetailFragment productDetailFragment = new ProductDetailFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("slug", slug);
                        bundle.putString("name", productName);
                        fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        productDetailFragment.setArguments(bundle);
                        fragmentTransaction.replace(R.id.framLayoutProductDetail, productDetailFragment);
                        fragmentTransaction.commit();
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return jsonArray.length();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            ImageView imageProduct, imgWish;
            TextView textProductName, textProductSalePrice, txt_product_base_price;
            FrameLayout framelayoutProduct;
            RatingBar reviewRatingbar;
            LinearLayout llProducts;
            RelativeLayout rlImgHolder;
            ProgressBar pbImgHolder;

            public Viewholder(View itemView) {
                super(itemView);
                framelayoutProduct = (FrameLayout) itemView.findViewById(R.id.framelayoutProduct);
                imageProduct = (ImageView) itemView.findViewById(R.id.imageProduct);
                imgWish = (ImageView) itemView.findViewById(R.id.imgWish);
                textProductName = (TextView) itemView.findViewById(R.id.textProductName);
                textProductSalePrice = (TextView) itemView.findViewById(R.id.textProductSalePrice);
                txt_product_base_price = (TextView) itemView.findViewById(R.id.txt_product_base_price);
                reviewRatingbar = (RatingBar) itemView.findViewById(R.id.reviewRatingbar);
                llProducts = (LinearLayout) itemView.findViewById(R.id.llProducts);
                rlImgHolder = (RelativeLayout) itemView.findViewById(R.id.rlImgHolder);
                pbImgHolder = (ProgressBar) itemView.findViewById(R.id.pbImgHolder);

                LayerDrawable stars = (LayerDrawable) reviewRatingbar.getProgressDrawable();
                stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                stars.getDrawable(1).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);

                textProductName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                textProductSalePrice.setTypeface(Typefaces.TypefaceCalibri_bold(context));
                txt_product_base_price.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

                textProductName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                textProductSalePrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txt_product_base_price.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));

            }
        }
    }
    //endregion

    //region FOR AddToWishList API..
    private void AddToWishList(String product_id, final ImageView imageView) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"product_id"};
        String[] val = {product_id};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.AddToWishlist);
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers1(getContext()));
        } else {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers2(getContext()));
        }
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                imageView.setImageResource(R.drawable.ic_select_heart);
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                mListener = (OnFragmentInteractionListener) getContext();
                                mListener.WishlistCount();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");

                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else if (strMessage.equals("Product already exists in Wishlist!")) {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    imageView.setClickable(false);
                                } else {
                                    imageView.setClickable(true);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR AddToWishListProduct API..
    private void AddToWishListProduct(String product_id) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"product_id"};
        String[] val = {product_id};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.AddToWishlist);
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers1(getContext()));
        } else {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers2(getContext()));
        }
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                RelatedProducts(strSlug);
                               /* mListener = (OnFragmentInteractionListener) getContext();
                                mListener.WishlistCount();*/
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else if (strMessage.equals("Product already exists in Wishlist!")) {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    txtAddtoWishlist.setClickable(false);
                                } else {
                                    txtAddtoWishlist.setClickable(true);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR getReviews API..
    private void getReviews(String slug) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"slug"};
        String[] val = {slug};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetReviews);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                reviewses = new ArrayList<>();
                                if (strStatus.equals("ok")) {
                                    Object object = response.get("payload");
                                    if (!object.equals("")) {
                                        JSONArray jsonArrayPayload = response.getJSONArray("payload");
                                        for (int i = 0; i < jsonArrayPayload.length(); i++) {
                                            JSONObject jsonObjectPayload = jsonArrayPayload.getJSONObject(i);
                                            String Username = jsonObjectPayload.getString("name");
                                            String description = jsonObjectPayload.getString("description");
                                            String Reviewdate = jsonObjectPayload.getString("added_on");
                                            String rating = jsonObjectPayload.getString("rating");
                                            float rating1 = Float.parseFloat(rating);
                                            floatRating = floatRating + rating1;
                                            reviewses.add(new Reviews(Username, Reviewdate, rating, description));
                                        }
                                        if (reviewses.size() > 0) {
                                            llReviews.setVisibility(View.VISIBLE);
                                            AdapterReviewsList adapterReviewsList = new AdapterReviewsList(getContext(), reviewses);
                                            recyclerviewReviews.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                            recyclerviewReviews.setAdapter(adapterReviewsList);
                                            floatRating = floatRating / reviewses.size();
                                            reviewRatingbar.setRating(floatRating);
                                        }
                                    } else {
                                        llReviews.setVisibility(View.GONE);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR Addtocart API...
    private void AddToCart(String ProductID) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key;
        String[] val;

        String userId = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        String SessionId = SharedPreference.GetPreference(getActivity(), Global.preferenceNameGuestUSer, Global.SessionId);
        if (userId != null) {
            key = new String[]{"session_id", "user_id", "product_id", "product_name", "category_id", "price"};
            val = new String[]{"", userId, ProductID, txtProductName.getText().toString(), "", textProductSalePrice.getText().toString()};
        } else {
            key = new String[]{"session_id", "user_id", "product_id"};
            val = new String[]{SessionId, "", ProductID, txtProductName.getText().toString(), "", textProductSalePrice.getText().toString()};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.AddToCart);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    productIDs.clear();
                                    variationAttribute.clear();
//                                    SharedPreference.ClearPreference(getContext(), Global.IsClickPreference);
                                    if (strMessage.equalsIgnoreCase("Product successfully added to cart!")) {
                                        txtAddtoCart.setText("go to cart");
                                        mListener = (OnFragmentInteractionListener) getContext();
                                        mListener.onAddToCartProduct();
                                    } else if (strMessage.equalsIgnoreCase("Product already exists in cart!")) {
                                        txtAddtoCart.setText("go to cart");
                                        txtAddtoCart.setClickable(true);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR CheckItemInCart API..
    private void CheckItemInCart(String strProductID) {
//        relativeProgress.setVisibility(View.VISIBLE);

        String[] key;
        String[] val;
        String userId = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        String SessionId = SharedPreference.GetPreference(getContext(), Global.preferenceNameGuestUSer, Global.SessionId);
        if (userId != null) {
            key = new String[]{"session_id", "user_id", "product_id"};
            val = new String[]{"", userId, strProductID};
        } else {
            key = new String[]{"session_id", "user_id", "product_id"};
            val = new String[]{SessionId, "", strProductID};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.CheckItemINCart);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
//                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                reviewses = new ArrayList<>();
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    int itemexists_count = Integer.parseInt(jsonObject.getString("itemexists_count"));
                                    if (itemexists_count > 0) {
                                        txtAddtoCart.setText("go to cart");
                                    } else {
                                        txtAddtoCart.setText("add to cart");
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
//                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR CheckItemInNofityMe API..
    private void CheckItemInNofityMe(final String ProductID) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"product_id"};
        String[] val = {ProductID};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.CheckNotifyMe);
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers1(getContext()));
        } else {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers2(getContext()));
        }
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    String count = jsonObject.getString("count");
                                    if (Integer.parseInt(count) <= 0) {
                                        AddToNotifyMe(ProductID);
                                        txtAddtoCart.setEnabled(true);
                                    } else {
                                        Toast.makeText(getContext(), "User Already Notify This Product !", Toast.LENGTH_SHORT).show();
                                        txtAddtoCart.setEnabled(false);
                                    }

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    // region FOR AddToNotifyMe API..
    private void AddToNotifyMe(String ProductID) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"product_id"};
        String[] val = {ProductID};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.NotifyMe);
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE,
                Global.USERTOKEN) != null) {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers1(getContext()));
        } else {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers2(getContext()));
        }
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    if (strMessage.equalsIgnoreCase("Notify added")) {
                                        Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region Keyboard Hide
    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    //endregion

    //region chanageTextViewBorder
    public void chanageTextViewBorder(TextView textView) {
        textView.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        textView.setBackgroundResource(R.drawable.ic_variation_background);
        GradientDrawable gd = (GradientDrawable) textView.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        gd.setCornerRadius(10);
    }
    //endregion

    //region chanageTextViewBorder1
    public void chanageTextViewBorder1(TextView textView) {
        textView.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        textView.setBackgroundResource(R.drawable.ic_variation_background);
        GradientDrawable gd = (GradientDrawable) textView.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#FFFFFF"));
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(10);
//        gd.setStroke(1, Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
    }
    //endregion

    //region chanageEditTextBorder
    public void chanageEditTextBorder(EditText editText) {
        editText.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editText.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) editText.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#FFFFFF"));
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
    }
    //endregion

    //region chanageButton
    public void chanageButton(Button button) {
        button.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
//        button.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TEXTCOLOR)));
        button.setBackgroundResource(R.drawable.ic_button);
        GradientDrawable gd = (GradientDrawable) button.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        /* gd.setStroke(2, Color.parseColor(StaticUtility.BORDERCOLOR));*/
    }
    //endregion

    //region FOR getDefualtVariationData API...
    private void getDefualtVariationData(String productid) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = new String[]{"product_id"};
        String[] val = new String[]{productid};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetDefualtVariationData);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    JSONArray jsonArrayDefualtVariation = jsonObjectPayload.getJSONArray("defaultVariationData");
                                    defaultVariations = new ArrayList<>();
                                    strArrayAttr = new String[jsonArrayDefualtVariation.length()];
                                    strArrayAttrTerm = new String[jsonArrayDefualtVariation.length()];
                                    for (int i = 0; i < jsonArrayDefualtVariation.length(); i++) {
                                        JSONObject jsonObjectDefaultVariation = jsonArrayDefualtVariation.getJSONObject(i);
                                        productVariationid = jsonObjectDefaultVariation.getString("z_productid_fk");
                                        Variationattritem_name = jsonObjectDefaultVariation.getString("attritem_name");
                                        String Proattritemrelid_pk = jsonObjectDefaultVariation.getString("z_proattritemrelid_pk");
                                        String Attributetermid_fk = jsonObjectDefaultVariation.getString("z_attributetermid_fk");
                                        String Attributeid_fk = jsonObjectDefaultVariation.getString("z_attributeid_fk");
                                        String Productattrrelid_fk = jsonObjectDefaultVariation.getString("z_productattrrelid_fk");
                                        String name = jsonObjectDefaultVariation.getString("name");
                                        String Attributeid_pk = jsonObjectDefaultVariation.getString("z_attributeid_pk");
                                        strArrayAttr[i] = jsonObjectDefaultVariation.optString("z_attributeid_fk");
                                        strArrayAttrTerm[i] = jsonObjectDefaultVariation.optString("z_attributetermid_fk");
                                        defaultVariations.add(new DefaultVariation(Proattritemrelid_pk,
                                                productVariationid, Attributetermid_fk, Attributeid_fk, Productattrrelid_fk,
                                                Variationattritem_name, name, Attributeid_pk));
                                    }
                                    strAttrTerm = new String[jsonArrayVariation.length()];
                                    AdapterAttributeTitle adapterAttributeTitle = new AdapterAttributeTitle(getContext(), jsonArrayVariation);
                                    recyclerviewVariation.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                    recyclerviewVariation.setAdapter(adapterAttributeTitle);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test Error", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion
}
