package com.pureweblopment.jvision.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.SlidingDrawer;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.pureweblopment.jvision.Global.Typefaces;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;

import com.pureweblopment.jvision.Activity.LoginActivity;
import com.pureweblopment.jvision.Activity.MainActivity;
import com.pureweblopment.jvision.Global.Global;
import com.pureweblopment.jvision.Global.SendMail;
import com.pureweblopment.jvision.Global.SharedPreference;
import com.pureweblopment.jvision.Global.StaticUtility;
import com.pureweblopment.jvision.Model.Attribute;
import com.pureweblopment.jvision.Model.Category;
import com.pureweblopment.jvision.Model.ProductList;
import com.pureweblopment.jvision.R;

import io.fabric.sdk.android.services.settings.AppSettingsData;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProductListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProductListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductListFragment extends Fragment implements View.OnClickListener, TextWatcher {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    RecyclerView recyclerviewProducts;
    RelativeLayout relativeProgress;
    LinearLayout llFilterBg;
    SlidingDrawer slidingDrawer;
    LinearLayout llContent;
    TextView TextViewFilter, TextViewApply, TextViewClear;
    ImageView imgCancel, imgUpColor, imgDownColor, imgUpCategory, imgDownCategory, imgUpSize, imgDownSize;
    RecyclerView recyclerviewColor, recyclerviewCategories, recyclerviewSize, recyclerview;

    Bundle bundle;
    String slug = null;

    String attributes = "", is_wishlist = "";
    LinearLayout llNoDataFound;
    ImageView imgRefresh;

    ArrayList<Attribute> attributesColor;
    ArrayList<Attribute> attributesSize;
    ArrayList<ProductList> productLists;

    EditText editSearch;
    LinearLayout llSreach;

    AdapterProductList adapterProductList;
    ImageView imageCartBack, imageNavigation, imageLogo;
    FrameLayout frameLayoutCart;
    TextView txtCatName;
    ArrayList<String> ArryListattributes = new ArrayList<>();


    JSONArray arrayAttributeColor = new JSONArray(), arrayAttributeSize = new JSONArray(), arrayAttribut = new JSONArray();
    JSONArray arrayAttributeVariation = new JSONArray();
    String[] AttributeColorSelection;
    String[] AttributeSizeSelection;
    private OnFragmentInteractionListener mListener;
    TextView txtNoRecordFound;

    FloatingActionButton FABFilter;

    public ProductListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProductListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProductListFragment newInstance(String param1, String param2) {
        ProductListFragment fragment = new ProductListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        MainActivity.manageBackPress(false);
        View view = inflater.inflate(R.layout.fragment_product_list, container, false);

        ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

        bundle = getArguments();
        slug = bundle.getString("category_slug");
        String categoryName = bundle.getString("catName");


        imageCartBack = (ImageView) getActivity().findViewById(R.id.imageCartBack);
        imageNavigation = (ImageView) getActivity().findViewById(R.id.imageNavigation);
        imageLogo = (ImageView) getActivity().findViewById(R.id.imageLogo);

        frameLayoutCart = (FrameLayout) getActivity().findViewById(R.id.frameLayoutCart);
        txtCatName = (TextView) getActivity().findViewById(R.id.txtCatName);
        txtCatName.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));

        imageCartBack.setVisibility(View.VISIBLE);
        txtCatName.setVisibility(View.VISIBLE);
        txtCatName.setText(categoryName);
        frameLayoutCart.setVisibility(View.VISIBLE);

        txtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        imageNavigation.setVisibility(View.GONE);
        imageLogo.setVisibility(View.GONE);

        imageCartBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    getActivity().onBackPressed();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });

        is_wishlist = SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active);

        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.Is_Cart_Active).equals("1")) {
            frameLayoutCart.setVisibility(View.VISIBLE);
        } else {
            frameLayoutCart.setVisibility(View.GONE);
        }

        Initialization(view);
        TypeFace();
        AppSettings();
        OnClickListener();

        FilterByCategory(slug, arrayAttribut);

        recyclerviewProducts.addOnScrollListener(new RecyclerView.OnScrollListener() {
            int y = 0;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                y = dy;
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (recyclerviewProducts.SCROLL_STATE_SETTLING == newState) {
                    if (y < 0) {
                        FABFilter.show();
                    } else if (y > 0) {
                        FABFilter.hide();
                    }
                }
            }
        });

        return view;
    }

    //region Initialization
    private void Initialization(View view) {

        recyclerviewProducts = (RecyclerView) view.findViewById(R.id.recyclerviewProducts);
        relativeProgress = (RelativeLayout) view.findViewById(R.id.relativeProgress);

        slidingDrawer = (SlidingDrawer) view.findViewById(R.id.slidingDrawer);
        llFilterBg = (LinearLayout) view.findViewById(R.id.llFilterBg);
        llContent = (LinearLayout) view.findViewById(R.id.llContent);
        TextViewFilter = (TextView) view.findViewById(R.id.TextViewFilter);
        TextViewApply = (TextView) view.findViewById(R.id.TextViewApply);
        TextViewClear = (TextView) view.findViewById(R.id.TextViewClear);
        imgCancel = (ImageView) view.findViewById(R.id.imgCancel);

        txtNoRecordFound = (TextView) view.findViewById(R.id.txtNoRecordFound);

        recyclerviewColor = (RecyclerView) view.findViewById(R.id.recyclerviewColor);
        recyclerviewCategories = (RecyclerView) view.findViewById(R.id.recyclerviewCategories);
        recyclerviewSize = (RecyclerView) view.findViewById(R.id.recyclerviewSize);
        recyclerview = (RecyclerView) view.findViewById(R.id.recyclerview);

        llNoDataFound = (LinearLayout) view.findViewById(R.id.llNoDataFound);

        imgRefresh = (ImageView) view.findViewById(R.id.imgRefresh);
        imgUpColor = (ImageView) view.findViewById(R.id.imgUpColor);
        imgDownColor = (ImageView) view.findViewById(R.id.imgDownColor);
        imgUpCategory = (ImageView) view.findViewById(R.id.imgUpCategory);
        imgDownCategory = (ImageView) view.findViewById(R.id.imgDownCategory);
        imgUpSize = (ImageView) view.findViewById(R.id.imgUpSize);
        imgDownSize = (ImageView) view.findViewById(R.id.imgDownSize);

        editSearch = (EditText) view.findViewById(R.id.editSearch);
        llSreach = (LinearLayout) view.findViewById(R.id.llSreach);
        FABFilter = (FloatingActionButton) view.findViewById(R.id.FABFilter);
        imgCancel.setColorFilter(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
    }
    //endregion

    //region OnClickListener
    public void OnClickListener() {
        llContent.setOnClickListener(this);
        imgCancel.setOnClickListener(this);
        TextViewApply.setOnClickListener(this);
        TextViewClear.setOnClickListener(this);
        /*imgRefresh.setOnClickListener(this);*/
        FABFilter.setOnClickListener(this);
        editSearch.addTextChangedListener(this);

    }
    //endregion

    //region AppSettings
    public void AppSettings() {
        TextViewApply.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        TextViewApply.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        TextViewClear.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        TextViewFilter.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        editSearch.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtNoRecordFound.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        llSreach.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        FABFilter.setBackgroundTintList(ColorStateList.valueOf(Color
                .parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
    }
    //endregion

    //region TypeFace
    public void TypeFace() {
        TextViewApply.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
        TextViewClear.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
        TextViewFilter.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
    }
    //endregion

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.FABFilter:
                FABFilter.hide();
                mListener.FilterClick(true);
                llFilterBg.setVisibility(View.VISIBLE);
                attributes.equals("0");
                arrayAttributeColor = new JSONArray();
                arrayAttributeSize = new JSONArray();
                arrayAttribut = new JSONArray();
                arrayAttributeVariation = new JSONArray();

                if (slidingDrawer.isOpened()) {
                    slidingDrawer.animateClose();
                    llNoDataFound.setVisibility(View.GONE);
                    llFilterBg.setBackgroundColor(Color.TRANSPARENT);
                } else if (!slidingDrawer.isOpened()) {
                    /*llFilterBg.setBackgroundColor(Color.parseColor("#85000000"));*/
                    llNoDataFound.setVisibility(View.VISIBLE);
                    slidingDrawer.animateOpen();
                }
                break;

            case R.id.llContent:
                llFilterBg.setBackgroundColor(Color.TRANSPARENT);
                break;

            case R.id.imgCancel:
                FABFilter.show();
                mListener.FilterClick(false);
                if (slidingDrawer.isOpened()) {
                    slidingDrawer.animateClose();
                    llFilterBg.setBackgroundColor(Color.TRANSPARENT);
                    llFilterBg.setVisibility(View.GONE);
                    llNoDataFound.setVisibility(View.VISIBLE);
                }
//                ArryListattributes.clear();
//                SortByAttribute();
                /*FilterByCategory(slug, arrayAttributeColor);*/
                break;

            case R.id.TextViewApply:
                FABFilter.show();
                for (int i = 0; i < ArryListattributes.size(); i++) {
                    arrayAttributeColor.put(ArryListattributes.get(i));
                }
                if (arrayAttributeColor.length() > 0) {
                    FilterByCategory(slug, arrayAttributeColor);
                } else {
                    FilterByCategory(slug, arrayAttributeColor);
                }
                mListener.FilterClick(false);
                if (slidingDrawer.isOpened()) {
                    slidingDrawer.animateClose();
                    llFilterBg.setBackgroundColor(Color.TRANSPARENT);
                    llFilterBg.setVisibility(View.GONE);
                }
                break;

            case R.id.imgRefresh:
                editSearch.setText("");
                hideKeyboard(getActivity());
                attributes.equals("0");
                arrayAttributeColor = new JSONArray();
                arrayAttributeSize = new JSONArray();
                arrayAttribut = new JSONArray();
                FilterByCategory(slug, arrayAttribut);
                break;

            case R.id.TextViewClear:
                ArryListattributes.clear();
                SortByAttribute();
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.length() >= 2) {
            final ArrayList<ProductList> filteredModelList = filter(productLists, String.valueOf(s));
            if (filteredModelList.size() > 0) {
                adapterProductList.setFilter(filteredModelList);
                recyclerviewProducts.setVisibility(View.VISIBLE);
                llNoDataFound.setVisibility(View.GONE);
            } else {
                llNoDataFound.setVisibility(View.VISIBLE);
                recyclerviewProducts.setVisibility(View.GONE);
            }
        } else if (s.length() == 0) {
            FilterByCategory(slug, arrayAttribut);
            Global.HideSystemKeyboard(getActivity());
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void FilterClick(Boolean values);

        void OnProductClick(String slug, String Productname, String product_id);

        void WishlistCount();

    }

    //region FOR FilterByCategory API...
    private void FilterByCategory(String slug, JSONArray jsonArray) {
        relativeProgress.setVisibility(View.VISIBLE);
        JSONObject objFilter = null;

        String user_id = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID) != null) {
            objFilter = new JSONObject();
            try {
                if (jsonArray.length() > 0) {
                    objFilter.put("slug", slug);
                    objFilter.put("attrtermsid", jsonArray);
                    objFilter.put("user_id", user_id);
                } else {
                    objFilter.put("slug", slug);
                    objFilter.put("attrtermsid", jsonArray);
                    objFilter.put("user_id", user_id);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            objFilter = new JSONObject();
            try {
                if (jsonArray.length() > 0) {
                    objFilter.put("slug", slug);
                    objFilter.put("attrtermsid", jsonArray);
                    objFilter.put("user_id", "");
                } else {
                    objFilter.put("slug", slug);
                    objFilter.put("attrtermsid", jsonArray);
                    objFilter.put("user_id", "");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetProductList);
        postRequestBuilder.addJSONObjectBody(objFilter)
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                        SortByAttribute();
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                productLists = new ArrayList<>();
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    if (jsonObjectPayload.has("products")) {
                                        llNoDataFound.setVisibility(View.GONE);
                                        recyclerviewProducts.setVisibility(View.VISIBLE);
                                        Object objProducts = jsonObjectPayload.get("products");
                                        if (!objProducts.equals("")) {
                                            JSONArray jsonArrayProducts = jsonObjectPayload.getJSONArray("products");
                                            for (int i = 0; i < jsonArrayProducts.length(); i++) {
                                                JSONObject jsonObject = jsonArrayProducts.getJSONObject(i);
                                                String productName = jsonObject.getString("name");
                                                String ProductSlug = jsonObject.getString("slug");
                                                String BasePrice = jsonObject.getString("price");
                                                String SalePrice = jsonObject.getString("sale_price");
                                                String product_id = jsonObject.getString("product_id");
                                                String exists_in_wishlist = jsonObject.getString("exists_in_wishlist");
                                                String strRating = jsonObject.getString("avg_rating");
                                              /*  JSONArray jsonArrayImage = jsonObject.getJSONArray("main_image");
                                                String image = jsonArrayImage.get(0).toString();*/
                                                JSONObject jsonObjectImages = jsonObject.getJSONObject("main_image");
                                                String image = jsonObjectImages.getString("main_image");
                                                /* JSONObject jsonObjectImages = jsonObject.getJSONObject("main_image");
                                                   String images = jsonObjectImages.getString("main_image");*/
                                                productLists.add(new ProductList(productName, BasePrice, SalePrice,
                                                        product_id, image, ProductSlug, exists_in_wishlist, strRating));
                                            }
                                            if (productLists.size() > 0) {
                                                GridLayoutManager manager = new GridLayoutManager(getActivity(), 2, LinearLayoutManager.VERTICAL, false);
                                                adapterProductList = new AdapterProductList(getContext(), productLists);
                                                recyclerviewProducts.setLayoutManager(manager);
                                                recyclerviewProducts.setAdapter(adapterProductList);
                                            } else {
                                                llNoDataFound.setVisibility(View.VISIBLE);
                                                recyclerviewProducts.setVisibility(View.GONE);
                                            }
                                        } else {
                                            llNoDataFound.setVisibility(View.VISIBLE);
                                            recyclerviewProducts.setVisibility(View.GONE);
                                        }
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                        SortByAttribute();
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductListFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region ADAPTER List..
    public class AdapterProductList extends RecyclerView.Adapter<AdapterProductList.Viewholder> {

        Context context;
        ArrayList<ProductList> productLists;

        public AdapterProductList(Context context, ArrayList<ProductList> productLists) {
            this.context = context;
            this.productLists = productLists;
        }

        public void setFilter(ArrayList<ProductList> filteredModelList) {
            productLists = new ArrayList<>();
            productLists.addAll(filteredModelList);
            notifyDataSetChanged();
        }

        @Override
        public AdapterProductList.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_productlist, viewGroup, false);
            return new AdapterProductList.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder viewholder, final int position) {
            try {

                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        viewholder.pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
                    }
                }
                final ProductList productList = productLists.get(position);
                viewholder.textProductName.setText(productList.getProductname());
                final String Productname = productList.getProductname();
                final String ProductSlug = productList.getSlug();
                String strRating = productList.getRating();

                float floatBasePrice = Float.parseFloat(productList.getProductprice());
                int intBasePrice = Math.round(floatBasePrice);
                float floatSalePrice = Float.parseFloat(productList.getProductsale_price());
                int intSalePrice = Math.round(floatSalePrice);

                if (!strRating.equals("")) {
                    float rating = Float.parseFloat(strRating);
                    viewholder.reviewRatingbar.setVisibility(View.VISIBLE);
                    viewholder.reviewRatingbar.setRating(rating);
                }

                if (intSalePrice > 0) {
                    if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("1")) {
                        viewholder.textProductSalePrice.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + productList.getProductsale_price());
                        viewholder.txt_product_base_price.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + productList.getProductprice());
                    } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("2")) {
                        viewholder.textProductSalePrice.setText(productList.getProductsale_price() + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        viewholder.txt_product_base_price.setText(productList.getProductprice() + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                    }
                } else {
                    if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("1")) {
                        viewholder.textProductSalePrice.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + productList.getProductprice());
                        viewholder.txt_product_base_price.setVisibility(View.GONE);
                    } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("2")) {
                        viewholder.textProductSalePrice.setText(productList.getProductprice() + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        viewholder.txt_product_base_price.setVisibility(View.GONE);
                    }
                }

                final String product_id = productList.getProductId();
                String image = productList.getMain_image();

                //region Image
                String picUrl = null;
                try {
                    URL urla = null;
                    urla = new URL(image.replaceAll("%20", " "));
                    URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    // Capture position and set to the ImageView
                    Picasso.with(context)
                            .load(picUrl)
                            .into(viewholder.imageProduct, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                    viewholder.rlImgHolder.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }
                            });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductListFragment.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductListFragment.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
                //endregion

                if (is_wishlist.equals("1")) {
                    viewholder.imgWish.setVisibility(View.VISIBLE);
                } else {
                    viewholder.imgWish.setVisibility(View.GONE);
                }

               /* if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
                    CheckWishList(product_id, viewholder.imgWish);
                }*/

                String exists_in_wishlist = productList.getExists_in_wishlist();
                if (exists_in_wishlist.equals("0")) {
                    viewholder.imgWish.setImageResource(R.drawable.ic_heart);
                } else if (exists_in_wishlist.equals("1")) {
                    viewholder.imgWish.setImageResource(R.drawable.ic_select_heart);
                }

                viewholder.imgWish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AddToWishList(product_id, viewholder.imgWish, position);
                    }
                });

                viewholder.llProducts.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String product_id = productList.getProductId();
                        mListener = (OnFragmentInteractionListener) context;
                        mListener.OnProductClick(ProductSlug, Productname, product_id);
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return productLists.size();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            ImageView imageProduct, imgWish;
            TextView textProductName, textProductSalePrice, txt_product_base_price;
            LinearLayout llProducts;
            RatingBar reviewRatingbar;
            RelativeLayout rlImgHolder;
            ProgressBar pbImgHolder;

            public Viewholder(View itemView) {
                super(itemView);
                llProducts = (LinearLayout) itemView.findViewById(R.id.llProducts);
                imageProduct = (ImageView) itemView.findViewById(R.id.imageProduct);
                imgWish = (ImageView) itemView.findViewById(R.id.imgWish);
                textProductName = (TextView) itemView.findViewById(R.id.textProductName);
                textProductSalePrice = (TextView) itemView.findViewById(R.id.textProductSalePrice);
                txt_product_base_price = (TextView) itemView.findViewById(R.id.txt_product_base_price);
                reviewRatingbar = (RatingBar) itemView.findViewById(R.id.reviewRatingbar);
                rlImgHolder = (RelativeLayout) itemView.findViewById(R.id.rlImgHolder);
                pbImgHolder = (ProgressBar) itemView.findViewById(R.id.pbImgHolder);

                LayerDrawable stars = (LayerDrawable) reviewRatingbar.getProgressDrawable();
                stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                stars.getDrawable(1).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);

                textProductName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                textProductSalePrice.setTypeface(Typefaces.TypefaceCalibri_bold(context));
                txt_product_base_price.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

                textProductName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                textProductSalePrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txt_product_base_price.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));

            }
        }
    }
    //endregion

    //region FOR AddToWishList API..
    private void AddToWishList(String product_id, final ImageView imageView, final int position) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"product_id"};
        String[] val = {product_id};
        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.AddToWishlist);
        ;
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers1(getContext()));
        } else {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers2(getContext()));
        }

        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                imageView.setImageResource(R.drawable.ic_select_heart);
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                mListener = (OnFragmentInteractionListener) getContext();
                                mListener.WishlistCount();
                                ProductList productList = productLists.get(position);
                                productList.setExists_in_wishlist("1");
                                recyclerviewProducts.getAdapter().notifyDataSetChanged();
//                                FilterByCategory(slug, arrayAttributeColor);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strMessage.equalsIgnoreCase("Invalid Credentials")) {
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getContext(), LoginActivity.class);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else if (strMessage.equalsIgnoreCase("Product already exists in Wishlist!")) {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    imageView.setClickable(false);
                                } else {
                                    imageView.setClickable(true);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductListFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR SortByAttribute API..
    private void SortByAttribute() {
        relativeProgress.setVisibility(View.VISIBLE);
        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.SortByAttribute);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONArray jsonArrayProducts = response.getJSONArray("payload");
                                    AdapterVariationTitle adapterVariationTitle = new AdapterVariationTitle(getContext(), jsonArrayProducts);
//                                    GridLayoutManager manager = new GridLayoutManager(getActivity(), 4, LinearLayoutManager.VERTICAL, false);
                                    recyclerview.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                    recyclerview.setAdapter(adapterVariationTitle);
                                  /*  for (int i = 0; i < jsonArrayProducts.length(); i++) {
                                        JSONObject jsonObject = jsonArrayProducts.getJSONObject(i);
                                        attributesColor = new ArrayList<>();
                                        if (jsonObject.getString("name").equalsIgnoreCase("Color")) {
                                            if (jsonObject.has("attributesterms")) {
                                                JSONArray jsonArrayAttributes = jsonObject.getJSONArray("attributesterms");
                                                for (int j = 0; j < jsonArrayAttributes.length(); j++) {
                                                    JSONObject jsonObjectAttribute = jsonArrayAttributes.getJSONObject(j);
                                                    String name = jsonObjectAttribute.getString("attributetermsname");
                                                    String id = jsonObjectAttribute.getString("attributeterm_id");
                                                    String id1 = jsonObjectAttribute.getString("z_attributetermid_pk");
                                                    attributesColor.add(new Attribute(name, id1));
                                                    if (j >= 7) {
                                                        imgDownColor.setVisibility(View.VISIBLE);
                                                        imgUpColor.setVisibility(View.GONE);
                                                    } else {
                                                        imgDownColor.setVisibility(View.GONE);
                                                    }
                                                }
                                                if (attributesColor.size() > 0) {
                                                    AdapterAttribute adapterAttribute = new AdapterAttribute(getContext(), attributesColor, imgDownColor, imgUpColor);
                                                    GridLayoutManager manager = new GridLayoutManager(getActivity(), 4, LinearLayoutManager.VERTICAL, false);
                                                    recyclerviewColor.setLayoutManager(manager);
                                                    recyclerviewColor.setAdapter(adapterAttribute);
                                                }


                                            }
                                        }
                                    }*/
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductListFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region ADAPTER AdapterVariationTitle...
    public class AdapterVariationTitle extends RecyclerView.Adapter<AdapterVariationTitle.Viewholder> {

        Context context;
        JSONArray jsonArray;
        int selectedPosition = -1;

        public AdapterVariationTitle(Context context, JSONArray jsonArray) {
            this.context = context;
            this.jsonArray = jsonArray;
        }

        @Override
        public AdapterVariationTitle.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_variation_title, viewGroup, false);
            return new AdapterVariationTitle.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder viewholder, final int position) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(position);
                String variationTitle = jsonObject.getString("name");
                if (jsonObject.has("attributesterms")) {
                    viewholder.txtTitleVariation.setVisibility(View.VISIBLE);
                    viewholder.txtTitleVariation.setText(variationTitle);
                    JSONArray jsonArrayAttributes = jsonObject.getJSONArray("attributesterms");
                    attributesColor = new ArrayList<>();
                    for (int j = 0; j < jsonArrayAttributes.length(); j++) {
                        JSONObject jsonObjectAttribute = jsonArrayAttributes.getJSONObject(j);
                        String name = jsonObjectAttribute.getString("attributetermsname");
                        String id = jsonObjectAttribute.getString("attributeterm_id");
                        String id1 = jsonObjectAttribute.getString("z_attributetermid_pk");
                        attributesColor.add(new Attribute(name, id1));
                        if (j >= 7) {
                            viewholder.imgDown.setVisibility(View.VISIBLE);
                            viewholder.imgUp.setVisibility(View.GONE);
                        } else {
                            viewholder.imgDown.setVisibility(View.GONE);
                        }
                    }
                    if (attributesColor.size() > 0) {
                        AdapterAttribute adapterAttribute = new AdapterAttribute(getContext()
                                , attributesColor, viewholder.imgDown, viewholder.imgUp);
                        GridLayoutManager manager = new GridLayoutManager(getActivity(), 4, LinearLayoutManager.VERTICAL, false);
                        viewholder.recyclerviewVariation.setLayoutManager(manager);
                        viewholder.recyclerviewVariation.setAdapter(adapterAttribute);
                    }
                } else {
                    viewholder.txtTitleVariation.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return jsonArray.length();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            TextView txtTitleVariation;
            RecyclerView recyclerviewVariation;
            ImageView imgDown, imgUp;

            public Viewholder(View itemView) {
                super(itemView);
                txtTitleVariation = (TextView) itemView.findViewById(R.id.txtTitleVariation);
                recyclerviewVariation = (RecyclerView) itemView.findViewById(R.id.recyclerviewVariation);
                imgDown = (ImageView) itemView.findViewById(R.id.imgDown);
                imgUp = (ImageView) itemView.findViewById(R.id.imgUp);

                txtTitleVariation.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                txtTitleVariation.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            }
        }
    }
    //endregion

    //region ADAPTER AdapterAttribute...
    public class AdapterAttribute extends RecyclerView.Adapter<AdapterAttribute.Viewholder> {

        Context context;
        JSONArray jsonArray;
        ArrayList<Attribute> attributess = new ArrayList<>();
        ImageView imageView, imgUpColor;
        boolean isclick = false;
        int[] IntArrayPosition;

        public AdapterAttribute(Context context, ArrayList<Attribute> attributess, ImageView imageView, ImageView imgUpColor) {
            this.context = context;
            this.attributess = attributess;
            this.imageView = imageView;
            this.imgUpColor = imgUpColor;
            AttributeColorSelection = new String[attributess.size()];
            IntArrayPosition = new int[attributess.size()];
        }

        @Override
        public AdapterAttribute.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_attribute, viewGroup, false);
            return new AdapterAttribute.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder viewholder, final int position) {
            try {
                final Attribute attribute = attributess.get(position);
                final String attributeID = attribute.getAttributeterm_id();

                if (isclick) {
                    viewholder.cardViewAttribute.setVisibility(View.VISIBLE);
                    viewholder.txtAttributeName.setText(attribute.getAttributetermsname());

                    if (ArryListattributes.contains(attributeID)) {
                        viewholder.txtAttributeName.setTextColor(Color.parseColor("#FFFFFF"));
                        viewholder.txtAttributeName.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                    } else {
                        viewholder.txtAttributeName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                        viewholder.txtAttributeName.setBackgroundColor(Color.parseColor("#dcdcdc"));
                    }

                } else {
                    if (position <= 7) {
                        viewholder.cardViewAttribute.setVisibility(View.VISIBLE);
                        viewholder.txtAttributeName.setText(attribute.getAttributetermsname());
                    } else {
                        viewholder.cardViewAttribute.setVisibility(View.GONE);
                    }
                    if (ArryListattributes.contains(attributeID)) {
                        viewholder.txtAttributeName.setTextColor(Color.parseColor("#FFFFFF"));
                        viewholder.txtAttributeName.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                    } else {
                        viewholder.txtAttributeName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                        viewholder.txtAttributeName.setBackgroundColor(Color.parseColor("#dcdcdc"));
                    }
                }

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!isclick) {
                            notifyDataSetChanged();
                            isclick = true;
                            imageView.setVisibility(View.GONE);
                            imgUpColor.setVisibility(View.VISIBLE);
                        }
                    }
                });

                imgUpColor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isclick) {
                            notifyDataSetChanged();
                            imageView.setVisibility(View.VISIBLE);
                            imgUpColor.setVisibility(View.GONE);
                            isclick = false;
                        }
                    }
                });

                viewholder.txtAttributeName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewholder.txtAttributeName.getTextColors().getDefaultColor() ==
                                Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor))) {
                            viewholder.txtAttributeName.setTextColor(Color.parseColor("#FFFFFF"));
                            viewholder.txtAttributeName.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
//                            AttributeColorSelection[position] = attribute.getAttributeterm_id();
//                            getVariations(AttributeColorSelection);
                            ArryListattributes.add(attribute.getAttributeterm_id());
//                            getCategoriesID(AttributeColorSelection, "color");
                            IntArrayPosition[position] = position;
                        } else {
                            viewholder.txtAttributeName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                            viewholder.txtAttributeName.setBackgroundColor(Color.parseColor("#dcdcdc"));
                           /* String str = AttributeColorSelection[position];
                            if (!TextUtils.isEmpty(str)) {
                                if (str.contains("," + attribute.getAttributeterm_id())) {
                                    str = str.replace("," + attribute.getAttributeterm_id(), "");
                                } else if (str.contains(attribute.getAttributeterm_id() + ",")) {
                                    str = str.replace(attribute.getAttributeterm_id() + ",", "");
                                } else if (str.contains(attribute.getAttributeterm_id())) {
                                    str = str.replace(attribute.getAttributeterm_id(), "");
                                }
//                                AttributeColorSelection[position] = str;
//                                getCategoriesID(AttributeColorSelection, "color");
                                getVariations(AttributeColorSelection);
                            }*/

                            for (int i = 0; i < ArryListattributes.size(); i++) {
                                if (ArryListattributes.contains(attributeID)) {
                                    ArryListattributes.remove(attributeID);
                                }
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return attributess.size();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            TextView txtAttributeName;
            CardView cardViewAttribute;

            public Viewholder(View itemView) {
                super(itemView);
                txtAttributeName = (TextView) itemView.findViewById(R.id.txtAttributeName);
                cardViewAttribute = (CardView) itemView.findViewById(R.id.cardViewAttribute);

                txtAttributeName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

                txtAttributeName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            }
        }

    }
    //endregion

    //region ADAPTER AdapterSizeAttribute
    public class AdapterSizeAttribute extends RecyclerView.Adapter<AdapterSizeAttribute.Viewholder> {

        Context context;
        JSONArray jsonArray;
        ArrayList<Attribute> attributess = new ArrayList<>();
        ImageView imageView, imgUpColor;
        boolean isclick = false;
        boolean ischeckposition = false;
        int[] IntArrayPosition;

        public AdapterSizeAttribute(Context context, ArrayList<Attribute> attributess, ImageView imgUpSize, ImageView imgDownSize) {
            this.context = context;
            this.attributess = attributess;
            AttributeSizeSelection = new String[attributess.size()];
            this.imageView = imgDownSize;
            this.imgUpColor = imgUpSize;
            IntArrayPosition = new int[attributess.size()];
        }

        @Override
        public AdapterSizeAttribute.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_attribute, viewGroup, false);
            return new AdapterSizeAttribute.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder viewholder, final int position) {
            try {
                final Attribute attribute = attributess.get(position);
                final String attributeID = attribute.getAttributeterm_id();

                if (isclick) {
                    viewholder.cardViewAttribute.setVisibility(View.VISIBLE);
                    viewholder.txtAttributeName.setText(attribute.getAttributetermsname());
                    if (IntArrayPosition != null) {
                        if (ischeckposition) {
                            if (IntArrayPosition[position] == position) {
                                viewholder.txtAttributeName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
                                viewholder.txtAttributeName.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                            } else {
                                viewholder.txtAttributeName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                                viewholder.txtAttributeName.setBackgroundColor(Color.parseColor("#dcdcdc"));
                            }
                        } else {
                            if (IntArrayPosition[position] == position) {
                                if (IntArrayPosition[position] != 0) {
                                    viewholder.txtAttributeName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
                                    viewholder.txtAttributeName.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                                } else {
                                    viewholder.txtAttributeName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                                    viewholder.txtAttributeName.setBackgroundColor(Color.parseColor("#dcdcdc"));
                                }
                            } else {
                                viewholder.txtAttributeName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                                viewholder.txtAttributeName.setBackgroundColor(Color.parseColor("#dcdcdc"));
                            }
                        }

                    }
                } else {
                    if (position <= 7) {
                        viewholder.cardViewAttribute.setVisibility(View.VISIBLE);
                        viewholder.txtAttributeName.setText(attribute.getAttributetermsname());
                    } else {
                        viewholder.cardViewAttribute.setVisibility(View.GONE);
                    }
                    if (ischeckposition) {
                        if (IntArrayPosition[position] == position) {
                            viewholder.txtAttributeName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
                            viewholder.txtAttributeName.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                        } else {
                            viewholder.txtAttributeName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                            viewholder.txtAttributeName.setBackgroundColor(Color.parseColor("#dcdcdc"));
                        }
                    } else {
                        if (IntArrayPosition[position] == position) {
                            if (IntArrayPosition[position] != 0) {
                                viewholder.txtAttributeName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
                                viewholder.txtAttributeName.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                            } else {
                                viewholder.txtAttributeName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                                viewholder.txtAttributeName.setBackgroundColor(Color.parseColor("#dcdcdc"));
                            }
                        } else {
                            viewholder.txtAttributeName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                            viewholder.txtAttributeName.setBackgroundColor(Color.parseColor("#dcdcdc"));
                        }
                    }
                }

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isclick) {
                            notifyDataSetChanged();
                            isclick = false;
                            imgUpColor.setVisibility(View.GONE);
                            ischeckposition = true;
                        }
                    }
                });

                imgUpColor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isclick) {
                            notifyDataSetChanged();
                            imageView.setVisibility(View.VISIBLE);
                            imgUpColor.setVisibility(View.GONE);
                            isclick = false;
                            ischeckposition = true;
                        }
                    }
                });


                viewholder.cardViewAttribute.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       /* selectedPosition = position;
                        notifyDataSetChanged();*/
                        if (viewholder.txtAttributeName.getTextColors().getDefaultColor() ==
                                Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor))) {
                            viewholder.txtAttributeName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
                            viewholder.txtAttributeName.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                            AttributeSizeSelection[position] = attribute.getAttributeterm_id();
                            getCategoriesID(AttributeSizeSelection, "size");
                        } else {
                            viewholder.txtAttributeName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                            viewholder.txtAttributeName.setBackgroundColor(Color.parseColor("#dcdcdc"));
                            String str = AttributeSizeSelection[position];
                            if (!TextUtils.isEmpty(str)) {
                                if (str.contains("," + attribute.getAttributeterm_id())) {
                                    str = str.replace("," + attribute.getAttributeterm_id(), "");
                                } else if (str.contains(attribute.getAttributeterm_id() + ",")) {
                                    str = str.replace(attribute.getAttributeterm_id() + ",", "");
                                } else if (str.contains(attribute.getAttributeterm_id())) {
                                    str = str.replace(attribute.getAttributeterm_id(), "");
                                }
                                AttributeSizeSelection[position] = str;
                                getCategoriesID(AttributeSizeSelection, "size");
                            }
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return attributess.size();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            TextView txtAttributeName;
            CardView cardViewAttribute;

            public Viewholder(View itemView) {
                super(itemView);
                txtAttributeName = (TextView) itemView.findViewById(R.id.txtAttributeName);
                cardViewAttribute = (CardView) itemView.findViewById(R.id.cardViewAttribute);

                txtAttributeName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

                txtAttributeName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            }
        }

    }
    //endregion

    //region SET FILTER METHOD FOR SEARCH...
    private ArrayList<ProductList> filter(ArrayList<ProductList> itemList, String s) {
        s = s.toLowerCase();
        final ArrayList<ProductList> filteredModelList = new ArrayList<>();
        for (ProductList model : itemList) {
            final String text = model.getProductname().toLowerCase();
            if (text.contains(s)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }
    //endregion

    //region Keyboard Hide
    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    //endregion

    //region FOR GET SELECTED ID FROM ADAPTER...
    public void getCategoriesID(String[] itemType, String string) {
        /*strArrayItemTypeId = itemType;*/
        if (itemType != null) {
            List<String> list = new ArrayList<String>();
            for (String s : itemType) {
                if (s != null && s.length() > 0) {
                    list.add(s);
                }
            }
            itemType = list.toArray(new String[list.size()]);
            if (string.equalsIgnoreCase("color")) {
                attributes = "1";
                arrayAttributeColor = new JSONArray(Arrays.asList(itemType));
            } else if (string.equals("size")) {
                attributes = "1";
                arrayAttributeSize = new JSONArray(Arrays.asList(itemType));
            }
        }
    }
    //endregion

    //region chanageButton
    public void ChangeFilterImage(ImageView imageView) {
        imageView.setBackgroundResource(R.drawable.bg_cricler);
        GradientDrawable gd = (GradientDrawable) imageView.getBackground().getCurrent();
        gd.setShape(GradientDrawable.OVAL);
        gd.setColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
    }
    //endregion

}