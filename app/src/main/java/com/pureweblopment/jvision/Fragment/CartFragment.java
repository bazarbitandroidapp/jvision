package com.pureweblopment.jvision.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.androidnetworking.model.Progress;
import com.pureweblopment.jvision.Activity.LoginActivity;
import com.pureweblopment.jvision.Activity.MainActivity;
import com.pureweblopment.jvision.Global.Global;
import com.pureweblopment.jvision.Global.SharedPreference;
import com.pureweblopment.jvision.Global.StaticUtility;
import com.pureweblopment.jvision.Global.Typefaces;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.concurrent.Executors;

import com.pureweblopment.jvision.Global.SendMail;

import com.pureweblopment.jvision.R;

import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CartFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CartFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CartFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    RecyclerView recyclerviewCartItems;
    RelativeLayout relativeProgress;
    String ordercart_id = "";

    ImageView imageCartBack, imageNavigation, imageLogo;
    FrameLayout frameLayoutCart;
    LinearLayout llBottomNavigation;
    CardView cardviewBottomNavigation;
    TextView txtCatName, txtTotalAmount, txtCheckout, txtcartempty, txtcartempty1;
    LinearLayout llCartEmpty, llCardPayment;
    Button btnContinueShopping;

    float TotalPrice = 0;
    int product_quantity = 0, count = 0;
    int qaunty = 1;

    DecimalFormat decimalFormat = new DecimalFormat("0.00");

    public CartFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CartFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CartFragment newInstance(String param1, String param2) {
        CartFragment fragment = new CartFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        MainActivity.manageBackPress(false);
        View view = inflater.inflate(R.layout.fragment_cart, container, false);
        MainActivity.ClearSharePreference = true;

        ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

        Bundle bundle = getArguments();
        imageCartBack = (ImageView) getActivity().findViewById(R.id.imageCartBack);
        txtCatName = (TextView) getActivity().findViewById(R.id.txtCatName);
        frameLayoutCart = (FrameLayout) getActivity().findViewById(R.id.frameLayoutCart);
        txtCatName.setText(R.string.shoppingcart);

        imageNavigation = (ImageView) getActivity().findViewById(R.id.imageNavigation);
        imageLogo = (ImageView) getActivity().findViewById(R.id.imageLogo);
        llBottomNavigation = (LinearLayout) getActivity().findViewById(R.id.llBottomNavigation);
        cardviewBottomNavigation = (CardView) getActivity().findViewById(R.id.cardviewBottomNavigation);

        imageCartBack.setVisibility(View.VISIBLE);
        txtCatName.setVisibility(View.VISIBLE);

        imageNavigation.setVisibility(View.GONE);
        imageLogo.setVisibility(View.GONE);
        frameLayoutCart.setVisibility(View.GONE);
        cardviewBottomNavigation.setVisibility(View.VISIBLE);


        imageCartBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    try {
                        getActivity().onBackPressed();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });

        Initialization(view);
        TypeFace();
        AppSettings();

        chanageButton(btnContinueShopping);

        txtCheckout.setOnClickListener(this);
        btnContinueShopping.setOnClickListener(this);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        CartItems();
        /*if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID) != null) {
            CartItems();
        } else {
            recyclerviewCartItems.setVisibility(View.GONE);
            llCartEmpty.setVisibility(View.VISIBLE);
            txtCheckout.setEnabled(false);
            llCardPayment.setVisibility(View.GONE);
            txtTotalAmount.setText("");
        }*/
    }

    //region AppSettings
    private void AppSettings() {
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
            txtTotalAmount.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        }
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor) != "") {
            txtTotalAmount.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
            txtCheckout.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        }
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            txtCheckout.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
        txtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
    }
    //endregion

    //region TypeFace
    private void TypeFace() {
        txtTotalAmount.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
        txtCheckout.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
        txtcartempty.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtcartempty1.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
    }
    //endregion

    //region Initialization
    private void Initialization(View view) {
        recyclerviewCartItems = (RecyclerView) view.findViewById(R.id.recyclerviewCartItems);
        relativeProgress = (RelativeLayout) view.findViewById(R.id.relativeProgress);
        txtTotalAmount = (TextView) view.findViewById(R.id.txtTotalAmount);
        txtCheckout = (TextView) view.findViewById(R.id.txtCheckout);
        txtcartempty = (TextView) view.findViewById(R.id.txtcartempty);
        txtcartempty1 = (TextView) view.findViewById(R.id.txtcartempty1);
        llCartEmpty = (LinearLayout) view.findViewById(R.id.llCartEmpty);
        llCardPayment = (LinearLayout) view.findViewById(R.id.llCardPayment);
        btnContinueShopping = (Button) view.findViewById(R.id.btnContinueShopping);
    }
    //endregion

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtCheckout:
                CartItemsRefresh();
                break;
            case R.id.btnContinueShopping:
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
                MainActivity.manageBackPress(true);
                break;
        }
    }

    //region FOR Cart Items Refresh API..
    private void CartItemsRefresh() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {};
        String[] val = {};

        String userId = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        String SessionId = SharedPreference.GetPreference(getActivity(), Global.preferenceNameGuestUSer, Global.SessionId);
        if (userId != null) {
            key = new String[]{"session_id", "user_id"};
            val = new String[]{"", userId};
        } else {
            key = new String[]{"session_id", "user_id"};
            val = new String[]{SessionId, ""};
        }
        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetCartItems);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    if (jsonObjectPayload.has("cart_items")) {
                                        JSONArray jsonArraycartitems = jsonObjectPayload.getJSONArray("cart_items");
                                        if (jsonArraycartitems.length() > 0) {
                                            LastOrderShippingAddress();
                                        } else {
                                            recyclerviewCartItems.setVisibility(View.GONE);
                                            llCartEmpty.setVisibility(View.VISIBLE);
                                            txtCheckout.setEnabled(false);
                                            llCardPayment.setVisibility(View.GONE);
                                            txtTotalAmount.setText("");
                                        }
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in CheckoutFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name

        void onFragmentInteraction(Uri uri);

        void gotoCheckoutCart();
    }

    //region FOR CartItems API...
    private void CartItems() {
        TotalPrice = 0;
        count = 0;
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key;
        String[] val;

        String userId = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        String SessionId = SharedPreference.GetPreference(getActivity(), Global.preferenceNameGuestUSer, Global.SessionId);
        if (userId != null) {
            key = new String[]{"session_id", "user_id"};
            val = new String[]{"", userId};
        } else {
            key = new String[]{"session_id", "user_id"};
            val = new String[]{SessionId, ""};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetCartItems);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    if (jsonObjectPayload.has("cart_items")) {
                                        JSONArray jsonArraycartitems = jsonObjectPayload.getJSONArray("cart_items");
                                        if (jsonArraycartitems.length() > 0) {
                                            for (int i = 0; i < jsonArraycartitems.length(); i++) {
                                                JSONObject jsonObjectCartItems = jsonArraycartitems.getJSONObject(i);
                                                int product_quantity = Integer.parseInt(jsonObjectCartItems.getString("product_quantity"));
                                                float floatPrice = Float.parseFloat(jsonObjectCartItems.getString("product_price"));
                                                int intPrice = Math.round(floatPrice) * product_quantity;
                                                TotalPrice = TotalPrice + intPrice;
                                                count++;
                                            }
                                            if(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                    StaticUtility.CurrencySignPosition) != null) {
                                                if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                        StaticUtility.CurrencySignPosition).equals("1")) {
                                                    txtTotalAmount.setText(SharedPreference.GetPreference(getContext(),
                                                            Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + String.valueOf(decimalFormat.format(TotalPrice)) + "( " + count + " items" + ")");
                                                } else {
                                                    txtTotalAmount.setText(String.valueOf(decimalFormat.format(TotalPrice)) + ". " + SharedPreference.GetPreference(getContext(),
                                                            Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + "( " + count + " items" + ")");
                                                }
                                            }else {
                                                txtTotalAmount.setText(String.valueOf(decimalFormat.format(TotalPrice)) + "( " + count + " items" + ")");
                                            }
                                            recyclerviewCartItems.setVisibility(View.VISIBLE);
                                            llCardPayment.setVisibility(View.VISIBLE);
                                            llCartEmpty.setVisibility(View.GONE);
                                            AdapterCart adapterHomeProduct = new AdapterCart(getContext(), jsonArraycartitems);
                                            recyclerviewCartItems.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                            recyclerviewCartItems.setAdapter(adapterHomeProduct);
                                            txtCheckout.setEnabled(true);
                                        } else {
                                            recyclerviewCartItems.setVisibility(View.GONE);
                                            llCartEmpty.setVisibility(View.VISIBLE);
                                            txtCheckout.setEnabled(false);
                                            llCardPayment.setVisibility(View.GONE);
                                            txtTotalAmount.setText("");
                                        }
                                    }
                                }

                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in CartFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region AdapterCart...
    public class AdapterCart extends RecyclerView.Adapter<AdapterCart.Viewholder> {

        Context context;
        JSONArray jsonArray;

        public AdapterCart(Context context, JSONArray jsonArray) {
            this.context = context;
            this.jsonArray = jsonArray;
        }

        @Override
        public AdapterCart.Viewholder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_cart_items, viewGroup, false);
            return new AdapterCart.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder holder, int position) {
            try {

                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        holder.pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
                    }
                }

                final JSONObject jsonObject = jsonArray.getJSONObject(position);
                final JSONObject jsonObjectProductData = jsonObject.getJSONObject("product_data");

                holder.txtProductName.setText(jsonObjectProductData.getString("name"));
                holder.txtProductDes.setText(jsonObjectProductData.getString("short_description"));

                product_quantity = Integer.parseInt(jsonObject.getString("product_quantity"));
                holder.txtQunty.setText(String.valueOf(product_quantity));

             /*   float floatPrice = Float.parseFloat(jsonObjectProductData.getString("sale_price"));
                int intPrice = Math.round(floatPrice) * product_quantity;
                TotalPrice = TotalPrice + intPrice;*/

                float floatBasePrice = Float.parseFloat(jsonObjectProductData.getString("price"));
                int intBasePrice = Math.round(floatBasePrice);
                float floatSalePrice = Float.parseFloat(jsonObjectProductData.getString("sale_price"));
                int intSalePrice = Math.round(floatSalePrice);

               /* if (intSalePrice > 0) {
                    float floatPrice = Float.parseFloat(jsonObjectProductData.getString("sale_price"));
                    int intPrice = Math.round(floatPrice) * product_quantity;
                    TotalPrice = TotalPrice + intPrice;
                } else {
                    float floatPrice = Float.parseFloat(jsonObjectProductData.getString("price"));
                    int intPrice = Math.round(floatPrice) * product_quantity;
                    TotalPrice = TotalPrice + intPrice;
                }*/

                if (intSalePrice > 0) {
                    if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("1")) {
                        holder.textProductSalePrice.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + jsonObjectProductData.getString("sale_price"));
                        holder.txt_product_base_price.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + jsonObjectProductData.getString("price"));
                        /*txtTotalAmount.setText(SharedPreference.GetPreference(getContext(),
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + String.valueOf(totalPrice) + "( " + count + " items" + ")");*/
                    } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("2")) {
                        holder.textProductSalePrice.setText(jsonObjectProductData.getString("sale_price") + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        holder.txt_product_base_price.setText(jsonObjectProductData.getString("price") + ". " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        /*txtTotalAmount.setText(SharedPreference.GetPreference(getContext(),
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + String.valueOf(totalPrice) + "( " + count + " items" + ")");*/
                    }
                } else {
                    if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("1")) {
                        holder.textProductSalePrice.setText(SharedPreference.GetPreference(context,
                                Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + jsonObjectProductData.getString("price"));
                        holder.txt_product_base_price.setVisibility(View.GONE);
                    } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.CurrencySignPosition).equals("2")) {
                        holder.textProductSalePrice.setText(jsonObjectProductData.getString("price") + " " +
                                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                        holder.txt_product_base_price.setVisibility(View.GONE);
                    }
                }

                holder.ImgIncrese.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            int incresqua = Integer.parseInt(holder.txtQunty.getText().toString());

                            if (incresqua < 5) {
                                ordercart_id = jsonObject.getString("ordercart_id");
                                IncressQaunty("1", holder.txtQunty, jsonObjectProductData.getString("sale_price"));
                            } else {
                                Toast.makeText(getContext(), "Maximum quantity reached!", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                holder.ImgDecrese.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            ordercart_id = jsonObject.getString("ordercart_id");
                            DecreseQaunty("0", holder.txtQunty, jsonObjectProductData.getString("sale_price"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                holder.imgRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            ordercart_id = jsonObject.getString("ordercart_id");
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setCancelable(true);
                            builder.setMessage("Are you sure want to remove this product from cart?");
                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    RemoveCart();
                                }
                            });

                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            builder.show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

             /*   JSONArray jsonArrayImages = jsonObjectProductData.getJSONArray("main_image");
                String image = String.valueOf(jsonArrayImages.get(0));*/

                JSONObject jsonObjectImages = jsonObjectProductData.getJSONObject("main_image");
                String image = jsonObjectImages.getString("main_image");

                //region Image
                String picUrl = null;
                try {
                    URL urla = null;
                /*images = images.replace("[", "");
                image = image.replace("]", "");*/
                    urla = new URL(image.replaceAll("%20", " "));
                    URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    // Capture position and set to the ImageView
                    Picasso.with(context)
                            .load(picUrl)
                            .into(holder.imageProduct, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                    holder.rlImgHolder.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }
                            });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in CartFragment.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in CartFragment.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
                //endregion

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return jsonArray.length();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            ImageView imageProduct, ImgIncrese, ImgDecrese, imgRemove;
            TextView txtProductName, txtProductDes, textProductSalePrice,
                    txt_product_base_price, txtQunty;
            RelativeLayout rlImgHolder;
            ProgressBar pbImgHolder;

            public Viewholder(View itemView) {
                super(itemView);
                imageProduct = (ImageView) itemView.findViewById(R.id.imageProduct);
                ImgIncrese = (ImageView) itemView.findViewById(R.id.ImgIncrese);
                ImgDecrese = (ImageView) itemView.findViewById(R.id.ImgDecrese);
                imgRemove = (ImageView) itemView.findViewById(R.id.imgRemove);

                txtProductName = (TextView) itemView.findViewById(R.id.txtProductName);
                txtProductDes = (TextView) itemView.findViewById(R.id.txtProductDes);
                textProductSalePrice = (TextView) itemView.findViewById(R.id.textProductSalePrice);
                txt_product_base_price = (TextView) itemView.findViewById(R.id.txt_product_base_price);
                txtQunty = (TextView) itemView.findViewById(R.id.txtQunty);
                rlImgHolder = (RelativeLayout) itemView.findViewById(R.id.rlImgHolder);
                pbImgHolder = (ProgressBar) itemView.findViewById(R.id.pbImgHolder);

                txtProductName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
                txtProductDes.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
                textProductSalePrice.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
                txt_product_base_price.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
                txtQunty.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));

                if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
                    txtQunty.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                    txtProductName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                    textProductSalePrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                }
                if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor) != "") {
                    txtProductDes.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                    txt_product_base_price.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                }
                if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                    ImgDecrese.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                    ImgIncrese.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                }
            }
        }
    }
    //endregion

    //region FOR IncressDecreseQaunty API...
    private void IncressQaunty(final String Type, final TextView textView,
                               final String sale_price) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key;
        String[] val;

        String userId = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        String SessionId = SharedPreference.GetPreference(getActivity(), Global.preferenceNameGuestUSer, Global.SessionId);
        if (userId != null) {
            key = new String[]{"session_id", "user_id", "ordercart_id", "quantity", "type"};
            val = new String[]{"", userId, ordercart_id, "", Type};
        } else {
            key = new String[]{"session_id", "user_id", "ordercart_id", "quantity", "type"};
            val = new String[]{SessionId, "", ordercart_id, "", Type};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetQauntyIncreseDecrese);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    qaunty = Integer.parseInt(jsonObject.getString("current_qty"));
                                    textView.setText(String.valueOf(qaunty));
                                    float floatPrice = Float.parseFloat(sale_price);
                                    int intPrice = Math.round(floatPrice);
                                    TotalPrice = TotalPrice + intPrice;

                                    if(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                            StaticUtility.CurrencySignPosition) != null) {
                                        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                StaticUtility.CurrencySignPosition).equals("1")) {
                                            txtTotalAmount.setText(SharedPreference.GetPreference(getContext(),
                                                    Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + String.valueOf(decimalFormat.format(TotalPrice)) + "( " + count + " items" + ")");
                                        } else {
                                            txtTotalAmount.setText(String.valueOf(decimalFormat.format(TotalPrice)) + ". " + SharedPreference.GetPreference(getContext(),
                                                    Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + "( " + count + " items" + ")");
                                        }
                                    }else {
                                        txtTotalAmount.setText(String.valueOf(decimalFormat.format(TotalPrice)) + "( " + count + " items" + ")");
                                    }


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in CartFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    // region FOR DecreseQaunty API...
    private void DecreseQaunty(final String Type, final TextView textView,
                               final String sale_price) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key;
        String[] val;

        String userId = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        String SessionId = SharedPreference.GetPreference(getActivity(), Global.preferenceNameGuestUSer, Global.SessionId);
        if (userId != null) {
            key = new String[]{"session_id", "user_id", "ordercart_id", "quantity", "type"};
            val = new String[]{"", userId, ordercart_id, "", Type};
        } else {
            key = new String[]{"session_id", "user_id", "ordercart_id", "quantity", "type"};
            val = new String[]{SessionId, "", ordercart_id, "", Type};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetQauntyIncreseDecrese);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    qaunty = Integer.parseInt(jsonObject.getString("current_qty"));
                                    textView.setText(jsonObject.getString("current_qty"));

                                    float floatPrice = Float.parseFloat(sale_price);
                                    int intPrice = Math.round(floatPrice);
                                    TotalPrice = TotalPrice - intPrice;

                                    if(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                            StaticUtility.CurrencySignPosition) != null) {
                                        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                                                StaticUtility.CurrencySignPosition).equals("1")) {
                                            txtTotalAmount.setText(SharedPreference.GetPreference(getContext(),
                                                    Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + " " + String.valueOf(decimalFormat.format(TotalPrice)) + "( " + count + " items" + ")");
                                        } else {
                                            txtTotalAmount.setText(String.valueOf(decimalFormat.format(TotalPrice)) + ". " + SharedPreference.GetPreference(getContext(),
                                                    Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + "( " + count + " items" + ")");
                                        }
                                    }else {
                                        txtTotalAmount.setText(String.valueOf(decimalFormat.format(TotalPrice)) + "( " + count + " items" + ")");
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in CartFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR RemoveCart API...
    private void RemoveCart() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key;
        String[] val;

        String userId = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        String SessionId = SharedPreference.GetPreference(getActivity(), Global.preferenceNameGuestUSer, Global.SessionId);
        if (userId != null) {
            key = new String[]{"session_id", "user_id", "cartitem_id"};
            val = new String[]{"", userId, ordercart_id};
        } else {
            key = new String[]{"session_id", "user_id", "cartitem_id"};
            val = new String[]{SessionId, "", ordercart_id};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.RemoveCart);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    /*mListener = (OnFragmentInteractionListener) getContext();
                                    mListener.CartCountRemove();*/
                                    CartItems();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in CartFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region chanageButton
    public void chanageButton(Button button) {
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor) != "") {
            button.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        }
        button.setBackgroundResource(R.drawable.ic_continueshopping);
        GradientDrawable gd = (GradientDrawable) button.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(10);
        /* gd.setStroke(2, Color.parseColor(StaticUtility.BORDERCOLOR));*/
    }
    //endregion

    //region FOR LastOrderShippingAddress API..
    private void LastOrderShippingAddress() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.LastOrderAddress);
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers1(getContext()));
        }
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    SharedPreference.CreatePreference(getContext(), Global.CheckoutTotalPrice);
                                    SharedPreference.SavePreference(StaticUtility.strTotalPrice, String.valueOf(decimalFormat.format(TotalPrice)));
                                    SharedPreference.SavePreference(StaticUtility.strTotalItems, String.valueOf(count));
                                    mListener = (OnFragmentInteractionListener) getContext();
                                    mListener.gotoCheckoutCart();
                                    String userToken = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERTOKEN);
                                    /*if (userToken != null) {
                                        SharedPreference.CreatePreference(getContext(), Global.CheckoutTotalPrice);
                                        SharedPreference.SavePreference(StaticUtility.strTotalPrice, String.valueOf(TotalPrice));
                                        SharedPreference.SavePreference(StaticUtility.strTotalItems, String.valueOf(count));
                                        mListener = (OnFragmentInteractionListener) getContext();
                                        mListener.gotoCheckoutCart();
                                    } else {
                                        SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                        SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                        SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                        SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                        SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                                        *//*Toast.makeText(getContext(), "You have to Login!", Toast.LENGTH_SHORT).show();*//*
                                        startActivity(new Intent(getActivity(), LoginActivity.class));
                                        *//*getActivity().finish();*//*
                                    }*/
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    /*Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();*/
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in CheckOutFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

}
