package com.pureweblopment.jvision.Fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pureweblopment.jvision.Activity.MainActivity;
import com.pureweblopment.jvision.Global.Global;
import com.pureweblopment.jvision.Global.SharedPreference;
import com.pureweblopment.jvision.Global.Typefaces;
import com.pureweblopment.jvision.Global.StaticUtility;

import com.pureweblopment.jvision.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PlaceOrderSuccessFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PlaceOrderSuccessFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PlaceOrderSuccessFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    TextView txtStatus, txtThankYou;
    ImageView imgCompletedSuccess, imgCompletedAddress, imgCompletedPaymane;

    LinearLayout llPaymentHeader, llBottomNavigation;
    ImageView imageCartBack, imageNavigation, imageLogo;
    FrameLayout frameLayoutCart;
    CardView cardviewBottomNavigation;
    TextView txtCatName;
    Bundle bundle;

    public static Boolean isclickback = true;

    private OnFragmentInteractionListener mListener;
    boolean flag = true;
    ImageView imgStatus;

    public PlaceOrderSuccessFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PlaceOrderSuccessFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PlaceOrderSuccessFragment newInstance(String param1, String param2) {
        PlaceOrderSuccessFragment fragment = new PlaceOrderSuccessFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        MainActivity.manageBackPress(true);
        MainActivity.PlaceOrderSuccessback = true;
        View view = inflater.inflate(R.layout.fragment_place_order_success, container, false);

        bundle = getArguments();
        String payment_method = bundle.getString("payment_method");
        String urlStatus = bundle.getString("urlStatus");

        imageCartBack = (ImageView) getActivity().findViewById(R.id.imageCartBack);
        imageNavigation = (ImageView) getActivity().findViewById(R.id.imageNavigation);
        imageLogo = (ImageView) getActivity().findViewById(R.id.imageLogo);

        frameLayoutCart = (FrameLayout) getActivity().findViewById(R.id.frameLayoutCart);
        txtCatName = (TextView) getActivity().findViewById(R.id.txtCatName);
        llBottomNavigation = (LinearLayout) getActivity().findViewById(R.id.llBottomNavigation);
        cardviewBottomNavigation = (CardView) getActivity().findViewById(R.id.cardviewBottomNavigation);

        imageCartBack.setVisibility(View.VISIBLE);
        txtCatName.setVisibility(View.VISIBLE);
        txtCatName.setText(R.string.successorder);

        imageNavigation.setVisibility(View.GONE);
        imageLogo.setVisibility(View.GONE);
        frameLayoutCart.setVisibility(View.GONE);
//        llBottomNavigation.setVisibility(View.GONE);
        cardviewBottomNavigation.setVisibility(View.GONE);
        imageCartBack.setVisibility(View.GONE);

        imgCompletedSuccess = (ImageView) view.findViewById(R.id.imgCompletedSuccess);
        imgCompletedAddress = (ImageView) view.findViewById(R.id.imgCompletedAddress);
        imgCompletedPaymane = (ImageView) view.findViewById(R.id.imgCompletedPaymane);
        llPaymentHeader = (LinearLayout) view.findViewById(R.id.llPaymentHeader);
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            llPaymentHeader.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
//        MainActivity.checkback = true;
        txtStatus = (TextView) view.findViewById(R.id.txtStatus);
        txtThankYou = (TextView) view.findViewById(R.id.txtThankYou);
        imgStatus = (ImageView) view.findViewById(R.id.imgStatus);
        txtStatus.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtThankYou.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));

        if (payment_method.equals("1")) {
            txtStatus.setVisibility(View.GONE);
            imgCompletedSuccess.setVisibility(View.VISIBLE);
            imgCompletedAddress.setVisibility(View.VISIBLE);
        } else if (payment_method.equals("2")) {
            txtStatus.setVisibility(View.VISIBLE);
            imgCompletedSuccess.setVisibility(View.VISIBLE);
            imgCompletedAddress.setVisibility(View.VISIBLE);
            imgCompletedPaymane.setVisibility(View.VISIBLE);
        } else if (payment_method.equals("4")) {
            txtStatus.setVisibility(View.VISIBLE);
            imgCompletedSuccess.setVisibility(View.VISIBLE);
            imgCompletedAddress.setVisibility(View.VISIBLE);
            imgCompletedPaymane.setVisibility(View.VISIBLE);
        }

        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
            txtStatus.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtThankYou.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        }

        if (!urlStatus.equalsIgnoreCase("")) {
            txtStatus.setText(urlStatus);
            if (urlStatus.equals("Transaction Successfully!")) {
                txtThankYou.setText(R.string.thank_you_for_your_order);
                imgCompletedPaymane.setImageResource(R.drawable.ic_completed);
                imgCompletedSuccess.setImageResource(R.drawable.ic_completed);
                imgStatus.setImageResource(R.drawable.ic_placeordersuccess);
            } else {
                txtThankYou.setText(R.string.thank_you_for_your_response);
                imgStatus.setImageResource(R.drawable.ic_error);
                imgCompletedPaymane.setImageResource(R.drawable.ic_worrng);
                imgCompletedSuccess.setImageResource(R.drawable.ic_worrng);
            }
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isclickback) {
                    try {
                        MainActivity.manageBackPress(true);
                        Intent i = new Intent(getActivity(), MainActivity.class);
                        startActivity(i);
                        getActivity().finish();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, 3000);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
