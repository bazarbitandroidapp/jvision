package com.pureweblopment.jvision.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SlidingDrawer;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;

import com.pureweblopment.jvision.Activity.LoginActivity;
import com.pureweblopment.jvision.Activity.MainActivity;
import com.pureweblopment.jvision.Global.Global;
import com.pureweblopment.jvision.Global.SendMail;
import com.pureweblopment.jvision.Global.SharedPreference;
import com.pureweblopment.jvision.Global.StaticUtility;
import com.pureweblopment.jvision.Global.Typefaces;
import com.pureweblopment.jvision.Model.Footer;
import com.pureweblopment.jvision.Model.Item;
import com.pureweblopment.jvision.Model.OrderHistory;
import com.pureweblopment.jvision.R;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OrderHistoryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OrderHistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OrderHistoryFragment extends Fragment implements View.OnClickListener, View.OnTouchListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    ImageView imageCartBack, imageNavigation, imageLogo, imageOrderHistoryFilter;
    FrameLayout frameLayoutCart;
    TextView txtCatName;
    LinearLayout llBottomNavigation;

    RelativeLayout relativeProgress;
    LinearLayout llNoOrderAvailable;
    RecyclerView recyclerviewOrderHistory;
    TextView txtNoOrderAvailable;
    Toolbar toolbar;

    private LinearLayout llFilterBg, llContent;
    private SlidingDrawer slidingDrawer;
    ImageView imageCancel;
    TextView txtApply, TextViewFilter;
    EditText editFromDate, editToDate;
    LinearLayout llFilterMain;
    View view1;
    CardView cardviewBottomNavigation;

    ArrayList<OrderHistory> orderHistories;
    AdapterOrderHistory adapterOrderHistory;

    String name, image, order_item_status, order_id, order_datetime, order_product_amount;

    private List<Item> postdata;
    private ArrayList<HashMap<String, String>> postList = new ArrayList<HashMap<String, String>>();
    private boolean hasMore;
    int limit = 10, offset = 0;
    private boolean flag = true;
    String strFormDate = "", strToDate = "";

    public OrderHistoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OrderHistoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OrderHistoryFragment newInstance(String param1, String param2) {
        OrderHistoryFragment fragment = new OrderHistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        MainActivity.manageBackPress(false);
        View view = inflater.inflate(R.layout.fragment_order_history, container, false);

        ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

        imageCartBack = (ImageView) getActivity().findViewById(R.id.imageCartBack);
        imageNavigation = (ImageView) getActivity().findViewById(R.id.imageNavigation);
        imageLogo = (ImageView) getActivity().findViewById(R.id.imageLogo);
        imageOrderHistoryFilter = (ImageView) getActivity().findViewById(R.id.imageOrderHistoryFilter);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);

        frameLayoutCart = (FrameLayout) getActivity().findViewById(R.id.frameLayoutCart);
        txtCatName = (TextView) getActivity().findViewById(R.id.txtCatName);
        llBottomNavigation = (LinearLayout) getActivity().findViewById(R.id.llBottomNavigation);
        cardviewBottomNavigation = (CardView) getActivity().findViewById(R.id.cardviewBottomNavigation);

        imageCartBack.setVisibility(View.VISIBLE);
        imageOrderHistoryFilter.setVisibility(View.VISIBLE);
        txtCatName.setVisibility(View.VISIBLE);
        txtCatName.setText(R.string.orderhistory);

        imageNavigation.setVisibility(View.GONE);
        imageLogo.setVisibility(View.GONE);
        frameLayoutCart.setVisibility(View.GONE);
//        llBottomNavigation.setVisibility(View.GONE);
        cardviewBottomNavigation.setVisibility(View.GONE);

        imageCartBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        Initialization(view);
        TypeFace();
        OnClickListener();
        AppSettings();

        chanageEditTextBorder(editFromDate);
        chanageEditTextBorder(editToDate);
//        chanageImages(imageCancel);

        imageOrderHistoryFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbar.setVisibility(View.GONE);
                llFilterBg.setVisibility(View.VISIBLE);
                if (slidingDrawer.isOpened()) {
                    slidingDrawer.animateClose();
                    llFilterBg.setBackgroundColor(Color.TRANSPARENT);
                } else if (!slidingDrawer.isOpened()) {
                    llFilterBg.setBackgroundColor(Color.parseColor("#85000000"));
                    slidingDrawer.animateOpen();
                }
            }
        });

        if (postdata != null) {
            try {
                adapterOrderHistory.clearData();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            postdata.clear();
            postList.clear();
        }

        OrderHistory("", "");

        recyclerviewOrderHistory.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (hasMore) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    //position starts at 0
                    int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                    int itemCount = layoutManager.getItemCount();
                    if (layoutManager.findLastVisibleItemPosition() >= layoutManager.getItemCount() - 5) {
                        flag = true;
                        if (strFormDate.equals("")) {
                            OrderHistory1("", "");
                        } else {
                            OrderHistory1(strFormDate, strToDate);
                        }
                    }
                }
            }
        });

        editFromDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    strFormDate = editFromDate.getText().toString();
                    editToDate.setEnabled(true);
                } else {
                    editToDate.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editToDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    strToDate = editToDate.getText().toString();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        /*if (editFromDate.getText().length() > 0) {
            editToDate.setEnabled(true);
        } else {
            editToDate.setEnabled(false);
        }*/

        return view;
    }

    private boolean hasFooter() {
        return postdata.get(postdata.size() - 1) instanceof Footer;
    }

    //region Initialization
    private void Initialization(View view) {
        recyclerviewOrderHistory = (RecyclerView) view.findViewById(R.id.recyclerviewOrderHistory);
        relativeProgress = (RelativeLayout) view.findViewById(R.id.relativeProgress);
        llNoOrderAvailable = (LinearLayout) view.findViewById(R.id.llNoOrderAvailable);
        txtNoOrderAvailable = (TextView) view.findViewById(R.id.txtNoOrderAvailable);
        llFilterBg = (LinearLayout) view.findViewById(R.id.llFilterBg);
        llContent = (LinearLayout) view.findViewById(R.id.llContent);
        slidingDrawer = (SlidingDrawer) view.findViewById(R.id.slidingDrawer);
        imageCancel = (ImageView) view.findViewById(R.id.imgCancel);
        txtApply = (TextView) view.findViewById(R.id.txtApply);
        TextViewFilter = (TextView) view.findViewById(R.id.TextViewFilter);
        editFromDate = (EditText) view.findViewById(R.id.editFromDate);
        editToDate = (EditText) view.findViewById(R.id.editToDate);
        llFilterMain = (LinearLayout) view.findViewById(R.id.llFilterMain);
        view1 = (View) view.findViewById(R.id.view1);
        imageCancel.setColorFilter(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

    }
    //endregion

    //region TypeFace
    private void TypeFace() {
        txtNoOrderAvailable.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtApply.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
        TextViewFilter.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
        editFromDate.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editToDate.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
    }
    //endregion

    //region OnClickListener
    private void OnClickListener() {
        llContent.setOnClickListener(this);
        imageCancel.setOnClickListener(this);
        txtApply.setOnClickListener(this);
        editFromDate.setOnTouchListener(this);
        editToDate.setOnTouchListener(this);
    }
    //endregion

    //region AppSettings
    private void AppSettings() {
        txtNoOrderAvailable.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editFromDate.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        editToDate.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        llFilterMain.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        TextViewFilter.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtApply.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        view1.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editFromDate.setHintTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editToDate.setHintTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editToDate.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editFromDate.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
    }
    //endregion


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llContent:
                toolbar.setVisibility(View.VISIBLE);
                slidingDrawer.animateClose();
                llFilterBg.setBackgroundColor(Color.TRANSPARENT);
                if (slidingDrawer.isOpened()) {
                    slidingDrawer.animateClose();
                    llFilterBg.setBackgroundColor(Color.TRANSPARENT);
                }
                break;
            case R.id.imgCancel:
                editFromDate.setText("");
                editToDate.setText("");
                toolbar.setVisibility(View.VISIBLE);
                if (slidingDrawer.isOpened()) {
                    slidingDrawer.animateClose();
                    llFilterBg.setBackgroundColor(Color.TRANSPARENT);
                    llFilterBg.setVisibility(View.GONE);
                }
                break;
            case R.id.txtApply:

                editFromDate.setText("");
                editToDate.setText("");

                toolbar.setVisibility(View.VISIBLE);
                if (slidingDrawer.isOpened()) {
                    slidingDrawer.animateClose();
                    llFilterBg.setBackgroundColor(Color.TRANSPARENT);
                    llFilterBg.setVisibility(View.GONE);
                }
                if (strFormDate.equals("")) {
                    OrderHistory("", "");
                } else {
                    OrderHistory(strFormDate, strToDate);
                }
//                DateViseFilterOrderHistory();
                break;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.editFromDate:
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Global.DateDialogFromTo newFragment = new Global.DateDialogFromTo(getContext(), editFromDate, false);
                    newFragment.show(getFragmentManager(), "datePicker");
                    return true;
                }
                break;
            case R.id.editToDate:
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Global.DateDialogFromTo newFragment = new Global.DateDialogFromTo(getContext(), editToDate, true);
                    newFragment.show(getFragmentManager(), "datePicker");
                    return true;
                }
                break;
        }
        return false;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void GotoOrderDetail(String slug);
    }

    //region FOR OrderHistory...
    private void OrderHistory(String fromdate, String todate) {
        offset = 0;
        relativeProgress.setVisibility(View.VISIBLE);
        String[] key = {};
        String[] val = {};
        if (fromdate.equals("")) {
            key = new String[]{"offset", "limit"};
            val = new String[]{String.valueOf(offset), String.valueOf(limit)};
        } else {
            key = new String[]{"fromdate", "todate", "offset", "limit"};
            val = new String[]{fromdate, todate, String.valueOf(offset), String.valueOf(limit)};
        }

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.OrderHistory);
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers1(getContext()));
        } else {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers2(getContext()));
        }
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.optString("status");
                                String strMessage = response.optString("message");
                                if (strStatus.equalsIgnoreCase("ok")) {
                                    JSONArray jsonArray = response.getJSONArray("payload");
//                                    orderHistories = new ArrayList<>();
                                    ArrayList<Item> orderHistorys = new ArrayList<Item>(10);
                                    if (jsonArray.length() > 0) {
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject jsonObjectPayload = jsonArray.getJSONObject(i);
                                            order_id = jsonObjectPayload.getString("order_id");
                                            order_datetime = jsonObjectPayload.getString("order_datetime");
                                            order_product_amount = jsonObjectPayload.getString("order_product_amount");
                                            JSONArray jsonArrayOrderItems = jsonObjectPayload.getJSONArray("ordereditems");
                                            for (int j = 0; j < jsonArrayOrderItems.length(); j++) {
                                                JSONObject jsonObjectOrder = jsonArrayOrderItems.getJSONObject(j);
                                                name = jsonObjectOrder.getString("name");
                                                JSONObject jsonObjectImage = jsonObjectOrder.getJSONObject("main_image");
                                                image = jsonObjectImage.getString("main_image");
//                                                image = jsonObjectOrder.getString("main_image");
                                                order_item_status = jsonObjectOrder.getString("order_item_status");
                                            }
                                           /* OrderHistory orderHistory = new OrderHistory(order_id, order_datetime, order_product_amount,
                                                    name, order_item_status, image);
                                            orderHistorys.add(orderHistory);
                                        /*orderHistories.add(new OrderHistory(order_id, order_datetime, order_product_amount,
                                                name, order_item_status, image));*/
                                        }
                                        postdata = orderHistorys;
                                        offset = offset + limit;

                                        if (postdata.size() > 0) {
                                            hasMore = true;
                                            llNoOrderAvailable.setVisibility(View.GONE);
                                            recyclerviewOrderHistory.setVisibility(View.VISIBLE);
                                            AdapterOrderHistory adapterOrderHistory = new AdapterOrderHistory(getContext(), postdata);
                                            recyclerviewOrderHistory.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                            recyclerviewOrderHistory.setAdapter(adapterOrderHistory);
                                           /* if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, StaticUtility.strOrderHistoryLastClick) != null) {
                                                int position = Integer.parseInt(SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, StaticUtility.strOrderHistoryLastClick));
                                                recyclerviewOrderHistory.smoothScrollToPosition(position);
                                            }*/
                                        } else {
                                            hasMore = false;
                                            llNoOrderAvailable.setVisibility(View.VISIBLE);
                                            recyclerviewOrderHistory.setVisibility(View.GONE);
                                        }

                                   /* if(orderHistories.size() > 0)
                                    {
                                        llNoOrderAvailable.setVisibility(View.GONE);
                                        recyclerviewOrderHistory.setVisibility(View.VISIBLE);
                                        AdapterOrderHistory adapterOrderHistory = new AdapterOrderHistory(getContext(), orderHistories);
                                        recyclerviewOrderHistory.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                        recyclerviewOrderHistory.setAdapter(adapterOrderHistory);
                                    }else {
                                        llNoOrderAvailable.setVisibility(View.VISIBLE);
                                        recyclerviewOrderHistory.setVisibility(View.GONE);
                                    }*/
                                    /*if (jsonArray.length() > 0) {
                                        llNoOrderAvailable.setVisibility(View.GONE);
                                        recyclerviewOrderHistory.setVisibility(View.VISIBLE);
                                        AdapterOrderHistory adapterOrderHistory = new AdapterOrderHistory(getContext(), jsonArray);
                                        recyclerviewOrderHistory.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                        recyclerviewOrderHistory.setAdapter(adapterOrderHistory);
                                    } else {
                                        llNoOrderAvailable.setVisibility(View.VISIBLE);
                                        recyclerviewOrderHistory.setVisibility(View.GONE);
                                    }*/
                                    } else {
                                        llNoOrderAvailable.setVisibility(View.VISIBLE);
                                        recyclerviewOrderHistory.setVisibility(View.GONE);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                if (strMessage.equalsIgnoreCase("Invalid Credentials")) {
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in OrderHistoryFragment.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR OrderHistory1...
    private void OrderHistory1(String fromdate, String todate) {
        relativeProgress.setVisibility(View.VISIBLE);
        String[] key = {};
        String[] val = {};
        if (fromdate.equals("")) {
            key = new String[]{"offset", "limit"};
            val = new String[]{String.valueOf(offset), String.valueOf(limit)};
        } else {
            key = new String[]{"fromdate", "todate", "offset", "limit"};
            val = new String[]{fromdate, todate, String.valueOf(offset), String.valueOf(limit)};
        }

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.OrderHistory);
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers1(getContext()));
        } else {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers2(getContext()));
        }
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if (flag) {
                                    flag = false;
                                    String strStatus = response.optString("status");
                                    String strMessage = response.optString("message");
                                    if (strStatus.equalsIgnoreCase("ok")) {
                                        JSONArray jsonArray = response.getJSONArray("payload");
//                                    orderHistories = new ArrayList<>();
                                        ArrayList<Item> orderHistorys = new ArrayList<Item>(10);
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject jsonObjectPayload = jsonArray.getJSONObject(i);
                                            order_id = jsonObjectPayload.getString("order_id");
                                            order_datetime = jsonObjectPayload.getString("order_datetime");
                                            order_product_amount = jsonObjectPayload.getString("order_product_amount");
                                            JSONArray jsonArrayOrderItems = jsonObjectPayload.getJSONArray("ordereditems");
                                            for (int j = 0; j < jsonArrayOrderItems.length(); j++) {
                                                JSONObject jsonObjectOrder = jsonArrayOrderItems.getJSONObject(j);
                                                name = jsonObjectOrder.getString("name");
                                                image = jsonObjectOrder.getString("main_image");
                                                order_item_status = jsonObjectOrder.getString("order_item_status");
                                            }
                                       /*     OrderHistory orderHistory = new OrderHistory(order_id, order_datetime, order_product_amount,
                                                    name, order_item_status, image);
                                            orderHistorys.add(orderHistory);*/
                                        /*orderHistories.add(new OrderHistory(order_id, order_datetime, order_product_amount,
                                                name, order_item_status, image));*/
                                        }
                                        offset = offset + limit;

                                        if (orderHistorys.size() > 0 && hasMore) {
                                            hasMore = true;
                                            postdata.addAll(orderHistorys);
                                            recyclerviewOrderHistory.setHasFixedSize(true);
                                            recyclerviewOrderHistory.getAdapter().notifyDataSetChanged();
                                            /*if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, StaticUtility.strOrderHistoryLastClick) != null) {
                                                int position = Integer.parseInt(SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, StaticUtility.strOrderHistoryLastClick));
                                                recyclerviewOrderHistory.smoothScrollToPosition(position);
                                            }*/
                                               /* recyclerviewOrderHistory.setVisibility(View.VISIBLE);
                                                recyclerviewOrderHistory.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                                                OrderAdapter orderAdapter = new OrderAdapter(getContext(), postdata, "2");
                                                recyclerviewOrderHistory.setAdapter(orderAdapter);*/

                                        } else {
                                            hasMore = false;
                                        }
                                        if (limit > orderHistorys.size()) {
                                            hasMore = false;
                                        }

                                   /* if(orderHistories.size() > 0)
                                    {
                                        llNoOrderAvailable.setVisibility(View.GONE);
                                        recyclerviewOrderHistory.setVisibility(View.VISIBLE);
                                        AdapterOrderHistory adapterOrderHistory = new AdapterOrderHistory(getContext(), orderHistories);
                                        recyclerviewOrderHistory.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                        recyclerviewOrderHistory.setAdapter(adapterOrderHistory);
                                    }else {
                                        llNoOrderAvailable.setVisibility(View.VISIBLE);
                                        recyclerviewOrderHistory.setVisibility(View.GONE);
                                    }*/
                                    /*if (jsonArray.length() > 0) {
                                        llNoOrderAvailable.setVisibility(View.GONE);
                                        recyclerviewOrderHistory.setVisibility(View.VISIBLE);
                                        AdapterOrderHistory adapterOrderHistory = new AdapterOrderHistory(getContext(), jsonArray);
                                        recyclerviewOrderHistory.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                        recyclerviewOrderHistory.setAdapter(adapterOrderHistory);
                                    } else {
                                        llNoOrderAvailable.setVisibility(View.VISIBLE);
                                        recyclerviewOrderHistory.setVisibility(View.GONE);
                                    }*/

                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                if (strMessage.equalsIgnoreCase("Invalid Credentials")) {
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in OrderHistoryFragment.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR DateViseFilterOrderHistory...
    private void DateViseFilterOrderHistory() {
        relativeProgress.setVisibility(View.VISIBLE);
        String[] key = {"fromdate", "todate"};
        String[] val = {editFromDate.getText().toString(), editToDate.getText().toString()};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.OrderHistory);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.optString("status");
                                String strMessage = response.optString("message");
                                if (strStatus.equalsIgnoreCase("ok")) {
                                    JSONArray jsonArray = response.getJSONArray("payload");
                                    if (jsonArray.length() > 0) {
                                        llNoOrderAvailable.setVisibility(View.GONE);
                                        recyclerviewOrderHistory.setVisibility(View.VISIBLE);
                                        AdapterOrderHistory adapterOrderHistory = new AdapterOrderHistory(getContext(), jsonArray);
                                        recyclerviewOrderHistory.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                        recyclerviewOrderHistory.setAdapter(adapterOrderHistory);
                                    } else {
                                        llNoOrderAvailable.setVisibility(View.VISIBLE);
                                        recyclerviewOrderHistory.setVisibility(View.GONE);
                                    }

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                if (strMessage.equalsIgnoreCase("Invalid Credentials")) {
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in OrderHistoryFragment.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region AdapterOrderHistory...
    public class AdapterOrderHistory extends RecyclerView.Adapter<AdapterOrderHistory.Viewholder> {

        Context context;
        JSONArray jsonArray;
        List<Item> orders;
        ArrayList<OrderHistory> orderHistories = new ArrayList<>();

        public AdapterOrderHistory(Context context, JSONArray jsonArray) {
            this.context = context;
            this.jsonArray = jsonArray;
        }

        public AdapterOrderHistory(Context context, ArrayList<OrderHistory> orderHistories) {
            this.context = context;
            this.orderHistories = orderHistories;
        }

        public AdapterOrderHistory(Context context, List<Item> orders) {
            this.orders = orders;
            this.context = context;
        }

        @Override
        public AdapterOrderHistory.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_orderhistory, viewGroup, false);
            return new AdapterOrderHistory.Viewholder(view);
        }

        public void clearData() {
            int size = orders.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    orders.remove(0);
                }
                this.notifyItemRangeRemoved(0, size);
            }
        }

        @Override
        public void onBindViewHolder(Viewholder holder, final int position) {

//            final OrderHistory orderHistory = orderHistories.get(position);
           /* final OrderHistory orderHistory = (OrderHistory) orders.get(position);
            holder.txtProductName.setText(orderHistory.getName());
            holder.txtOrderID.setText("Order Id :- " + orderHistory.getOrder_id());
            holder.txtProducDate.setText(Global.changeDateFormate(orderHistory.getOrder_datetime(),
                    "yyyy-MM-dd hh:mm:ss", "dd-MMM-yyyy"));

            float floatPrice = Float.parseFloat(orderHistory.getOrder_product_amount());
            int intPrice = Math.round(floatPrice);
            String image = orderHistory.getImage();
            String image1 = image.replace("[", "").replace("]", "").replace("\"", "");

            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                    StaticUtility.CurrencySignPosition).equals("1")) {
                holder.txtProductPrice.setText(SharedPreference.GetPreference(context,
                        Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + String.valueOf(intPrice));
            } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                    StaticUtility.CurrencySignPosition).equals("2")) {
                holder.txtProductPrice.setText(String.valueOf(intPrice) + ". " +
                        SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
            }
            String order_item_status = orderHistory.getOrder_item_status();

            if (order_item_status.equals("0")) {
                holder.txtProductStatus.setText("Pending Order");
                holder.txtProductStatus.setBackgroundColor(Color.parseColor("#FF7F50"));
                holder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
            } else if (order_item_status.equals("1")) {
                holder.txtProductStatus.setText("Confirm Order");
                holder.txtProductStatus.setBackgroundColor(Color.parseColor("#28b62c"));
                holder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
            } else if (order_item_status.equals("2")) {
                holder.txtProductStatus.setText("In Process");
                holder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                holder.txtProductStatus.setBackgroundColor(Color.parseColor("#d4af37"));
            } else if (order_item_status.equals("3")) {
                holder.txtProductStatus.setText("Out for Delivery");
                holder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                holder.txtProductStatus.setBackgroundColor(Color.parseColor("#88b04b"));
            } else if (order_item_status.equals("4")) {
                holder.txtProductStatus.setText("Deliveried");
                holder.txtProductStatus.setBackgroundColor(Color.parseColor("#076f30"));
                holder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
            } else if (order_item_status.equals("5")) {
                holder.txtProductStatus.setText("Cancel Order");
                holder.txtProductStatus.setBackgroundColor(Color.parseColor("#FF0000"));
                holder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
            } else if (order_item_status.equals("6")) {
                holder.txtProductStatus.setText("Return Order");
                holder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                holder.txtProductStatus.setBackgroundColor(Color.parseColor("#158cba"));
            } else if (order_item_status.equals("7")) {
                holder.txtProductStatus.setText("Refund Order");
                holder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                holder.txtProductStatus.setBackgroundColor(Color.parseColor("#72c29b"));
            }

            //region Image
            String picUrl = null;
            try {
                URL urla = null;
                urla = new URL(image1);
                URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                picUrl = String.valueOf(urin.toURL());
                // Capture position and set to the ImageView
                Picasso.with(context)
                        .load(picUrl)
                        .into(holder.imageProduct, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onError() {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }
                        });
            } catch (MalformedURLException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in WishlistFragment.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            } catch (URISyntaxException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in WishlistFragment.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            }
            //endregion

            holder.llOrderhistory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String order_id = orderHistory.getOrder_id();
                    mListener = (OnFragmentInteractionListener) context;
                    mListener.GotoOrderDetail(order_id);
                   *//* SharedPreference.CreatePreference(getContext(), Global.LOGIN_PREFERENCE);
                    SharedPreference.SavePreference(StaticUtility.strOrderHistoryLastClick, String.valueOf(position));*//*
                }
            });*/

           /* try {
                final JSONObject jsonObject = jsonArray.getJSONObject(position);
                JSONArray jsonArray = jsonObject.getJSONArray("ordereditems");
                JSONObject jsonObjectOrder = jsonArray.getJSONObject(0);
                String name = jsonObjectOrder.getString("name");
                String price = jsonObjectOrder.getString("price");
                String order_item_status = jsonObjectOrder.getString("order_item_status");
                holder.txtProductName.setText(name);
                holder.txtOrderID.setText("Order Id :- " + jsonObject.getString("order_id"));
                holder.txtProducDate.setText(Global.changeDateFormate(jsonObject.getString("order_datetime"),
                        "yyyy-MM-dd hh:mm:ss", "dd-MMM-yyyy"));
                float floatPrice = Float.parseFloat(jsonObject.getString("order_product_amount"));
                int intPrice = Math.round(floatPrice);
                String image = jsonObjectOrder.getString("main_image");
                String image1 = image.replace("[", "").replace("]", "").replace("\"", "");

                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                        StaticUtility.CurrencySignPosition).equals("1")) {
                    holder.txtProductPrice.setText(SharedPreference.GetPreference(context,
                            Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + String.valueOf(intPrice));
                } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                        StaticUtility.CurrencySignPosition).equals("2")) {
                    holder.txtProductPrice.setText(String.valueOf(intPrice) + ". " +
                            SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
                }

                if (order_item_status.equals("0")) {
                    holder.txtProductStatus.setText("Pending Order");
                    holder.txtProductStatus.setBackgroundColor(Color.parseColor("#FF7F50"));
                    holder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                } else if (order_item_status.equals("1")) {
                    holder.txtProductStatus.setText("Confirm Order");
                    holder.txtProductStatus.setBackgroundColor(Color.parseColor("#28b62c"));
                    holder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                } else if (order_item_status.equals("2")) {
                    holder.txtProductStatus.setText("In Process");
                    holder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                    holder.txtProductStatus.setBackgroundColor(Color.parseColor("#d4af37"));
                } else if (order_item_status.equals("3")) {
                    holder.txtProductStatus.setText("Out for Delivery");
                    holder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                    holder.txtProductStatus.setBackgroundColor(Color.parseColor("#88b04b"));
                } else if (order_item_status.equals("4")) {
                    holder.txtProductStatus.setText("Deliveried");
                    holder.txtProductStatus.setBackgroundColor(Color.parseColor("#076f30"));
                    holder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                } else if (order_item_status.equals("5")) {
                    holder.txtProductStatus.setText("Cancel Order");
                    holder.txtProductStatus.setBackgroundColor(Color.parseColor("#FF0000"));
                    holder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                } else if (order_item_status.equals("6")) {
                    holder.txtProductStatus.setText("Return Order");
                    holder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                    holder.txtProductStatus.setBackgroundColor(Color.parseColor("#158cba"));
                } else if (order_item_status.equals("7")) {
                    holder.txtProductStatus.setText("Refund Order");
                    holder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                    holder.txtProductStatus.setBackgroundColor(Color.parseColor("#72c29b"));
                }

                //region Image
                String picUrl = null;
                try {
                    URL urla = null;
                    urla = new URL(image1);
                    URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    // Capture position and set to the ImageView
                    Picasso.with(context)
                            .load(picUrl)
                            .into(holder.imageProduct, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }

                                @Override
                                public void onError() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }
                            });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in WishlistFragment.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in WishlistFragment.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
                //endregion

                holder.llOrderhistory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            String order_id = jsonObject.getString("order_id");
                            mListener = (OnFragmentInteractionListener) context;
                            mListener.GotoOrderDetail(order_id);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }*/

        }

        @Override
        public int getItemCount() {
//            return jsonArray.length();
//            return orderHistories.size();
            return orders.size();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            ImageView imageProduct;
            TextView txtProductName, txtOrderID, txtProductPrice, txtProductStatus, txtProducDate;
            LinearLayout llOrderhistory;

            public Viewholder(View itemView) {
                super(itemView);
                txtProductName = (TextView) itemView.findViewById(R.id.txtProductName);
                txtOrderID = (TextView) itemView.findViewById(R.id.txtOrderID);
                txtProductPrice = (TextView) itemView.findViewById(R.id.txtProductPrice);
                txtProductStatus = (TextView) itemView.findViewById(R.id.txtProductStatus);
                txtProducDate = (TextView) itemView.findViewById(R.id.txtProducDate);
                imageProduct = (ImageView) itemView.findViewById(R.id.imageProduct);
                llOrderhistory = (LinearLayout) itemView.findViewById(R.id.llOrderhistory);

                txtProductName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
                txtOrderID.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
                txtProductPrice.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
                txtProductStatus.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
                txtProducDate.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));

                txtProductName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                txtOrderID.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                txtProducDate.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                txtProductPrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txtProductStatus.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txtProductStatus.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

            }
        }

    }
    //endregion

    //region chanageEditTextBorder
    public void chanageEditTextBorder(EditText editText) {
        editText.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editText.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) editText.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        /*gd.setColor(Color.parseColor("#000000"));*/
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(50);
        gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
    }
    //endregion

}
