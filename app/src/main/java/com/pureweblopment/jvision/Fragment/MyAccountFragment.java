package com.pureweblopment.jvision.Fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.pureweblopment.jvision.Activity.LoginActivity;
import com.pureweblopment.jvision.Activity.MainActivity;
import com.pureweblopment.jvision.Global.Global;
import com.pureweblopment.jvision.Global.SharedPreference;
import com.pureweblopment.jvision.Global.Typefaces;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URL;
import java.util.concurrent.Executors;

import com.pureweblopment.jvision.Global.SendMail;
import com.pureweblopment.jvision.Global.StaticUtility;

import com.pureweblopment.jvision.R;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyAccountFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyAccountFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyAccountFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ImageView imageCartBack, imageNavigation, imageLogo;
    FrameLayout frameLayoutCart;
    TextView txtCatName;
    CardView cardviewBottomNavigation;

    private OnFragmentInteractionListener mListener;

    CircleImageView circularImageViewUser;
    /*CircularImageView circularImageViewUser;*/
    TextView txtUserName, txtUserEmail;
    ImageView imageMyAccount, imageMyAddress, imageOrderHistory, imageChangePWD, imageLogout, imageReviews;
    TextView txtMyAccount, txtMyAddress, txtOrderHistory, txtChangePWD, txtLogout, txtReviews;
    LinearLayout llMyAccount, llMyAddress, llOrderHistory, llChangePWD, llLogout, llReviews, llMyAccountHeader;

    String picUrl = null;
    private static URL urla = null;
    private static URI urin = null;

    ImageView imgUser;

    public MyAccountFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyAccountFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyAccountFragment newInstance(String param1, String param2) {
        MyAccountFragment fragment = new MyAccountFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        MainActivity.manageBackPress(false);
        View view = inflater.inflate(R.layout.fragment_my_account, container, false);


        imageCartBack = (ImageView) getActivity().findViewById(R.id.imageCartBack);
        imageNavigation = (ImageView) getActivity().findViewById(R.id.imageNavigation);
        imageLogo = (ImageView) getActivity().findViewById(R.id.imageLogo);

        frameLayoutCart = (FrameLayout) getActivity().findViewById(R.id.frameLayoutCart);
        txtCatName = (TextView) getActivity().findViewById(R.id.txtCatName);
        cardviewBottomNavigation = (CardView) getActivity().findViewById(R.id.cardviewBottomNavigation);

        imageCartBack.setVisibility(View.GONE);
        txtCatName.setVisibility(View.VISIBLE);
        cardviewBottomNavigation.setVisibility(View.VISIBLE);
        txtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        txtCatName.setText(R.string.myaccount);

        imageNavigation.setVisibility(View.VISIBLE);
        imageLogo.setVisibility(View.GONE);
        frameLayoutCart.setVisibility(View.GONE);

        String userID = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        if (userID != null) {

        } else {
            Toast.makeText(getContext(), "You have to Login First!", Toast.LENGTH_SHORT).show();
            SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
            SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
            SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
            SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
            SharedPreference.ClearPreference(getContext(), Global.ISCheck);
            startActivity(intent);
            getActivity().finish();
        }

        Initialization(view);
        TypeFace();
        OnClickListener();
        AppSettings();

        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_EMAIL) != null) {
            txtUserEmail.setText(SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_EMAIL));
        }
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_Name) != null) {
            txtUserName.setText(SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_Name));
        }

        if (!SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_Profile_Picture).equalsIgnoreCase("")) {
            String stringProfile = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_Profile_Picture);
            if (!stringProfile.equalsIgnoreCase("")) {
                imgUser.setVisibility(View.GONE);
                circularImageViewUser.setVisibility(View.VISIBLE);
                try {
                    picUrl = String.valueOf(SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_Profile_Picture));
                    urla = new URL(picUrl);
                    urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    Picasso.with(getContext())
                            .load(picUrl)
                            .into(circularImageViewUser, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }

                                @Override
                                public void onError() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in MyAccountFragment.java When parsing image url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
            } else {
                imgUser.setVisibility(View.VISIBLE);
                circularImageViewUser.setVisibility(View.GONE);
            }
        } else {
            /*circularImageViewUser.setImageResource(R.drawable.ic_user_image);*/
            imgUser.setVisibility(View.VISIBLE);
            circularImageViewUser.setVisibility(View.GONE);
        }

        return view;
    }

    //region Initialization...
    private void Initialization(View view) {
        circularImageViewUser = view.findViewById(R.id.circularImageViewUser);
        imgUser = (ImageView) view.findViewById(R.id.imgUser);
        txtUserName = (TextView) view.findViewById(R.id.txtUserName);
        txtUserEmail = (TextView) view.findViewById(R.id.txtUserEmail);

        txtMyAccount = (TextView) view.findViewById(R.id.txtMyAccount);
        txtMyAddress = (TextView) view.findViewById(R.id.txtMyAddress);
        txtOrderHistory = (TextView) view.findViewById(R.id.txtOrderHistory);
        txtChangePWD = (TextView) view.findViewById(R.id.txtChangePWD);
        txtLogout = (TextView) view.findViewById(R.id.txtLogout);
        txtReviews = (TextView) view.findViewById(R.id.txtReviews);

        llMyAccount = (LinearLayout) view.findViewById(R.id.llMyAccount);
        llMyAddress = (LinearLayout) view.findViewById(R.id.llMyAddress);
        llOrderHistory = (LinearLayout) view.findViewById(R.id.llOrderHistory);
        llChangePWD = (LinearLayout) view.findViewById(R.id.llChangePWD);
        llLogout = (LinearLayout) view.findViewById(R.id.llLogout);
        llReviews = (LinearLayout) view.findViewById(R.id.llReviews);
        llMyAccountHeader = (LinearLayout) view.findViewById(R.id.llMyAccountHeader);

        imageMyAccount = (ImageView) view.findViewById(R.id.imageMyAccount);
        imageMyAddress = (ImageView) view.findViewById(R.id.imageMyAddress);
        imageOrderHistory = (ImageView) view.findViewById(R.id.imageOrderHistory);
        imageChangePWD = (ImageView) view.findViewById(R.id.imageChangePWD);
        imageLogout = (ImageView) view.findViewById(R.id.imageLogout);
        imageReviews = (ImageView) view.findViewById(R.id.imageReviews);
    }
    //endregion

    //region TypeFace...
    private void TypeFace() {
        txtUserEmail.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtUserName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtMyAccount.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtMyAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtOrderHistory.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtChangePWD.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtLogout.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtLogout.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtReviews.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
    }
    //endregion

    //region OnClickListener...
    private void OnClickListener() {
        llMyAddress.setOnClickListener(this);
        llMyAccount.setOnClickListener(this);
        llOrderHistory.setOnClickListener(this);
        llChangePWD.setOnClickListener(this);
        llLogout.setOnClickListener(this);
        llReviews.setOnClickListener(this);
    }
    //endregion

    //region AppSettings...
    private void AppSettings() {
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor) != "") {
            txtUserName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
            txtUserEmail.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        }
        txtMyAccount.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtMyAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtOrderHistory.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtChangePWD.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtLogout.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtReviews.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            llMyAccountHeader.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
    }
    //endregion

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llMyAddress:
                mListener = (OnFragmentInteractionListener) getContext();
                mListener.gotoMyAccountAddressListing();
                break;
            case R.id.llMyAccount:
                mListener = (OnFragmentInteractionListener) getContext();
                mListener.gotoUserProfile();
                break;
            case R.id.llOrderHistory:
                mListener = (OnFragmentInteractionListener) getContext();
                mListener.gotoOrderHistory();
                break;
            case R.id.llChangePWD:
                mListener = (OnFragmentInteractionListener) getContext();
                mListener.gotoChangepwd();
                break;
            case R.id.llReviews:
                mListener = (OnFragmentInteractionListener) getContext();
                mListener.gotoReviews();
                break;
            case R.id.llLogout:
                if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                    txtLogout.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                    imageLogout.setImageResource(R.drawable.ic_logout);
                    imageLogout.setColorFilter(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                } else {
                    imageLogout.setImageResource(R.drawable.ic_logout_select);
                }
                llLogout.setEnabled(false);
                Logout();
                break;
        }
    }

    //region FOR Logout API..
    private void Logout() {

        String[] key = {"user_token"};
        String[] val = {String.valueOf(SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERTOKEN))};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.Logout);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                llLogout.setEnabled(true);
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                                Intent intent = new Intent(getActivity(), LoginActivity.class);
                                intent.putExtra("Redirect", "logout");
                               /* intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                                        | Intent.FLAG_ACTIVITY_CLEAR_TASK);*/
                                startActivity(intent);
                                /*getActivity().finish();*/
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            llLogout.setEnabled(false);
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strMessage.equalsIgnoreCase("user token mismatched!")) {
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                                            | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    /*finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                                            | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    /*finish();*/
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in MyAccountFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void gotoReviews();

        void gotoOrderHistory();

        void gotoUserProfile();

        void gotoChangepwd();

        void gotoMyAccountAddressListing();
    }
}
