package com.pureweblopment.jvision.Global;

/**
 * Created by divya on 4/9/17.
 */

public class StaticUtility {

    //Live URL
    public static String URL = "https://www.bazarbit.com/bazzarbit/";

    public static String Checkregister = "app/frontend/checksocialdetails";
    public static String Registration = "app/frontend/register";
    public static String ForgotPWD = "app/frontend/forgotpassword";
    public static String Login = "app/frontend/login";
    public static String Logout = "app/frontend/logout";
    public static String getCountryCode = "countrycodes";
    public static String ApplicationSettings = "app/frontend/getapplicationsettings";
    public static String AppSetting = "app/frontend/getappsetting";
    public static String GETSLIDER = "app/frontend/getslider";
    public static String getCategory = "app/frontend/categories";
    public static String getBestSeller = "app/frontend/bestSeller";
    public static String getWantedProduct = "app/frontend/mostwantedproducts";
    public static String getNewArrivals = "app/frontend/newarrivals";
    public static String getBenner = "app/frontend/getbanner";
    public static String UPDATE_CART_BY_USERID_AFTER_LOGIN = "app/frontend/user/updatecartsession";
    public static String AddToWishlist = "app/frontend/user/addtowishlist";
    public static String GetProductList = "app/frontend/getcatwiseproducts";
    public static String CheckWishlist = "app/frontend/user/checkiteminwishlist";
    public static String SortByAttribute = "app/frontend/getattributessorting";
    public static String GetSigalProduct = "app/frontend/getproductdetailbyslug";
    public static String GetCheckPincode = "app/frontend/checkpincodeexists";
    public static String RelatedProduct = "app/frontend/relatedProducts";
    public static String GetReviews = "app/frontend/reviewdetail/gets";
    public static String CheckItemINCart = "app/frontend/checkitemincart";
    public static String AddToCart = "app/frontend/addtocart";
    public static String CartCount = "app/frontend/cartcount";
    public static String NotifyMe = "app/frontend/user/notifyme";
    public static String CheckNotifyMe = "app/frontend/user/checkiteminnotifyme";
    public static String GetPayUMoneyCredential = "app/frontend/getpayumoneycredential";
    public static String GetCartItems = "app/frontend/getcartitems";
    public static String GetQauntyIncreseDecrese = "app/frontend/updatecartitemqty";
    public static String RemoveCart = "app/frontend/removecartitem";
    public static String LastOrderAddress = "app/frontend/user/getlastorderaddress";
    public static String Countries = "countries";
    public static String States = "states";
    public static String ADDAddresses = "app/frontend/user/myaccount/adduseraddress";
    public static String AddressList = "app/frontend/user/myaccount/useraddresslist";
    public static String DeleteAddress = "app/frontend/user/myaccount/deleteuseraddress";
    public static String EditAddress = "app/frontend/user/myaccount/updateuseraddress";
    public static String ApplyPromocode = "app/frontend/validatepromocode";
    public static String RemovePromocode = "app/frontend/removepromocode";
    public static String GetPaymentGetway = "app/frontend/getpaymentgateway";
    public static String Getshipping_charge_Delivery = "app/frontend/getshippingcharges";
    public static String GetCartTotal = "app/frontend/getcarttotal";
    public static String PlaceOrder = "app/frontend/user/transaction/placeorder";
    public static String Wishlist = "app/frontend/user/getwishlistitems";
    public static String RemoveWishlist = "app/frontend/user/removeWishlistItem";
    public static String OrderHistory = "app/frontend/user/getorders";
    public static String SingleOrderDetail = "app/frontend/user/getsingleorder";
    public static String CancelOrder = "app/frontend/user/updateitemstatus";
    public static String ReturnCODOrder = "app/frontend/user/returnorder";
    public static String CancelOnlineOrder = "app/frontend/user/updateitemstatus";
    public static String ReturnNetOrder = "app/frontend/user/returnnetorder";
    public static String SubmitReview = "app/frontend/reviewdetail/create";
    public static String ALLReviews = "app/frontend/user/myaccount/listreview";
    public static String GetLastUserInfo = "app/frontend/user/myaccount/getbyid";
    public static String UpdateProfile = "app/frontend/user/myaccount/update";
    public static String GetFeedbackForm = "app/frontend/feedbackform";
    public static String SubmitFeedback = "app/frontend/user/submitfeedback";
    public static String GetCMSListing = "app/frontend/cmslist";
    public static String CMSStaticPage = "app/frontend/cmsstaticpage";
    public static String ChangePWD = "app/frontend/user/myaccount/changepassword";
    public static String GetSearchResult = "app/frontend/searchproductlist";
    public static String DisableVariationOption = "app/frontend/disabledvariationoption";
    public static String GetVariationData = "app/frontend/productvariation";
    public static String GetDefualtVariationData = "app/frontend/getdefaultvariationbyproductid";
    public static String GetMegaMenu = "app/menu/getRows";
    public static String Feedback = "app/frontend/user/getdeliveredorders";
    public static String SubmitContactUs = "app/frontend/createcontactform";
    public static String verifyOTP = "app/frontend/verifyotp";
    public static String ResendOTP = "app/frontend/resendotp";
    public static String SendOTP = "app/frontend/user/transaction/sendotp";
    public static String PlaceOrderVerifyOTP = "app/frontend/user/transaction/verifyotp";
    public static String FeatureSetting = "app/frontend/getfeaturessettings";

    //region STRINGS FOR USE IN NOTIFICATION...
    public static final String NOTIFICATION_LEGACY_SERVER_KEY = "AIzaSyAjR81KEqwhRIsIMCe-912GnzhbpU7YCXE";
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String GCM_TOKEN = "gcm_token";
    //endregion

    public static final String CLIENT_ID = "02090aa093f24a0695f3f0eab1134016";
    public static final String CLIENT_SECRET = "fd9a2c34417b476bb42d0dc9cf9bd186";
    public static final String CALLBACK_URL = "http://192.168.0.212/bazzarbitFront/index.php";


    public static final String LinkedinCLIENT_ID = "81va8uw7fll2ey";
    public static final String LinkedinCLIENT_SECRET = "x9QzbF8Zkidi5pLD";

    public static final String PinterestApp_ID = "4942597597022925280";
    public static final String PinteresApp_SECRET = "13f706620cf85f8893a44efc49bb93dc8104c342ee66f21e929edb2bb10d51b6";

    public static final String ThemePrimaryColor = "themeprimarycolor";
    public static final String ThemePrimaryDarkColor = "themeprimarydarkcolor";
    public static final String TextColor = "textcolor";
    public static final String TextLightColor = "textlightcolor";
    public static final String ButtonTextColor = "btntextcolor";

    public static final String IMG_LOGO = "imglogo";
    public static final String IMG_Facebook = "imgFacebook";
    public static final String IMG_google = "imggoogle";
    public static final String IMG_Linkedin = "imglinkedin";
    public static final String IMG_pinterest = "imgpinterest";
    public static final String IMG_insta = "imginsta";
    public static final String APP_LOGO = "applogo";
    public static final String Feature_Social_Media = "featuresocialmedia";

    public static final String Is_Slider_Active = "is_slider";
    public static final String Is_Cart_Active = "is_cart";
    public static final String Is_Order_Active = "is_order";
    public static final String Is_Wishlist_Active = "is_wishlist";
    public static final String Is_Arrival_Active = "is_arrival";
    public static final String Is_Seller_Active = "is_seller";
    public static final String Is_Mostwanted_Active = "is_mostwanted";
    public static final String Is_MEGAMENU_Active = "is_megamenu_active";

    public static final String CurrencyName = "currencyname";
    public static final String CurrencySign = "currencysign";
    public static final String CurrencySignPosition = "currencysignposition";

    public static final String IsSelectAddress = "isSelectAddress";
    public static final String strShippingUserName = "strshippingusername";
    public static final String strShippingAddress = "strshippingaddresss";
    public static final String strShippingPhoneno = "strshippingphoneno";
    public static final String strPincode = "strPincode";
    public static final String strShippingFname = "strshippingfname";
    public static final String strShippingLname = "strshippinglname";
    public static final String strShippingAddress1 = "strshippingaddress";
    public static final String strShippingLandmark = "strshippinglandmark";
    public static final String strShippingCountry = "strshippingcounty";
    public static final String strShippingCity = "strshippingcity";
    public static final String strShippingState = "strshippingState";

    public static final String strBillingUserName = "strbillingusername";
    public static final String strBillingAddress = "strbillingaddresss";
    public static final String strBillingPhoneno = "strbillingphoneno";
    public static final String strBillingPincode = "strbillingPincode";
    public static final String strBillingFname = "strbillingfname";
    public static final String strBillingLname = "strbillinglname";
    public static final String strBillingAddress1 = "strbillingaddress";
    public static final String strBillingLandmark = "strbillinglandmark";
    public static final String strBillingCountry = "strbillingcounty";
    public static final String strBillingCity = "strbillingcity";
    public static final String strBillingState = "strbillingState";

    public static final String strTotalPrice = "totalPrice";
    public static final String strTotalItems = "totalitems";
    public static final String PREFERENCEOrderHistory = "Preferenceorderhistory";
    public static final String strOrderHistoryLastClick = "orderhistorylastclick";

    public static final String strIscheck = "ischeck";
    public static final String strIsPromocode = "ispromocode";
    public static final String WishlistCount = "wishlistcount";
    public static final String UserProfile = "userprofile";
    public static final String FullName = "fullname";
    public static final String ArrayAttribut = "arrayattribute";
    public static final String ArrayAttributTerm = "arrayattributeterm";

    public static final String Fromdate = "userprofile";
    public static final String sPincode = "pincode";

}
