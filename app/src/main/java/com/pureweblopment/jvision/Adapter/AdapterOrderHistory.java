package com.pureweblopment.jvision.Adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pureweblopment.jvision.Activity.OrderHistoryActivity;
import com.pureweblopment.jvision.Global.Global;
import com.pureweblopment.jvision.Global.SendMail;
import com.pureweblopment.jvision.Global.SharedPreference;
import com.pureweblopment.jvision.Global.StaticUtility;
import com.pureweblopment.jvision.Global.Typefaces;
import com.pureweblopment.jvision.Model.OrderHistoryItems;
import com.pureweblopment.jvision.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by divya on 20/12/17.
 */

public class AdapterOrderHistory extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    ArrayList<OrderHistoryItems> orderHistoryItemses;
    String strPaymentType;

    public AdapterOrderHistory(Context context, ArrayList<OrderHistoryItems> orderHistoryItemses, String strPaymentType) {
        this.orderHistoryItemses = orderHistoryItemses;
        this.context = context;
        this.strPaymentType = strPaymentType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_orderhistory, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder paletteViewHolder = (ViewHolder) holder;
            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    paletteViewHolder.pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
                }
            }
            final OrderHistoryItems orderHistoryItems = (OrderHistoryItems) orderHistoryItemses.get(position);
            paletteViewHolder.txtProductName.setText(orderHistoryItems.getName());
            paletteViewHolder.txtProducDate.setText(Global.changeDateFormate(orderHistoryItems.getOrder_datetime(),
                    "yyyy-MM-dd hh:mm:ss", "dd-MMM-yyyy"));

            float floatPrice = Float.parseFloat(orderHistoryItems.getOrder_product_amount());
            int intPrice = Math.round(floatPrice);
            String image = orderHistoryItems.getImage();
            String image1 = image.replace("[", "").replace("]", "").replace("\"", "");

            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                    StaticUtility.CurrencySignPosition).equals("1")) {
                paletteViewHolder.txtProductPrice.setText(SharedPreference.GetPreference(context,
                        Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign) + ". " + orderHistoryItems.getOrder_product_amount());
            } else if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                    StaticUtility.CurrencySignPosition).equals("2")) {
                paletteViewHolder.txtProductPrice.setText(orderHistoryItems.getOrder_product_amount() + ". " +
                        SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.CurrencySign));
            }

            String order_item_status = orderHistoryItems.getOrder_item_status();

            if (order_item_status.equals("0")) {
                if (Integer.parseInt(strPaymentType) > 1) {
                    if (order_item_status.equalsIgnoreCase("0") &&
                            orderHistoryItems.getOrder_item_payment_status().
                                    equalsIgnoreCase("0")) {
                        paletteViewHolder.txtProductStatus.setText("Cancelled Order");
                        paletteViewHolder.txtProductStatus.setBackgroundColor(Color.parseColor("#FF0000"));
                        paletteViewHolder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                    } else {
                        paletteViewHolder.txtProductStatus.setText("Pending Order");
                        paletteViewHolder.txtProductStatus.setBackgroundColor(Color.parseColor("#FF7F50"));
                        paletteViewHolder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                    }
                } else {
                    paletteViewHolder.txtProductStatus.setText("Pending Order");
                    paletteViewHolder.txtProductStatus.setBackgroundColor(Color.parseColor("#FF7F50"));
                    paletteViewHolder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                }
            } else if (order_item_status.equals("1")) {
                paletteViewHolder.txtProductStatus.setText("Confirm Order");
                paletteViewHolder.txtProductStatus.setBackgroundColor(Color.parseColor("#28b62c"));
                paletteViewHolder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
            } else if (order_item_status.equals("2")) {
                paletteViewHolder.txtProductStatus.setText("In Process");
                paletteViewHolder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                paletteViewHolder.txtProductStatus.setBackgroundColor(Color.parseColor("#d4af37"));
            } else if (order_item_status.equals("3")) {
                paletteViewHolder.txtProductStatus.setText("Out for Delivery");
                paletteViewHolder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                paletteViewHolder.txtProductStatus.setBackgroundColor(Color.parseColor("#88b04b"));
            } else if (order_item_status.equals("4")) {
                paletteViewHolder.txtProductStatus.setText("Delivered");
                paletteViewHolder.txtProductStatus.setBackgroundColor(Color.parseColor("#076f30"));
                paletteViewHolder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
            } else if (order_item_status.equals("5")) {
                paletteViewHolder.txtProductStatus.setText("Cancelled Order");
                paletteViewHolder.txtProductStatus.setBackgroundColor(Color.parseColor("#FF0000"));
                paletteViewHolder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
            } else if (order_item_status.equals("6")) {
                paletteViewHolder.txtProductStatus.setText("Return Order");
                paletteViewHolder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                paletteViewHolder.txtProductStatus.setBackgroundColor(Color.parseColor("#158cba"));
            } else if (order_item_status.equals("7")) {
                paletteViewHolder.txtProductStatus.setText("Refund Order");
                paletteViewHolder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                paletteViewHolder.txtProductStatus.setBackgroundColor(Color.parseColor("#72c29b"));
            }

            //region Image
            String picUrl = null;
            try {
                URL urla = null;
                urla = new URL(image.replaceAll("%20", " "));
                URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                picUrl = String.valueOf(urin.toURL());
                // Capture position and set to the ImageView
                Picasso.with(context)
                        .load(picUrl)
                        .into(paletteViewHolder.imageProduct, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                                paletteViewHolder.rlImgHolder.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }
                        });
            } catch (MalformedURLException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in OrderHistoryActivity.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            } catch (URISyntaxException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in OrderHistoryActivity.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            }
            //endregion
        }
    }

    @Override
    public int getItemCount() {
        return orderHistoryItemses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageProduct;
        TextView txtProductName, txtOrderID, txtProductPrice, txtProductStatus, txtProducDate;
        LinearLayout llOrderhistory;
        TextView txtProductOrderID;
        RelativeLayout rlImgHolder;
        ProgressBar pbImgHolder;

        public ViewHolder(View itemView) {
            super(itemView);
            txtProductName = (TextView) itemView.findViewById(R.id.txtProductName);
            txtProductOrderID = (TextView) itemView.findViewById(R.id.txtProductOrderID);
            txtOrderID = (TextView) itemView.findViewById(R.id.txtOrderID);
            txtProductPrice = (TextView) itemView.findViewById(R.id.txtProductPrice);
            txtProductStatus = (TextView) itemView.findViewById(R.id.txtProductStatus);
            txtProducDate = (TextView) itemView.findViewById(R.id.txtProducDate);
            imageProduct = (ImageView) itemView.findViewById(R.id.imageProduct);
            llOrderhistory = (LinearLayout) itemView.findViewById(R.id.llOrderhistory);
            rlImgHolder = (RelativeLayout) itemView.findViewById(R.id.rlImgHolder);
            pbImgHolder = (ProgressBar) itemView.findViewById(R.id.pbImgHolder);

            txtProductName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
            txtOrderID.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
            txtProductPrice.setTypeface(Typefaces.TypefaceCalibri_bold(context));
            txtProductStatus.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
            txtProducDate.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
            txtProductOrderID.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

            txtProductName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            txtProductOrderID.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            txtOrderID.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            txtProducDate.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            txtProductPrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtProductStatus.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtProductStatus.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        }
    }
}
