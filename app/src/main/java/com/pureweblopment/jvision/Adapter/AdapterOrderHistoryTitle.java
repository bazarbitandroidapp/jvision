package com.pureweblopment.jvision.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pureweblopment.jvision.Activity.OrderHistoryActivity;
import com.pureweblopment.jvision.Fragment.OrderDetailFragment;
import com.pureweblopment.jvision.Global.Global;
import com.pureweblopment.jvision.Global.SharedPreference;
import com.pureweblopment.jvision.Global.StaticUtility;
import com.pureweblopment.jvision.Global.Typefaces;
import com.pureweblopment.jvision.Model.Footer;
import com.pureweblopment.jvision.Model.Item;
import com.pureweblopment.jvision.Model.OrderHistory;
import com.pureweblopment.jvision.Model.OrderHistoryItems;
import com.pureweblopment.jvision.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by divya on 20/12/17.
 */

public class AdapterOrderHistoryTitle extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_POST = 0;
    private static final int TYPE_FOOTER = 1;
    Context context;
    List<Item> orders;
    OrderDetail orderDetail;


    public AdapterOrderHistoryTitle(Context context, List<Item> orders) {
        this.orders = orders;
        this.context = context;
    }

    public void clearData() {
        int size = orders.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                orders.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (orders.get(position) instanceof Footer) {
            return TYPE_FOOTER;
        } else {
            return TYPE_POST;
        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {
        private ProgressBar progressBar;

        public FooterViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.footer);
        }

        public ProgressBar getProgressBar() {
            return progressBar;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if (viewType == TYPE_POST) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_orderhistory_title, parent, false);
            return new ViewHolder(view);
        } else {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_footer, parent, false);
            FooterViewHolder vh = new FooterViewHolder(row);
            return vh;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, final int position) {
        if (viewholder instanceof ViewHolder) {
            final ViewHolder paletteViewHolder = (ViewHolder) viewholder;
            final OrderHistory orderHistory = (OrderHistory) orders.get(position);
            paletteViewHolder.txtProductOrderID.setText("Order Id :- " + orderHistory.getOrder_id());
            ArrayList<OrderHistoryItems> orderHistoryItemses = orderHistory.getArrayOrderHistoryItems();
            AdapterOrderHistory adapterOrderHistory =
                    new AdapterOrderHistory(context, orderHistoryItemses, orderHistory.getOrder_payment_type());
            paletteViewHolder.recyclerviewOrderItems.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            paletteViewHolder.recyclerviewOrderItems.setAdapter(adapterOrderHistory);

            paletteViewHolder.llOrderHistory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String order_id = orderHistory.getOrder_id();
                    orderDetail = (OrderDetail)context;
                    orderDetail.gotoOrderDetailscreen(order_id);
                    SharedPreference.CreatePreference(context, StaticUtility.PREFERENCEOrderHistory);
                    SharedPreference.SavePreference(StaticUtility.strOrderHistoryLastClick, String.valueOf(position));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtProductOrderID;
        RecyclerView recyclerviewOrderItems;
        LinearLayout llOrderHistory;

        public ViewHolder(View itemView) {
            super(itemView);
            txtProductOrderID = (TextView) itemView.findViewById(R.id.txtProductOrderID);
            recyclerviewOrderItems = (RecyclerView) itemView.findViewById(R.id.recyclerviewOrderItems);
            llOrderHistory = (LinearLayout) itemView.findViewById(R.id.llOrderHistory);

            txtProductOrderID.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

            txtProductOrderID.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        }
    }

    public interface OrderDetail{
        void gotoOrderDetailscreen(String Order_Id);
    }
}
